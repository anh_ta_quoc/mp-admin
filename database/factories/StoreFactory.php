<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Store;
use Faker\Generator as Faker;

$factory->define(Store::class, function (Faker $faker) {

    return [
        'trial_user_id' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'bank_branch' => $faker->word,
        'bank_code' => $faker->word,
        'bank_number' => $faker->word,
        'currency' => $faker->word,
        'merchant_address' => $faker->text,
        'merchant_brand' => $faker->word,
        'merchant_code' => $faker->word,
        'merchant_contact_email' => $faker->word,
        'merchant_contact_name' => $faker->word,
        'merchant_contact_phone' => $faker->word,
        'merchant_district_code' => $faker->word,
        'merchant_name' => $faker->word,
        'merchant_province_code' => $faker->word,
        'merchant_type' => $faker->word,
        'merchant_type_business' => $faker->word,
        'merchant_ward_code' => $faker->word,
        'terminal_address' => $faker->text,
        'terminal_contact_email' => $faker->word,
        'terminal_contact_name' => $faker->word,
        'terminal_contact_phone' => $faker->word,
        'terminal_description' => $faker->text,
        'terminal_district_code' => $faker->word,
        'terminal_id' => $faker->word,
        'terminal_name' => $faker->word,
        'terminal_province_code' => $faker->word,
        'terminal_type' => $faker->word,
        'terminal_ward_code' => $faker->word,
        'bank_account' => $faker->word,
        'mms_status' => $faker->word,
        'merchant_file_business_cert' => $faker->word,
        'merchant_file_contract' => $faker->word,
        'merchant_date_of_issue' => $faker->word,
        'merchant_place_of_issue' => $faker->word,
        'department_id' => $faker->word,
        'staff_id' => $faker->word,
        'merchant_file_business_tax_cert' => $faker->word,
        'merchant_file_domain_cert' => $faker->word,
        'merchant_file_identify_card' => $faker->word,
        'wallet_status' => $faker->word
    ];
});
