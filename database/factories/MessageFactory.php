<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Message;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'body' => $faker->text,
        'link' => $faker->word,
        'read' => $faker->word,
        'tracking' => $faker->word,
        'type' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'schedule_at' => $faker->date('Y-m-d H:i:s'),
        'sent_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
