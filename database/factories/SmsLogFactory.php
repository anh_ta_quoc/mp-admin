<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SmsLog;
use Faker\Generator as Faker;

$factory->define(SmsLog::class, function (Faker $faker) {

    return [
        'phone' => $faker->word,
        'message' => $faker->word,
        'status' => $faker->word,
        'error_code' => $faker->word,
        'error_message' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
