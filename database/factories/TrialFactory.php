<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Trial;
use Faker\Generator as Faker;

$factory->define(Trial::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'phone' => $faker->word,
        'email' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
