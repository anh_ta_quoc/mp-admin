<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MpTerminalBatch;
use Faker\Generator as Faker;

$factory->define(MpTerminalBatch::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
