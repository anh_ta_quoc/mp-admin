<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOldDataAndNewDataToTerminalHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('terminal_history', function (Blueprint $table) {
            $table->longText('old_data')->nullable();
            $table->longText('new_data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('terminal_history', function (Blueprint $table) {
            $table->dropColumn('old_data');
            $table->dropColumn('new_data');
        });
    }
}
