<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrackingNumberToMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('message', function (Blueprint $table) {
            $table->integer('plan_send_number')->default(0);
            $table->integer('sent_number')->default(0);
            $table->integer('plan_read_number')->default(0);
            $table->integer('read_number')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message', function (Blueprint $table) {
            $table->dropColumn('plan_send_number');
            $table->dropColumn('sent_number');
            $table->dropColumn('plan_read_number');
            $table->dropColumn('read_number');
        });
    }
}
