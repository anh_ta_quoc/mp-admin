<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMpTerminal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mp_terminals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merchant_code',20);
            $table->string('terminal_id',15);
            $table->string('terminal_code',191)->nullable();
            $table->string('terminal_name',191);
            $table->string('terminal_type',20);
            $table->string('product_description',400)->nullable();
            $table->string('mcc',6);
            $table->string('terminal_address',150)->nullable();
            $table->string('terminal_province_code',10)->default('');
            $table->string('terminal_district_code',10)->default('');
            $table->string('terminal_ward_code',10);
            $table->string('website',100)->nullable();
            $table->string('facebook',100)->nullable();
            $table->string('file_contract',255)->nullable();
            $table->string('file_auth_letter',255)->nullable();
            $table->string('file_other',255)->nullable();
            $table->smallInteger('register_qr')->default(1);
            $table->smallInteger('register_vnpayment')->nullable();
            $table->string('bank_code',255)->default('');
            $table->string('bank_branch',255)->nullable();
            $table->string('bank_number',30)->default('');
            $table->string('bank_account',150)->default('');
            $table->string('receive_method',10)->default('0');
            $table->string('terminal_contact_name',50)->default('');
            $table->string('terminal_contact_phone',15)->default('');
            $table->string('terminal_contact_email',100)->default('');
            $table->smallInteger('create_terminal_app')->default(0);
            $table->smallInteger('status')->default(0);
            $table->smallInteger('mms_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mp_terminals');
    }
}
