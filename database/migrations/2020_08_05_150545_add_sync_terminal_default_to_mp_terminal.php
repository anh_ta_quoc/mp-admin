<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSyncTerminalDefaultToMpTerminal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mp_terminals', function (Blueprint $table) {
            $table->smallInteger('sync_first_terminal')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mp_terminals', function (Blueprint $table) {
            $table->dropColumn('sync_first_terminal');
        });
    }
}
