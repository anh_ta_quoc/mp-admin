<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 50)->nullable(false);
            $table->text('body')->nullable(true);
            $table->string('link', 255)->nullable(false);
            $table->tinyInteger('read')->default(0);
            $table->tinyInteger('tracking')->default(1);
            $table->integer('type')->default(1);
            $table->tinyInteger('status')->default(0);
            $table->dateTime('schedule_at')->nullable(true);
            $table->dateTime('sent_at')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message');
    }
}
