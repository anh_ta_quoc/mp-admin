<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMpTerminalBatchesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mp_terminal_batches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merchant_code');
            $table->string('bank_code');
            $table->string('bank_branch');
            $table->string('bank_number');
            $table->string('bank_account');
            $table->string('file_contract');
            $table->string('file_authorize');
            $table->string('file_terminals');
            $table->string('file_result')->nullable()->default(null);
            $table->integer('status');
            $table->integer('total_rows')->nullable()->default(null);
            $table->integer('precheck_success')->nullable()->default(null);
            $table->string('process_lock')->nullable()->default(null);
            $table->text('process_msg')->nullable()->default(null);
            $table->integer('retry_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mp_terminal_batches');
    }
}
