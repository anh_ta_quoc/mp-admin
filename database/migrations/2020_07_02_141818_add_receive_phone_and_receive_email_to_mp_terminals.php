<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReceivePhoneAndReceiveEmailToMpTerminals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mp_terminals', function (Blueprint $table) {
            $table->string('receive_phone')->nullable()->default(null);
            $table->string('receive_email')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mp_terminals', function (Blueprint $table) {
            $table->dropColumn('receive_phone');
            $table->dropColumn('receive_email');
        });
    }
}
