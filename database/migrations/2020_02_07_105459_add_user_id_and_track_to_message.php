<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdAndTrackToMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('message', function (Blueprint $table) {
            $table->dropColumn('tracking');
            $table->tinyInteger('track_sent')->default(0);
            $table->tinyInteger('track_read')->default(1);
            $table->integer('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message', function (Blueprint $table) {
            $table->tinyInteger('tracking')->default(1);
            $table->dropColumn('track_sent');
            $table->dropColumn('track_read');
            $table->dropColumn('user_id');
        });
    }
}
