<li class="nav-item {{ Request::is('trials*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('trials.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Trials</span>
    </a>
</li>
<li class="nav-item {{ Request::is('smsLogs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('smsLogs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Sms Logs</span>
    </a>
</li>
<li class="nav-item {{ Request::is('stores*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('stores.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Stores</span>
    </a>
</li>
<li class="nav-item {{ Request::is('roles*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('roles.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Roles</span>
    </a>
</li>
<li class="nav-item {{ Request::is('permissions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('permissions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Permsisions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Users</span>
    </a>
</li>
<li class="nav-item {{ Request::is('permissions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('permissions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Permissions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('messages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('messages.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Messages</span>
    </a>
</li>
<li class="nav-item {{ Request::is('mpTerminalBatches*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('terminalBatches.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Mp Terminal Batches</span>
    </a>
</li>
