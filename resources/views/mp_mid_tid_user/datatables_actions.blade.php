{!! Form::open(['route' => ['mp_mid_tid_user.destroy', $id],
'id' => 'delete_form_'.$id,
'method' => 'delete',
'onsubmit' => 'return confirm("Bạn chắc chắn thực hiện thao tác?")']) !!}
<div class="hidden-sm hidden-xs action-buttons">
    <a class="green" href="{{ route('mp_mid_tid_user.edit', $id) }}">
        <i class="ace-icon fa fa-pencil bigger-130"></i>
    </a>
</div>
{!! Form::close() !!}



