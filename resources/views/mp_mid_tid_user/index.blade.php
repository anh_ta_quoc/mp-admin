@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Quản lý tài khoản', 'buttons' => [['title' => 'Tạo tài khoản', 'link' => route('mp_mid_tid_user.create'), 'icon' => 'fa fa-plus-circle']]])
    @include('flash::message')
    <div class="row dataTables_filter ">
        <div class="col-sm-3 form-group">
            <label>Tìm kiếm:</label>
            <input id="filter_query" class="form-control" placeholder="-- Nhập từ khóa tìm kiếm --">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            @include('mp_mid_tid_user.table')
        </div>
    </div>
@endsection

