@extends('layouts_mms.app', ['new_select2' => true])

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Tạo tài khoản mới', 'buttons' => [['title' => 'Quay lại', 'link' => route('mp_mid_tid_user.index'), 'icon' => 'fa fa-arrow-left']]])
    <div class="row">
        {!! Form::open(['route' => 'mp_mid_tid_user.store']) !!}

        @include('mp_mid_tid_user.fields')

        {!! Form::close() !!}
    </div>
@endsection
