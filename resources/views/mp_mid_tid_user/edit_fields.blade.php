<div class="col-md-6 col-sm-12 col-sm-offset-0">
    <div class="row">
        <div class="form-group col-xs-12">
            {!! Form::label('type', 'Loại tài khoản', ['class' => 'require']) !!}
            <select class="form-control select2" id="type" name="type">
                <option value="merchant_web" >Merchant Web</option>
                <option value="terminal_web">Terminal Web</option>
                <option value="merchant_app">Merchant App</option>
                <option value="terminal_app">Terminal App</option>
            </select>
            <div class="error hidden" id="error-receivers">
            </div>
        </div>
        <div class="form-group col-xs-12">
            {!! Form::label('user_id', 'Số điện thoại', ['class' => 'require']) !!}
            <select class="form-control select2-user-ajax" id="user_id" name="user_id">
            </select>
            <div class="error hidden" id="error-receivers">
            </div>
        </div>
        <div class="form-group col-xs-12">
            {!! Form::label('merchant_id', 'Merchant', ['class' => 'require']) !!}
            <select class="form-control select2-ajax" id="merchant_id" name="merchant_id">
            </select>
            <div class="error hidden" id="error-receivers">
            </div>
        </div>
        <div class="form-group col-xs-12">
            {!! Form::label('terminal_id', 'Terminal', ['class' => 'require']) !!}
            <select class="form-control select2-terminal-ajax" id="terminal_id" name="terminal_id">
            </select>

            <select class="form-control select2-terminal-ajax" id="terminal_ids" name="terminal_ids[]" multiple style="display: none">
            </select>
            <div class="error hidden" id="error-receivers">
            </div>
        </div>



        <!-- Submit Field -->
        <div class="form-group col-sm-12 center m-t-10">
            {!! Form::submit('Lưu', ['class' => 'btn btn-primary btn-create-banner']) !!}
            <a href="{{ route('banners.index') }}" class="btn btn-secondary">Hủy</a>
        </div>
    </div>
</div>
<div class="col-md-6 col-sm-12 col-sm-offset-0">
    <div class="row">
        <div class="form-group col-xs-12">
            {!! Form::label('type', 'Quyền', ['class' => 'require']) !!}
            <div id="web_permission" style="display: none">
                @foreach($web_permissions as $w_permission)
                    <input type="checkbox"  name="permissions[]" value="{{$w_permission->id}}">
                    <label for="permissions"> {{$w_permission->name}}</label><br>
                    @if(count($w_permission->childs))
                        @include('mp_mid_tid_user.child_permission',['childs' => $w_permission->childs,'level'=>1])
                    @endif
                @endforeach
            </div>
            <div id="app_permission" style="display: none">
                @foreach($app_permissions as $permission)
                    <input type="checkbox"  name="permissions[]" value="{{$permission->id}}">
                    <label for="permissions"> {{$permission->name}}</label><br>
                    @if(count($permission->childs))
                        @include('mp_mid_tid_user.child_permission',['childs' => $permission->childs,'level'=>1])
                    @endif

                @endforeach
            </div>
        </div>

    </div>
</div>
@section('css')
    {{--    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />--}}
    <link rel="stylesheet" href="{{asset('css/bootstrap-timepicker.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endsection

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function () {
            $('.select2-ajax').select2({
                placeholder: "-- Chọn merchant --",
                minimumInputLength: 2,
                ajax: {
                    url: '/terminals/ajaxListMerchantWithAll',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

            $('.select2-user-ajax').select2({
                placeholder: "-- Chọn số điện thoại --",
                minimumInputLength: 9,
                ajax: {
                    url: '/mp_user/ajaxListPhone',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                formatResult: function(element){
                    return element.text + ' (' + element.id + ')';
                },
                formatSelection: function(element){
                    return element.text + ' (' + element.id + ')';
                },
                escapeMarkup: function(m) {
                    return m;
                }

            });
            var user_data = {
                id: '{{$mp_mid_tid_user->mp_user->id}}',
                text: '{{$mp_mid_tid_user->mp_user->phone}}'
            };
            var merchant_data = {
                id: '{{$mp_mid_tid_user->merchant_code}}',
                text: '{{$mp_mid_tid_user->merchant_code}} - {{$mp_mid_tid_user->store->merchant_name}}'
            };
            @if($mp_mid_tid_user->terminal_id!= null)
            var terminal_data = {
                id: '{{$mp_mid_tid_user->terminal_id}}',
                text :'{{$mp_mid_tid_user->terminal_id}}'
            }
            var newTerminalOption = new Option(terminal_data.text, terminal_data.id, false, false);
            $('.select2-terminal-ajax').append(newTerminalOption).trigger('select2:select');
            @endif

            @if($mp_mid_tid_user->mp_permissions)
                    @foreach($mp_mid_tid_user->mp_permissions as $permission)
                     $("input[name='permissions[]'][value='{{$permission->permission_id}}']").prop('checked', true);
                   @endforeach
            @endif

            $('.select2-terminal-ajax').select2();
            var newOption = new Option(user_data.text, user_data.id, false, false);
            var newMerchantOption = new Option(merchant_data.text, merchant_data.id, false, false);
            $('.select2-user-ajax').append(newOption).trigger('select2:select');
            $('.select2-ajax').append(newMerchantOption).trigger('select2:select');

            $('#terminal_ids').next(".select2-container").hide();

            $('.select2-ajax').on('select2:select', function (e){
                var merchant_id = $(this).val();

                $('.select2-terminal-ajax').select2({
                    placeholder: "-- Chọn Terminal --",

                    ajax: {
                        url: '{{route('terminals.ajaxListTerminalByMerchantCode')}}',
                        dataType: 'json',
                        data: function ()  {
                            return {
                                q: merchant_id
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
                $('.select2-terminal-ajax').val(null).trigger('select2:select');

                var type = $('#type').val();
                if (type == 'terminal_web') {
                    $('#terminal_ids').next(".select2-container").show();
                    $('#terminal_id').next(".select2-container").hide();

                } else {
                    $('#terminal_ids').next(".select2-container").hide();
                    $('#terminal_id').next(".select2-container").show();


                }
            });
            $('#type').select2();
            $('#type').on('select2:select', function (e){
                $("#web_permission").hide();
                $("#app_permission").hide();
                var type = $(this).val();

                if (type == 'terminal_web') {
                    $("#web_permission").show();
                    $('#terminal_ids').next(".select2-container").show();
                    $('#terminal_id').next(".select2-container").hide();


                } else {
                    $('#terminal_ids').next(".select2-container").hide();
                    $('#terminal_id').next(".select2-container").show();

                }

                if (type == 'terminal_app') {
                    $("#app_permission").show();
                };
            });


            $('#type').val('{{$mp_mid_tid_user->type}}').trigger('select2:select');


            $('.select2-terminal-ajax').val('{{$mp_mid_tid_user->terminal_id}}').trigger('select2:select');
        });
    </script>
@endsection
