@extends('layouts_mms.app', ['new_select2' => true])

@section('content')
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-body">
                              {!! Form::model($mp_mid_tid_user, ['route' => ['mp_mid_tid_user.update', $mp_mid_tid_user->id], 'method' => 'patch']) !!}

                              @include('mp_mid_tid_user.edit_fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection
