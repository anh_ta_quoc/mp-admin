@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Cập nhật người dùng'])
    @include('coreui-templates::common.errors')
    @include('flash::message')
    <div class="row">
        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}

        @include('users.fields')

        {!! Form::close() !!}
    </div>
@endsection
