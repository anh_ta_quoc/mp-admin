<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Tên:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','required' => 'required', 'maxlength' => 255]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    @if(isset($user))
        {!! Form::email('email', null, ['class' => 'form-control','readonly' => 'readonly']) !!}
    @else
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    @endif
</div>
@if(isset($user))

<div class="form-group col-sm-6">
    {!! Form::label('new-password', 'Mật khẩu mới:') !!}
    {!! Form::password('new-password', array('placeholder' => '','class' => 'form-control password-field', 'maxlength' => 255)) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('confirm-new-password', 'Nhập lại mật khẩu mới:') !!}
    {!! Form::password('confirm-new-password', array('placeholder' => '','class' => 'form-control password-field', 'maxlength' => 255)) !!}
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Mật khẩu:') !!}
    {!! Form::password('password', array('placeholder' => '','class' => 'form-control password-field','required' => 'required', 'maxlength' => 255)) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('confirm-password', 'Nhập lại mật khẩu:') !!}
    {!! Form::password('confirm-password', array('placeholder' => '','class' => 'form-control password-field', 'maxlength' => 255, 'maxlength' => 255)) !!}
</div>
@endif

    @if(Auth::user()->hasrole('Super admin') || Auth::user()->email === 'devmp@gmail.com')
    <div class="form-group col-sm-6">
        <input type="hidden" name="change-role" id="change-role" value=""/>
        {!! Form::label('role', 'Vai trò:') !!}

        @if(isset($userRole))
        {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control select2','multiple', 'id' => 'user-roles')) !!}
        @else
            {!! Form::select('roles[]', $roles,[], array('class' => 'form-control select2','multiple', 'id' => 'user-roles')) !!}
        @endif
    </div>
    <div class="form-group col-sm-6">
        <label>Phân quyền:</label>

        <br/>

        @foreach($permissions as $value)
            @if(isset($userPermission))
                {{ Form::checkbox('permission[]',  $value->id, in_array($value->id, $userPermission) ? true : false, array('class' => 'name')) }}
                {{ $value->name }}
                <br/>
            @else
                {{ Form::checkbox('permission[]',  $value->id, false, array('class' => 'name')) }}
                {{ $value->name }}

                <br/>
            @endif

        @endforeach
    </div>
    @endif


{{--<!-- Email Verified At Field -->--}}
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('email_verified_at', 'Email Verified At:') !!}--}}
    {{--{!! Form::text('email_verified_at', null, ['class' => 'form-control','id'=>'email_verified_at']) !!}--}}
{{--</div>--}}



<!-- Password Field -->


<!-- Remember Token Field -->


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
{{--    <a href="{{ route('users.index') }}" class="btn btn-secondary">Quay lại</a>--}}
</div>
@section('scripts')
    <script type="text/javascript">
        // $('#email_verified_at').datetimepicker({
        //     format: 'YYYY-MM-DD HH:mm:ss',
        //     useCurrent: true,
        //     icons: {
        //         up: "icon-arrow-up-circle icons font-2xl",
        //         down: "icon-arrow-down-circle icons font-2xl"
        //     },
        //     sideBySide: true
        // });

        $('.password-field').keyup(function(){
            var str = $(this).val();
            var str = str.replace(/\s/g,'');
            $(this).val(str);
        });

        $('#user-roles').on('click', function () {
            if($(this).val()) {
                $('#change-role').val('yes');
            }
            else {
                $('#change-role').val('no');
            }
        })
    </script>
@endsection
