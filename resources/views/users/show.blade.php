@extends('layouts_mms.app')

@section('content')
    @include('flash::message')
    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('coreui-templates::common.errors')
            @include('layouts_mms.page_header', ['header' => 'Thông tin tài khoản'])
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-body">
                             @include('users.show_fields')
                         </div>
                     </div>
                 </div>
             </div>
          </div>
    </div>
@endsection
