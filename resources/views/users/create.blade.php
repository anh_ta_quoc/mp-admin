@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Thêm mới người dùng', 'buttons' => [['title' => 'Quay lại', 'link' => route('users.index'), 'icon' => 'fa fa-arrow-left']]])
    @include('coreui-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'users.store']) !!}

        @include('users.fields')

        {!! Form::close() !!}
    </div>
@endsection
