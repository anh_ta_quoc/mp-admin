{!! Form::open(['route' => ['users.destroy', $id],
'id' => 'delete_form_'.$id,
'method' => 'delete',
'onsubmit' => 'return confirm("Bạn chắc chắn thực hiện thao tác?")']) !!}
<div class="hidden-sm hidden-xs action-buttons">
    <a class="blue" href="{{ route('users.show', $id) }}">
        <i class="ace-icon fa fa-search-plus bigger-130"></i>
    </a>
    <a class="green" href="{{ route('users.edit', $id) }}">
    <i class="ace-icon fa fa-pencil bigger-130"></i>
    </a>

    <a class="red" href="javascript:$('#{{'delete_form_'.$id}}').submit();">
    <i class="ace-icon fa fa-trash-o bigger-130" onclick=""></i>
    </a>
</div>
{!! Form::close() !!}


