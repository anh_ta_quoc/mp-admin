@extends('layouts_mms.app')

@section('content')
    @include('flash::message')
    <div class="row dataTables_filter ">
        <div class="col-sm-3 form-group">
            <label>Tìm kiếm:</label>
            <input id="filter_query" class="form-control" placeholder="-- Nhập từ khóa tìm kiếm --" value="">
        </div>
        <div class="col-sm-3 form-group">
            <label>Thời gian:</label>
            <input class="form-control drp" id="date_range_search" placeholder="-- Chọn ngày tìm kiếm --">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            @include('enjoyments.table')
        </div>
    </div>
@endsection

