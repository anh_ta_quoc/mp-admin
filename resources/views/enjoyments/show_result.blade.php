@extends('layouts_mms.app', ['new_select2' => true])
@section('content')
    @include('layouts_mms.page_header', ['header' => 'Thay đổi thông tin thụ hưởng', 'buttons' => []])
    @include('flash::message')
    {!! Form::open(['route' => 'enjoyments.upsertEnjoyment', 'files' => true]) !!}
    <div class="widget-box transparent">
        <div class="widget-body" style="display: block;">
            <div class="box row">
                <div class="col-sm-6">
                    <div class="row">
                        <label for="merchantId" class="col-sm-5 control-label">Merchant ID - Tên Merchant <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <input class="form-control" id="merchant_code" name="merchant_code" value="{{ $requestData[0]['merchant_code'] }}" readonly/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="widget-box transparent">
        <div class="widget-header widget-header-flat">
            <h4 class="widget-title lighter">Thông tin Tài khoản thụ hưởng</h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>
        <div class="widget-body" style="display: block;">
            <div class="widget-main">
                <div class="box row">
                    <div class="col-sm-6">
                        <div class="row form-group">
                            <label for="account_number" class="col-sm-5 control-label">Số tài khoản thụ hưởng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('account_number', $requestData[0]['benef_number'], ['class' => 'form-control', 'id' => 'account_number', 'readonly' => true]) !!}
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="bank_name" class="col-sm-5 control-label">Tên chủ tài khoản thụ hưởng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('fullname', $requestData[0]['benef_name'], ['class' => 'form-control', 'id' => 'fullname', 'readonly' => true]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row form-group">
                            <label for="bank" class="col-sm-5 control-label">Ngân hàng thụ hưởng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                {!! Form::select('bank_code', $banks->pluck('bank_title','bank_code'), [$requestData[0]['benef_bank_code']], ['class' => 'form-control select2', 'id' => 'bank_code', 'readonly' => true]) !!}
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="branch" class="col-sm-5 control-label">Chi nhánh ngân hàng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('branch', $requestData[0]['benef_bank_branch'], ['class' => 'form-control', 'id' => 'branch', 'readonly' => true]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="widget-box transparent">
        <div class="widget-header widget-header-flat">
            <h4 class="widget-title lighter">Hồ sơ đính kèm</h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main">
                <div class="box row">
                    <div class="col-sm-6">
                        <div class="row form-group">
                            <label for="file_documentary" class="col-sm-5 control-label">Công văn thay đổi thụ hưởng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <a class="btn btn-primary" href="{{url('/api_uploads').'/'. $new_name }}" target="_blank"><i
                                            class="fa fa-eye"></i> Xem</a>
                                </div>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="text-center">
            <a href="javascript:history.back()" class="btn btn-default">Quay lại</a>
            <button class="btn buttons-export" type="submit">Xuất excel</button>
        </div>
    </div>
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script>
        $(function () {
            $(document).ready(function () {
                if ("{{ $requestData[0]['benef_bank_code'] }}") {
                    $("#bank_code").val("{{ $requestData[0]['benef_bank_code'] }}").trigger('change');
                    $("bank_code").select2("readonly", true);
                };
            });
        });
    </script>
@endsection
