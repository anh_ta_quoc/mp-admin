@extends('layouts_mms.app', ['new_select2' => true])
@section('content')
    @include('layouts_mms.page_header', ['header' => 'Thay đổi thông tin thụ hưởng', 'buttons' => []])
    {!! Form::open(['route' => 'enjoyments.chooseMerchant', 'files' => true]) !!}
        <div class="widget-box transparent">
            <div class="widget-body" style="display: block;">
                <div class="box row">
                    <div class="col-sm-6">
                        <div class="row">
                            <label for="merchantId" class="col-sm-5 control-label">Merchant ID - Tên Merchant <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                <select class="form-control select2-ajax" id="merchant_code" name="merchant_code">
                                    @if(old('merchant_code'))
                                        @php
                                            $old_merchant = \App\Libraries\BaseFunction::getMerchantByCode(old('merchant_code'))
                                        @endphp
                                        <option value="{{old('merchant_code')}}" selected="selected">{{ sprintf('%s - %s', $old_merchant['merchant_code'], $old_merchant['merchant_name']) }}</option>
                                    @endif
                                </select>
                                @if($errors->has('merchant_code'))
                                    <div class="error">{{ $errors->first('merchant_code') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="list-terminals" style="display: none;">
                    <div class="col-xs-12">
                        @include('enjoyments.table')
                    </div>

                    <div class="text-center">
                        <a href="{{ route('enjoyments.changeEnjoyment') }}" class="btn btn-default">Quay lại</a>
                        <button class="btn btn-primary" type="submit">Tiếp theo</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection
