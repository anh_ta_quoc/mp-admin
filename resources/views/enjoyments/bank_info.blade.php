@extends('layouts_mms.app', ['new_select2' => true])
@section('content')
    @include('layouts_mms.page_header', ['header' => 'Thay đổi thông tin thụ hưởng', 'buttons' => []])
    {!! Form::open(['route' => 'enjoyments.upsertEnjoyment', 'files' => true]) !!}
    <input type="hidden" value="{{ implode(',', $mpTerminalIds) }}" name="mp-terminal-ids"/>
    <input type="hidden" value="{{ implode(',', $mpTerminalNames) }}" name="mp-terminal-names"/>
    <div class="widget-box transparent">
        <div class="widget-body" style="display: block;">
            <div class="box row">
                <div class="col-sm-6">
                    <div class="row">
                        <label for="merchantId" class="col-sm-5 control-label">Merchant ID - Tên Merchant <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <input class="form-control" id="merchant_code" name="merchant_code" value="{{ old('merchant_code', $merchantCode) }}" readonly/>
                            @if($errors->has('merchant_code'))
                                <div class="error">{{ $errors->first('merchant_code') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="widget-box transparent">
        <div class="widget-header widget-header-flat">
            <h4 class="widget-title lighter">Thông tin Tài khoản thụ hưởng</h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>
        <div class="widget-body" style="display: block;">
            <div class="widget-main">
                <div class="box row">
                    <div class="col-sm-6">
                        <div class="row form-group">
                            <label for="account_number" class="col-sm-5 control-label">Số tài khoản thụ hưởng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('account_number', old('account_number', ''), ['class' => 'form-control', 'id' => 'account_number']) !!}
                                @if($errors->has('account_number'))
                                    <div class="error">{{ $errors->first('account_number') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="bank_account" class="col-sm-5 control-label">Tên chủ tài khoản thụ hưởng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('bank_account', old('bank_account', ''), ['class' => 'form-control', 'id' => 'bank_account']) !!}
                                @if($errors->has('bank_account'))
                                    <div class="error">{{ $errors->first('bank_account') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row form-group">
                            <label for="bank" class="col-sm-5 control-label">Ngân hàng thụ hưởng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                {!! Form::select('bank_code', $banks->pluck('bank_title','bank_code'), [old('bank_code', '')], ['class' => 'form-control select2', 'id' => 'bank_code']) !!}
                                @if($errors->has('bank_code'))
                                    <div class="error">{{ $errors->first('bank_code') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="branch" class="col-sm-5 control-label">Chi nhánh ngân hàng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('branch', old('branch', ''), ['class' => 'form-control', 'id' => 'branch']) !!}
                                @if($errors->has('branch'))
                                    <div class="error">{{ $errors->first('branch') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="widget-box transparent">
        <div class="widget-header widget-header-flat">
            <h4 class="widget-title lighter">Hồ sơ đính kèm</h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main">
                <div class="box row">
                    <div class="col-sm-6">
                        <div class="row form-group">
                            <label for="file_documentary" class="col-sm-5 control-label">Công văn thay đổi thụ hưởng <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <label class="input-group-btn">
                                    <span class="btn btn-primary">Chọn file&hellip;
                                        <input name="file_documentary"
                                               id="file_documentary"
                                               type="file"
                                               style="display: none;"
                                               accept="application/pdf,image/x-png,.jpg,.rar"
                                        >
                                    </span>
                                    </label>
                                    <input type="text" id="file_documentary__name_field" class="form-control" disabled>
                                </div>
                                @if($errors->has('file_documentary'))
                                    <div class="error">{{ $errors->first('file_documentary') }}</div>
                                @endif
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <span>File có định dạng pdf, jpg, rar, dung lượng tối đa 10MB</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="text-center">
            <a href="{{ route('enjoyments.changeEnjoyment') }}" class="btn btn-default">Quay lại</a>
            <button class="btn btn-primary" type="submit">Cập nhật</button>
        </div>
    </div>
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script>
        $(function () {
            $(document).on('change', ':file', function () {
                if (this.files[0]) {
                    var file = this.files[0],
                        extension = file.name.substr((file.name.lastIndexOf('.') + 1)),
                        input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    if (['pdf', 'png', 'jpg', 'rar', 'xlsx'].indexOf(extension) < 0) {
                        input.trigger('fileTypeError');
                    }
                    else if (file.size > 2*5242880) {
                        input.trigger('fileSizeError');
                    }
                    else {
                        input.trigger('fileSelected', [numFiles, label]);
                    }
                }
            });

            $(document).ready(function () {
                $(':file').on('fileTypeError', function (event) {
                    $(this).val("");
                    $(this).parents('.input-group').find(':text').val('');
                    $(this).parents('.form-group').addClass('has-error').find('.help-block').text('File có định dạng pdf, png, jpg, rar');
                });
                $(':file').on('fileSizeError', function (event) {
                    $(this).val("");
                    $(this).parents('.input-group').find(':text').val('');
                    $(this).parents('.form-group').addClass('has-error').find('.help-block').text('Dung lượng file tối đa là 10MB');
                });
                $(':file').on('fileSelected', function (event, numFiles, label) {
                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    }
                    $(this).parents('.form-group').removeClass('has-error').find('.help-block').text('');

                });
                if ("{{ old('bank_code') }}") {
                    $("#bank_code").val("{{ old('bank_code') }}").trigger('change')
                };
            });
        });
        $('#account_number').change(function () {
            if ($(this).val().trim().length >= 9){
                $.ajax({
                    url: "{{ url('/enjoyments/ajaxFindBankAccount') }}?merchant_code={{ $merchantCode }}&q=" + $(this).val().trim(),
                    method: 'GET',
                    success: function(data) {
                        if(data.length){
                            let value = data[0];
                            $('#bank_account').val(value.fullname);
                            $('#branch').val(value.branch);
                            $('#bank_code').val(value.bank_code).trigger('change');
                        }
                        else {
                            $('#bank_account').val('');
                            $('#branch').val('');
                            $('#bank_code').val('').trigger('change');
                        }
                    }
                });
            }
        })
    </script>
@endsection
