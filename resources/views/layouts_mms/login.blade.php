<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta charset="utf-8"/>
    <title>Merchant Portal Management</title>
    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/theme.css')}}"/>
</head>
<body class="login-layout" cz-shortcut-listen="true">

<div class="main-container" id="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="position-relative border-radius10">
                        <div id="login-box" class="login-box visible widget-box no-border border-radius10">
                            <div class="widget-body border-radius10">
                                <div class="widget-main border-radius10">
                                    <div class="header-logo">
                                        <div class="logo"><img src="{{asset('images/logo.png')}}" alt="logo"></div>
                                        <div class="header-text-login">
                                            <p><span class="clred">Merchant</span><span class="clblue"> Portal</span> Management</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    @yield('content')
                                </div>
                                <!-- /.widget-main -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.login-box -->
                        <!-- /.signup-box -->
                    </div>
                    <!-- /.position-relative -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.page-content -->
    </div>
    <!-- /.main-content -->
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse"> <i class="default-icon fa fa-angle-double-up icon-only bigger-110"></i> </a>
</div>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
{{--<script src="{{asset('js/java.js')}}"></script>--}}
{{--<script src="{{asset('js/sms-extra.min.js')}}"></script>--}}
<!-- Bo sung JS datetime va datatable -->
<script src="{{asset('js/moment-with-locales.js')}}"></script>
<!-- End JS datetime va datatable -->
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/additional-methods.min.js')}}"></script>
</body>
</html>
