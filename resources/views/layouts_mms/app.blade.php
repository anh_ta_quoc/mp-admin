<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta charset="utf-8"/>
    <title>Merchant Portal Management</title>
    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"/>
    @if (isset($new_select2) && $new_select2)
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" rel="stylesheet" />
    @else
        <link rel="stylesheet" href="{{asset('css/select2.css')}}"/>
    @endif
    <link rel="stylesheet" href="{{asset('css/theme.css')}}?v=2"/>
    <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.css')}}">
    <link id="bsdp-css" href="{{asset('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/switch-button.css')}}"/>
    @if(isset($new_select2) && $new_select2)
        <style>
            .select2-search:after{
                content: none;
            }
        </style>
    @endif
    @yield('css')
</head>
<body class="no-skin">
<div id="navbar" class="navbar navbar-default">
    <div class="navbar-container" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" style="z-index: 1">
            <span class="sr-only">menu</span><span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="navbar-header pull-left">
            <div class="box clearfix">
                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i class="default-icon fa fa-angle-double-left" data-icon1="default-icon fa fa-angle-double-left"
                       data-icon2="default-icon fa fa-angle-double-right"></i>
                </div>
                <a href="#" class="navbar-brand hidden-xs">
                    <img src="{{asset('images/logo.png')}}" width="182" height="41" alt="logo">
                </a>
            </div>
        </div>
        <div class="header-text hidden-xs">
            <p><span class="clred">Merchant</span><span class="clblue"> Portal</span> Management</p>
        </div>
        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="users">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img src="{{asset('images/ava.png')}}" width="40" height="40" alt="ava">
                        <span class="user-info">{{ Auth::user()->name }}</span>
                        <i class="default-icon fa fa-caret-down"></i>
                    </a>
                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-caret dropdown-close">
                        <li><a href="{{ url('/users/' . Auth::user()->id . '/edit') }}"><i class="default-icon fa fa-user"></i>Chi tiết tài khoản</a></li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('/logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="default-icon fa fa-power-off"></i>Đăng xuất
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="main-container" id="main-container">
    @include('layouts_mms.sidebar')
    <div class="main-content">
        {{ Breadcrumbs::render() }}
        <div class="page-content">
            <div class="page-content-area">
                @yield('content')
            </div>
        </div>
    </div>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse"> <i
                class="default-icon fa fa-angle-double-up icon-only bigger-110"></i> </a>
</div>

<script src="{{asset('js/ie8-responsive-file-warning.js')}}"></script>
<script src="{{asset('js/ie10-viewport-bug-workaround.js')}}"></script>
<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="//unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script src="//unpkg.com/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.vi.min.js" charset="UTF-8"></script>
@if (isset($new_select2) && $new_select2)
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
@else
<script src="{{asset('js/select2.min.js')}}"></script>
@endif
<script src="{{asset('js/java.js')}}"></script>
<script src="{{asset('js/sms-extra.min.js')}}"></script>
<script src="{{asset('js/moment-with-locales.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.select2').select2({'allowClear': true});
        $('.dp').datepicker({
            language: "vi"
        });
    })
</script>
@yield('scripts')

</body>

</html>
