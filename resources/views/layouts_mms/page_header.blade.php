<div class="page-header">
    <h1 class="page-header-left">{{$header}}</h1>
    @if (isset($buttons))
        @if ($buttons)
            @foreach( $buttons as $button )
                <div class="page-header-right">
                    <a class="btn btn-primary" href="{{$button['link']}}">
                        <i class="{{$button['icon']}}"></i>{{$button['title']}}
                    </a>
                </div>
            @endforeach
        @endif
    @endif
    <div class="clearfix"></div>
</div>