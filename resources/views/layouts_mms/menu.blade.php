<li>
    <a href="#"><i class="menu-icon fa fa-dashboard"></i><span class="menu-text"> Bảng điều khiển</span></a>
</li>
<li class="{{ Request::is('trials*') ? 'active' : '' }}">
    <a href="{{ route('trials.index') }}"><i class="menu-icon fa fa-users"></i><span class="menu-text">Tài khoản thử nghiệm</span></a>
</li>
<li class="{{ Request::is('stores*') ? 'active' : '' }}">
    <a href="#" class="dropdown-toggle"><i class="menu-icon fa fa-list-alt"></i><span class="menu-text">Quản lý Merchant</span><b
            class="arrow fa fa-angle-down"></b></a><b class="arrow"></b>
    <ul class="submenu">
        <li class="hsub {{ \Route::current()->getName() == 'stores.index' ? 'active' : '' }}">
            <a href="{{ route('stores.index') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách Merchant</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{ \Route::current()->getName() == 'stores.activeList' ? 'active' : '' }}">
            <a href="{{ route('stores.activeList') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách đã duyệt</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{ \Route::current()->getName() == 'stores.registerList' ? 'active' : '' }}">
            <a href="{{ route('stores.registerList') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách gửi đăng ký</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{\Route::current()->getName() == 'stores.needApproveList' ? 'active' : '' }}">
            <a href="{{ route('stores.needApproveList') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách chờ duyệt</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{ \Route::current()->getName() == 'stores.deniedList' ? 'active' : '' }}">
            <a href="{{ route('stores.deniedList') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách bị từ chối</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{ \Route::current()->getName() == 'stores.lockedList' ? 'active' : '' }}">
            <a href="{{ route('stores.lockedList') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách đóng</span></a>
            <b class="arrow"></b>
        </li>
    </ul>
</li>
<li class="{{ Request::is('terminals*') || Request::is('terminalBatches*') ? 'active' : '' }}">
    <a href="#" class="dropdown-toggle"><i class="menu-icon fa fa-terminal"></i><span class="menu-text">Quản lý Terminal</span><b
            class="arrow fa fa-angle-down"></b></a><b class="arrow"></b>
    <ul class="submenu">
        <li class="hsub {{ \Route::current()->getName() == 'terminals.index' ? 'active' : '' }}">
            <a href="{{ route('terminals.index') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách Terminal</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{\Route::current()->getName() == 'terminals.needApproveList' ? 'active' : '' }}">
            <a href="{{ route('terminals.needApproveList') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách chờ duyệt</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{ \Route::current()->getName() == 'terminals.approvedList' ? 'active' : '' }}">
            <a href="{{ route('terminals.approvedList') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách đã duyệt</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{ \Route::current()->getName() == 'terminals.deniedList' ? 'active' : '' }}">
            <a href="{{ route('terminals.deniedList') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách bị từ chối</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{ \Route::current()->getName() == 'terminals.lockedList' ? 'active' : '' }}">
            <a href="{{ route('terminals.lockedList') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách đóng</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{ Request::is('terminalBatches*') ? 'active' : '' }}">
            <a href="{{ route('terminalBatches.index') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách theo lô</span></a>
            <b class="arrow"></b>
        </li>
    </ul>
</li>
<li class="{{ Request::is('enjoyments*') ? 'active' : '' }}">
    <a href="#" class="dropdown-toggle"><i class="menu-icon fa fa-credit-card"></i><span class="menu-text">Quản lý thụ hưởng</span><b
            class="arrow fa fa-angle-down"></b></a><b class="arrow"></b>
    <ul class="submenu">
        <li class="hsub {{ \Route::current()->getName() == 'enjoyments.changeEnjoyment' ||
            \Route::current()->getName() == 'enjoyments.chooseMerchant' ? 'active' : '' }}">
            <a href="{{ route('enjoyments.changeEnjoyment') }}"><i class="menu-icon fa"></i><span class="menu-text">Tạo thay đổi thụ hưởng</span></a>
            <b class="arrow"></b>
        </li>
        <li class="hsub {{ \Route::current()->getName() == 'enjoyments.index' ||
            \Route::current()->getName() == 'enjoyments.index' ? 'active' : '' }}">
            <a href="{{ route('enjoyments.index') }}"><i class="menu-icon fa"></i><span class="menu-text">Danh sách đổi thụ hưởng</span></a>
            <b class="arrow"></b>
        </li>
    </ul>
</li>
@if(Auth::user()->hasrole('Mở khóa Merchant User(s)') || Auth::user()->email === 'devmp@gmail.com')
    <li class="{{ Request::is('utils*') ? 'active' : '' }}">
        <a href="{{ route('utils.index') }}"><i class="menu-icon fa fa-database"></i><span class="menu-text">Mở khóa Merchant User</span></a>
    </li>
@endif
@if(Auth::user()->email === 'devmp@gmail.com' || Auth::user()->hasPermissionTo('Notification'))
<li class="{{ Request::is('messages*') ? 'active' : '' }}">
    <a href="{{ route('messages.index') }}"><i class="menu-icon fa fa-bell"></i><span class="menu-text">Quản lý thông báo</span></a>
</li>
@endif
@if(Auth::user()->email === 'devmp@gmail.com' || Auth::user()->hasPermissionTo('Banner'))
<li class="{{ Request::is('banners*') ? 'active' : '' }}">
    <a href="{{ route('banners.index') }}"><i class="menu-icon fa fa-flag"></i><span class="menu-text">Quản lý banner</span></a>
</li>
@endif

@if(Auth::user()->email === 'devmp@gmail.com' || Auth::user()->hasrole('Super admin'))
    <li class="{{ Request::is('smsLogs*') ? 'active' : '' }}">
        <a href="{{ route('smsLogs.index') }}"><i class="menu-icon fa fa-comment"></i><span
                class="menu-text">SMS Logging</span></a>
    </li>
    <li class="{{ (Request::is('users*') ||Request::is('roles*') || Request::is('permissions*'))  ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle"><i class="menu-icon fa fa-cogs"></i><span class="menu-text">Thiết lập</span><b
                    class="arrow fa fa-angle-down"></b></a><b class="arrow"></b>
        <ul class="submenu">
                <li class="hsub {{ Request::is('users*') ? 'active' : '' }}">
                    <a href="{{ route('users.index') }}"><i class="menu-icon fa fa-caret-right"></i>Tài khoản</a>
                    <b class="arrow"></b>
                </li>
            <li class="hsub {{ Request::is('roles*') ? 'active' : '' }}">
                <a href="{{ route('roles.index') }}"><i class="menu-icon fa fa-caret-right"></i>Vai trò</a>
                <b class="arrow"></b>
            </li>
            <li class="hsub {{ Request::is('permissions*') ? 'active' : '' }}">
                <a href="{{ route('permissions.index') }}"><i class="menu-icon fa fa-caret-right"></i>Các quyền</a>
                <b class="arrow"></b>
            </li>
            {{--<li class="hsub">--}}
                {{--<a href="#"><i class="menu-icon fa fa-caret-right"></i>Đồng bộ từ MMS DB</a>--}}
                {{--<b class="arrow"></b>--}}
            {{--</li>--}}
        </ul>
    </li>
@endif
