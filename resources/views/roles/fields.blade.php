<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Tên:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">

    <label>Quyền:</label>

        <br/>

        @foreach($permissions as $value)
           @if(isset($rolePermissions))
           {{ Form::checkbox('permission[]',  $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                {{ $value->name }}
            <br/>
           @else
            {{ Form::checkbox('permission[]',  $value->id, false, array('class' => 'name')) }}
            {{ $value->name }}

            <br/>
           @endif

        @endforeach

</div>
{{--<!-- Guard Name Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('guard_name', 'Mã:') !!}--}}
{{--{!! Form::text('guard_name', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('roles.index') }}" class="btn btn-secondary">Quay Lại</a>
</div>
