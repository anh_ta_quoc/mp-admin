@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Vai trò', 'buttons' => [['title' => 'Thêm mới', 'link' => route('roles.create'), 'icon' => 'fa fa-plus-circle']]])
    @include('flash::message')
    <div class="row">
        <div class="col-xs-12">
            @include('roles.table')
        </div>
    </div>
@endsection