@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Thêm mới Vai trò', 'buttons' => [['title' => 'Quay lại', 'link' => route('roles.index'), 'icon' => 'fa fa-arrow-left']]])
    @include('coreui-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'roles.store']) !!}

        @include('roles.fields')

        {!! Form::close() !!}
    </div>
@endsection
