@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Cập nhật quyền', 'buttons' => [['title' => 'Quay lại', 'link' => route('roles.index'), 'icon' => 'fa fa-arrow-left']]])
    @include('coreui-templates::common.errors')
    <div class="row">
        {!! Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'patch']) !!}

        @include('roles.fields')

        {!! Form::close() !!}
    </div>
@endsection