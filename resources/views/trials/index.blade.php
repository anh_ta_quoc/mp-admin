@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Tài khoản thử nghiệm'])
    @include('flash::message')
    <div class="row">
        <div class="col-xs-12">
            @include('trials.table')
        </div>
    </div>
@endsection

