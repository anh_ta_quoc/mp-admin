@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Thêm mới Quyền', 'buttons' => [['title' => 'Quay lại', 'link' => route('permissions.index'), 'icon' => 'fa fa-arrow-left']]])
    @include('coreui-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'permissions.store']) !!}

        @include('permissions.fields')

        {!! Form::close() !!}
    </div>
@endsection
