@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Quyền', 'buttons' => [['title' => 'Thêm mới', 'link' => route('permissions.create'), 'icon' => 'fa fa-plus-circle']]])
    @include('flash::message')
    <div class="row">
        <div class="col-xs-12">
            @include('permissions.table')
        </div>
    </div>
@endsection