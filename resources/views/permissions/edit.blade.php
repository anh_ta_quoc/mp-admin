@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Cập nhật quyền', 'buttons' => [['title' => 'Quay lại', 'link' => route('permissions.index'), 'icon' => 'fa fa-arrow-left']]])
    @include('coreui-templates::common.errors')
    <div class="row">
        {!! Form::model($permission, ['route' => ['permissions.update', $permission->id], 'method' => 'patch']) !!}

        @include('permissions.fields')

        {!! Form::close() !!}
    </div>
@endsection