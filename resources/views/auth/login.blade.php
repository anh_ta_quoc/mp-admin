@extends('layouts_mms.login')

@section('content')
    <div class="header blue lighter bigger text-center">Đăng nhập</div>
    <form method="post" action="{{ url('/login') }}">
        @csrf
        <div class="wrapper">
            <label class="block clearfix">
                <i class="fa fa-envelope"></i>
                <input type="email" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}"
                       placeholder="Email">
            </label>
        </div>
        @if ($errors->has('email'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <div class="wrapper">
            <label class="block clearfix">
                <i class="fa fa-lock"></i>
                <input type="password" class="form-control {{ $errors->has('password')?'is-invalid':'' }}" placeholder="Mật khẩu" name="password">
            </label>
        </div>
        @if ($errors->has('password'))
            <span class="invalid-feedback">
               <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <div class="wrapper text-right">
            <a class="link-underline clblue" href="{{ url('/password/reset') }}">Quên mật khẩu</a>
        </div>
        <div class="space"></div>
        <div class="row clearfix" style="margin-bottom: 30px">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary bigger-120 btn-login">Đăng nhập</button>
            </div>
        </div>
        <div class="space-4"></div>
    </form>
@endsection
