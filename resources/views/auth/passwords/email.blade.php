
@extends('layouts_mms.login')

@section('content')
    <div class="header blue lighter bigger text-center">Quên mật khẩu</div>
    <form method="post" action="{{ url('/password/email') }}">
        @csrf
        <div class="wrapper">
            <label class="block clearfix">
                <i class="fa fa-envelope"></i>
                <input type="email" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}"
                       placeholder="Email">
            </label>
        </div>
        @if ($errors->has('email'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <div class="space"></div>
        <div class="row clearfix" style="margin-bottom: 30px">
            <div class="col-xs-6">
                <a href="{{url('/login')}}" class="btn btn-light bigger-120 btn-login">Quay lại</a>
            </div>
            <div class="col-xs-6">
                <button type="submit" class="btn btn-primary bigger-120 btn-login">Gửi email</button>
            </div>
        </div>
        <div class="space-4"></div>
    </form>
@endsection

