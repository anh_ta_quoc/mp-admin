@extends('layouts_mms.login')

@section('content')
    <div class="header blue lighter bigger text-center">Tạo mật khẩu mới</div>
    <form method="post" action="{{ url('/password/reset') }}">
        @csrf
        <input type="hidden" value="{{ $token }}" name="token"/>
        <div class="wrapper">
            <label class="block clearfix">
                <i class="fa fa-envelope"></i>
                <input type="email" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}" placeholder="Email">
            </label>
        </div>
        @if ($errors->has('email'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <div class="wrapper">
            <label class="block clearfix">
                <i class="fa fa-lock"></i>
                <input type="password" class="form-control {{ $errors->has('password')?'is-invalid':''}}" name="password" placeholder="Mật khẩu">
            </label>
        </div>
        @if ($errors->has('password'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

        <div class="wrapper">
            <label class="block clearfix">
                <i class="fa fa-lock"></i>
                <input type="password" name="password_confirmation" class="form-control"
                       placeholder="Nhập lại mật khẩu">
            </label>
        </div>
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                               </span>
        @endif

        <div class="space"></div>
        <div class="row clearfix" style="margin-bottom: 30px">
            <div class="col-xs-6">
                <a href="{{url('/login')}}" class="btn btn-light bigger-120 btn-login">Trở về đăng nhập</a>
            </div>
            <div class="col-xs-6">
                <button type="submit" class="btn btn-primary bigger-120 btn-login">Đặt lại mật khẩu</button>
            </div>
        </div>
        <div class="space-4"></div>
    </form>
@endsection


