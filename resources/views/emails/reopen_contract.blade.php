<div>
    <p>Hello <b>Sales admin</b>,<br/><br/>
        Hồ sơ Merchant {{ $merchant_brand }} đã được mở lại để upload hợp đồng!<br/>
        Sau khi trình ký, xin dấu, scan lấy bản mềm, vui lòng:<br/></p>
        <ul>
            <li style="list-style-type: none;">1. Nhập các thông tin về hợp đồng (sẽ có trong link chức năng Upload hợp đồng dưới đây)</li>
            <li style="list-style-type: none;">2. Upload bản mềm hợp đồng lên cho Merchant</li>
        </ul>
        <p>Để thực hiện các mục việc này, vui lòng nhấn vào<br/>
        {{ $store_info_url }}<br/><br/>
        <i>Đây là email tự động sinh từ hệ thống, vui lòng không trả lời!</i><br/>
        Thanks,<br/>
        Team Merchant Portal<br/>
        </p>
</div>
