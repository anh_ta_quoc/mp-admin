<div>
    <p>Xin chào <b>Sale Admin</b>,<br/><br/>
        KD vừa từ chối duyệt hồ sơ đăng ký bổ sung cho Terminal sau: <b>{{ sprintf('%s - %s', $terminalId, $terminalName) }}</b><br/>
        thuộc đơn vị kinh doanh <b>{{ sprintf('%s - %s', $merchantCode, $merchantName) }}</b><br/>
        @if ($reason)
            Lý do: <b>{{ $reason }}</b><br/>
        @endif
        Vui lòng kiểm tra lại hồ sơ Terminal và bổ sung nếu cần.
        {{ $terminalDetailUrl }}<br/><br/>
        <i>Đây là email tự động sinh từ hệ thống, vui lòng không trả lời!</i><br/>
        Thanks,<br/>
        Team Merchant Portal<br/>
    </p>
</div>
