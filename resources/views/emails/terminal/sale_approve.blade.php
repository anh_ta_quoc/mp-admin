<div>
    <p>Xin chào <b>Đối soát</b>,<br/><br/>
        <b>{{ sprintf('%s - %s', $terminalId, $terminalName) }}</b><br/>
        thuộc đơn vị kinh doanh <b>{{ sprintf('%s - %s', $merchantCode, $merchantName) }}</b> có yêu cầu kết nối.<br/>
        Vui lòng kiểm tra thông tin để tiến hành xét duyệt.
        {{ $terminalDetailUrl }}<br/><br/>
        <i>Đây là email tự động sinh từ hệ thống, vui lòng không trả lời!</i><br/>
        Thanks,<br/>
        Team Merchant Portal<br/>
    </p>
</div>
