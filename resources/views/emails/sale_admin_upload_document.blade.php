<div>
    <p>Hello <b>Hỗ trợ Kinh doanh</b>,<br/><br/>
        Hệ thống mới ghi nhận 01 Merchant {{ $merchant_brand }} yêu cầu hoàn thiện hồ sơ ở địa chỉ sau:<br/>
        {{ $store_info_url }}<br/><br/>
        Để xem chi tiết và duyệt, vui lòng nhấn vào link trên.<br/><br/>
        <i>Đây là email tự động sinh từ hệ thống, vui lòng không trả lời!</i><br/>
        Thanks,<br/>
        Team Merchant Portal<br/>
    </p>
</div>
