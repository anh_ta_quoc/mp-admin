@extends('layouts_mms.app', ['new_select2' => true])

@section('content')
    @include('flash::message')

    <div class="container-fluid">
        <div class="animated fadeIn">
            {{--            @include('coreui-templates::common.errors')--}}
            @include('layouts_mms.page_header', ['header' => 'Thêm mới Terminal theo lô'])
            {!! Form::open(['route' => 'terminalBatches.store','files' =>true,'enctype'=>'multipart/form-data']) !!}

            @include('mp_terminal_batches.fields')

            {!! Form::close() !!}
        </div>
    </div>
@endsection
