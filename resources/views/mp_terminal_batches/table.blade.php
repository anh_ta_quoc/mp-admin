@section('css')
    @include('layouts_mms.datatables_css')
    @include('layouts_mms.daterange_picker_css')
@endsection

{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}

@section('scripts')
    @include('layouts_mms.daterange_picker_js', ['init_id' => ['date_range_search']])
    @include('layouts_mms.datatables_js')
    {!! $dataTable->scripts() !!}
    <script>
        var queryTypingTimer;
        var doneTypingInterval = 1000;
        var $input = $('#filter_query');
        $input.on('keyup', function () {
            clearTimeout(queryTypingTimer);
            queryTypingTimer = setTimeout(doneFilterTyping, doneTypingInterval);
        });
        $input.on('keydown', function () {
            clearTimeout(queryTypingTimer);
        });
        function doneFilterTyping () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].search($input.val()).draw();
            }
        }
        $('#filter_status').on('change', function () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        });
        $('#merchant_code').on('change', function () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        });
        $(document).ready(function () {
            $('.select2-ajax').select2({
                placeholder: "-- Chọn merchant --",
                minimumInputLength: 2,
                ajax: {
                    url: '/terminals/ajaxListMerchant',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@endsection
