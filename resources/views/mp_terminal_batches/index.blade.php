@extends('layouts_mms.app', ['new_select2' => false])

@section('content')
    @if(Auth::user()->hasrole('Sale admin'))
        @include('layouts_mms.page_header', ['header' => 'Tổng hợp danh sách Terminal theo lô', 'buttons' => [['title' => 'Thêm mới', 'link' => route('terminalBatches.create'), 'icon' => 'fa fa-plus-circle']]])
    @else
        @include('layouts_mms.page_header', ['header' => 'Tổng hợp danh sách Terminal theo lô'])
    @endif
    @include('flash::message')
    <div class="row dataTables_filter ">
        <div class="col-sm-3 form-group">
            <label>Tìm kiếm:</label>
            <input id="filter_query" class="form-control" placeholder="-- Nhập từ khóa tìm kiếm --" value="{{ session()->get('terminal_batch_filter_query') }}">
        </div>
        <div class="col-sm-3 form-group">
            <label>Thời gian:</label>
            <input class="form-control drp" id="date_range_search" placeholder="-- Chọn ngày tìm kiếm --">
        </div>
        @if (count($filterable_status) > 0)
            <div class="col-sm-3 form-group">
                <label>Trạng thái:</label>
                <select id="filter_status" name="filter_status" class="form-control select2"
                        data-placeholder="-- Chọn trạng thái --">
                    <option></option>
                    @foreach($filterable_status as $filterStatus)
                        <option value="{{ $filterStatus }}"
                            {{ session()->get('terminal_filter_status') && session()->get('terminal_batch_filter_status') == $filterStatus ? 'selected' : '' }}
                        >
                            {{ \App\MpTerminalBatch::STATUS[$filterStatus] }}
                        </option>
                    @endforeach
                </select>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-xs-12">
            @include('mp_terminal_batches.table')
        </div>
    </div>
@endsection

