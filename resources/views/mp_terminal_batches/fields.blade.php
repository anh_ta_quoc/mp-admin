<div class="widget-box transparent">
    <div class="widget-body" style="display: block;">
        <div class="box row">
            <div class="col-sm-6">
                <div class="row">
                    <label for="merchantId" class="col-sm-5 control-label">Merchant ID - Tên Merchant <span class="red">(*)</span></label>
                    <div class="col-sm-7">
                        <select class="form-control select2-ajax" id="merchant-code" name="merchant_code">
                            @if(old('merchant_code'))
                                @php
                                    $old_merchant = \App\Libraries\BaseFunction::getMerchantByCode(old('merchant_code'))
                                @endphp
                                <option value="{{old('merchant_code')}}" selected="selected">{{ sprintf('%s - %s', $old_merchant['merchant_code'], $old_merchant['merchant_name']) }}</option>
                            @endif
                        </select>
                        @if($errors->has('merchant_code'))
                            <div class="error">{{ $errors->first('merchant_code') }}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="widget-box transparent">
    <div class="widget-header widget-header-flat">
        <h4 class="widget-title lighter">Thông tin tài khoản thụ hưởng</h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div class="box row">
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label for="bank_number" class="col-sm-5 control-label">Số tài khoản thụ hưởng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('bank_number', null, ['class' => 'form-control', 'maxlength' => 30, 'id' => 'bank_number']) !!}
                            @if($errors->has('bank_number'))
                                <div class="error">{{ $errors->first('bank_number') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="bank_name" class="col-sm-5 control-label">Tên chủ tài khoản thụ hưởng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('bank_account', null, ['class' => 'form-control', 'maxlength' => 150, 'id' => 'bank_account']) !!}
                            @if($errors->has('bank_account'))
                                <div class="error">{{ $errors->first('bank_account') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label for="bank" class="col-sm-5 control-label">Ngân hàng thụ hưởng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <select
                                class="form-control select2"
                                id="bank_code"
                                name="bank_code"
                                data-placeholder="-- Chọn --"
                            >
                                <option></option>
                                @foreach($banks as $bank)
                                    <option value="{{ $bank['bank_code'] }}">{{ $bank['bank_title'] }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('bank_code'))
                                <div class="error">{{ $errors->first('bank_code') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="branch" class="col-sm-5 control-label">Chi nhánh ngân hàng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('bank_branch', null, ['class' => 'form-control', 'maxlength' => 100, 'id' => 'bank_branch']) !!}
                            @if($errors->has('bank_branch'))
                                <div class="error">{{ $errors->first('bank_branch') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="widget-box transparent">
    <div class="widget-header widget-header-flat">
        <h4 class="widget-title lighter">Hồ sơ đính kèm</h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div class="box row">
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label for="file_contract" class="col-sm-5 control-label">Biên bản/Hợp đồng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">Chọn file&hellip;
                                        <input name="file_contract"
                                               id="file_contract"
                                               type="file"
                                               style="display: none;"
                                               accept="application/pdf,image/x-png,.jpg,.rar"
                                        >
                                    </span>
                                </label>
                                <input type="text" id="file_contract__name_field" class="form-control" disabled>
                            </div>
                            @if($errors->has('file_contract'))
                                <div class="error">{{ $errors->first('file_contract') }}</div>
                            @endif
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box row">
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label for="file_authorize" class="col-sm-5 control-label">Giấy ủy quyền <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">Chọn file&hellip;
                                        <input name="file_authorize"
                                               id="file_authorize"
                                               type="file"
                                               style="display: none;"
                                               accept="application/pdf,image/x-png,.jpg,.rar"
                                        >
                                    </span>
                                </label>
                                <input type="text" id="file_authorize__name_field" class="form-control" disabled>
                            </div>
                            @if($errors->has('file_authorize'))
                                <div class="error">{{ $errors->first('file_authorize') }}</div>
                            @endif
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box row">
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label for="file_terminals" class="col-sm-5 control-label">File terminal <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">Chọn file&hellip;
                                        <input name="file_terminals"
                                               id="file_terminals"
                                               type="file"
                                               style="display: none;"
                                               accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                        >
                                    </span>
                                </label>
                                <input type="text" id="file_terminals_cert__name_field" class="form-control" disabled>
                            </div>
                            @if($errors->has('file_terminals'))
                                <div class="error">{{ $errors->first('file_terminals') }}</div>
                            @endif
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <a href="/templates/terminal_batch.xlsx" download="">
                        <span class="fa fa-download"></span>
                        Tải file mẫu
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="text-center">
    <a href="{{ route('terminalBatches.index') }}" class="btn btn-default">Quay lại</a>
    <button class="btn btn-success" type="submit" value="1">Lưu</button>
</div>

@section('scripts')
<script>
    $(function () {
        $(document).on('change', ':file', function () {
            if (this.files[0]) {
                var file = this.files[0],
                    extension = file.name.substr((file.name.lastIndexOf('.') + 1)),
                    input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                if (['pdf', 'png', 'jpg', 'rar', 'xlsx'].indexOf(extension) < 0) {
                    input.trigger('fileTypeError');
                }
                else if (file.size > 2*5242880) {
                    input.trigger('fileSizeError');
                }
                else {
                    input.trigger('fileSelected', [numFiles, label]);
                }
            }
        });


        $(document).ready(function () {
            $(':file').on('fileTypeError', function (event) {
                $(this).val("");
                $(this).parents('.input-group').find(':text').val('');
                $(this).parents('.form-group').addClass('has-error').find('.help-block').text('File có định dạng pdf, png, jpg, rar');
            });
            $(':file').on('fileSizeError', function (event) {
                $(this).val("");
                $(this).parents('.input-group').find(':text').val('');
                $(this).parents('.form-group').addClass('has-error').find('.help-block').text('Dung lượng file tối đa là 10MB');
            });
            $(':file').on('fileSelected', function (event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if (input.length) {
                    input.val(log);
                }
                $(this).parents('.form-group').removeClass('has-error').find('.help-block').text('');

            });
            if ("{{old('terminal_province_code')}}") {
                $("#terminal_provinces").val("{{ old('terminal_province_code') }}").trigger('change', function () {
                    $("#terminal_districts").val("{{ old('terminal_district_code') }}").trigger('change', function () {
                        $("#terminal_wards").val("{{ old('terminal_ward_code') }}").trigger('change')
                    })
                });
            };
            if ("{{ old('bank_code') }}") {
                $("#bank_code").val("{{ old('bank_code') }}").trigger('change')
            };
            if ("{{ old('receive_method') }}") {
                $("#receive_method").val("{{ old('receive_method') }}").trigger('change')
            };
            $('.select2-ajax').select2({
                placeholder: "-- Chọn merchant --",
                minimumInputLength: 2,
                ajax: {
                    url: '/terminals/ajaxListMerchant',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        });

    });
</script>
@endsection
