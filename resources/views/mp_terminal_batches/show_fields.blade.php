<div class="widget-box transparent">
    <div class="widget-body" style="display: block;">
        <div class="box row">
            <div class="col-sm-6">
                <div class="row form-group">
                    <label for="merchantId" class="col-sm-5 control-label">Merchant ID - Tên Merchant <span class="red">(*)</span></label>
                    <div class="col-sm-7">
                        <label><b>{{$batch->merchant_code . ' - ' . $batch->merchant_name}}</b></label>
                    </div>
                </div>
                <div class="row form-group">
                    <label for="merchantId" class="col-sm-5 control-label">Trạng thái</label>
                    <div class="col-sm-7">
                        <strong>{!! $batch->status_label !!}</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="widget-box transparent">
    <div class="widget-header widget-header-flat">
        <h4 class="widget-title lighter">Thông tin tài khoản thụ hưởng</h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div class="box row">
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label for="terminalId" class="col-sm-5 control-label">Số tài khoản thụ hưởng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <label>
                                <b>{{$batch->bank_number}}</b>
                            </label>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="terminalId" class="col-sm-5 control-label">Tên chủ tài khoản thụ hưởng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <label>
                                <b>{{$batch->bank_account}}</b>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label for="terminalId" class="col-sm-5 control-label">Ngân hàng thụ hưởng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <label>
                                <b>{{$batch->bank_name}}</b>
                            </label>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="terminalId" class="col-sm-5 control-label">Chi nhánh ngân hàng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <label>
                                <b>{{$batch->bank_branch}}</b>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="widget-box transparent">
    <div class="widget-header widget-header-flat">
        <h4 class="widget-title lighter">Hồ sơ đính kèm</h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div class="box row">
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label for="terminalId" class="col-sm-5 control-label">Biên bản/Hợp đồng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <a href="{{'/api_uploads/terminal_batch/' . $batch->merchant_code . '/' . $batch->file_contract}}" download>
                                <b>{{$batch->file_contract}}</b>
                            </a>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="terminalId" class="col-sm-5 control-label">Giấy ủy quyền <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <a href="{{'/api_uploads/terminal_batch/' . $batch->merchant_code . '/' . $batch->file_authorize}}" download>
                                <b>{{$batch->file_authorize}}</b>
                            </a>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="terminalId" class="col-sm-5 control-label">File terminal <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            <a href="{{'/api_uploads/terminal_batch/' . $batch->merchant_code . '/' . $batch->file_terminals}}" download>
                                <b>{{$batch->file_terminals}}</b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if($batch->status < \App\MpTerminalBatch::STATUS_PRECHECK_ERROR)
    <div class="widget-box transparent">
        <div class="widget-header widget-header-flat">
            <h4 class="widget-title lighter">Kêt quả kiểm tra</h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="box row">
                    <div class="red">
                        Hệ thống đang xử lý, vui lòng kiểm tra sau
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($batch->status == \App\MpTerminalBatch::STATUS_PRECHECK_ERROR && ($batch->file_result == "" || $batch->file_result == null))
    <div class="widget-box transparent">
        <div class="widget-header widget-header-flat">
            <h4 class="widget-title lighter">Kêt quả kiểm tra</h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="box row">
                    <div class="red">
                        Lỗi format file
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($batch->status == \App\MpTerminalBatch::STATUS_PRECHECK_ERROR && $batch->file_result)
    <div class="widget-box transparent">
        <div class="widget-header widget-header-flat">
            <h4 class="widget-title lighter">Kêt quả kiểm tra</h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="box row">
                    <div class="red">
                        Tổng số terminal lỗi: {{ $batch->getErrorTerminals() }} / {{ $batch->total_rows }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($batch->status > \App\MpTerminalBatch::STATUS_WAIT_MMS)
    <div class="widget-box transparent">
        <div class="widget-header widget-header-flat">
            <h4 class="widget-title lighter">Kêt quả kiểm tra</h4>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="box row">
                    <div class="{{ $batch->getErrorTerminals() ? "red" : "blue" }}">
                        Tổng số terminal lỗi: {{ $batch->getErrorTerminals() }} / {{ $batch->total_rows }}
                    </div>
                </div>
                <div class="widget-main">
                    <div class="box row">
                        @include('mp_terminal_batches.list_terminals_table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<div class="text-center">
    <a href="{{ route('terminalBatches.index') }}" class="btn btn-default">Quay lại</a>

    @if($batch->status == \App\MpTerminalBatch::STATUS_PRECHECK_ERROR && $batch->file_result != "" && $batch->file_result != null)
        <a class="btn btn-danger" href="{{'/api_uploads/terminal_batch/' . $batch->merchant_code . '/' . $batch->file_result}}" download>Tải file lỗi</a>
    @elseif ($batch->status == \App\MpTerminalBatch::STATUS_MMS_ERROR)
        <a class="btn btn-danger" href="{{ route('terminalBatches.reopenForEditTerminals', ['id' => $batch->id]) }}">Mở sửa terminals</a>
    @elseif ($batch->status == \App\MpTerminalBatch::STATUS_WAIT_SALE_APPROVE)
        <button class="btn btn-danger" data-toggle="modal" data-target="#rejectModal">Từ chối</button>
        <a class="btn btn-success" href="{{ route('terminalBatches.approve', ['id' => $batch->id]) }}">Duyệt</a>
    @elseif ($batch->status == \App\MpTerminalBatch::STATUS_WAIT_CONTROL_APPROVE)
        <button class="btn btn-danger" data-toggle="modal" data-target="#rejectModal">Từ chối</button>
        <a class="btn btn-success" href="{{ route('terminalBatches.approve', ['id' => $batch->id]) }}">Duyệt</a>
    @endif
</div>

<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="rejectTerminalForm" method="post" action="{{ route('terminalBatches.reject', ['id' => $batch->id]) }}">
                @csrf
                <div class="modal-header">
                    <span class="modal-title" id="exampleModalLabel">Từ chối duyệt Terminal theo lô</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p class="error error-reject-reason"></p>
                        <input class="form-check-input" type="radio" name="reject_reason" id="reject_reason_1"
                               value="Thông tin không đúng">
                        <label class="form-check-label" for="reject_reason_1">
                            Thông tin không đúng
                        </label>
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="radio" name="reject_reason" id="reject_reason_2"
                               value="Thiếu thông tin">
                        <label class="form-check-label" for="reject_reason_2">
                            Thiếu thông tin
                        </label>
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="radio" name="reject_reason" id="reject_reason_3"
                               value="Sản phẩm phạm pháp">
                        <label class="form-check-label" for="reject_reason_3">
                            Sản phẩm phạm pháp
                        </label>
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="radio" name="reject_reason" id="reject_reason_4"
                               value="Sản phẩm không đủ điều kiện đăng ký">
                        <label class="form-check-label" for="reject_reason_4">
                            Sản phẩm không đủ điều kiện đăng ký
                        </label>
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="radio" name="reject_reason" id="reject_reason_5" value="0">
                        <label class="form-check-label" for="reject_reason_5">
                            Khác
                        </label>
                    </div>
                    <div class="form-group">
                        <label><b>Vui lòng nhập lý do từ chối duyệt hồ sơ (Trường hợp lý do "Khác")</b></label>
                        <textarea name="reason" maxlength="255" cols="2" class="form-control" placeholder="Vui lòng nhập lý do từ chối duyệt Terminal"></textarea>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-xs-6">
                            <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Bỏ qua</button>
                        </div>
                        <div class="col-xs-6 align-right">
                            <button id="reject-terminal-batch" name="reject" value="1" type="button" class="m-0 btn btn-danger">Xác nhận từ chối</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
