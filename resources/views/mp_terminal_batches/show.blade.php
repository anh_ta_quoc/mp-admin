@extends('layouts_mms.app')

@section('content')
    @include('flash::message')

    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('coreui-templates::common.errors')
            @include('layouts_mms.page_header', ['header' => 'Chi tiết Terminal theo lô'])
            @include('mp_terminal_batches.show_fields')
        </div>
    </div>
@endsection

@section('scripts')
    @include('layouts_mms.datatables_js')
    {!! $dataTable->scripts() !!}

    <script type="text/javascript">
        $("input[name='reject_reason']").on('change', function () {
            $('.error-reject-reason').html('')
        });

        $('#reject-terminal-batch').on('click', function () {
            var reject_type = $("input[name='reject_reason']:checked").val();
            if (reject_type) {
                if (reject_type == 0 && $("textarea[name='reason']").val().length == 0) {
                    $('.error-reject-reason').html('Vui lòng nhập lý do từ chối duyệt Terminal')
                }
                else {
                    $('.error-reject-reason').html('');
                    $("#rejectTerminalForm").submit()
                }
            }
            else {
                $('.error-reject-reason').html('Vui lòng chọn 1 lý do')
            }
        })
    </script>
@endsection
