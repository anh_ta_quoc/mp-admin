<div class="breadcrumbs" id="breadcrumbs">
    @if (count($breadcrumbs))
        <ol class="breadcrumb">
            @foreach ($breadcrumbs as $breadcrumb)
                @if ($breadcrumb->url && !$loop->last)
                    <li class="breadcrumb-item">{!! property_exists($breadcrumb, 'icon') ? $breadcrumb->icon : '' !!}<a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                @else
                    <li class="breadcrumb-item active">{!! property_exists($breadcrumb, 'icon') ? $breadcrumb->icon : '' !!}{{ $breadcrumb->title }}</li>
                @endif
            @endforeach
        </ol>
    @endif
</div>