<div class="col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
    <div class="row">
        <div class="form-group col-xs-12">
            {!! Form::label('receiver', 'Người nhận', ['class' => 'require']) !!}
            <select class="form-control select2-ajax" id="receiver" name="receivers[]" multiple="multiple">
            </select>
            <div class="error hidden" id="error-receivers">
            </div>
        </div>
        <div class="form-group col-xs-12 col-lg-12">
            {!! Form::label('content', 'Nội dung', ['class' => 'require']) !!}
            {!! Form::textarea('content', null, ['class' => 'form-control msg-body', 'rows' => 3, 'maxlength' => 250, 'placeholder' => 'Nhập nội dung banner']) !!}
            <div class="error hidden" id="error-body">
            </div>
        </div>
        <div class="form-group col-xs-12">
            <div style="display: inline-block; float: left;">
                <input class="tgl tgl-ios" id="cb2" name="status" type="checkbox"/>
                <label class="tgl-btn" for="cb2"></label>
            </div>
            <span style="margin-left: 5px; margin-top: 5px; line-height: 25px;">Bật/tắt banner</span>
        </div>

        <!-- Submit Field -->
        <div class="form-group col-sm-12 center m-t-10">
            {!! Form::submit('Lưu', ['class' => 'btn btn-primary btn-create-banner']) !!}
            <a href="{{ route('banners.index') }}" class="btn btn-secondary">Hủy</a>
        </div>
    </div>
</div>

@section('css')
{{--    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />--}}
    <link rel="stylesheet" href="{{asset('css/bootstrap-timepicker.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endsection

@section('scripts')
    <script type="text/javascript">
        $('.btn-create-banner').on('click', function () {
            var has_error_receiver = false;
            // check receiver
            if ($('#receiver').val().length === 0) {
                has_error_receiver = true;
                $('#error-receivers').html('Vui lòng chọn ít nhất 1 người nhận').removeClass('hidden');
            }
            else {
                if ($('#receiver').val().includes('all') && $('#receiver').val().length > 1) {
                    has_error_receiver = true;
                    $('#error-receivers').html('Chọn người nhận không hợp lệ, không được bao gồm tất cả và người nhận ' +
                        'khác').removeClass('hidden');
                }
                else {
                    has_error_receiver = false;
                    $('#error-receivers').html('').addClass('hidden');
                }
            }
            // check body
            var has_error_body = false;
            if ($('.msg-body').val().trim() === '') {
                has_error_body = true;
                $('#error-body').html('Vui lòng nhập nội dung').removeClass('hidden');
            }
            else {
                has_error_body = false;
                $('#error-body').html('').addClass('hidden');
            }
            if (has_error_receiver || has_error_body) {
                return false;
            }
        })
        $(document).ready(function () {
            $('.select2-ajax').select2({
                placeholder: "-- Chọn merchant --",
                minimumInputLength: 2,
                ajax: {
                    url: '/terminals/ajaxListMerchantWithAll',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@endsection
