@extends('layouts_mms.app', ['new_select2' => true])

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Tạo banner mới', 'buttons' => [['title' => 'Quay lại', 'link' => route('banners.index'), 'icon' => 'fa fa-arrow-left']]])
    <div class="row">
        {!! Form::open(['route' => 'banners.store']) !!}

        @include('banners.fields')

        {!! Form::close() !!}
    </div>
@endsection
