{!! Form::open(['route' => ['banners.destroy', $id],
'id' => 'delete_form_'.$id,
'method' => 'delete',
'onsubmit' => 'return confirm("Bạn chắc chắn thực hiện thao tác?")']) !!}
<div class="hidden-sm hidden-xs action-buttons">
    <a class="green" href="{{ route('banners.edit', $id) }}">
        <i class="ace-icon fa fa-pencil bigger-130"></i>
    </a>
</div>
{!! Form::close() !!}



