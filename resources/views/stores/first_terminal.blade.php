<table class="table table-bordered">
    <thead>
    <tr>
        <th>STT</th>
        <th>Terminal ID</th>
        <th>Tên Terminal</th>
        <th>Tên liên hệ</th>
        <th>SĐT liên hệ</th>
        <th>Tài khoản thụ hưởng</th>
        <th>Ngân hàng</th>
        <th>Chi nhánh</th>
        <th>Chủ tài khoản</th>
        <th>CTT</th>
        <th>Ngày đăng ký</th>
        <th>Ngày duyệt/từ chối</th>
        <th>Trạng thái</th>
        <th>Thao tác</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>{{ $store->terminal_id }}</td>
        <td>{{ $store->terminal_name }}</td>
        <td>{{ $store->terminal_contact_name }}</td>
        <td>{{ $store->terminal_contact_phone }}</td>
        <td>{{ $store->bank_number }}</td>
        <td>{{ $store->bank_brand }}</td>
        <td>{{ $store->bank_branch }}</td>
        <td>{{ $store->bank_account }}</td>
        <td class="text-center">{!! $store->terminal_register_vnpayment ?
                                '<input type="checkbox" checked="checked" disabled="disabled"></input>' :
                                '<input type="checkbox" disabled="disabled"></input>'
                                !!}
        </td>
        <td>{{ date_format(new \DateTime($store->created_at), 'd/m/Y H:i:s') }}</td>
        <td>
            @if(!in_array($store->status, [
                \App\Libraries\BaseFunction::STATUS_DRAFT,
                \App\Libraries\BaseFunction::STATUS_INIT,
                \App\Libraries\BaseFunction::STATUS_WAIT_FOR_ACTION
            ]))
                {{ date_format(new \DateTime($store->updated_at), 'd/m/Y H:i:s') }}
            @endif
        </td>
        <td>
            @if($store->status == 0 && !$store->merchant_file_contract)
                <label class="{{ \App\Libraries\BaseFunction::getStoreStatusClass()[99] }}">
                    {{ \App\Libraries\BaseFunction::getStoreStatus()[99] }}
                </label>
            @else
                <label class="{{ \App\Libraries\BaseFunction::getStoreStatusClass()[$store->status] }}">
                    {{ \App\Libraries\BaseFunction::getStoreStatus()[$store->status] }}
                </label>
            @endif
        </td>
        <td>
            <a class="blue" href="{{ route('terminals.showFirstTerminal', ['id' => $store->id]) }}" target="_blank">
                <i class="ace-icon fa fa-search-plus bigger-130"></i>
            </a>
        </td>
    </tr>
    </tbody>
</table>
