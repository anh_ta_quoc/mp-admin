<div class="hidden-sm hidden-xs action-buttons">
    <a class="blue" href="{{ route('stores.show', $id) }}">
        <i class="ace-icon fa fa-search-plus bigger-130"></i>
    </a>

    @php
        $displayLockButton = false;
        if (Auth::user()->email === 'devmp@gmail.com' || Auth::user()->hasrole('Kinh doanh')) {
            if ($mms_status >= \App\Libraries\BaseFunction::STATUS_MMS_CREATED && $status != \App\Libraries\BaseFunction::STATUS_LOCK) {
                if ($src) {
                    if ($src == "SYNC_MMS" && in_array($status, [\App\Libraries\BaseFunction::STATUS_CONTROL_ACCEPTED])) {
                        $displayLockButton = true;
                    }
                    elseif (in_array($src, ["MR", "SAPO"]) && !in_array($status, [\App\Libraries\BaseFunction::STATUS_SALE_REJECTED])) {
                        $displayLockButton = true;
                    }
                }
                else {
                    if (in_array($status, [\App\Libraries\BaseFunction::STATUS_CONTROL_ACCEPTED])) {
                        $displayLockButton = true;
                    }
                }
            }
        }
    @endphp


    @if ($displayLockButton)
        <span class="red" style="cursor: pointer;" onclick="handleLockMerchant({{ $id }})">
            <i class="ace-icon fa fa-lock bigger-130"></i>
        </span>
    @endif
    @if ((Auth::user()->email === 'devmp@gmail.com' || Auth::user()->hasrole('Kinh doanh')) &&
        $status == \App\Libraries\BaseFunction::STATUS_LOCK)
        <span class="green" style="cursor: pointer;" onclick="handleUnLockMerchant({{ $id }})">
            <i class="ace-icon fa fa-unlock bigger-130"></i>
        </span>
    @endif
</div>

