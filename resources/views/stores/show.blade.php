@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Xem Thông tin Merchant', 'buttons' => [['title' => 'Quay lại', 'link' => route('stores.index'), 'icon' => 'fa fa-arrow-left']]])
    @include('flash::message')
    @include('coreui-templates::common.errors')
    <div class="row">
        @include('stores.show_fields')
    </div>
    <!-- Modal -->
    @if (($store->status === 0 && !$store->merchant_file_contract) || $store->status === 5)
        <div class="modal fade" id="uploadContractModal" tabindex="-1" role="dialog" aria-labelledby="uploadContractModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="uploadContractForm" method="post" action="{{ route('stores.updateContract', ['id' => $store->id]) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">Cập nhật hợp đồng</span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="steps">
                                <p class="error" id="error-msg" style="text-align: center;"></p>
                                <div class="step" data-step="1">
                                    <div class="step-header">
                                        <div class="step_header">Upload hợp đồng</div>
                                        <div class="subheader">File có định dạng pdf,png,jpg,rar, dung lượng file tối đa là 10MB</div>
                                        <div class="step-content">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label class="input-group-btn">
                                                        <span class="btn btn-primary">
                                                            Chọn file&hellip;
                                                            <input name="merchant_file_contract" id="merchant_file_contract" type="file" style="display: none;" accept="application/pdf,image/x-png,.jpg,.rar">
                                                        </span>
                                                    </label>
                                                    <input type="text" id="merchant_file_contract__name_field" class="form-control" disabled>
                                                </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="step" data-step="2">
                                    <div class="step-header">
                                        <div class="step_header">Thông tin hợp đồng</div>
                                        {{--<div class="subheader">The content is a little bigger!</div>--}}
                                        <div class="step-content">
                                            <div class="form-group row">
                                                {!! Form::label('contract_number', 'Số hợp đồng:', ['class' => 'require col-xs-5']) !!}
                                                <div class="col-xs-7">
                                                    {!! Form::text('contract_number', $store->contract_number, ['class' => 'form-control', 'id' => 'contract_number', 'maxlength' => 255]) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('contract_code', 'Tên viết tắt của hợp đồng:', ['class' => 'require col-xs-5']) !!}
                                                <div class="col-xs-7">
                                                    {!! Form::text('contract_code', $store->contract_code, ['class' => 'form-control', 'id' => 'contract_code', 'maxlength' => 255]) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('contract_date', 'Ngày ký hợp đồng:', ['class' => 'require col-xs-5']) !!}
                                                <div class="col-xs-7">
                                                    <div class="input-group date">
                                                        <input type="hidden" value="{{ $store->contract_date ?? '' }}" id="old_contract_date"/>
                                                        <input type="text" class="form-control dp-contract"
                                                               value="{{ $store->contract_date }}"
                                                               name="contract_date" id="contract_date" readonly="readonly"
                                                               style="background-color: white !important; color: inherit; cursor: pointer !important;">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="step" data-step="3">
                                    <div class="step-header">
                                        <div class="step_header">Nhân viên phụ trách</div>
                                        {{--<div class="subheader">Last but not the least!</div>--}}
                                        <div class="step-content">
                                            <div class="form-group row">
                                                {!! Form::label('department_id', 'Phòng ban:', ['class' => 'col-xs-5']) !!}
                                                <div class="col-xs-7">
                                                    {!! Form::select('department_id', $departments->pluck('department_name', 'department_id'), [],
                                                    [
                                                        'class' => 'form-control',
                                                        'id' => 'department',
                                                        'placeholder' => 'Chọn'
                                                    ]) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('staff_id', 'Nhân viên:', ['class' => 'col-xs-5']) !!}
                                                <div class="col-xs-7">
                                                    {!! Form::select('staff_id', [], [], ['class' => 'form-control', 'id' => 'staff', 'placeholder' => 'Chọn']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="center" style="margin: 10px 0">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                <button type="button" id="update-store-info-contract" class="btn btn-primary">Cập nhật hợp đồng</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
    @if ($store->merchant_file_contract && $store->status === \App\Libraries\BaseFunction::STATUS_WAIT_FOR_ACTION)
        <div class="modal fade" id="reUploadContractModal" tabindex="-1" role="dialog" aria-labelledby="reUploadContractModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="reUploadContractForm" method="post" action="{{ route('stores.reUploadContract', ['id' => $store->id]) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">Yêu cầu upload lại hợp đồng</span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea name="note" cols="2" maxlength="255" class="form-control" placeholder="Nhập lý do yêu cầu upload lại hợp đồng"></textarea>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-xs-6">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-6 align-right">
                                    <button onclick="this.disabled=true;this.form.submit();"
                                            type="submit" class="m-0 btn btn-primary">Yêu cầu upload lại hợp đồng</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
    @if ($store->merchant_file_contract && $store->status === \App\Libraries\BaseFunction::STATUS_WAIT_FOR_ACTION)
        <div class="modal fade" id="saleRejectModal" tabindex="-1" role="dialog" aria-labelledby="saleRejectModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="rejectContractForm" method="post" action="{{ route('stores.saleAction', ['id' => $store->id]) }}">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">Từ chối duyệt hoặc mở lại hồ sơ</span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea name="sale-note" maxlength="255" cols="2" class="form-control"
                                          required
                                          placeholder="Nhập lý do từ chối hoặc mở lại"></textarea>
                            </div>
                            <input type="hidden" value="sale-reject-contract" name="sale-action-type"/>
                            <div class="row m-t-20">
                                <div class="col-xs-4">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-4 align-center">
                                    <button onclick="this.disabled=true;this.form.submit();" type="submit" class="m-0 btn btn-danger">Xác nhận từ chối</button>
                                </div>
                                <div class="col-xs-4 align-right">
                                    <button name="reopen" value="1" type="submit" class="m-0 btn btn-info">Mở lại hồ sơ</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
    @if ($store->merchant_file_contract && in_array($store->status, [\App\Libraries\BaseFunction::STATUS_SALE_REJECTED, \App\Libraries\BaseFunction::STATUS_CONTROL_REJECTED]))
        <div class="modal fade" id="saleReopenDocumentModal" tabindex="-1" role="dialog" aria-labelledby="saleReopenDocumentModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="rejectContractForm" method="post" action="{{ route('stores.saleAction', ['id' => $store->id]) }}">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">Mở lại hồ sơ</span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea name="sale-note" maxlength="255" cols="2" class="form-control"
                                          required
                                          placeholder="Nhập lý do mở lại hồ sơ"></textarea>
                            </div>
                            <input type="hidden" value="sale-reopen-document" name="sale-action-type"/>
                            <div class="row m-t-20">
                                <div class="col-xs-4">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-4 align-center">

                                </div>
                                <div class="col-xs-4 align-right">
                                    <button id="sale-reopen-document" onclick="this.disabled=true;this.form.submit();"  type="submit" class="m-0 btn btn-yellow">Mở lại hồ sơ</button>-
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
    @if ($store->merchant_file_contract && $store->status === \App\Libraries\BaseFunction::STATUS_SALE_ACCEPTED)
        <div class="modal fade" id="controlRejectModal" tabindex="-1" role="dialog" aria-labelledby="controlRejectModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="controlRejectForm" method="post" action="{{ route('stores.controlAction', ['id' => $store->id]) }}">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">Trả lại hồ sơ</span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea name="control-note" maxlength="255" cols="2" class="form-control" placeholder="Nhập lý do" required></textarea>
                            </div>
                            <input type="hidden" value="control-reject-contract" name="control-action-type"/>
                            <div class="row m-t-20">
                                <div class="col-xs-4">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-4 align-center">

                                </div>
                                <div class="col-xs-4 align-right">
                                    <button onclick="this.disabled=true;this.form.submit();" type="submit" class="m-0 btn btn-danger">Xác nhận</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection

@include('stores.script.script_show')
