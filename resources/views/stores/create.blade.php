@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Thêm mới merchant', 'buttons' => [['title' => 'Quay lại', 'link' => route('stores.index'), 'icon' => 'fa fa-arrow-left']]])
    {{--@include('coreui-templates::common.errors')--}}
    <div class="row">
        {!! Form::open(['route' => 'stores.store' ,'files' =>true,'enctype'=>'multipart/form-data']) !!}

        @include('stores.fields')

        {!! Form::close() !!}
    </div>
@endsection

@include('stores.script.script_create')
