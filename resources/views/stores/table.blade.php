@section('css')
    @include('layouts_mms.datatables_css')
    @include('layouts_mms.daterange_picker_css')
@endsection

{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}

@section('scripts')
    @include('layouts_mms.daterange_picker_js', ['init_id' => ['date_range_search']])
    @include('layouts_mms.datatables_js')
    {!! $dataTable->scripts() !!}
    <script>
        var queryTypingTimer;
        var doneTypingInterval = 1000;
        var $input = $('#filter_query');
        $input.on('keyup', function () {
            clearTimeout(queryTypingTimer);
            queryTypingTimer = setTimeout(doneFilterTyping, doneTypingInterval);
        });
        $input.on('keydown', function () {
            clearTimeout(queryTypingTimer);
        });
        function doneFilterTyping () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                // window.LaravelDataTables['dataTableBuilder'].search($input.val()).draw();
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        }
        $('#filter_status').on('change', function () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        });
        $('#filter_business_type').on('change', function () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        });
        $('#filter_src').on('change', function () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        });
    </script>
    <script type="text/javascript">
            function handleLockMerchant(storeId) {
                if (storeId && $('#storeId').length > 0) {
                    $('#storeId').val(storeId);
                    $('.error-lock-merchant').html("");
                    $('#confirmLockMerchantModal').modal('show');
                }
            };

            function handleUnLockMerchant(storeId) {
                if (storeId && $('#storeIdToUnlock').length > 0) {
                    $('#storeIdToUnlock').val(storeId);
                    $('#confirmUnLockMerchantModal').modal('show');
                }
            }

            $('.reason-lock-merchant').on('change', function () {
                $('.error-lock-merchant').html("");
            })

            $('.accept-lock-merchant').on('click', function (e) {
                $(this).attr('disabled', true);
                if($('.reason-lock-merchant').val().trim().length === 0) {
                    $(this).attr('disabled', false);
                    $('.error-lock-merchant').html("Vui lòng nhập lý do");
                    e.preventDefault();
                }
                else {
                    $('#lockMerchant').submit();
                }
            })

            $('.reason-unlock-merchant').on('change', function () {
                $('.error-unlock-merchant').html("");
            })

            $('.accept-unlock-merchant').on('click', function (e) {
                $(this).attr('disabled', true);
                if($('.reason-unlock-merchant').val().trim().length === 0) {
                    $(this).attr('disabled', false);
                    $('.error-unlock-merchant').html("Vui lòng nhập lý do");
                    e.preventDefault();
                }
                else {
                    $('#unlockMerchant').submit();
                }
            })
    </script>
    <script type="text/javascript">
        $('.hsub').on('click', function (e) {
            var linkRedirect = $("a",this).attr('href');
            e.preventDefault();
            $.ajax({
                url: "{{ url('/api/clear_filter') }}?module=store_info",
                method: 'GET',
                success: function(data) {
                    if (data) {
                        try {
                            var resStatus = JSON.parse(data);
                            if (resStatus.hasOwnProperty('status') && resStatus.status === 'OK') {
                                window.location.href = linkRedirect
                            }
                        }
                        catch (e) {

                        }
                    }
                }
            });
        })
    </script>
@endsection
