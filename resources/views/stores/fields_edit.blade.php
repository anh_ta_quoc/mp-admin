@csrf
<div class="col-sm-12">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#tab_hosomerchant">Hồ sơ Merchant</a></li>
        <li><a data-toggle="tab" href="#tab_danhsachterminal">Danh sách Terminal</a></li>
    </ul>
    <div class="tab-content" style="padding: 20px 0">
        <div id="tab_hosomerchant" class="tab-pane fade in active">
            <div class="widget-box transparent">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title lighter">Thông tin Merchant</h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body" style="display: block;">
                    <div class="widget-main">
                        @if(in_array($store->src, ['MR', 'SAPO']) && $store->status != \App\Libraries\BaseFunction::STATUS_CONTROL_ACCEPTED)
                        <div class="box row" style="margin-left: 1px; color: red; font-style: italic;">
                            <h4>Lưu ý: kiểm tra lại thông tin MCC VNPAY tại
                                <a href="{{ route('terminals.showFirstTerminal', ['id' => $store->id]) }}">đây</a>
                            </h4>
                            <br/>
                        </div>
                        @endif
                        <div class="box row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <input type="hidden" name="id" value="{{$store->id}}">
                                    {!! Form::label('merchant_code', 'Merchant ID:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_code', old('merchant_code', $store->merchant_code), ['class' => 'form-control', 'maxlength' => 20, 'disabled' => true]) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_name', 'Tên Merchant:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_name', old('merchant_name', $store->merchant_name), ['class' => 'form-control', 'maxlength' => 100]) !!}
                                        @if($errors->has('merchant_name'))
                                            <div class="error">{{ $errors->first('merchant_name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_brand', 'Tên viết tắt:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_brand', old('merchant_brand', $store->merchant_brand), ['class' => 'form-control', 'maxlength' => 25]) !!}
                                        @if($errors->has('merchant_brand'))
                                            <div class="error">{{ $errors->first('merchant_brand') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_code', 'MST/CMND:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_code', old('merchant_code', $store->merchant_code), ['class' => 'form-control', 'maxlength' => 20, 'readonly' => $store->mms_status >= \App\Libraries\BaseFunction::STATUS_MMS_CREATED]) !!}
                                        @if($errors->has('merchant_code'))
                                            <div class="error">{{ $errors->first('merchant_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_type_business', 'Loại hình doanh nghiệp:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::select('merchant_type_business', $merchantTypeBusiness,[old('merchant_type_business', $store->merchant_type_business)], ['class' => 'form-control select2']) !!}
                                        @if($errors->has('merchant_type_business'))
                                            <div class="error">{{ $errors->first('merchant_type_business') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_type', 'Loại hình kinh doanh:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::select('merchant_type', $merchantTypeOptions, [old('merchant_type', $store->merchant_type)], ['class' => 'form-control select2']) !!}
                                        @if($errors->has('merchant_type'))
                                            <div class="error">{{ $errors->first('merchant_type') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label(null, 'Master Merchant:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>CTCP Giải pháp Thanh toán Việt Nam VNPAY</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('created_at', 'Ngày đăng ký:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ date('d-m-Y H:i:s', strtotime($store->created_at)) }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('updated_at', 'Ngày duyêt/từ chối:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        @if(in_array($store->status, [
                                           \App\Libraries\BaseFunction::STATUS_SALE_ACCEPTED,
                                           \App\Libraries\BaseFunction::STATUS_CONTROL_ACCEPTED,
                                           \App\Libraries\BaseFunction::STATUS_SALE_REJECTED,
                                           \App\Libraries\BaseFunction::STATUS_CONTROL_REJECTED,
                                           \App\Libraries\BaseFunction::STATUS_SALE_REOPEN_TO_EDIT,
                                           \App\Libraries\BaseFunction::STATUS_CONTROL_REOPEN_TO_EDIT
                                        ]))
                                            <label><b>{{ date('d-m-Y H:i:s', strtotime($store->updated_at)) }}</b></label>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('issue_invoice', 'Xuất hóa đơn:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label class="middle">
                                            {!! Form::checkbox('issue_invoice', 1, old('issue_invoice', $store->issue_invoice == 1), ['class' => 'ace']) !!}
                                            <span class="lbl"></span>
                                        </label>
                                        @if($errors->has('issue_invoice'))
                                            <div class="error">{{ $errors->first('issue_invoice') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    {!! Form::label('merchant_address', 'Địa chỉ đăng ký kinh doanh:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_address', old('merchant_address', $store->merchant_address), ['class' => 'form-control', 'rows' => 2, 'maxlength' => 150]) !!}
                                        @if($errors->has('merchant_address'))
                                            <div class="error">{{ $errors->first('merchant_address') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_business_address', 'Địa chỉ kinh doanh:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_business_address', old('merchant_business_address', $store->merchant_business_address), ['class' => 'form-control', 'rows' => 2, 'maxlength' => 150]) !!}
                                        @if($errors->has('merchant_business_address'))
                                            <div class="error">{{ $errors->first('merchant_business_address') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_province_code', 'Tỉnh thành doanh nghiệp:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::select('merchant_province_code', $provinceOptions, [old('merchant_province_code', $store->merchant_province_code)], ['class' => 'form-control select2','id'=>'merchant_provinces']) !!}
                                        @if($errors->has('merchant_province_code'))
                                            <div class="error">{{ $errors->first('merchant_province_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_district_code', 'Quận/huyện doanh nghiệp:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::select('merchant_district_code',$districtOptions ,[], ['class' => 'form-control select2','id'=>'merchant_districts']) !!}
                                        @if($errors->has('merchant_district_code'))
                                            <div class="error">{{ $errors->first('merchant_district_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_ward_code', 'Phường/xã doanh nghiệp:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::select('merchant_ward_code', $wardsOptions, [], ['class' => 'form-control select2','id'=>'merchant_wards']) !!}
                                        @if($errors->has('merchant_ward_code'))
                                            <div class="error">{{ $errors->first('merchant_ward_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_website', 'Website doanh nghiệp:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_website', old('merchant_website', $store->merchant_website), ['class' => 'form-control', 'maxlength' => 100]) !!}
                                        @if($errors->has('merchant_website'))
                                            <div class="error">{{ $errors->first('merchant_website') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('contract_number', 'Số hợp đồng:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('contract_number', old('contract_number', $store->contract_number), ['class' => 'form-control', 'maxlength' => 255]) !!}
                                        @if($errors->has('contract_number'))
                                            <div class="error">{{ $errors->first('contract_number') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('contract_code', 'Tên viết tắt hợp đồng:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('contract_code', old('contract_code', $store->contract_code), ['class' => 'form-control', 'maxlength' => 255]) !!}
                                        @if($errors->has('contract_code'))
                                            <div class="error">{{ $errors->first('contract_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('contract_date', 'Ngày ký hợp đồng:', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('contract_date', old('contract_date', $store->contract_date), ['class' => 'form-control date-picker', 'data-date-format' => "dd/mm/yyyy"]) !!}
                                        @if($errors->has('contract_date'))
                                            <div class="error">{{ $errors->first('contract_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('status', 'Trạng thái hồ sơ:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        @if($store->merchant_file_contract || $store->src == 'SYNC_MMS')
                                            <span class="{{ \App\Libraries\BaseFunction::getStoreStatusClass()[$store->status] }}">
                                                {{ \App\Libraries\BaseFunction::getStoreStatus()[$store->status] }}
                                            </span>
                                        @else
                                                @if ($store->status < 0)
                                                <span class="label label-warning">
                                                    Chờ hoàn thiện hồ sơ
                                                </span>
                                                @else
                                                <span class="label label-primary">
                                                    Chờ cập nhật Hợp đồng
                                                </span>
                                                @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box transparent">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title lighter">Tài liệu đính kèm</h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="box row">
                            <div class="col-sm-12">
                                <div class="row form-group">
                                    <label class="col-sm-2 control-label require">Giấy phép đăng ký kinh doanh</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary">Chọn file&hellip;
                                                    {!! Form::file('merchant_file_business_cert', ['class' => 'form-control', "accept" => "application/pdf,image/x-png,.jpg,.rar", 'style' => 'display: none;']) !!}
                                                </span>
                                            </label>
                                            <input type="text" id="merchant_file_business_cert__name_field" class="form-control" disabled>
                                        </div>
                                        @if($errors->has('merchant_file_business_cert'))
                                            <div class="error">{{ $errors->first('merchant_file_business_cert') }}</div>
                                        @endif
                                        <div class="help-block"></div>
                                    </div>
                                    @if ($store->merchant_file_business_cert)
                                        <div class="col-sm-4">
                                            @if(strpos($store->merchant_file_business_cert, 'http') > -1)
                                                <a href="{{ $store->merchant_file_business_cert }}" target="_blank">
                                                    <b>Giấy phép đăng ký kinh doanh</b>
                                                </a>
                                            @else
                                                <a href="{{ url('/api_uploads').'/'.$store->merchant_file_business_cert }}" target="_blank">
                                                    <b>Giấy phép đăng ký kinh doanh</b>
                                                </a>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-2 control-label require">Hợp đồng liên kết VNPAY</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary">Chọn file&hellip;
                                                    {!! Form::file('merchant_file_contract', ['class' => 'form-control', "accept" => "application/pdf,image/x-png,.jpg,.rar", 'style' => 'display: none;']) !!}
                                                </span>
                                            </label>
                                            <input type="text" id="merchant_file_contract__name_field" class="form-control" disabled>
                                        </div>
                                        @if($errors->has('merchant_file_contract'))
                                            <div class="error">{{ $errors->first('merchant_file_contract') }}</div>
                                        @endif
                                        <div class="help-block"></div>
                                    </div>
                                    @if ($store->merchant_file_contract)
                                        <div class="col-sm-4">
                                            @if(strpos($store->merchant_file_contract, 'http') > -1)
                                                <a href="{{ $store->merchant_file_contract }}" target="_blank">
                                                    <b>Hợp đồng liên kết VNPAY</b>
                                                </a>
                                            @else
                                                <a href="{{ url('/api_uploads').'/'.$store->merchant_file_contract }}" target="_blank">
                                                    <b>Hợp đồng liên kết VNPAY</b>
                                                </a>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-2 control-label">Giấy phép đăng ký tên miền</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary">Chọn file&hellip;
                                                    {!! Form::file('merchant_file_domain_cert', ['class' => 'form-control', "accept" => "application/pdf,image/x-png,.jpg,.rar", 'style' => 'display: none;']) !!}
                                                </span>
                                            </label>
                                            <input type="text" id="merchant_file_domain_cert__name_field" class="form-control" disabled>
                                        </div>
                                        @if($errors->has('merchant_file_domain_cert'))
                                            <div class="error">{{ $errors->first('merchant_file_domain_cert') }}</div>
                                        @endif
                                        <div class="help-block"></div>
                                    </div>
                                    @if ($store->merchant_file_domain_cert)
                                        <div class="col-sm-4">
                                            @if(strpos($store->merchant_file_domain_cert, 'http') > -1)
                                                <a href="{{ $store->merchant_file_domain_cert }}" target="_blank">
                                                    <b>Giấy phép đăng ký tên miền</b>
                                                </a>
                                            @else
                                                <a href="{{ url('/api_uploads').'/'.$store->merchant_file_domain_cert }}" target="_blank">
                                                    <b>Giấy phép đăng ký tên miền</b>
                                                </a>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-2 control-label">GPĐK tài khoản với CQT</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary">Chọn file&hellip;
                                                    {!! Form::file('merchant_file_business_tax_cert', ['class' => 'form-control', "accept" => "application/pdf,image/x-png,.jpg,.rar", 'style' => 'display: none;']) !!}
                                                </span>
                                            </label>
                                            <input type="text" id="merchant_file_business_tax_cert__name_field" class="form-control" disabled>
                                        </div>
                                        @if($errors->has('merchant_file_business_tax_cert'))
                                            <div class="error">{{ $errors->first('merchant_file_business_tax_cert') }}</div>
                                        @endif
                                        <div class="help-block"></div>
                                    </div>
                                    @if ($store->merchant_file_business_tax_cert)
                                        <div class="col-sm-4">
                                            @if(strpos($store->merchant_file_business_tax_cert, 'http') > -1)
                                                <a href="{{ $store->merchant_file_business_tax_cert }}" target="_blank">
                                                    <b>GPĐK tài khoản với CQT</b>
                                                </a>
                                            @else
                                                <a href="{{ url('/api_uploads').'/'.$store->merchant_file_business_tax_cert }}" target="_blank">
                                                    <b>GPĐK tài khoản với CQT</b>
                                                </a>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-2 control-label">CMND/CCCD</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary">Chọn file&hellip;
                                                    {!! Form::file('merchant_file_identify_card', ['class' => 'form-control', "accept" => "application/pdf,image/x-png,.jpg,.rar", 'style' => 'display: none;']) !!}
                                                </span>
                                            </label>
                                            <input type="text" id="merchant_file_identify_card__name_field" class="form-control" disabled>
                                        </div>
                                        @if($errors->has('merchant_file_identify_card'))
                                            <div class="error">{{ $errors->first('merchant_file_identify_card') }}</div>
                                        @endif
                                        <div class="help-block"></div>
                                    </div>
                                    @if ($store->merchant_file_identify_card)
                                        <div class="col-sm-4">
                                            @if(strpos($store->merchant_file_identify_card, 'http') > -1)
                                                <a href="{{ $store->merchant_file_identify_card }}" target="_blank">
                                                    <b>CMND/CCCD</b>
                                                </a>
                                            @else
                                                <a href="{{ url('/api_uploads').'/'.$store->merchant_file_identify_card }}" target="_blank">
                                                    <b>CMND/CCCD</b>
                                                </a>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box transparent">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title lighter">Thông tin người liên hệ</h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body" style="display: block;">
                    <div class="widget-main">
                        <div class="box row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    {!! Form::label('merchant_contact_name', 'Họ và tên', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_contact_name', old('merchant_contact_name', $store->merchant_contact_name), ['class' => 'form-control', 'rows' => 2, 'maxlength' => 50]) !!}
                                        @if($errors->has('merchant_contact_name'))
                                            <div class="error">{{ $errors->first('merchant_contact_name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_contact_phone', 'Số điện thoại', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_contact_phone',
                                        old('merchant_contact_phone', $store->merchant_contact_phone),
                                        [
                                            'class' => 'form-control',
                                            'rows' => 2
                                        ]) !!}
                                        @if($errors->has('merchant_contact_phone'))
                                            <div class="error">{{ $errors->first('merchant_contact_phone') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_contact_email', 'Email', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_contact_email', old('merchant_contact_email', $store->merchant_contact_email), ['class' => 'form-control', 'rows' => 2, 'maxlength' => 100]) !!}
                                        @if($errors->has('merchant_contact_email'))
                                            <div class="error">{{ $errors->first('merchant_contact_email') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box transparent">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title lighter">Tài khoản đăng nhập</h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body" style="display: block;">
                    <div class="widget-main">
                        <div class="box row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    {!! Form::label('merchant_app_user', 'Số điện thoại merchant', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::text('merchant_app_user',
                                        old('merchant_app_user', $store->merchant_app_user),
                                        [
                                            'class' => 'form-control',
                                            'rows' => 2,
                                            'readonly' => true
                                        ]) !!}
                                        @if($errors->has('merchant_app_user'))
                                            <div class="error">{{ $errors->first('merchant_app_user') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box transparent">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title lighter">Bộ phận phát triển</h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body" style="display: block;">
                    <div class="widget-main">
                        <div class="box row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    {!! Form::label('department_id', 'Phòng ban', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::select('department_id', $departmentsOptions,null, ['class' => 'form-control select2','id'=>'departments']) !!}
                                        @if($errors->has('department_id'))
                                            <div class="error">{{ $errors->first('department_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('staff_id', 'Nhân viên', ['class' => 'col-sm-4 control-label require']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::select('staff_id', [['' => '-- Chọn --']], null, ['class' => 'form-control select2','id'=>'staffs']) !!}
                                        @if($errors->has('staff_id'))
                                            <div class="error">{{ $errors->first('staff_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Submit Field -->
            <div class="form-group m-t-10 text-center">
                <a href="{{ route('stores.show', ['id' => $store->id]) }}" class="btn btn-secondary">Quay lại</a>
                @if(in_array($store->src, ['MR', 'SAPO']))
                    @if(in_array($store->status, [\App\Libraries\BaseFunction::STATUS_CONTROL_ACCEPTED]))
                        <button class="btn btn-primary" type="submit">Cập nhật</button>
                    @endif
                @else
                    <button class="btn btn-primary" type="submit">
                        {{ $store->mms_status >= \App\Libraries\BaseFunction::STATUS_MMS_CREATED ? 'Cập nhật' : 'Lưu' }}
                    </button>
                @endif
                @if(!in_array($store->status, [\App\Libraries\BaseFunction::STATUS_CONTROL_ACCEPTED]))
                    <button class="btn btn-success" type="submit" name="send_request" value="1">Gửi duyệt</button>
                @endif
            </div>
        </div>
        <div id="tab_danhsachterminal" class="tab-pane fade in" style="padding: 0 20px;">
            @if ($store->status != \App\Libraries\BaseFunction::STATUS_CONTROL_ACCEPTED)
                @include('stores.first_terminal')
            @else
                @include('stores.list_terminals_table')
            @endif
        </div>
    </div>
</div>
