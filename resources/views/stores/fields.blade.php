<div class="col-sm-12">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#tab_thongtindoanhnghiep">Thông tin doanh nghiệp</a></li>
        <li><a data-toggle="tab" href="#tab_thongtindiemban">Thông tin điểm bán</a></li>
        <li><a data-toggle="tab" href="#tab_thongtinnganhang">Thông tin ngân hàng</a></li>
        <li><a data-toggle="tab" href="#tab_giayphepvahopdong">Tài liệu đính kèm</a></li>
        <li><a data-toggle="tab" href="#tab_thongtinkhac">Thông tin khác</a></li>
    </ul>
    <div class="tab-content">
        <div id="tab_thongtindoanhnghiep" class="tab-pane fade in active">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Merchant Name Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_name', 'Tên doanh nghiệp:', ['class' => 'require']) !!}
                            {!! Form::text('merchant_name', null, ['class' => 'form-control', 'maxlength' => 100]) !!}
                            @if($errors->has('merchant_name'))
                                <div class="error">{{ $errors->first('merchant_name') }}</div>
                            @endif
                        </div>

                        <!-- Merchant Brand Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_brand', 'Tên viết tắt doanh nghiệp:', ['class' => 'require']) !!}
                            {!! Form::text('merchant_brand', null, ['class' => 'form-control', 'maxlength' => 25]) !!}
                            @if($errors->has('merchant_brand'))
                                <div class="error">{{ $errors->first('merchant_brand') }}</div>
                            @endif
                        </div>

                        <!-- Merchant Code Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_code', 'MST/CMTND doanh nghiệp:', ['class' => 'require']) !!}
                            {!! Form::text('merchant_code', null, ['class' => 'form-control', 'maxlength' => 20]) !!}
                            @if($errors->has('merchant_code'))
                                <div class="error">{{ $errors->first('merchant_code') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Merchant Type Business Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_type_business', 'Loại hình doanh nghiệp:', ['class' => 'require']) !!}
                            {!! Form::select('merchant_type_business', $merchantTypeBusiness,null, ['class' => 'form-control select2']) !!}
                            @if($errors->has('merchant_type_business'))
                                <div class="error">{{ $errors->first('merchant_type_business') }}</div>
                            @endif
                        </div>

                        <!-- Merchant Type Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_type', 'Loại hình kinh doanh:', ['class' => 'require']) !!}
                            {!! Form::select('merchant_type', $merchantTypeOptions, [], ['class' => 'form-control select2']) !!}
                            @if($errors->has('merchant_type'))
                                <div class="error">{{ $errors->first('merchant_type') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Merchant Province Code Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_province_code', 'Tỉnh thành doanh nghiệp:', ['class' => 'require']) !!}
                            {!! Form::select('merchant_province_code', $provinceOptions,[], ['class' => 'form-control select2','id'=>'merchant_provinces']) !!}
                            @if($errors->has('merchant_province_code'))
                                <div class="error">{{ $errors->first('merchant_province_code') }}</div>
                            @endif
                        </div>

                        <!-- Merchant District Code Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_district_code', 'Quận/huyện doanh nghiệp:', ['class' => 'require']) !!}
                            {!! Form::select('merchant_district_code',$districtOptions ,[], ['class' => 'form-control select2','id'=>'merchant_districts']) !!}
                            @if($errors->has('merchant_district_code'))
                                <div class="error">{{ $errors->first('merchant_district_code') }}</div>
                            @endif
                        </div>

                        <!-- Merchant Ward Code Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_ward_code', 'Phường/xã doanh nghiệp:', ['class' => 'require']) !!}
                            {!! Form::select('merchant_ward_code', $wardsOptions, [], ['class' => 'form-control select2','id'=>'merchant_wards']) !!}
                            @if($errors->has('merchant_ward_code'))
                                <div class="error">{{ $errors->first('merchant_ward_code') }}</div>
                            @endif
                        </div>

                        <!-- Merchant Address Field -->
                        <div class="form-group col-sm-12 col-lg-12">
                            {!! Form::label('merchant_address', 'Địa chỉ đăng ký kinh doanh:', ['class' => 'require']) !!}
                            {!! Form::textarea('merchant_address', null, [
                                'class' => 'form-control', 'rows' => 2, 'maxlength' => 150
                            ]) !!}
                            @if($errors->has('merchant_address'))
                                <div class="error">{{ $errors->first('merchant_address') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Merchant Contact Name Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_contact_name', 'Tên người đăng ký:', ['class' => 'require']) !!}
                            {!! Form::text('merchant_contact_name', null, ['class' => 'form-control', 'maxlength' => 50]) !!}
                            @if($errors->has('merchant_contact_name'))
                                <div class="error">{{ $errors->first('merchant_contact_name') }}</div>
                            @endif
                        </div>

                        <!-- Merchant Contact Email Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_contact_email', 'Email liên hệ doanh nghiệp:', ['class' => 'require']) !!}
                            {!! Form::text('merchant_contact_email', null, ['class' => 'form-control', 'maxlength' => 100]) !!}
                            @if($errors->has('merchant_contact_email'))
                                <div class="error">{{ $errors->first('merchant_contact_email') }}</div>
                            @endif
                        </div>

                        <!-- Merchant Contact Phone Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_contact_phone', 'Số điện thoại người liên hệ doanh nghiệp:', ['class' => 'require']) !!}
                            {!! Form::text('merchant_contact_phone', null, ['class' => 'form-control', 'maxlength' => 10]) !!}
                            @if($errors->has('merchant_contact_phone'))
                                <div class="error">{{ $errors->first('merchant_contact_phone') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="middle">
                            {!! Form::checkbox('create_merchant_app', 1, false,['class' => 'ace', 'checked' => true, 'id' => 'has-merchant-app-checkbox', 'disabled' => true]) !!}
                            <span class="lbl"> Tạo tài khoản đăng nhập:</span>
                        </label>
                    </div>
                    <div class="row form-group">
                        <label for="phone" class="col-sm-5 control-label">Số điện thoại merchant app <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('merchant_app_user', null, [
                                'class' => 'form-control',
                                'id' => 'merchant-app-user',
                                'maxlength' => 10,
                            ]) !!}
                            @if($errors->has('merchant_app_user'))
                                <div class="error">{{ $errors->first('merchant_app_user') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label class="middle">
                                {!! Form::checkbox('issue_invoice', 1, null, ['class' => 'ace']) !!}
                                <span class="lbl"> Xuất hóa đơn</span>
                            </label>
                            @if($errors->has('issue_invoice'))
                                <div class="error">{{ $errors->first('issue_invoice') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tab_thongtindiemban" class="tab-pane fade">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Terminal Id Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_id', 'Mã điểm bán:', ['class' => 'require']) !!}
                            {!! Form::text('terminal_id', null, ['class' => 'form-control', 'maxlength' => 8]) !!}
                            @if($errors->has('terminal_id'))
                                <div class="error">{{ $errors->first('terminal_id') }}</div>
                            @endif
                        </div>

                        <!-- Terminal Name Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_name', 'Tên điểm bán:', ['class' => 'require']) !!}
                            {!! Form::text('terminal_name', null, ['class' => 'form-control', 'maxlength' => 30]) !!}
                            @if($errors->has('terminal_name'))
                                <div class="error">{{ $errors->first('terminal_name') }}</div>
                            @endif
                        </div>

                        <!-- Terminal Type Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_type', 'Sản phẩm kinh doanh:', ['class' => 'require']) !!}
                            {!! Form::select('terminal_type', $businessProductsOptions, [], ['class' => 'form-control select2']) !!}
                            @if($errors->has('terminal_type'))
                                <div class="error">{{ $errors->first('terminal_type') }}</div>
                            @endif
                        </div>

                        <!-- Terminal Description Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('terminal_description', 'Mô tả cho sản phẩm (nếu sản phẩm kinh doanh là Khác):') !!}
                            {!! Form::textarea('terminal_description', null, ['class' => 'form-control', 'rows' => 2, 'maxlength' => 200]) !!}
                            @if($errors->has('terminal_description'))
                                <div class="error">{{ $errors->first('terminal_description') }}</div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <!-- Terminal Province Code Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_province_code', 'Tỉnh thành điểm bán:', ['class' => 'require']) !!}
                            {!! Form::select('terminal_province_code', $provinceOptions, null, ['class' => 'form-control select2','id'=>'terminal_provinces']) !!}
                            @if($errors->has('terminal_province_code'))
                                <div class="error">{{ $errors->first('terminal_province_code') }}</div>
                            @endif
                        </div>

                        <!-- Terminal District Code Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_district_code', 'Quận/huyện điểm bán:', ['class' => 'require']) !!}
                            {!! Form::select('terminal_district_code',$districtOptions, null, ['class' => 'form-control select2','id'=>'terminal_districts']) !!}
                            @if($errors->has('terminal_district_code'))
                                <div class="error">{{ $errors->first('terminal_district_code') }}</div>
                            @endif
                        </div>

                        <!-- Terminal Ward Code Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_ward_code', 'Phường/xã điểm bán:', ['class' => 'require']) !!}
                            {!! Form::select('terminal_ward_code',$wardsOptions, null, ['class' => 'form-control select2','id'=>'terminal_wards']) !!}
                            @if($errors->has('terminal_ward_code'))
                                <div class="error">{{ $errors->first('terminal_ward_code') }}</div>
                            @endif
                        </div>

                        <!-- Terminal Address Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('terminal_address', 'Địa chỉ kinh doanh điểm bán:', ['class' => 'require']) !!}
                            {!! Form::textarea('terminal_address', null, ['class' => 'form-control', 'rows' => 2, 'maxlength' => 150]) !!}
                            @if($errors->has('terminal_address'))
                                <div class="error">{{ $errors->first('terminal_address') }}</div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <!-- Terminal Contact Name Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_contact_name', 'Tên người đăng ký:', ['class' => 'require']) !!}
                            {!! Form::text('terminal_contact_name', null, ['class' => 'form-control', 'maxlength' => 50]) !!}
                            @if($errors->has('terminal_contact_name'))
                                <div class="error">{{ $errors->first('terminal_contact_name') }}</div>
                            @endif
                        </div>

                        <!-- Terminal Contact Email Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_contact_email', 'Email liên hệ điểm bán:', ['class' => 'require']) !!}
                            {!! Form::text('terminal_contact_email', null, ['class' => 'form-control', 'maxlength' => 100]) !!}
                            @if($errors->has('terminal_contact_email'))
                                <div class="error">{{ $errors->first('terminal_contact_email') }}</div>
                            @endif
                        </div>

                        <!-- Terminal Contact Phone Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_contact_phone', 'Số điện thoại liên hệ điểm bán (9 số cuối):', ['class' => 'require']) !!}
                            {!! Form::text('terminal_contact_phone', null, ['class' => 'form-control', 'maxlength' => 10]) !!}
                            @if($errors->has('terminal_contact_phone'))
                                <div class="error">{{ $errors->first('terminal_contact_phone') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="middle">
                            {!! Form::checkbox('create_terminal_app', 1, false,['class' => 'ace', 'id' => 'has-terminal-app-checkbox', 'disabled' => false]) !!}
                            <span class="lbl"> Tạo tài khoản đăng nhập Terminal App</span>
                        </label>
                    </div>
                    <div class="row form-group" id="terminal-app-user-area" style="display: none;">
                        <label for="phone" class="col-sm-5 control-label">Số điện thoại đăng nhập ter app <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('terminal_app_user', null, [
                                'class' => 'form-control',
                                'id' => 'terminal-app-user',
                                'maxlength' => 10,
                                'placeholder' => ''
                            ]) !!}
                            @if($errors->has('terminal_app_user'))
                                <div class="error">{{ $errors->first('terminal_app_user') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <div>
                                <label class="middle">
                                    {!! Form::checkbox('terminal_register_vnpayment', 1, null, ['class' => 'ace']) !!}
                                    <span class="lbl"> Đăng ký dịch vụ cổng thanh toán</span>
                                </label>
                                @if($errors->has('terminal_register_vnpayment'))
                                    <div class="error">{{ $errors->first('terminal_register_vnpayment') }}</div>
                                @endif
                            </div>
                            <div>
                                {!! Form::label('terminal_website', 'Website (nếu đăng ký dịch vụ cổng)', []) !!}
                                {!! Form::text('terminal_website', null, [
                                    'class' => 'form-control',
                                    'placeholder' => '',
                                    'maxlength' => 100
                                    ]) !!}
                                @if($errors->has('terminal_website'))
                                    <div class="error">{{ $errors->first('terminal_website') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Terminal MCC Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('terminal_mcc', 'MCC VNPAY:', ['class' => 'require']) !!}
                            <select class="form-control select2" id="terminal_mcc" name="terminal_mcc">
                                <option value="">-- Chọn --</option>
                                @foreach($mcc as $item)
                                    <option
                                        value="{{ $item['type_code'] }}"
                                        {{ $item['type_code'] === old('terminal_mcc') ? 'selected' : '' }}
                                    >{{ sprintf('%s - %s', $item['type_code'], $item['brand_name']) }}
                                    </option>
                                @endforeach
                            </select>
                            @if($errors->has('terminal_mcc'))
                                <div class="error">{{ $errors->first('terminal_mcc') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tab_thongtinnganhang" class="tab-pane fade">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Currency Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('currency', 'Tiền tệ:') !!}
                            {!! Form::select('currency', ['VND'=>'VND'], null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <!-- Bank Number Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('bank_number', 'Số tài khoản ngân hàng:', ['class' => 'require']) !!}
                            {!! Form::text('bank_number', null, ['class' => 'form-control', 'maxlength' => 30]) !!}
                            @if($errors->has('bank_number'))
                                <div class="error">{{ $errors->first('bank_number') }}</div>
                            @endif
                        </div>

                        <!-- Bank Account Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('bank_account', 'Chủ tài khoản:', ['class' => 'require']) !!}
                            {!! Form::text('bank_account', null, ['class' => 'form-control', 'maxlength' => 150]) !!}
                            @if($errors->has('bank_account'))
                                <div class="error">{{ $errors->first('bank_account') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Bank Code Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('bank_code', 'Tên ngân hàng:', ['class' => 'require']) !!}
                            {!! Form::select('bank_code', $banksOptions, null, ['class' => 'form-control select2']) !!}
                            @if($errors->has('bank_code'))
                                <div class="error">{{ $errors->first('bank_code') }}</div>
                            @endif
                        </div>

                        <!-- Bank Branch Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('bank_branch', 'Chi nhánh:', ['class' => 'require']) !!}
                            {!! Form::text('bank_branch', null, ['class' => 'form-control', 'maxlength' => 100]) !!}
                            @if($errors->has('bank_branch'))
                                <div class="error">{{ $errors->first('bank_branch') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tab_thongtinkhac" class="tab-pane fade">
            <div class="row">
                <div class="form-group col-sm-4">
                    {!! Form::label('contract_number', 'Số hợp đồng:', ['class' => 'require']) !!}
                    {!! Form::text('contract_number', null, ['class' => 'form-control', 'maxlength' => 255]) !!}
                    @if($errors->has('contract_number'))
                        <div class="error">{{ $errors->first('contract_number') }}</div>
                    @endif
                </div>
                <div class="form-group col-sm-4">
                    {!! Form::label('contract_code', 'Tên viết tắt hợp đồng:', ['class' => 'require']) !!}
                    {!! Form::text('contract_code', null, ['class' => 'form-control', 'maxlength' => 255]) !!}
                    @if($errors->has('contract_code'))
                        <div class="error">{{ $errors->first('contract_code') }}</div>
                    @endif
                </div>
                <div class="form-group col-sm-4">
                    {!! Form::label('contract_date', 'Ngày ký hợp đồng:', ['class' => 'require']) !!}
                    {!! Form::text('contract_date', null, ['class' => 'form-control date-picker', 'data-date-format' => "dd/mm/yyyy"]) !!}
                    @if($errors->has('contract_date'))
                        <div class="error">{{ $errors->first('contract_date') }}</div>
                    @endif
                </div>
            </div>
            <div class="row">
                <!-- Department Id Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('department_id', 'Phòng ban:', ['class' => 'require']) !!}
                    {!! Form::select('department_id', $departmentsOptions, null, ['class' => 'form-control select2','id'=>'departments']) !!}
                    @if($errors->has('department_id'))
                        <div class="error">{{ $errors->first('department_id') }}</div>
                    @endif
                </div>

                <!-- Staff Id Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('staff_id', 'Nhân viên:', ['class' => 'require']) !!}
                    {!! Form::select('staff_id', $staffsOptions, null, ['class' => 'form-control select2','id'=>'staffs']) !!}
                    @if($errors->has('staff_id'))
                        <div class="error">{{ $errors->first('staff_id') }}</div>
                    @endif
                </div>
            </div>
        </div>
        <div id="tab_giayphepvahopdong" class="tab-pane fade">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Merchant File Business Cert Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_file_business_cert', 'Giấy phép đăng ký kinh doanh (file có định dạng pdf,png,jpg,rar, dung lượng file tối đa là 10MB):', ['class' => 'require']) !!}
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">Chọn file&hellip;
                                        <input name="merchant_file_business_cert"
                                               id="merchant_file_business_cert"
                                               type="file"
                                               style="display: none;"
                                               accept="application/pdf,image/x-png,.jpg,.rar"
                                        >
                                    </span>
                                </label>
                                <input type="text" id="merchant_file_business_cert__name_field" class="form-control" disabled>
                            </div>
                            <div class="error">
                                @if($errors->has('merchant_file_business_cert'))
                                    {{ $errors->first('merchant_file_business_cert') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Merchant File Contract Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_file_contract', 'Hợp đồng (file có định dạng pdf,png,jpg,rar, dung lượng file tối đa là 10MB):', ['class' => 'require']) !!}
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">Chọn file&hellip; <input name="merchant_file_contract" id="merchant_file_contract" type="file" style="display: none;"
                                                                                           accept="application/pdf,image/x-png,.jpg,.rar"></span>
                                </label>
                                <input type="text" id="merchant_file_contract__name_field" class="form-control" disabled>
                            </div>
                            <div class="error">
                            @if($errors->has('merchant_file_contract'))
                                {{ $errors->first('merchant_file_contract') }}
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Merchant File Domain Cert Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_file_domain_cert', 'Giấy phép đăng ký tên miền (file có định dạng pdf,png,jpg,rar, dung lượng file tối đa là 10MB):') !!}
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">Chọn file&hellip; <input name="merchant_file_domain_cert" id="merchant_file_domain_cert" type="file" style="display: none;"
                                                                                           accept="application/pdf,image/x-png,.jpg,.rar"></span>
                                </label>
                                <input type="text" id="merchant_file_domain_cert__name_field" class="form-control" disabled>
                            </div>
                            <div class="error">
                                @if($errors->has('merchant_file_domain_cert'))
                                    {{ $errors->first('merchant_file_domain_cert') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Merchant File Identify Card Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_file_identify_card', 'CMND/ thẻ căn cước công dân (file có định dạng pdf,png,jpg,rar, dung lượng file tối đa là 10MB):') !!}
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">Chọn file&hellip; <input name="merchant_file_identify_card" id="merchant_file_identify_card" type="file" style="display: none;"
                                                                                           accept="application/pdf,image/x-png,.jpg,.rar"></span>
                                </label>
                                <input type="text" id="merchant_file_identify_card__name_field" class="form-control" disabled>
                            </div>
                            <div class="error">
                                @if($errors->has('merchant_file_identify_card'))
                                    {{ $errors->first('merchant_file_identify_card') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Merchant File Business Tax Cert Field -->
                        <div class="form-group col-sm-4">
                            {!! Form::label('merchant_file_business_tax_cert', 'Giấy phép đăng ký tài khoản với cơ quan thuế (file có định dạng pdf,png,jpg,rar, dung lượng file tối đa là 10MB):') !!}
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">Chọn file&hellip; <input name="merchant_file_business_tax_cert" id="merchant_file_business_tax_cert" type="file" style="display: none;"
                                                                                           accept="application/pdf,image/x-png,.jpg,.rar"></span>
                                </label>
                                <input type="text" id="merchant_file_business_tax_cert__name_field" class="form-control" disabled>
                            </div>
                            <div class="error">
                                @if($errors->has('merchant_file_business_tax_cert'))
                                    {{ $errors->first('merchant_file_business_tax_cert') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 m-t-10 text-center">
    <a href="{{ route('stores.index') }}" class="btn btn-secondary">Quay lại</a>
    {!! Form::submit('Thêm mới', ['class' => 'btn btn-primary']) !!}
    <button class="btn btn-success" type="submit" name="send_request" value="1">Gửi duyệt</button>
</div>


