<div class="col-sm-12">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#tab_hosomerchant">Hồ sơ Merchant</a></li>
        <li><a data-toggle="tab" href="#tab_danhsachterminal">Danh sách Terminal</a></li>
        <li><a id="href_tab_lichsupheduyet" data-toggle="tab" href="#tab_lichsupheduyet" onclick="getHistories()">Lịch sử phê duyệt</a></li>
        <script type="text/javascript">
            function getHistories() {
                $.ajax({
                    url: "{{ url('/api/get_audit_logs') }}?store_id=" + "{{ $store->id }}",
                    method: 'GET',
                    success: function(data) {
                        $('#tab_lichsupheduyet').html(data.html);
                    }
                });
            };
        </script>
    </ul>
    <div class="tab-content" style="padding: 20px 0">
        <div id="tab_hosomerchant" class="tab-pane fade in active">
            <div class="widget-box transparent">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title lighter">Thông tin Merchant</h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body" style="display: block;">
                    <div class="widget-main">
                        <div class="box row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    {!! Form::label('merchant_code', 'Merchant ID:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_code }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_name', 'Tên Merchant:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_name }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_brand', 'Tên viết tắt:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_brand }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_code', 'MST/CMND:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_code }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_type_business', 'Loại hình doanh nghiệp:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->type_business_name }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_type', 'Loại hình kinh doanh:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->type_name }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label(null, 'Master Merchant:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>CTCP Giải pháp Thanh toán Việt Nam VNPAY</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('created_at', 'Ngày đăng ký:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ date('H:i:s d/m/Y', strtotime($store->created_at)) }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('updated_at', 'Ngày duyêt/từ chối:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        @if(in_array($store->status, [
                                           \App\Libraries\BaseFunction::STATUS_SALE_ACCEPTED,
                                           \App\Libraries\BaseFunction::STATUS_CONTROL_ACCEPTED,
                                           \App\Libraries\BaseFunction::STATUS_SALE_REJECTED,
                                           \App\Libraries\BaseFunction::STATUS_CONTROL_REJECTED,
                                           \App\Libraries\BaseFunction::STATUS_SALE_REOPEN_TO_EDIT,
                                           \App\Libraries\BaseFunction::STATUS_CONTROL_REOPEN_TO_EDIT
                                        ]))
                                            @if($approveLog)
                                            <label><b>{{ date('H:i:s d/m/Y', strtotime($approveLog->created_at)) }}</b></label>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('issue_invoice', 'Xuất hóa đơn:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label>
                                            {!! $store->issue_invoice && $store->issue_invoice == 1 ?
                                                '<input type="checkbox" checked="checked" disabled="disabled"></input>' :
                                                '<input type="checkbox" disabled="disabled"></input>'
                                            !!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    {!! Form::label('merchant_address', 'Địa chỉ đăng ký kinh doanh:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_address . ", " . $store->merchant_ward_name . ", " . $store->merchant_district_name . ", " . $store->merchant_province_name }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('terminal_address', 'Địa chỉ kinh doanh:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_business_address . ", " . $store->merchant_ward_name . ", " . $store->merchant_district_name  . ", " . $store->merchant_province_name }}</b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_file_business_cert', 'Giấy phép đăng ký kinh doanh:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        @if ($store->merchant_file_business_cert)
                                            @if(strpos($store->merchant_file_business_cert, 'http') > -1)
                                                <a class="btn btn-primary" href="{{ $store->merchant_file_business_cert }}" target="_blank"><i
                                                            class="fa fa-eye"></i> Xem</a>
                                            @else
                                                <a class="btn btn-primary" href="{{url('/api_uploads').'/'.$store->merchant_file_business_cert }}" target="_blank"><i
                                                        class="fa fa-eye"></i> Xem</a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_file_contract', 'Hợp đồng ký kết với VNPAY:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                            @if (!$store->merchant_file_contract)
{{--                                                <a class="btn btn-primary" href="{{url('/api_uploads').'/'.$store->merchant_file_contract }}" download="{{url('/api_uploads').'/'.$store->merchant_file_contract }}"><i class="fa fa-download"></i> Tải hợp đồng mẫu</a>--}}
                                            @else
                                                @if(strpos($store->merchant_file_contract, 'http') > -1)
                                                <a class="btn btn-primary" href="{{ $store->merchant_file_contract }}" target="_blank"><i class="fa fa-eye"></i> Xem</a>
                                                @else
                                                    <a class="btn btn-primary" href="{{url('/api_uploads').'/'.$store->merchant_file_contract }}" target="_blank"><i class="fa fa-eye"></i> Xem</a>
                                                @endif

                                            <span title="{{ $store->contract_number }}">
                                                {{ strlen($store->contract_number) > 20 ? substr($store->contract_number, 0, 20) . '...' :  $store->contract_number}}
                                            </span>
                                            @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_file_domain_cert', 'Giấy phép đăng ký tên miền:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        @if ($store->merchant_file_domain_cert)
                                            @if(strpos($store->merchant_file_domain_cert, 'http') > -1)
                                                <a class="btn btn-primary" href="{{ $store->merchant_file_domain_cert }}" target="_blank"><i class="fa fa-eye"></i>
                                                    Xem</a>
                                            @else
                                                <a class="btn btn-primary" href="{{url('/api_uploads').'/'.$store->merchant_file_domain_cert }}" target="_blank"><i class="fa fa-eye"></i>
                                                    Xem</a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_file_business_tax_cert', 'GPĐK tài khoản với CQT:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        @if ($store->merchant_file_business_tax_cert)
                                            @if(strpos($store->merchant_file_business_tax_cert, 'http') > -1)
                                                <a class="btn btn-primary" href="{{ $store->merchant_file_business_tax_cert }}" target="_blank"><i
                                                        class="fa fa-eye"></i> Xem</a>
                                            @else
                                            <a class="btn btn-primary" href="{{url('/api_uploads').'/'.$store->merchant_file_business_tax_cert }}" target="_blank"><i
                                                        class="fa fa-eye"></i> Xem</a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('merchant_file_identify_card', 'CMND/CCCD:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        @if ($store->merchant_file_identify_card)
                                            @if(strpos($store->merchant_file_identify_card, 'http') > -1)
                                                <a class="btn btn-primary" href="{{ $store->merchant_file_identify_card }}" target="_blank"><i
                                                        class="fa fa-eye"></i> Xem</a>
                                            @else
                                            <a class="btn btn-primary" href="{{url('/api_uploads').'/'.$store->merchant_file_identify_card }}" target="_blank"><i
                                                        class="fa fa-eye"></i> Xem</a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label(null, 'Website doanh nghiệp:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b><a href="{{ $store->merchant_website }}" target="_blank">{{ $store->merchant_website }}</a></b></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('status', 'Trạng thái hồ sơ:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        @if($store->mms_status == \App\Libraries\BaseFunction::STATUS_MMS_CREAT_FAILED)
                                            <span class="{{ \App\Libraries\BaseFunction::getStoreStatusClass()[\App\Libraries\BaseFunction::STATUS_DRAFT] }}">
                                            {{ \App\Libraries\BaseFunction::getStoreStatus()[\App\Libraries\BaseFunction::STATUS_DRAFT] }}
                                        @elseif($store->merchant_file_contract || $store->src == 'SYNC_MMS')
                                            <span class="{{ \App\Libraries\BaseFunction::getStoreStatusClass()[$store->status] }}">
                                            {{ \App\Libraries\BaseFunction::getStoreStatus()[$store->status] }}
                                        </span>
                                        @elseif($store->status == \App\Libraries\BaseFunction::STATUS_LOCK)
                                            <span class="label label-inverse label-lock">
                                                Đóng
                                            </span>
                                        @else
                                            @if ($store->status < 0)
                                                <span class="label label-warning">
                                                    Chờ hoàn thiện hồ sơ
                                                </span>
                                                @else
                                                <span class="label label-primary">
                                                    Chờ cập nhật Hợp đồng
                                                </span>
                                                @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box transparent">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title lighter">Thông tin người liên hệ Merchant</h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body" style="display: block;">
                    <div class="widget-main">
                        <div class="box row">
                            <div class="col-sm-4">
                                <div class="form-group row">
                                    {!! Form::label('merchant_contact_name', 'Họ và tên:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_contact_name }}</b></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group row">
                                    {!! Form::label('merchant_contact_phone', 'Số điện thoại:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_contact_phone }}</b></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group row">
                                    {!! Form::label('merchant_contact_email', 'Email:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_contact_email }}</b></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box transparent">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title lighter">Thông tin đăng nhập</h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body" style="display: block;">
                    <div class="widget-main">
                        <div class="box row">
                            <div class="col-sm-4">
                                <div class="form-group row">
                                    {!! Form::label('merchant_app_user', 'Số điện thoại:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->merchant_app_user }}</b></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box transparent">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title lighter">Bộ phận phát triển</h4>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                    </div>
                </div>
                <div class="widget-body" style="display: block;">
                    <div class="widget-main">
                        <div class="box row">
                            <div class="col-sm-4">
                                <div class="form-group row">
                                    {!! Form::label('department_id', 'Phòng ban:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->department_name }}</b></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group row">
                                    {!! Form::label('staff_id', 'Nhân viên:', ['class' => 'col-sm-4 control-label']) !!}
                                    <div class="col-sm-8">
                                        <label><b>{{ $store->staff_info }}</b></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Submit Field -->
            <div class="form-group m-t-10 text-center">
                <a href="{{ route('stores.index') }}" class="btn btn-secondary">Quay lại</a>
                @if (Auth::user()->hasrole('Sale admin'))
                    @if (($store->status === \App\Libraries\BaseFunction::STATUS_WAIT_FOR_ACTION && !$store->merchant_file_contract)
                        || $store->status === \App\Libraries\BaseFunction::STATUS_REOPEN_CONTRACT)
                        <a href="{{ route('stores.edit', ['id' => $store->id]) }}" class="btn btn-primary">Sửa thông tin</a>
                    @endif
                @endif
                @if (Auth::user()->hasrole('Kinh doanh'))
                    @if ($store->status == \App\Libraries\BaseFunction::STATUS_WAIT_FOR_ACTION && $store->merchant_file_contract && !$store->need_reupload_contract)
                        @if (!$store->trial_user_id || ($store->trial_user_id && $store->mms_status == 1))
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#saleRejectModal">Từ chối</button>
                            <button type="button" class="btn btn-success" data-toggle="modal" id="sale-approve">Duyệt</button>
                        @endif
                    @endif
                @endif
                @if (Auth::user()->hasrole('Đối soát'))
                    @if ($store->merchant_file_contract && $store->status === \App\Libraries\BaseFunction::STATUS_SALE_ACCEPTED)
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#controlRejectModal">Từ chối</button>
                        <button type="button" class="btn btn-success" data-toggle="modal" id="control-approve">Duyệt</button>
                    @endif
                @endif
                @if (in_array($store->status, [
                   \App\Libraries\BaseFunction::STATUS_SALE_REOPEN_TO_EDIT,
                   \App\Libraries\BaseFunction::STATUS_CONTROL_REOPEN_TO_EDIT,
                   \App\Libraries\BaseFunction::STATUS_CONTROL_ACCEPTED,
                   \App\Libraries\BaseFunction::STATUS_DRAFT,
                   \App\Libraries\BaseFunction::STATUS_INIT
                ]) && \Auth::user()->hasrole('Sale admin'))
                    <a href="{{ route('stores.edit', ['id' => $store->id]) }}" class="btn btn-primary">Sửa thông tin</a>
                @endif
                @if (Auth::user()->hasrole('Sale admin') && in_array($store->status, [
                   \App\Libraries\BaseFunction::STATUS_DRAFT,
                   \App\Libraries\BaseFunction::STATUS_SALE_REOPEN_TO_EDIT,
                   \App\Libraries\BaseFunction::STATUS_CONTROL_REOPEN_TO_EDIT
                ]))
                <button id="sendRequestToSale" class="btn btn-success" type="button" name="send_request">Gửi duyệt</button>
                @endif
            </div>
        </div>
        <div id="tab_danhsachterminal" class="tab-pane fade in" style="padding: 0 20px;">
            <div class="row dataTables_filter ">
                @if ($hasMpTerminal)
                    <div class="col-sm-3 form-group">
                        <label>Tìm kiếm:</label>
                        <input id="filter_query" class="form-control" placeholder="-- Nhập từ khóa tìm kiếm --">
                    </div>
                    @if (count($filterable_status) > 0)
                        <div class="col-sm-3 form-group">
                            <label>Trạng thái:</label>
                            <select id="filter_status" name="filter_status" class="form-control select2"
                                    data-placeholder="-- Chọn trạng thái --">
                                <option></option>
                                @foreach($filterable_status as $filterStatus)
                                    <option value="{{ $filterStatus }}">
                                        {{ \App\MpTerminal::STATUS[$filterStatus] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                    <div class="col-sm-3 form-group">
                        <label>&nbsp;</label>
                        <a class="btn btn-primary btn-large" style="height: 34px; padding-top: 7px" href="{{route('terminals.create')}}">Thêm mới</a>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-12">
                    @if (!$hasMpTerminal)
                        @include('stores.first_terminal')
                    @else
                        @include('stores.list_terminals_table')
                    @endif
                </div>
            </div>
        </div>
        <div id="tab_lichsupheduyet" class="tab-pane fade in" style="padding: 0 20px;">
        </div>
    </div>
</div>
