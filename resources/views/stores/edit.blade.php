@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Sửa thông tin merchant', 'buttons' => [['title' => 'Quay lại', 'link' => route('stores.index'), 'icon' => 'fa fa-arrow-left']]])
{{--    @include('coreui-templates::common.errors')--}}
    <div class="row">
        {!! Form::open(['route' => ['stores.updateStore', $store->id], 'method' => 'post' ,'files' =>true,'enctype'=>'multipart/form-data']) !!}
{{--        <form id="updateStores" method="post" action="{{ route('stores.update', ['id' => $store->id]) }}" enctype="multipart/form-data">--}}
        @include('stores.fields_edit')

        {!! Form::close() !!}
    </div>
@endsection

@include('stores.script.script_edit')
