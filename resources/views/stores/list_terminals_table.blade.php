@section('css')
    @include('layouts_mms.datatables_css')
    @include('layouts_mms.daterange_picker_css')
@endsection

{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}
