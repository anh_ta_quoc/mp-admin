@extends('layouts_mms.app')

@section('content')
    @if (Auth::user()->hasrole('Sale admin') || Auth::user()->email === 'devmp@gmail.com')
        @include('layouts_mms.page_header', [
            'header' => \App\Libraries\BaseFunction::PAGE_HEADER['stores'][\Route::current()->action['as']],
            'buttons' => [['title' => 'Thêm mới', 'link' => route('stores.create'), 'icon' => 'fa fa-plus-circle']]]
            )
    @else
        @include('layouts_mms.page_header', [
            'header' => \App\Libraries\BaseFunction::PAGE_HEADER['stores'][\Route::current()->action['as']]]
            )
    @endif
    @include('flash::message')
    <div class="row dataTables_filter ">
        <div class="col-sm-3 form-group">
            <label>Tìm kiếm:</label>
            <input id="filter_query" class="form-control" placeholder="-- Nhập từ khóa tìm kiếm --" value="{{  session('store_info_filter_query') }}">
        </div>
        <div class="col-sm-3 form-group">
            <label>Thời gian:</label>
            <input class="form-control drp" id="date_range_search" placeholder="-- Chọn ngày tìm kiếm --">
        </div>
        <div class="col-sm-3 form-group">
            <label>Loại hình doanh nghiệp:</label>
            <select id="filter_business_type" name="filter_business_type" class="form-control select2"
                    data-placeholder="-- Loại hình doanh nghiệp --">
                <option></option>
                @foreach($merchantTypeBusiness as $code => $name)
                    <option value="{{ $code }}"
                        {{ session('store_info_filter_business_type') == $code ? 'selected': '' }}
                    >
                        {{ $name }}
                    </option>
                @endforeach
            </select>
        </div>
        @if (count($filterStatus) > 0)
            <div class="col-sm-3 form-group">
                <label>Trạng thái:</label>
                <select id="filter_status" name="filter_status" class="form-control select2"
                        data-placeholder="-- Chọn trạng thái --">
                    <option></option>

                    @foreach($filterStatus as $status)
                        <option value="{{ $status }}"
                            {{ !empty(session('store_info_filter_status')) && session('store_info_filter_status') == $status ? 'selected': '' }}
                        >
                            {{ \App\Libraries\BaseFunction::getStoreStatus()[$status] }}
                        </option>
                    @endforeach
                </select>
            </div>
        @endif
        <div class="col-sm-3 form-group">
            <label>Nguồn:</label>
            <select id="filter_src" name="filter_src" class="form-control select2"
                    data-placeholder="-- Nguồn --">
                <option></option>
                <option value="MR"
                    {{ session('store_info_filter_src') == 'MR' ? 'selected': '' }}
                >MR</option>
                <option value="BE"
                    {{ session('store_info_filter_src') == 'BE' ? 'selected': '' }}
                >BE</option>
                <option value="SAPO"
                    {{ session('store_info_filter_src') == 'SAPO' ? 'selected': '' }}
                >SAPO</option>
                <option value="MMS"
                    {{ session('store_info_filter_src') == 'MMS' ? 'selected': '' }}
                >MMS</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            @include('stores.table')
        </div>
        <div class="modal fade" id="confirmLockMerchantModal" tabindex="-1" role="dialog"
             aria-labelledby="confirmLockMerchantModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content text-center">
                    <form id="lockMerchant" method="post" action="{{ route('stores.lockMerchant') }}">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">
                                Vui lòng nhập lý do khóa Merchant <span class="red">(*)</span>
                            </span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea
                                    name="reason"
                                    required
                                    maxlength="255"
                                    cols="10"
                                    class="form-control reason-lock-merchant"
                                    placeholder="Nhập lý do khóa Merchant"
                                ></textarea>
                            </div>
                            <div class="row m-t-20">
                                <p class="error-lock-merchant red"></p>
                                <div class="col-xs-6 align-left">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-6 align-right">
                                    <input type="hidden" name="storeId" id="storeId" value=""/>
                                    <button name="reopen" value="1" type="button" class="m-0 btn btn-info accept-lock-merchant">Đồng ý</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirmUnLockMerchantModal" tabindex="-1" role="dialog"
             aria-labelledby="confirmUnLockMerchantModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content text-center">
                    <form id="unlockMerchant" method="post" action="{{ route('stores.unlockMerchant') }}">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">
                                Vui lòng nhập lý do mở khóa Merchant <span class="red">(*)</span>
                            </span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea
                                    name="reason"
                                    required
                                    maxlength="255"
                                    cols="10"
                                    class="form-control reason-unlock-merchant"
                                    placeholder="Nhập lý do mở khóa Merchant"
                                ></textarea>
                            </div>
                            <div class="row m-t-20">
                                <p class="error-unlock-merchant red"></p>
                                <div class="col-xs-6 align-left">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-6 align-right">
                                    <input type="hidden" name="storeId" id="storeIdToUnlock" value=""/>
                                    <button name="reopen" value="1" type="button" class="m-0 btn btn-info accept-unlock-merchant">Đồng ý</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

