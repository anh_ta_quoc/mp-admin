@section('scripts')
    <script>
        $(function () {
            $(document).on('change', ':file', function () {
                if (this.files[0]) {
                    var file = this.files[0],
                        extension = file.name.substr((file.name.lastIndexOf('.') + 1)),
                        input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    if (['pdf', 'png', 'jpg', 'rar'].indexOf(extension.toLowerCase()) < 0) {
                        input.trigger('fileTypeError');
                    }
                    else if (file.size > 2*5242880) {
                        input.trigger('fileSizeError');
                    }
                    else {
                        input.trigger('fileSelected', [numFiles, label]);
                    }
                }
            });


            $(document).ready(function () {
                $(':file').on('fileTypeError', function (event) {
                    $(this).val("");
                    $(this).parents('.input-group').find(':text').val('');
                    $(this).parents('.form-group').addClass('has-error').find('.help-block').text('File có định dạng pdf, png, jpg, rar');
                });
                $(':file').on('fileSizeError', function (event) {
                    $(this).val("");
                    $(this).parents('.input-group').find(':text').val('');
                    $(this).parents('.form-group').addClass('has-error').find('.help-block').text('Dung lượng file tối đa là 10MB');
                });
                $(':file').on('fileSelected', function (event, numFiles, label) {
                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    }
                    $(this).parents('.form-group').removeClass('has-error').find('.help-block').text('');

                });

                if ("{{ old('merchant_province_code')}}") {
                    $("#merchant_provinces").val("{{ old('merchant_province_code')}}").trigger('change', function () {
                        $("#merchant_districts").val("{{ old('merchant_district_code') }}").trigger('change', function () {
                            $("#merchant_wards").val("{{ old('merchant_ward_code') }}").trigger('change')
                        })
                    });
                }

                if ("{{ old('terminal_province_code')}}") {
                    $("#terminal_provinces").val("{{ old('terminal_province_code')}}").trigger('change', function () {
                        $("#terminal_districts").val("{{ old('terminal_district_code') }}").trigger('change', function () {
                            $("#terminal_wards").val("{{ old('terminal_ward_code') }}").trigger('change')
                        })
                    });
                }

                var dateToday = getToday()
                $('.date-picker').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    endDate: dateToday,
                }).next().on('click', function(){
                    $(this).prev().focus();
                });
                $('.date-picker').datepicker('setDate', dateToday);

                if ($('#has-terminal-app-checkbox').is(":checked")) {
                    $('#terminal-app-user-area').css('display', 'block');
                    $('#terminal-app-user').attr('disabled', false);
                }

                $('#has-terminal-app-checkbox').on('click', function (e) {
                    if ($(this).is(":checked")) {
                        $('#terminal-app-user-area').css('display', 'block');
                        $('#terminal-app-user').attr('disabled', false);
                    }
                    else {
                        $('#terminal-app-user-area').css('display', 'none');
                        $('#terminal-app-user').attr('disabled', true);
                    }
                })
            });

        });
        function getToday() {
            let date = new Date();
            let today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            return today;
        }
    </script>
    <script type="text/javascript">
        $("#merchant_provinces").change(function(event, callback){
            $.ajax({
                url: "{{ url('/api/get_districts_from_province_code') }}?province_code=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#merchant_districts').html(data.html);
                    if (callback){
                        callback();
                    }
                    else {
                        $('#merchant_districts').trigger('change.select2');

                        setTimeout($("#merchant_districts").val($("#merchant_districts option:first").val()).change(),3000);
                    }
                }
            });
        });
        $("#merchant_districts").change(function(event, callback){
            $.ajax({
                url: "{{ url('/api/get_wards_from_district_code_and_province_code') }}?district_code=" + $(this).val()+'&province_code='+ $("#merchant_provinces").val(),
                method: 'GET',
                success: function(data) {
                    $('#merchant_wards').html(data.html);
                    if (callback){
                        callback();
                    }
                    else {
                        $('#merchant_wards').trigger('change.select2');
                    }
                }
            });
        });
        $("#terminal_provinces").change(function(event, callback){
            $.ajax({
                url: "{{ url('/api/get_districts_from_province_code') }}?province_code=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#terminal_districts').html(data.html);
                    if (callback){
                        callback();
                    }
                    else {
                        $('#terminal_districts').trigger('change.select2');

                        setTimeout($("#terminal_districts").val($("#terminal_districts option:first").val()).change(), 3000);
                    }

                }
            });
        });
        $("#terminal_districts").change(function(event, callback){
            $.ajax({
                url: "{{ url('/api/get_wards_from_district_code_and_province_code') }}?district_code=" + $(this).val()+'&province_code='+ $("#terminal_provinces").val(),
                method: 'GET',
                success: function(data) {
                    $('#terminal_wards').html(data.html);
                    if (callback){
                        callback();
                    }
                    else {
                        $('#terminal_wards').trigger('change.select2');
                    }
                }
            });
        });
        $("#departments").change(function(){
            $.ajax({
                url: "{{ url('/api/get_staff_from_department') }}?department_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#staffs').html(data.html);
                    $('#staffs').trigger('change.select2');

                    setTimeout($("#staffs").val($("#staffs option:first").val()).change(),3000);

                }
            });
        });
    </script>
@endsection
