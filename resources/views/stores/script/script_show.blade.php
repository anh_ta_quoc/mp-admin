@section('scripts')
    @include('layouts_mms.daterange_picker_js', ['init_id' => ['date_range_search']])
    @include('layouts_mms.datatables_js')
    {!! $dataTable->scripts() !!}
    <script>
        var queryTypingTimer;
        var doneTypingInterval = 1000;
        var $input = $('#filter_query');
        $input.on('keyup', function () {
            clearTimeout(queryTypingTimer);
            queryTypingTimer = setTimeout(doneFilterTyping, doneTypingInterval);
        });
        $input.on('keydown', function () {
            clearTimeout(queryTypingTimer);
        });
        function doneFilterTyping () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].search($input.val()).draw();
            }
        }
        $('#filter_status').on('change', function () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        });
    </script>

    <script>
        // $('#uploadContractModal').on('hidden.bs.modal', function () {
        //     $('#uploadContractForm').trigger('reset').find(".has-error").each(function () {
        //         $(this).removeClass('has-error').find('.help-block').text('')
        //     });
        //     // $('#department').val('');
        //     // $('#staff').html('');
        // });
        $('#uploadContractModal').on('show.bs.modal', function () {
            let departmentId = "{{ $store->department_id }}";
            if (departmentId) {
                $('#department').val(departmentId).trigger('change');
            }
        });
        var checkHasValidFile = false;
        $(document).on('change', ':file', function () {
            checkHasValidFile = false;
            if (this.files[0]) {
                var file = this.files[0],
                    extension = file.name.substr((file.name.lastIndexOf('.') + 1)),
                    input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                if (['pdf', 'png', 'jpg', 'rar'].indexOf(extension.toLowerCase()) < 0) {
                    input.trigger('fileTypeError');
                }
                else if (file.size > 2*5242880) {
                    input.trigger('fileSizeError');
                }
                else {
                    input.trigger('fileSelected', [numFiles, label]);
                    checkHasValidFile = true;
                }
            }
        });

        $(document).ready(function () {
            $(':file').on('fileTypeError', function (event) {
                $(this).val("");
                $(this).parents('.input-group').find(':text').val('');
                $(this).parents('.form-group').addClass('has-error').find('.help-block').text('File có định dạng pdf, png, jpg, rar');
            });
            $(':file').on('fileSizeError', function (event) {
                $(this).val("");
                $(this).parents('.input-group').find(':text').val('');
                $(this).parents('.form-group').addClass('has-error').find('.help-block').text('Dung lượng file tối đa là 10MB');
            });
            $(':file').on('fileSelected', function (event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if (input.length) {
                    input.val(log);
                }
                $(this).parents('.form-group').removeClass('has-error').find('.help-block').text('');

            });

            $('#update-store-info-contract').on('click', function () {
                this.disabled = true;
                let error_msg = '';
                if (!checkHasValidFile) {
                    error_msg = 'Vui lòng chọn file upload';
                } else {
                    if ($('#contract_number').val().trim() === '' || $('#contract_code').val().trim() === '' || $('#contract_date').val().trim() === '') {
                        error_msg = 'Vui lòng điền các thông tin có dấu *';
                    }
                }

                if (!error_msg) {
                    $('#error-msg').html('');
                    $('#uploadContractForm').submit();
                }
                else {
                    this.disabled = false;
                    $('#error-msg').html(error_msg);
                }
            });

            $('.dp-contract').datepicker({
                language: "vi",
                endDate: new Date(),
                autoclose: true
            });

            function getToday() {
                let date = new Date();
                let today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

                return today;
            }

            if (!$('#old_contract_date').val()) {
                $('.dp-contract').datepicker('setDate', getToday());
            }

            $("#department").on('change', function () {
                $.ajax({
                    url: "{{ url('/api/get_staff_from_department') }}?department_id=" + $(this).val(),
                    method: 'GET',
                    success: function(data) {
                        $('#staff').html(data.html);
                        $('#staff').trigger('change.select2');

                        let staffId = "{{ $store->staff_id }}";
                        if (staffId) {
                            $('#staff').val(staffId).trigger('change');
                        }
                        else {
                            setTimeout($("#staff").val($("#staff option:first").val()).change(),3000);
                        }

                    }
                });
            });

            $('#sale-approve').on('click', function () {
                $(this).attr('disabled', true);
                $.ajax({
                    url: "{{ url('/stores/saleAction') }}/" + "{{ $store->id }}",
                    method: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "sale-action-type": "sale-approve"
                    },
                    success: function(data) {
                        window.location.reload();
                    }
                });
            });

            $('#control-approve').on('click', function () {
                let txt;
                let r = confirm("Bạn có chắc chắn thực hiện tác vụ này không?");
                if (r == true) {
                    $(this).attr('disabled', true);
                    $.ajax({
                        url: "{{ url('/stores/controlAction') }}/" + "{{ $store->id }}",
                        method: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "control-action-type": "control-approve"
                        },
                        success: function(data) {
                            if (data.status === 'OK') {
                                window.location.reload();
                            }
                            else {
                                alert(data.msg);
                            }
                        }
                    });
                }
            });

            $('#sendRequestToSale').on('click', function () {
                $(this).attr('disabled', true);
                $.ajax({
                    url: "{{ url('/stores/sendRequest') }}/" + "{{ $store->id }}",
                    method: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "control-action-type": "control-approve"
                    },
                    success: function(data) {
                        window.location.reload();
                    }
                });
            });
        });
    </script>
@endsection
