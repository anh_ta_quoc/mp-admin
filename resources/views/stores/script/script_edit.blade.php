@section('scripts')
    @include('layouts_mms.daterange_picker_js', ['init_id' => ['date_range_search']])
    @include('layouts_mms.datatables_js')
    {!! $dataTable->scripts() !!}
    <script>
        var queryTypingTimer;
        var doneTypingInterval = 1000;
        var $input = $('#filter_query');
        $input.on('keyup', function () {
            clearTimeout(queryTypingTimer);
            queryTypingTimer = setTimeout(doneFilterTyping, doneTypingInterval);
        });
        $input.on('keydown', function () {
            clearTimeout(queryTypingTimer);
        });
        function doneFilterTyping () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].search($input.val()).draw();
            }
        }
        $('#filter_status').on('change', function () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        });


        $(function () {
            $(document).on('change', ':file', function () {
                if (this.files[0]) {
                    var file = this.files[0],
                        extension = file.name.substr((file.name.lastIndexOf('.') + 1)),
                        input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    if (['pdf', 'png', 'jpg', 'rar'].indexOf(extension.toLowerCase()) < 0) {
                        input.trigger('fileTypeError');
                    }
                    else if (file.size > 2*5242880) {
                        input.trigger('fileSizeError');
                    }
                    else {
                        input.trigger('fileSelected', [numFiles, label]);
                    }
                }
            });
            $("#merchant_provinces").change(function(event, callback){
                $.ajax({
                    url: "{{ url('/api/get_districts_from_province_code') }}?province_code=" + $(this).val(),
                    method: 'GET',
                    success: function(data) {
                        $('#merchant_districts').html(data.html);
                        if (callback){
                            callback();
                        }
                        else {
                            $('#merchant_districts').trigger('change.select2');
                            setTimeout($("#merchant_districts").val($("#merchant_districts option:first").val()).change(),3000);
                        }

                    }
                });
            });
            $("#merchant_districts").change(function(event, callback){
                $.ajax({
                    url: "{{ url('/api/get_wards_from_district_code_and_province_code') }}?district_code=" + $(this).val()+'&province_code='+ $("#merchant_provinces").val(),
                    method: 'GET',
                    success: function(data) {
                        $('#merchant_wards').html(data.html);
                        if (callback){
                            callback();
                        }
                        else {
                            $('#merchant_wards').trigger('change.select2');
                        }
                    }
                });
            });
            $("#departments").change(function(event, callback){
                $.ajax({
                    url: "{{ url('/api/get_staff_from_department') }}?department_id=" + $(this).val(),
                    method: 'GET',
                    success: function(data) {
                        $('#staffs').html(data.html);
                        if (callback) {
                            callback();
                        }
                        else {
                            $('#staffs').trigger('change.select2');

                            setTimeout($("#staffs").val($("#staffs option:first").val()).change(), 3000);
                        }

                    }
                });
            });


            $(document).ready(function () {
                $(':file').on('fileTypeError', function (event) {
                    $(this).val("");
                    $(this).parents('.input-group').find(':text').val('');
                    $(this).parents('.form-group').addClass('has-error').find('.help-block').text('File có định dạng pdf, png, jpg, rar');
                });
                $(':file').on('fileSizeError', function (event) {
                    $(this).val("");
                    $(this).parents('.input-group').find(':text').val('');
                    $(this).parents('.form-group').addClass('has-error').find('.help-block').text('Dung lượng file tối đa là 10MB');
                });
                $(':file').on('fileSelected', function (event, numFiles, label) {
                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    }
                    $(this).parents('.form-group').removeClass('has-error').find('.help-block').text('');

                });
                let departmentId = "{{ old('department_id', $store->department_id) }}";
                if (departmentId) {
                    $('#departments').val(departmentId).trigger('change', function () {
                        $('#staffs').val("{{ old('staff_id', $store->staff_id) }}").trigger('change')
                    });
                }

                if ("{{ old('merchant_province_code', $store->merchant_province_code)}}") {
                    $("#merchant_provinces").val("{{ old('merchant_province_code', $store->merchant_province_code)}}").trigger('change', function () {
                        $("#merchant_districts").val("{{ old('merchant_district_code', $store->merchant_district_code) }}").trigger('change', function () {
                            $("#merchant_wards").val("{{ old('merchant_ward_code', $store->merchant_ward_code) }}").trigger('change')
                        })
                    });
                }

                if ("{{ old('terminal_province_code', $store->terminal_province_code)}}") {
                    $("#terminal_provinces").val("{{ old('terminal_province_code')}}").trigger('change', function () {
                        $("#terminal_districts").val("{{ old('terminal_district_code') }}").trigger('change', function () {
                            $("#terminal_wards").val("{{ old('terminal_ward_code') }}").trigger('change')
                        })
                    });
                }

                $('.date-picker').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    language: "vi",
                    endDate: new Date(),
                }).next().on('click', function(){
                    $(this).prev().focus();
                });
            });

        });
    </script>
@endsection
