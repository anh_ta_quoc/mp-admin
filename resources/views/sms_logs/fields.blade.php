<div class="col-md-6">
    <!-- Phone Field -->
    <div class="form-group">
        {!! Form::label('phone', 'Phone:') !!}
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Message Field -->
    <div class="form-group">
        {!! Form::label('message', 'Nội dung:') !!}
        {!! Form::textarea('message', null, ['class' => 'form-control', 'rows' => 3, 'style' => 'height: 83px']) !!}
    </div>
</div>
<div class="col-md-6">
    <!-- Status Field -->
    <div class="form-group">
        {!! Form::label('status', 'Trạng thái:') !!}
        {!! Form::text('status', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Error Code Field -->
    <div class="form-group">
        {!! Form::label('error_code', 'Mã lỗi:') !!}
        {!! Form::text('error_code', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Error Message Field -->
    <div class="form-group">
        {!! Form::label('error_message', 'Mô tả lỗi:') !!}
        {!! Form::text('error_message', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-md-12 text-center">
    <!-- Submit Field -->
    <div class="form-group">
        {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('smsLogs.index') }}" class="btn btn-secondary">Quay lại</a>
    </div>
</div>
