@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Thêm mới SMS', 'buttons' => [['title' => 'Quay lại', 'link' => route('smsLogs.index'), 'icon' => 'fa fa-arrow-left']]])
    @include('coreui-templates::common.errors')
    <div class="row">
        {!! Form::open(['route' => 'smsLogs.store']) !!}

        @include('sms_logs.fields')

        {!! Form::close() !!}
    </div>
@endsection
