@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Cập nhật SMS', 'buttons' => [['title' => 'Quay lại', 'link' => route('smsLogs.index'), 'icon' => 'fa fa-arrow-left']]])
    @include('coreui-templates::common.errors')
    <div class="row">
        {!! Form::model($smsLog, ['route' => ['smsLogs.update', $smsLog->id], 'method' => 'patch']) !!}

        @include('sms_logs.fields')

        {!! Form::close() !!}
    </div>
@endsection