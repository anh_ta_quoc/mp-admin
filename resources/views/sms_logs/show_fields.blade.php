<div class="col-md-6">
    <!-- Phone Field -->
    <div class="form-group">
        {!! Form::label('phone', 'Phone:') !!}
        <p>{{ $smsLog->phone }}</p>
    </div>

    <!-- Message Field -->
    <div class="form-group">
        {!! Form::label('message', 'Message:') !!}
        <p>{{ $smsLog->message }}</p>
    </div>
</div>
<div class="col-md-6">
    <!-- Status Field -->
    <div class="form-group">
        {!! Form::label('status', 'Status:') !!}
        <p>{{ $smsLog->status }}</p>
    </div>

    <!-- Error Code Field -->
    <div class="form-group">
        {!! Form::label('error_code', 'Error Code:') !!}
        <p>{{ $smsLog->error_code }}</p>
    </div>

    <!-- Error Message Field -->
    <div class="form-group">
        {!! Form::label('error_message', 'Error Message:') !!}
        <p>{{ $smsLog->error_message }}</p>
    </div>
</div>

