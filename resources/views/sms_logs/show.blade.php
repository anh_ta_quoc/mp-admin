@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Chi tiết SMS', 'buttons' => [['title' => 'Quay lại', 'link' => route('smsLogs.index'), 'icon' => 'fa fa-arrow-left']]])
    @include('coreui-templates::common.errors')
    <div class="row">
        @include('sms_logs.show_fields')
    </div>
@endsection
