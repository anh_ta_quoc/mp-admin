@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'SMS Logging'])
    @include('flash::message')
    <div class="row dataTables_filter ">
        <div class="col-sm-3 form-group">
            <label>Tìm kiếm:</label>
            <input id="filter_query" class="form-control" placeholder="-- Nhập từ khóa tìm kiếm --">
        </div>
        <div class="col-sm-3 form-group">
            <label>Thời gian:</label>
            <input class="form-control drp" id="date_range_search" placeholder="-- Chọn ngày tìm kiếm --">
        </div>

        {{--<div class="col-sm-3 form-group">--}}
            {{--<label>Trạng thái:</label>--}}
            {{--<select id="filter_status" name="filter_status" class="form-control select2" data-placeholder="-- Chọn trạng thái --">--}}
                {{--<option></option>--}}
                {{--<option value="-1">--}}
                    {{--Khởi tạo--}}
                {{--</option>--}}
                {{--<option value="-2">--}}
                    {{--Khởi tạo lại--}}
                {{--</option>--}}
                {{--<option value="0">--}}
                    {{--Chờ duyệt--}}
                {{--</option>--}}
                {{--<option value="1">--}}
                    {{--Kinh doanh duyệt--}}
                {{--</option>--}}
                {{--<option value="2">--}}
                    {{--Đối soát duyệt--}}
                {{--</option>--}}
                {{--<option value="3">--}}
                    {{--Kinh doanh từ chối--}}
                {{--</option>--}}
                {{--<option value="4">--}}
                    {{--Đối soát từ chối--}}
                {{--</option>--}}
            {{--</select>--}}
        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-xs-12">
            @include('sms_logs.table')
        </div>
    </div>
@endsection

