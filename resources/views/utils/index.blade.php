@extends('layouts_mms.app')

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Mở khóa Merchant User'])
    @include('flash::message')
    <div class="row dataTables_filter ">
        <div class="col-md-4 form-group">
            <label>Tìm kiếm:</label>
            <input id="filter-query" class="form-control" placeholder="-- Nhập tên / mã merchant --">
        </div>
    </div>
    <div class="row result" style="display: none;">
        <div class="col-md-4">
            <form id="resetUsersForm" method="post" action="{{ route('utils.resetUsers') }}">
                <div class="check-all">
                    <label for="check-all"><input type="checkbox" id="check-all" value="" onclick="checkAllUsers(this)"/>&nbsp;Chọn tất cả</label>
                </div>
                @csrf
                <div id="list-merchant-users-checkbox" style="margin-left: 10px;">

                </div>
                <div id="select-user-area">
                    <span id="select-user-error" class="help-block hidden">Vui lòng chọn ít nhất 1 user</span>
                </div>
                <button id="apply-reset-user" type="button" class="btn btn-danger">Áp dụng</button>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('#filter-query').keypress(function(e){
        if (e.which == 13) {
            $.ajax({
                url: "{{ url('/api/search_merchant_users') }}?search=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    var html = '';
                    if (data && data.length > 0) {
                        for (var i = 0; i < data.length; i ++) {
                            html += '<div class="form-group">\n' +
                                '<input name="merchant_user[]" type="checkbox" class="check-user" id="' + data[i].username + '" value="' + data[i].id + '" onclick="checkOneUser(this)"/>\n' +
                                '<label for="' + data[i].username + '">' + data[i].username + '</label>&nbsp;<i class="fa fa-user"></i>\n' +
                                '</div>'
                        }
                        $('.check-all').css('display', 'block');
                    }
                    else {
                        html = '<div class="form-group"><p>Không tìm thấy merchant user tương ứng!</p></div>';
                        $('.check-all').css('display', 'none');
                    }
                    $('#list-merchant-users-checkbox').html(html);
                    $('.result').css('display', 'block');
                }
            });
        }
    });

    $('#apply-reset-user').on('click', function () {
        var hasSelectedUser = false;
        var hasError = false;
        $('.check-user').each(function (e) {
            if (this.checked) {
                hasSelectedUser = true;
            }
        });
        if (!hasSelectedUser) {
            $('#select-user-area').addClass('has-error');
            $('#select-user-error').removeClass('hidden');
            hasError = true;
        }
        else {
            $('#select-user-area').removeClass('has-error');
            $('#select-user-error').addClass('hidden');
        }

        if (!hasError) {
            if (window.confirm("Bạn có muốn mở khóa cho các user này?")) {
                $('#resetUsersForm').submit();
            }
        }
    });

    function checkAllUsers(e) {
        if (e.checked === true) {
            $('.check-user').each(function (e) {
                $(this).prop('checked', true);
            });
        }
        else {
            $('.check-user').each(function (e) {
                $(this).prop('checked', false);
            });
        }
    };

    function checkOneUser(e) {
        if (e.checked === true) {
            $('#select-user-area').removeClass('has-error');
            $('#select-user-error').addClass('hidden');
        }
    }
</script>
@endsection

