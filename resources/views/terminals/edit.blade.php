@extends('layouts_mms.app')
@section('content')
    @include('layouts_mms.page_header', ['header' => 'Sửa terminal', 'buttons' => [['title' => 'Quay lại', 'link' => route('terminals.index'), 'icon' => 'fa fa-arrow-left']]])
    <div class="row">
        {!! Form::open(['route' => ['terminals.update', $terminal->id], 'method' => 'patch', 'files' => true]) !!}
        @include('flash::message')
        @include('terminals.edit_form')

        {!! Form::close() !!}
    </div>
@endsection
