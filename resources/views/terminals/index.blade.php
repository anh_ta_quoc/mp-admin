@extends('layouts_mms.app', ['new_select2' => true])

@section('content')
    @if(Auth::user()->hasrole('Sale admin'))
        @include('layouts_mms.page_header', ['header' => 'Quản lý Terminals', 'buttons' => [['title' => 'Thêm mới', 'link' => route('terminals.create'), 'icon' => 'fa fa-plus-circle']]])
    @else
        @include('layouts_mms.page_header', ['header' => 'Quản lý Terminals'])
    @endif
    @include('flash::message')
    <div class="row dataTables_filter ">
        <div class="col-sm-3 form-group">
            <label>Tìm kiếm:</label>
            <input id="filter_query" class="form-control" placeholder="-- Nhập từ khóa tìm kiếm --" value="{{ session()->get('terminal_filter_query') }}">
        </div>
        <div class="col-sm-3 form-group">
            <label>Thời gian:</label>
            <input class="form-control drp" id="date_range_search" placeholder="-- Chọn ngày tìm kiếm --">
        </div>
        @if (count($filterable_status) > 0)
            <div class="col-sm-3 form-group">
                <label>Trạng thái:</label>
                <select id="filter_status" name="filter_status" class="form-control select2"
                        data-placeholder="-- Chọn trạng thái --">
                    <option></option>
                    @foreach($filterable_status as $filterStatus)
                        <option value="{{ $filterStatus }}"
                        {{ session()->get('terminal_filter_status') && session()->get('terminal_filter_status') == $filterStatus ? 'selected' : '' }}
                        >
                            {{ \App\MpTerminal::STATUS[$filterStatus] }}
                        </option>
                    @endforeach
                </select>
            </div>
        @endif
        <div class="col-sm-3 form-group">
            <label for="merchant_code">Merchant:</label>
            <select class="form-control select2-ajax" id="merchant_code" name="merchant_code">
{{--            <select class="form-control select2" id="merchant_code" name="merchant_code" data-placeholder="-- Chọn merchant --">--}}
{{--                <option></option>--}}
{{--                @foreach($merchants as $merchant)--}}
{{--                    <option--}}
{{--                            value="{{ $merchant['merchant_code'] }}"--}}
{{--                            {{ $merchant['merchant_code'] === old('merchant_code') ? 'selected' : '' }}--}}
{{--                    >{{ sprintf('%s - %s', $merchant['merchant_code'], $merchant['merchant_name']) }}--}}
{{--                    </option>--}}
{{--                @endforeach--}}
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            @include('terminals.table')
        </div>
        <div class="modal fade" id="confirmLockTerminalModal" tabindex="-1" role="dialog"
             aria-labelledby="confirmLockTerminalModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content text-center">
                    <form id="lockTerminal" method="post" action="{{ route('terminals.lockTerminal') }}">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">
                                Vui lòng nhập lý do khóa Terminal <span class="red">(*)</span>
                            </span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea
                                    name="reason"
                                    required
                                    maxlength="500"
                                    cols="10"
                                    class="form-control reason-lock-terminal"
                                    placeholder="Nhập lý do khóa Terminal"
                                ></textarea>
                            </div>
                            <div class="row m-t-20">
                                <p class="error-lock-terminal red"></p>
                                <div class="col-xs-6 align-left">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-6 align-right">
                                    <input type="hidden" name="mpTerminalId" id="mpTerminalId" value=""/>
                                    <button name="reopen" value="1" type="button" class="m-0 btn btn-info accept-lock-terminal">Đồng ý</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirmUnLockTerminalModal" tabindex="-1" role="dialog"
             aria-labelledby="confirmUnLockTerminalModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content text-center">
                    <form id="unlockTerminal" method="post" action="{{ route('terminals.unlockTerminal') }}">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">
                                Vui lòng nhập lý do mở khóa Terminal <span class="red">(*)</span>
                            </span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea
                                    name="reason"
                                    required
                                    maxlength="500"
                                    cols="10"
                                    class="form-control reason-unlock-terminal"
                                    placeholder="Nhập lý do mở khóa Terminal"
                                ></textarea>
                            </div>
                            <div class="row m-t-20">
                                <p class="error-unlock-terminal red"></p>
                                <div class="col-xs-6 align-left">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-6 align-right">
                                    <input type="hidden" name="mpTerminalId" id="mpTerminalIdToUnLock" value=""/>
                                    <button name="reopen" value="1" type="button" class="m-0 btn btn-info accept-unlock-terminal">Đồng ý</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="unLockTerminalAlert" tabindex="-1" role="dialog"
             aria-labelledby="unLockTerminalAlert" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content text-center">
                    <div class="modal-header">
                        <span class="modal-title" id="exampleModalLabel" style="color: red !important;">
                            Thông báo
                        </span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                        <p style="color: red !important;">Terminal
                            <span id="unlock-terminal-name"></span> - <span id="unlock-terminal-id"></span>
                            thuộc Merchant <span id="unlock-merchant-name"></span> đang ở trạng thái Đóng. Vui lòng kiểm
                            tra lại trạng thái của Merchant
                        </p>
                        </div>
                        <div class="row m-t-20">
                            <div class="col-xs-12 align-center">
                                <button type="button" class="m-0 btn btn-info" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

