<div class="widget-box transparent">
	<div class="widget-body" style="display: block;">
		<div class="box row">
			<div class="col-sm-6">
				<div class="row">
					<label for="merchantId" class="col-sm-5 control-label">Merchant ID - Tên Merchant <span class="red">(*)</span></label>
					<div class="col-sm-7">
                        <select class="form-control select2-ajax" id="merchant-code" name="merchant_code">
                            @if(old('merchant_code'))
                                @php
                                    $old_merchant = \App\Libraries\BaseFunction::getMerchantByCode(old('merchant_code'))
                                @endphp
                                <option value="{{old('merchant_code')}}" selected="selected">{{ sprintf('%s - %s', $old_merchant['merchant_code'], $old_merchant['merchant_name']) }}</option>
                            @endif
                        </select>
						@if($errors->has('merchant_code'))
							<div class="error">{{ $errors->first('merchant_code') }}</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- Thông tin terminal --}}
<div class="widget-box transparent">
	<div class="widget-header widget-header-flat">
		<h4 class="widget-title lighter">Thông tin Terminal</h4>
		<div class="widget-toolbar">
			<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
		</div>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div class="box row">
				<div class="col-sm-6">
					<div class="row form-group">
						<label for="terminalId" class="col-sm-5 control-label">Terminal ID <span class="red">(*)</span></label>
						<div class="col-sm-7">
							{!! Form::text('terminal_id', null, ['class' => 'form-control', 'id' => 'terminal_id', 'maxlength' => 8]) !!}
							@if($errors->has('terminal_id'))
								<div class="error">{{ $errors->first('terminal_id') }}</div>
							@endif
						</div>
					</div>
					<div class="row form-group">
						<label for="terminalName" class="col-sm-5 control-label">Tên Terminal <span class="red">(*)</span></label>
						<div class="col-sm-7">
							{!! Form::text('terminal_name', null, ['class' => 'form-control', 'maxlength' => 20]) !!}
							@if($errors->has('terminal_name'))
								<div class="error">{{ $errors->first('terminal_name') }}</div>
							@endif
						</div>
					</div>
{{--                    <div class="row form-group">--}}
{{--                        <label for="mst" class="col-sm-5 control-label">MST/GTTT</label>--}}
{{--                        <div class="col-sm-7">--}}
{{--							{!! Form::text('terminal_code', null, ['class' => 'form-control']) !!}--}}
{{--							@if($errors->has('terminal_code'))--}}
{{--								<div class="error">{{ $errors->first('terminal_code') }}</div>--}}
{{--							@endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
					<div class="row form-group">
						<label for="saleProducts" class="col-sm-5 control-label">Sản phẩm kinh doanh <span class="red">(*)</span></label>
						<div class="col-sm-7">
                            <select
                                class="form-control select2"
                                id="terminal_type"
                                name="terminal_type"
                                data-placeholder="-- Chọn --"
                            >
                                <option></option>
                                @foreach($businessProducts as $item)
                                    <option
                                        value="{{ $item['type_code'] }}"
                                        {{ $item['type_code'] === old('terminal_type') ? 'selected' : '' }}
                                    >{{ $item['bp_title'] }}
                                    </option>
                                @endforeach
                            </select>
                            @if($errors->has('terminal_type'))
                                <div class="error">{{ $errors->first('terminal_type') }}</div>
                            @endif
						</div>
					</div>
					<div class="row form-group">
						<label for="product_description" class="col-sm-5 control-label">Mô tả thêm</label>
						<div class="col-sm-7">
							{!! Form::text('product_description', null, ['class' => 'form-control', 'placeholder' => 'Nếu sản phẩm kinh doanh là Khác', 'maxlength' => 200]) !!}
							@if($errors->has('product_description'))
								<div class="error">{{ $errors->first('product_description') }}</div>
							@endif
						</div>
					</div>
					<div class="row form-group">
						<label for="mcc" class="col-sm-5 control-label">MCC VNPAY <span class="red">(*)</span></label>
						<div class="col-sm-7">
                            <select
                                class="form-control select2"
                                id="mcc" name="mcc"
                                data-placeholder="-- Chọn --"
                            >
                                <option></option>
                                @foreach($mcc as $item)
                                    <option
                                        value="{{ $item['type_code'] }}"
                                        {{ $item['type_code'] === old('mcc') ? 'selected' : '' }}
                                    >{{ sprintf('%s - %s', $item['type_code'], $item['brand_name']) }}
                                    </option>
                                @endforeach
                            </select>
							@if($errors->has('mcc'))
								<div class="error">{{ $errors->first('mcc') }}</div>
							@endif
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row form-group">
						<label for="address" class="col-sm-5 control-label">Địa chỉ kinh doanh <span class="red">(*)</span></label>
						<div class="col-sm-7">
							{!! Form::text('terminal_address', null, ['class' => 'form-control', 'maxlength' => 150]) !!}
							@if($errors->has('terminal_address'))
								<div class="error">{{ $errors->first('terminal_address') }}</div>
							@endif
						</div>
					</div>
					<div class="row form-group">
						<label for="province" class="col-sm-5 control-label">Tỉnh/Thành phố <span class="red">(*)</span></label>
						<div class="col-sm-7">
                            <select
                                class="form-control select2"
                                id="terminal_provinces"
                                name="terminal_province_code"
                                data-placeholder="-- Chọn --"
                            >
                                <option></option>
                                @foreach($provinces as $province)
                                    <option value="{{ $province['province_code'] }}">{{ $province['province_name'] }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('terminal_province_code'))
                                <div class="error">{{ $errors->first('terminal_province_code') }}</div>
                            @endif
						</div>
					</div>
					<div class="row form-group">
						<label for="district" class="col-sm-5 control-label">Quận/Huyện <span class="red">(*)</span></label>
						<div class="col-sm-7">
                            <select
                                class="form-control select2"
                                id="terminal_districts"
                                name="terminal_district_code"
                                data-placeholder="-- Chọn --"
                            >
                                <option></option>
                                @foreach($districts as $district)
                                    <option value="{{ $district['district_code'] }}">{{ $district['district_name'] }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('terminal_district_code'))
                                <div class="error">{{ $errors->first('terminal_district_code') }}</div>
                            @endif
						</div>
					</div>
					<div class="row form-group">
						<label for="ward" class="col-sm-5 control-label">Phường/Xã <span class="red">(*)</span></label>
						<div class="col-sm-7">
                            <select
                                class="form-control select2"
                                id="terminal_wards"
                                name="terminal_ward_code"
                                data-placeholder="-- Chọn --"
                            >
                                <option></option>
                                @foreach($wards as $ward)
                                    <option value="{{ $ward['wards_code'] }}">{{ $ward['wards_name'] }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('terminal_ward_code'))
                                <div class="error">{{ $errors->first('terminal_ward_code') }}</div>
                            @endif
						</div>
					</div>
					<div class="row form-group">
						<label for="website" class="col-sm-5 control-label">Website (TMĐT)</label>
						<div class="col-sm-7">
							{!! Form::text('website', null, ['class' => 'form-control', 'maxlength' => 100, 'placeholder' => 'Bắt buộc khi chọn cổng thanh toán']) !!}
							@if($errors->has('website'))
								<div class="error">{{ $errors->first('website') }}</div>
							@endif
						</div>
					</div>
					<div class="row form-group">
						<label for="facebook" class="col-sm-5 control-label">Facebook</label>
						<div class="col-sm-7">
							{!! Form::text('facebook', null, ['class' => 'form-control', 'maxlength' => 100]) !!}
							@if($errors->has('facebook'))
								<div class="error">{{ $errors->first('facebook') }}</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- Hồ sơ đính kèm --}}
<div class="widget-box transparent">
    <div class="widget-header widget-header-flat">
        <h4 class="widget-title lighter">Hồ sơ đính kèm</h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            <div class="box row">
                <div class="col-sm-6">
                    <div class="row form-group">
                        <label for="contractFile" class="col-sm-5 control-label">Biên bản hợp đồng <span class="red">(*)</span></label>
                        <div class="col-sm-7">
							{!! Form::file('file_contract', ['class' => 'form-control']) !!}
							@if($errors->has('file_contract'))
								<div class="error">{{ $errors->first('file_contract') }}</div>
							@endif
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="authLetter" class="col-sm-5 control-label">Giấy ủy quyền</label>
                        <div class="col-sm-7">
							{!! Form::file('file_auth_letter', ['class' => 'form-control']) !!}
							@if($errors->has('file_auth_letter'))
								<div class="error">{{ $errors->first('file_auth_letter') }}</div>
							@endif
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="anotherFile" class="col-sm-5 control-label">Giấy tờ khác</label>
                        <div class="col-sm-7">
							{!! Form::file('file_other', ['class' => 'form-control']) !!}
							@if($errors->has('file_other'))
								<div class="error">{{ $errors->first('file_other') }}</div>
							@endif
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Dịch vụ đăng ký --}}
<div class="widget-box transparent">
    <div class="widget-header widget-header-flat">
        <h4 class="widget-title lighter">Dịch vụ đăng ký</h4>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            <div class="box row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="middle">
							{!! Form::checkbox('register_qr', 1, true, ['class' => 'ace', 'disabled' => true, 'checked' => true]) !!}
                            <span class="lbl"> QRCODE</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="middle">
							{!! Form::checkbox('register_vnpayment', 1, false,['class' => 'ace']) !!}
                            <span class="lbl"> Cổng thanh toán (PG)</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- thông tin ngân hàng thụ hưởng --}}
<div class="widget-box transparent">
	<div class="widget-header widget-header-flat">
		<h4 class="widget-title lighter">Thông tin Tài khoản thụ hưởng</h4>
		<div class="widget-toolbar">
			<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
		</div>
	</div>
	<div class="widget-body" style="display: block;">
		<div class="widget-main">
			<div class="box row">
				<div class="col-sm-6">
					<div class="row form-group">
						<label for="bank_number" class="col-sm-5 control-label">Số tài khoản thụ hưởng <span class="red">(*)</span></label>
						<div class="col-sm-7">
							{!! Form::text('bank_number', null, ['class' => 'form-control', 'maxlength' => 30, 'id' => 'bank_number']) !!}
							@if($errors->has('bank_number'))
								<div class="error">{{ $errors->first('bank_number') }}</div>
							@endif
						</div>
					</div>
					<div class="row form-group">
						<label for="bank_name" class="col-sm-5 control-label">Tên chủ tài khoản thụ hưởng <span class="red">(*)</span></label>
						<div class="col-sm-7">
							{!! Form::text('bank_account', null, ['class' => 'form-control', 'maxlength' => 150, 'id' => 'bank_account']) !!}
							@if($errors->has('bank_account'))
								<div class="error">{{ $errors->first('bank_account') }}</div>
							@endif
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row form-group">
						<label for="bank" class="col-sm-5 control-label">Ngân hàng thụ hưởng <span class="red">(*)</span></label>
						<div class="col-sm-7">
                            <select
                                class="form-control select2"
                                id="bank_code"
                                name="bank_code"
                                data-placeholder="-- Chọn --"
                            >
                                <option></option>
                                @foreach($banks as $bank)
                                    <option value="{{ $bank['bank_code'] }}">{{ $bank['bank_title'] }}</option>
                                @endforeach
                            </select>
							@if($errors->has('bank_code'))
								<div class="error">{{ $errors->first('bank_code') }}</div>
							@endif
						</div>
					</div>
					<div class="row form-group">
						<label for="branch" class="col-sm-5 control-label">Chi nhánh ngân hàng <span class="red">(*)</span></label>
						<div class="col-sm-7">
							{!! Form::text('bank_branch', null, ['class' => 'form-control', 'maxlength' => 100, 'id' => 'bank_branch']) !!}
							@if($errors->has('bank_branch'))
								<div class="error">{{ $errors->first('bank_branch') }}</div>
							@endif
						</div>
					</div>
					<div class="row form-group">
						<label for="receive_method" class="col-sm-5 control-label">Phương thức nhận tiền <span class="red">(*)</span></label>
						<div class="col-sm-7">
							<select
                                name="receive_method"
                                id="receive_method"
                                class="form-control select2"
                                data-placeholder="-- Chọn --"
                            >
                                <option></option>
								<option value="0">Nhận tiền bằng tài khoản ngân hàng</option>
							</select>
                            @if($errors->has('receive_method'))
                                <div class="error">{{ $errors->first('receive_method') }}</div>
                            @endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- thông tin người liên hệ --}}
<div class="widget-box transparent">
	<div class="widget-header widget-header-flat">
		<h4 class="widget-title lighter">Thông tin người liên hệ</h4>
		<div class="widget-toolbar">
			<a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
		</div>
	</div>
	<div class="widget-body" style="display: block;">
		<div class="widget-main">
			<div class="box row">
				<div class="col-sm-6">
					<div class="row form-group">
						<label for="fullname" class="col-sm-5 control-label">Họ và Tên <span class="red">(*)</span></label>
						<div class="col-sm-7">
                            {!! Form::text('terminal_contact_name', null, ['class' => 'form-control']) !!}
                            @if($errors->has('terminal_contact_name'))
                                <div class="error">{{ $errors->first('terminal_contact_name') }}</div>
                            @endif
						</div>
					</div>
					<div class="row form-group">
						<label for="phone" class="col-sm-5 control-label">Số điện thoại <span class="red">(*)</span></label>
						<div class="col-sm-7">
                            {!! Form::text('terminal_contact_phone', null, ['class' => 'form-control']) !!}
                            @if($errors->has('terminal_contact_phone'))
                                <div class="error">{{ $errors->first('terminal_contact_phone') }}</div>
                            @endif
						</div>
					</div>
					<div class="row form-group">
						<label for="email" class="col-sm-5 control-label">Email liên hệ <span class="red">(*)</span></label>
						<div class="col-sm-7">
                            {!! Form::email('terminal_contact_email', null, ['class' => 'form-control']) !!}
                            @if($errors->has('terminal_contact_email'))
                                <div class="error">{{ $errors->first('terminal_contact_email') }}</div>
                            @endif
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label class="middle">
							{!! Form::checkbox('create_terminal_app', 1, false,['class' => 'ace', 'id' => 'has-terminal-app-checkbox', 'disabled' => false]) !!}
							<span class="lbl"> Tạo tài khoản đăng nhập Terminal App</span>
						</label>
					</div>
                    <div class="row form-group" id="terminal-app-user-area" style="display: none;">
                        <label for="phone" class="col-sm-5 control-label">Số điện thoại đăng nhập ter app <span class="red">(*)</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('terminal_app_user', null, [
                                'class' => 'form-control',
                                'id' => 'terminal-app-user',
                                'maxlength' => 10,
                                'placeholder' => '10 số, là duy nhất trên hệ thống'
                            ]) !!}
                            @if($errors->has('terminal_app_user'))
                                <div class="error">{{ $errors->first('terminal_app_user') }}</div>
                            @endif
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="text-center">
	<a href="{{ route('terminals.index') }}" class="btn btn-default">Quay lại</a>
	<button class="btn btn-primary" type="submit">Lưu</button>
	<button class="btn btn-success" type="submit" name="send_request" value="1">Gửi duyệt</button>
</div>

@section('scripts')
    <script>
        $(function () {
            $(document).on('change', ':file', function () {
                if (this.files[0]) {
                    var file = this.files[0],
                        extension = file.name.substr((file.name.lastIndexOf('.') + 1)),
                        input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    if (['pdf', 'png', 'jpg', 'rar'].indexOf(extension.toLowerCase()) < 0) {
                        input.trigger('fileTypeError');
                    }
                    else if (file.size > 2*5242880) {
                        input.trigger('fileSizeError');
                    }
                    else {
                        input.trigger('fileSelected', [numFiles, label]);
                    }
                }
            });


            $(document).ready(function () {
                $(':file').on('fileTypeError', function (event) {
                    $(this).val("");
                    $(this).parents('.input-group').find(':text').val('');
                    $(this).parents('.form-group').addClass('has-error').find('.help-block').text('File có định dạng pdf, png, jpg, rar');
                });
                $(':file').on('fileSizeError', function (event) {
                    $(this).val("");
                    $(this).parents('.input-group').find(':text').val('');
                    $(this).parents('.form-group').addClass('has-error').find('.help-block').text('Dung lượng file tối đa là 10MB');
                });
                $(':file').on('fileSelected', function (event, numFiles, label) {
                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    }
                    $(this).parents('.form-group').removeClass('has-error').find('.help-block').text('');

                });
                if ("{{old('terminal_province_code')}}") {
                    $("#terminal_provinces").val("{{ old('terminal_province_code') }}").trigger('change', function () {
                        $("#terminal_districts").val("{{ old('terminal_district_code') }}").trigger('change', function () {
                            $("#terminal_wards").val("{{ old('terminal_ward_code') }}").trigger('change')
                        })
                    });
                };
                if ("{{ old('bank_code') }}") {
                    $("#bank_code").val("{{ old('bank_code') }}").trigger('change')
                };
                if ("{{ old('receive_method') }}") {
                    $("#receive_method").val("{{ old('receive_method') }}").trigger('change')
                };
                $('.select2-ajax').select2({
                    placeholder: "-- Chọn merchant --",
                    minimumInputLength: 2,
                    ajax: {
                        url: '/terminals/ajaxListMerchant',
                        dataType: 'json',
                        data: function (params) {
                            return {
                                q: $.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });

                if ($('#has-terminal-app-checkbox').is(":checked")) {
                    $('#terminal-app-user-area').css('display', 'block');
                    $('#terminal-app-user').attr('disabled', false);
                }

                $('#has-terminal-app-checkbox').on('click', function (e) {
                    if ($(this).is(":checked")) {
                        $('#terminal-app-user-area').css('display', 'block');
                        $('#terminal-app-user').attr('disabled', false);
                    }
                    else {
                        $('#terminal-app-user-area').css('display', 'none');
                        $('#terminal-app-user').attr('disabled', true);
                    }
                })
            });

        });
    </script>
    <script type="text/javascript">
        $("#terminal_provinces").change(function(event, callback){
            $('#terminal_districts').html("");
            $('#terminal_wards').html("");
            $.ajax({
                url: "{{ url('/api/get_districts_from_province_code') }}?province_code=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#terminal_districts').html(data.html);
                    if (callback){
                        callback();
                    }
                    else {
                        $('#terminal_districts').trigger('change.select2');

                        setTimeout($("#terminal_districts").val($("#terminal_districts option:first").val()).change(), 3000);
                    }
                }
            });
        });
        $("#terminal_districts").change(function(event, callback){
            $('#terminal_wards').html("");
            $.ajax({
                url: "{{ url('/api/get_wards_from_district_code_and_province_code') }}?district_code=" + $(this).val()+'&province_code='+ $("#terminal_provinces").val(),
                method: 'GET',
                success: function(data) {
                    $('#terminal_wards').html(data.html);
                    if (callback){
                        callback();
                    }
                    else {
                        $('#terminal_wards').trigger('change.select2');
                    }
                }
            });
        });
        $("#departments").change(function(){
            $.ajax({
                url: "{{ url('/api/get_staff_from_department') }}?department_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#staffs').html(data.html);
                    $('#staffs').trigger('change.select2');

                    setTimeout($("#staffs").val($("#staffs option:first").val()).change(),3000);

                }
            });
        });

        $('#merchant-code').change(function () {
            $.ajax({
                url: "{{ url('/api/get_latest_terminal_of_merchant') }}?merchant_code=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#terminal_id').val(data)
                }
            });
        })

        $('#bank_number').change(function () {
            if ($('#merchant-code').val() && $(this).val().trim().length >= 9){
                $.ajax({
                    url: "{{ url('/terminals/ajaxFindBankAccount') }}?merchant_code=" + $('#merchant-code').val() + '&q=' + $(this).val().trim(),
                    method: 'GET',
                    success: function(data) {
                        if(data.length){
                            let value = data[0];
                            $('#bank_account').val(value.bank_account);
                            $('#bank_branch').val(value.bank_branch);
                            $('#bank_code').val(value.bank_code).trigger('change');
                            $('#receive_method').val(0).trigger('change');
                        }
                    }
                });
            }
        })
		$(function () {
			if ($('#merchant-code').val()){
			    @if (old('terminal_id'))
                    $('#terminal_id').val('{{old('terminal_id')}}')
                @else
                $.ajax({
                    url: "{{ url('/api/get_latest_terminal_of_merchant') }}?merchant_code=" + $('#merchant-code').val(),
                    method: 'GET',
                    success: function(data) {
                        $('#terminal_id').val(data)
                    }
                });
                @endif
			}
        })
    </script>
@endsection
