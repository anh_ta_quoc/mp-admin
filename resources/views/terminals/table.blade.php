@section('css')
    @include('layouts_mms.datatables_css')
    @include('layouts_mms.daterange_picker_css')
@endsection

{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}

@section('scripts')
    @include('layouts_mms.daterange_picker_js', ['init_id' => ['date_range_search']])
    @include('layouts_mms.datatables_js')
    {!! $dataTable->scripts() !!}
    <script>
        var queryTypingTimer;
        var doneTypingInterval = 1000;
        var $input = $('#filter_query');
        $input.on('keyup', function () {
            clearTimeout(queryTypingTimer);
            queryTypingTimer = setTimeout(doneFilterTyping, doneTypingInterval);
        });
        $input.on('keydown', function () {
            clearTimeout(queryTypingTimer);
        });
        function doneFilterTyping () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].search($input.val()).draw();
            }
        }
        $('#filter_status').on('change', function () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        });
        $('#merchant_code').on('change', function () {
            if (window.LaravelDataTables && window.LaravelDataTables['dataTableBuilder']){
                window.LaravelDataTables['dataTableBuilder'].draw();
            }
        });
        $(document).ready(function () {
            $('.select2-ajax').select2({
                placeholder: "-- Chọn merchant --",
                minimumInputLength: 2,
                ajax: {
                    url: '/terminals/ajaxListMerchant',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term),
                            action: 'search'
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
    <script type="text/javascript">
        function handleLockTerminal(mpTerminalId) {
            if (mpTerminalId && $('#mpTerminalId').length > 0) {
                $('#mpTerminalId').val(mpTerminalId);
                $('#confirmLockTerminalModal').modal('show');
            }
        };

        function handleUnLockTerminal(mpTerminalId) {
            if (mpTerminalId && $('#mpTerminalId').length > 0) {
                $.ajax({
                    url: "{{ url('/api/get_combine_info_mer_ter') }}?mp_terminal_id=" + mpTerminalId,
                    method: 'GET',
                    success: function(data) {
                        var res = JSON.parse(data);
                        if (res.status == 'OK') {
                            $('#unlock-terminal-name').html(res.data.terminal_name);
                            $('#unlock-terminal-id').html(res.data.terminal_id);
                            $('#unlock-merchant-name').html(res.data.merchant_name);
                            $('#unLockTerminalAlert').modal('show');
                        }
                        else {
                            $('#mpTerminalIdToUnLock').val(mpTerminalId);
                            $('#confirmUnLockTerminalModal').modal('show');
                        }
                    }
                });
            }
        }

        $('.reason-lock-terminal').on('change', function () {
            $('.error-lock-terminal').html("");
        })

        $('.accept-lock-terminal').on('click', function (e) {
            $(this).attr('disabled', true);
            if($('.reason-lock-terminal').val().trim().length === 0) {
                $(this).attr('disabled', false);
                $('.error-lock-terminal').html("Vui lòng nhập lý do");
                e.preventDefault();
            }
            else {
                $('#lockTerminal').submit();
            }
        })

        $('.reason-unlock-terminal').on('change', function () {
            $('.error-unlock-terminal').html("");
        })

        $('.accept-unlock-terminal').on('click', function (e) {
            $(this).attr('disabled', true);
            if($('.reason-unlock-terminal').val().trim().length === 0) {
                $(this).attr('disabled', false);
                $('.error-unlock-terminal').html("Vui lòng nhập lý do");
                e.preventDefault();
            }
            else {
                $('#unlockTerminal').submit();
            }
        })
    </script>
    <script type="text/javascript">
        $('.hsub').on('click', function (e) {
            var linkRedirect = $("a",this).attr('href');
            e.preventDefault();
            $.ajax({
                url: "{{ url('/api/clear_filter') }}?module=terminal",
                method: 'GET',
                success: function(data) {
                    if (data) {
                        try {
                            var resStatus = JSON.parse(data);
                            if (resStatus.hasOwnProperty('status') && resStatus.status === 'OK') {
                                window.location.href = linkRedirect
                            }
                        }
                        catch (e) {

                        }
                    }
                }
            });
        })
    </script>
@endsection
