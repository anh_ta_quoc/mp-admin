@extends('layouts_mms.app')
@section('css')
    <link rel="stylesheet" href="{{asset('css/terminal.css')}}"/>
@endsection

@section('content')
    @include('flash::message')
    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('coreui-templates::common.errors')
            @include('layouts_mms.page_header', ['header' => 'Thông tin terminal'])
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab_terminalInfo">Thông tin Terminal</a></li>
                    </ul>
                    <div class="tab-content" style="padding: 5px 0">
                        <div id="tab_terminalInfo" class="tab-pane fade in active">
                            {{-- thông tin terminal --}}
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="box row">
                                        <div class="col-sm-6">
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Merchant - Tên Merchant</label>
                                                <label class="col-sm-7 control-label"><b>
                                                        {{ sprintf('%s - %s', $store->merchant_code, $store->merchant_name) }}
                                                    </b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Terminal ID</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->terminal_id}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Tên Terminal</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->terminal_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">MST/CMND</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->merchant_code}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Loại hình doanh nghiệp</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->type_business_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Sản phẩm kinh doanh</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->terminal_type_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Mô tả thêm</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->terminal_description}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">MCC VNPAY</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->mcc_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Trạng thái Terminal</label>
                                                <div class="col-sm-7">
                                                    @if($store->status == 0 && !$store->merchant_file_contract)
                                                        <label class="{{ \App\Libraries\BaseFunction::getStoreStatusClass()[99] }}">
                                                            {{ \App\Libraries\BaseFunction::getStoreStatus()[99] }}
                                                        </label>
                                                    @else
                                                        <label class="{{ \App\Libraries\BaseFunction::getStoreStatusClass()[$store->status] }}">
                                                            {{ \App\Libraries\BaseFunction::getStoreStatus()[$store->status] }}
                                                        </label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Địa chỉ kinh doanh</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->terminal_address}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Tỉnh/Thành phố</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->terminal_province_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Quận/Huyện</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->terminal_district_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Phường/Xã</label>
                                                <label class="col-sm-7 control-label"><b>{{$store->terminal_ward_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Website TMĐT</label>
                                                @if ($store->terminal_website)
                                                    <a href="{{$store->terminal_website}}">
                                                        <label class="col-sm-7 control-label"><b>{{$store->terminal_website}}</b></label>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Facebook</label>
                                                @if ($store->facebook)
                                                    <a href="{{$store->facebook}}">
                                                        <label class="col-sm-7 control-label"><b>{{$store->facebook}}</b></label>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Ngày đăng ký</label>
                                                <label class="col-sm-7 control-label"><b>{{date('d/m/Y H:i:s', strtotime($store->created_at))}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Ngày duyệt/Từ chối</label>
                                                <label class="col-sm-7 control-label"><b>{{date('d/m/Y H:i:s', strtotime($store->updated_at))}}</b></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Hồ sơ đính kèm --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Hồ sơ đính kèm</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Biên bản/hợp đồng</label>
                                                    <label class="col-sm-7 control-label">

                                                    </label>
                                                </div>

                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Giấy ủy quyền</label>
                                                    <label class="col-sm-7 control-label">

                                                    </label>
                                                </div>

                                                <div class="row">
                                                    <label class="col-sm-5 control-label">Giấy tờ khác</label>
                                                    <label class="col-sm-7 control-label">

                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Đăng ký dịch vụ --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Đăng ký dịch vụ</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="middle">
                                                        <input class="ace" type="checkbox" id="qr_code" name="qr_code" checked disabled>
                                                        <span class="lbl"> QRCODE</span>
                                                    </label>
                                                </div>
                                                <div class="group-checkbox">
                                                    <label class="middle">
                                                        <input class="ace" type="checkbox" id="payment_gate" name="payment_gate" @if($store->terminal_register_vnpayment) checked @endif disabled>
                                                        <span class="lbl"> Cổng thanh toán (PG)</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- thông tin ngân hàng thụ hưởng --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Thông tin Tài khoản thụ hưởng</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Số tài khoản thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b>{{$store->bank_number}}</b></label>
                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Chủ tài khoản thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b>{{$store->bank_account}}</b></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Ngân hàng thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b>{{ $store->bank_title }}</b></label>
                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Chi nhánh ngân hàng</label>
                                                    <label class="col-sm-7 control-label"><b>{{$store->bank_branch}}</b></label>
                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Phương thức nhận tiền</label>
                                                    <label class="col-sm-7 control-label"><b>{{\App\MpTerminal::RECEIVE_METHOD[0]}}</b></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- thông tin ví --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Thông tin Ví khách hàng</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Tên ví thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b></b></label>
                                                </div>
                                                <div class="row">
                                                    <label class="col-sm-5 control-label">Số Ví thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b></b></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- thông tin người liên hệ terminal --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Thông tin người liên hệ terminal</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Họ và tên</label>
                                                    <label class="col-sm-7 control-label"><b>{{$store->terminal_contact_name}}</b></label>
                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Số điện thoại</label>
                                                    <label class="col-sm-7 control-label"><b>{{$store->terminal_contact_phone}}</b></label>
                                                </div>
                                                <div class="row">
                                                    <label class="col-sm-5 control-label">Email</label>
                                                    <label class="col-sm-7 control-label"><b>{{$store->terminal_contact_email}}</b></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($store->terminal_app_user)
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Thông tin tài khoản đăng nhập Ter</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Số điện thoại</label>
                                                    <label class="col-sm-7 control-label"><b>{{$store->terminal_app_user}}</b></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="text-center mt-2">
                                <a href="{{ route('stores.show', ['id' => $store->id]) }}" class="btn btn-default">Về chi tiết Merchant</a>
                                @if(Auth::user()->hasrole('Sale admin'))
                                    @if(in_array($store->status, [
                                       \App\Libraries\BaseFunction::STATUS_DRAFT,
                                       \App\Libraries\BaseFunction::STATUS_SALE_REOPEN_TO_EDIT,
                                       \App\Libraries\BaseFunction::STATUS_CONTROL_REOPEN_TO_EDIT,
                                       \App\Libraries\BaseFunction::STATUS_INIT
                                    ]))
                                        <a href="{{ route('terminals.editFirstTerminal', ['id' => $store->id]) }}" class="btn btn-primary">Sửa thông tin</a>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ((Auth::user()->hasrole('Kinh doanh') && $store->status === \App\MpTerminal::STATUS_SALE_ADMIN_REQUESTED)
    || (Auth::user()->hasrole('Đối soát') && $store->status === \App\MpTerminal::STATUS_SALE_APPROVED) )
        <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="rejectTerminalForm" method="post" action="{{ route('terminals.reject', ['id' => $store->id]) }}">
                        @csrf
                        <div class="modal-header">
                            <span class="modal-title" id="exampleModalLabel">Từ chối duyệt hoặc mở lại hồ sơ</span>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea name="reason" required maxlength="500" cols="2" class="form-control" placeholder="Nhập lý do từ chối duyệt"></textarea>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-xs-4">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-4 align-center">
                                    <button name="reject" value="1" type="submit" class="m-0 btn btn-danger">Xác nhận từ chối</button>
                                </div>
                                <div class="col-xs-4 align-right">
                                    <button name="reopen" value="1" type="submit" class="m-0 btn btn-info">Mở lại hồ sơ</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection
