@extends('layouts_mms.app')
@section('css')
    <link rel="stylesheet" href="{{asset('css/terminal.css')}}"/>
@endsection

@section('content')
    @include('flash::message')
    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('coreui-templates::common.errors')
            @include('layouts_mms.page_header', ['header' => 'Thông tin terminal'])
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab_terminalInfo">Thông tin Terminal</a></li>
                        <li><a data-toggle="tab" href="#tab_approveHistory">Lịch sử cập nhật thông tin</a></li>
                    </ul>
                    <div class="tab-content" style="padding: 5px 0">
                        <div id="tab_terminalInfo" class="tab-pane fade in active">
                            {{-- thông tin terminal --}}
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="box row">
                                        <div class="col-sm-6">
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Merchant - Tên Merchant</label>
                                                <label class="col-sm-7 control-label"><b>
                                                        {{ sprintf('%s - %s', $terminal->merchant_code, \App\Libraries\BaseFunction::getMerchantByCode($terminal->merchant_code)['merchant_name']) }}
                                                    </b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Terminal ID</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->terminal_id}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Tên Terminal</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->terminal_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">MST/CMND</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->terminal_code}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Loại hình doanh nghiệp</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->type_business_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Sản phẩm kinh doanh</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->terminal_type_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Mô tả thêm</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->product_description}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">MCC VNPAY</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->mcc_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Trạng thái Terminal</label>
                                                <div class="col-sm-7">
                                                    {!! $terminal->getTerminalStatusLabel() !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Địa chỉ kinh doanh</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->terminal_address}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Tỉnh/Thành phố</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->terminal_province_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Quận/Huyện</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->terminal_district_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Phường/Xã</label>
                                                <label class="col-sm-7 control-label"><b>{{$terminal->terminal_ward_name}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Website TMĐT</label>
                                                @if ($terminal->website)
                                                    <a href="{{$terminal->website}}">
                                                        <label class="col-sm-7 control-label"><b>{{$terminal->website}}</b></label>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Facebook</label>
                                                @if ($terminal->facebook)
                                                    <a href="{{$terminal->facebook}}">
                                                        <label class="col-sm-7 control-label"><b>{{$terminal->facebook}}</b></label>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Ngày đăng ký</label>
                                                <label class="col-sm-7 control-label"><b>{{date('d/m/Y H:i:s', strtotime($terminal->created_at))}}</b></label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-5 control-label">Ngày duyệt/Từ chối</label>
                                                <label class="col-sm-7 control-label"><b>{{date('d/m/Y H:i:s', strtotime($terminal->updated_at))}}</b></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Hồ sơ đính kèm --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Hồ sơ đính kèm</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Biên bản/hợp đồng</label>
                                                    <label class="col-sm-7 control-label">
                                                        @if ($terminal->file_contract)
                                                            @if(strpos($terminal->file_contract, 'http') > -1)
                                                                <a href="{{ $terminal->file_contract }}" download>
                                                                    <b>Biên bản hợp đồng</b>
                                                                </a>
                                                            @else
                                                                @if(strpos($terminal->src, 'BATCH') !== false)
                                                                    <a href="{{'/api_uploads/terminal_batch/' . $terminal->merchant_code . '/' . $terminal->file_contract}}" download>
                                                                        <b>Biên bản hợp đồng</b>
                                                                    </a>
                                                                @else
                                                                    <a href="{{ Storage::url($terminal->file_contract) }}" download>
                                                                        <b>Biên bản hợp đồng</b>
                                                                    </a>
                                                                @endif
                                                            @endif
                                                        @endif
                                                    </label>
                                                </div>

                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Giấy ủy quyền</label>
                                                    <label class="col-sm-7 control-label">
                                                        @if ($terminal->file_auth_letter)
                                                            @if(strpos($terminal->src, 'BATCH') !== false)
                                                                <a href="{{'/api_uploads/terminal_batch/' . $terminal->merchant_code . '/' . $terminal->file_auth_letter}}" download>
                                                                    <b>Giấy ủy quyền</b>
                                                                </a>
                                                            @else
                                                            <a href="{{ Storage::url($terminal->file_auth_letter) }}" download>
                                                                <b>Giấy ủy quyền</b>
                                                            </a>
                                                            @endif
                                                        @endif
                                                    </label>
                                                </div>

                                                <div class="row">
                                                    <label class="col-sm-5 control-label">Giấy tờ khác</label>
                                                    <label class="col-sm-7 control-label">
                                                        @if ($terminal->file_other)
                                                            <a href="{{ Storage::url($terminal->file_other) }}" download>
                                                                <b>Giấy tờ khác</b>
                                                            </a>
                                                        @endif
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Đăng ký dịch vụ --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Đăng ký dịch vụ</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="middle">
                                                        <input class="ace" type="checkbox" id="qr_code" name="qr_code" @if($terminal->register_qr == 1) checked @endif disabled>
                                                        <span class="lbl"> QRCODE</span>
                                                    </label>
                                                </div>
                                                <div class="group-checkbox">
                                                    <label class="middle">
                                                        <input class="ace" type="checkbox" id="payment_gate" name="payment_gate" @if($terminal->register_vnpayment) checked @endif disabled>
                                                        <span class="lbl"> Cổng thanh toán (PG)</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- thông tin ngân hàng thụ hưởng --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Thông tin Tài khoản thụ hưởng</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Số tài khoản thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b>{{$terminal->bank_number}}</b></label>
                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Chủ tài khoản thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b>{{$terminal->bank_account}}</b></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Ngân hàng thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b>{{$terminal->bank_title }}</b></label>
                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Chi nhánh ngân hàng</label>
                                                    <label class="col-sm-7 control-label"><b>{{$terminal->bank_branch}}</b></label>
                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Phương thức nhận tiền</label>
                                                    <label class="col-sm-7 control-label"><b>{{$terminal->receive_method_name}}</b></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- thông tin ví --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Thông tin Ví khách hàng</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Tên ví thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b></b></label>
                                                </div>
                                                <div class="row">
                                                    <label class="col-sm-5 control-label">Số Ví thụ hưởng</label>
                                                    <label class="col-sm-7 control-label"><b></b></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- thông tin người liên hệ terminal --}}
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter text-uppercase">Thông tin người liên hệ terminal</h4>
                                    <div class="widget-toolbar">
                                        <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: block;">
                                    <div class="widget-main">
                                        <div class="box row">
                                            <div class="col-sm-6">
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Họ và tên</label>
                                                    <label class="col-sm-7 control-label"><b>{{$terminal->terminal_contact_name}}</b></label>
                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-sm-5 control-label">Số điện thoại</label>
                                                    <label class="col-sm-7 control-label"><b>{{$terminal->terminal_contact_phone}}</b></label>
                                                </div>
                                                <div class="row">
                                                    <label class="col-sm-5 control-label">Email</label>
                                                    <label class="col-sm-7 control-label"><b>{{$terminal->terminal_contact_email}}</b></label>
                                                </div>
                                            </div>
                                            @if($terminal->terminal_app_user)
                                                <div class="col-sm-6">
                                                    <div class="row form-group">
                                                        <label class="col-sm-5 control-label">Số điện thoại ter app</label>
                                                        <label class="col-sm-7 control-label"><b>{{$terminal->terminal_app_user}}</b></label>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mt-2">
                                <a href="{{ route('terminals.index') }}" class="btn btn-default">Quay lại</a>
                                @if(in_array($terminal->status, [\App\MpTerminal::STATUS_INIT, \App\MpTerminal::STATUS_REOPEN, \App\MpTerminal::STATUS_REVIEW_REOPEN, \App\MpTerminal::STATUS_REVIEW_APPROVED])
                                    && Auth::user()->hasrole('Sale admin') && $terminal->lock_edit != 1
                                )
                                    <a href="{{ route('terminals.edit', ['id' => $terminal->id]) }}" class="btn btn-primary">Sửa thông tin</a>
                                @endif

                                @if (Auth::user()->hasrole('Sale admin'))
                                    @if(in_array($terminal->status, [\App\MpTerminal::STATUS_INIT, \App\MpTerminal::STATUS_REOPEN, \App\MpTerminal::STATUS_REVIEW_REOPEN]) && $terminal->lock_edit != 1)
                                        <a class="btn btn-success" href="{{route('terminals.sendRequest', ['id' => $terminal->id])}}">Gửi duyệt</a>
                                    @endif
                                @endif
                                @if(Auth::user()->hasrole('Kinh doanh'))
                                    @if ($terminal->status == \App\MpTerminal::STATUS_SALE_ADMIN_REQUESTED)
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#rejectModal">Từ chối</button>
                                        <a class="btn btn-success" href="{{route('terminals.approve', ['id' => $terminal->id])}}">Duyệt</a>
                                    @endif
                                @endif
                                @if (Auth::user()->hasrole('Đối soát'))
                                    @if ($terminal->status == \App\MpTerminal::STATUS_SALE_APPROVED)
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#rejectModal">Từ chối</button>
                                        <a class="btn btn-success" href="{{route('terminals.approve', ['id' => $terminal->id])}}">Duyệt</a>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div id="tab_approveHistory" class="tab-pane fade in" style="padding:10px 20px;">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th>Tài khoản</th>
                                    <th>Thời gian cập nhật</th>
                                    <th>Nghiệp vụ</th>
                                    <th width="450">Ghi chú</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($histories as $idx => $history)
                                    <tr>
                                        <td class="text-center">{{$idx + 1}}</td>
                                        <td>{{$history->staff_name}}</td>
                                        <td>{{date('d/m/Y H:i:s', strtotime($history->updated_at))}}</td>
                                        <td>{{$history->type_name}}</td>
                                        <td>{{$history->reason}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ((Auth::user()->hasrole('Kinh doanh') && $terminal->status === \App\MpTerminal::STATUS_SALE_ADMIN_REQUESTED)
    || (Auth::user()->hasrole('Đối soát') && $terminal->status === \App\MpTerminal::STATUS_SALE_APPROVED) )
        <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="rejectTerminalForm" method="post" action="{{ route('terminals.reject', ['id' => $terminal->id]) }}">
                        @csrf
                        <div class="modal-header">
                            @if(Auth::user()->hasrole('Đối soát'))
                                <span class="modal-title" id="exampleModalLabel">Mở lại hồ sơ</span>
                            @else
                                <span class="modal-title" id="exampleModalLabel">Từ chối duyệt hoặc mở lại hồ sơ</span>
                            @endif
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                @if(Auth::user()->hasrole('Đối soát'))
                                    <textarea name="reason" required maxlength="500" cols="2" class="form-control" placeholder="Nhập lý do mở lại hồ sơ"></textarea>
                                @else
                                    <textarea name="reason" required maxlength="500" cols="2" class="form-control" placeholder="Nhập lý do từ chối duyệt hoặc mở lại hồ sơ"></textarea>
                                @endif
                            </div>
                            <div class="row m-t-20">
                                <div class="col-xs-4">
                                    <button type="button" class="m-0 btn btn-secondary" data-dismiss="modal">Hủy</button>
                                </div>
                                <div class="col-xs-4 align-center">
                                    @if($terminal->status === \App\MpTerminal::STATUS_SALE_ADMIN_REQUESTED)
                                    <button name="reject" value="1" type="submit" class="m-0 btn btn-danger">Xác nhận từ chối</button>
                                    @endif
                                </div>
                                <div class="col-xs-4 align-right">
                                    <button name="reopen" value="1" type="submit" class="m-0 btn btn-info">Mở lại hồ sơ</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection
