@extends('layouts_mms.app')
@section('content')
    @include('layouts_mms.page_header', ['header' => 'Sửa terminal'])
    <div class="row">
        {!! Form::open(['route' => ['terminals.updateFirstTerminal', $store->id], 'method' => 'patch', 'files' => true]) !!}
        @include('flash::message')

        <div class="widget-box transparent">
            <div class="widget-body" style="display: block;">
                <div class="box row">
                    <div class="col-sm-6">
                        <div class="row">
                            <label for="merchantId" class="col-sm-5 control-label">Merchant ID - Tên Merchant <span class="red">(*)</span></label>
                            <div class="col-sm-7">
                                <input type="hidden" name="id" value="{{$store->id}}">
                                <input type="text" class="form-control" disabled value="{{ sprintf('%s - %s', $store->merchant_code, $store->merchant_name) }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Thông tin terminal --}}
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">Thông tin Terminal</h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="box row">
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <label for="terminalId" class="col-sm-5 control-label">Terminal ID <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::text('terminal_id', $store->terminal_id, ['class' => 'form-control',
                                        'readonly' => $store->mms_status >= \App\Libraries\BaseFunction::STATUS_MMS_CREATED,
                                        'id' => 'terminal_id']) !!}
                                    @if($errors->has('terminal_id'))
                                        <div class="error">{{ $errors->first('terminal_id') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="terminalName" class="col-sm-5 control-label">Tên Terminal<span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::text('terminal_name', $store->terminal_name, [
                                        'class' => 'form-control',
                                        'readonly' => $store->mms_status >= \App\Libraries\BaseFunction::STATUS_MMS_CREATED,
                                    ]) !!}
                                    @if($errors->has('terminal_name'))
                                        <div class="error">{{ $errors->first('terminal_name') }}</div>
                                    @endif
                                </div>
                            </div>
                            {{--                    <div class="row form-group">--}}
                            {{--                        <label for="mst" class="col-sm-5 control-label">MST/GTTT</label>--}}
                            {{--                        <div class="col-sm-7">--}}
                            {{--                            {!! Form::text('terminal_code', $store->terminal_code, ['class' => 'form-control', 'disabled' => !in_array($store->status, [\App\MpTerminal::STATUS_INIT, \App\MpTerminal::STATUS_REOPEN, \App\MpTerminal::STATUS_REVIEW_REOPEN])]) !!}--}}
                            {{--                            @if($errors->has('terminal_code'))--}}
                            {{--                                <div class="error">{{ $errors->first('terminal_code') }}</div>--}}
                            {{--                            @endif--}}
                            {{--                        </div>--}}
                            {{--                    </div>--}}
                            <div class="row form-group">
                                <label for="saleProducts" class="col-sm-5 control-label">Sản phẩm kinh doanh <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::select('terminal_type', $businessProducts->pluck('bp_title','type_code'), [$store->terminal_type], ['class' => 'form-control select2']) !!}
                                    @if($errors->has('terminal_type'))
                                        <div class="error">{{ $errors->first('terminal_type') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="description" class="col-sm-5 control-label">Mô tả thêm</label>
                                <div class="col-sm-7">
                                    {!! Form::text('terminal_description', $store->terminal_description, ['class' => 'form-control']) !!}
                                    @if($errors->has('terminal_description'))
                                        <div class="error">{{ $errors->first('terminal_description') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="terminal_mcc" class="col-sm-5 control-label">MCC VNPAY <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    <?php
                                    $selectedMcc = old('terminal_mcc') ?? $store->terminal_mcc;
                                    ?>
                                    <select class="form-control select2" id="mcc" name="terminal_mcc">
                                        <option value="">-- Chọn --</option>
                                        @foreach($mcc as $item)
                                            <option
                                                value="{{ $item['type_code'] }}"
                                                {{ $item['type_code'] === $selectedMcc ? 'selected' : '' }}
                                            >{{ sprintf('%s - %s', $item['type_code'], $item['brand_name']) }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('terminal_mcc'))
                                        <div class="error">{{ $errors->first('terminal_mcc') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <label for="address" class="col-sm-5 control-label">Địa chỉ kinh doanh</label>
                                <div class="col-sm-7">
                                    {!! Form::text('terminal_address', $store->terminal_address, ['class' => 'form-control']) !!}
                                    @if($errors->has('terminal_address'))
                                        <div class="error">{{ $errors->first('terminal_address') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="province" class="col-sm-5 control-label">Tỉnh/Thành phố <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::select('terminal_province_code', $provinces->pluck('province_name','province_code'),[], ['class' => 'form-control select2','id'=>'terminal_provinces']) !!}
                                    @if($errors->has('terminal_province_code'))
                                        <div class="error">{{ $errors->first('terminal_province_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="district" class="col-sm-5 control-label">Quận/Huyện <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::select('terminal_district_code',$districts->pluck('district_name','district_code') ,[], ['class' => 'form-control select2','id'=>'terminal_districts']) !!}
                                    @if($errors->has('terminal_district_code'))
                                        <div class="error">{{ $errors->first('terminal_district_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="ward" class="col-sm-5 control-label">Phường/Xã <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::select('terminal_ward_code', $wards->pluck('wards_name','wards_code'), [], ['class' => 'form-control select2','id'=>'terminal_wards']) !!}
                                    @if($errors->has('terminal_ward_code'))
                                        <div class="error">{{ $errors->first('terminal_ward_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="terminal_website" class="col-sm-5 control-label">Website (TMĐT)</label>
                                <div class="col-sm-7">
                                    {!! Form::text('terminal_website', $store->terminal_website, ['class' => 'form-control', 'maxlength' => 100, 'placeholder' => 'Bắt buộc khi chọn cổng thanh toán']) !!}
                                    @if($errors->has('terminal_website'))
                                        <div class="error">{{ $errors->first('terminal_website') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Dịch vụ đăng ký --}}
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">Dịch vụ đăng ký</h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="box row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="middle">
                                    {!! Form::checkbox('register_qr', 1, true, ['class' => 'ace', 'disabled' => true, 'checked' => true]) !!}
                                    <span class="lbl"> QRCODE</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <?php
                                    $selectedRegisterVnpayment = old('terminal_register_vnpayment', $store->terminal_register_vnpayment);
                                ?>
                                <label class="middle">
                                    {!! Form::checkbox('terminal_register_vnpayment', 1, $selectedRegisterVnpayment ? true : false,['class' => 'ace']) !!}
                                    <span class="lbl"> Cổng thanh toán (PG)</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- thông tin ngân hàng thụ hưởng --}}
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">Thông tin Tài khoản thụ hưởng</h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                </div>
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main">
                    <div class="box row">
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <label for="bank_number" class="col-sm-5 control-label">Số tài khoản thụ hưởng <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::text('bank_number', $store->bank_number, ['class' => 'form-control']) !!}
                                    @if($errors->has('bank_number'))
                                        <div class="error">{{ $errors->first('bank_number') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="bank_name" class="col-sm-5 control-label">Tên chủ tài khoản thụ hưởng <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::text('bank_account', $store->bank_account, ['class' => 'form-control']) !!}
                                    @if($errors->has('bank_account'))
                                        <div class="error">{{ $errors->first('bank_account') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <label for="bank" class="col-sm-5 control-label">Ngân hàng thụ hưởng <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::select('bank_code', $banks->pluck('bank_title','bank_code'), [$store->bank_code], ['class' => 'form-control select2']) !!}
                                    @if($errors->has('bank_code'))
                                        <div class="error">{{ $errors->first('bank_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="branch" class="col-sm-5 control-label">Chi nhánh ngân hàng <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::text('bank_branch', $store->bank_branch, ['class' => 'form-control']) !!}
                                    @if($errors->has('bank_branch'))
                                        <div class="error">{{ $errors->first('bank_branch') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- thông tin người liên hệ --}}
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">Thông tin người liên hệ</h4>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a>
                </div>
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main">
                    <div class="box row">
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <label for="fullname" class="col-sm-5 control-label">Họ và Tên <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::text('terminal_contact_name', $store->terminal_contact_name, ['class' => 'form-control']) !!}
                                    @if($errors->has('terminal_contact_name'))
                                        <div class="error">{{ $errors->first('terminal_contact_name') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="phone" class="col-sm-5 control-label">Số điện thoại <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::text('terminal_contact_phone', $store->terminal_contact_phone, [
                                        'class' => 'form-control',
                                        'maxlength' => 10,
                                    ]) !!}
                                    @if($errors->has('terminal_contact_phone'))
                                        <div class="error">{{ $errors->first('terminal_contact_phone') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="email" class="col-sm-5 control-label">Email liên hệ <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::email('terminal_contact_email', $store->terminal_contact_email, ['class' => 'form-control']) !!}
                                    @if($errors->has('terminal_contact_email'))
                                        <div class="error">{{ $errors->first('terminal_contact_email') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="middle">
                                    {!! Form::checkbox('create_terminal_app', 1, $store->terminal_app_user ? true : false,['class' => 'ace', 'disabled' => true]) !!}
                                    <span class="lbl"> Tạo tài khoản đăng nhập Terminal App</span>
                                </label>
                            </div>
                            <div class="row form-group" id="terminal-app-user-area" style="display: {{ $store->terminal_app_user ? 'block' : 'none' }};">
                                <label for="phone" class="col-sm-5 control-label">Số điện thoại đăng nhập ter app <span class="red">(*)</span></label>
                                <div class="col-sm-7">
                                    {!! Form::text('terminal_app_user', $store->terminal_app_user, [
                                        'class' => 'form-control',
                                        'id' => 'terminal-app-user',
                                        'maxlength' => 10,
                                        'placeholder' => '',
                                        'readonly' => true,
                                    ]) !!}
                                    @if($errors->has('terminal_app_user'))
                                        <div class="error">{{ $errors->first('terminal_app_user') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center">
            <a href="{{ route('terminals.showFirstTerminal', $store->id) }}" class="btn btn-default">Quay lại</a>
            @if ($store->mms_status >= \App\Libraries\BaseFunction::STATUS_MMS_CREATED)
                <button class="btn btn-success" type="submit">Cập nhật</button>
            @else
                <button class="btn btn-success" type="submit">Lưu</button>
            @endif
        </div>

        @section('scripts')
            <script>
                (function($) {
                    $.fn.extend({
                        triggerAll: function (events, params) {
                            var el = this, i, evts = events.split(' ');
                            for (i = 0; i < evts.length; i += 1) {
                                el.trigger(evts[i], params);
                            }
                            return el;
                        }
                    });
                })(jQuery);
                $(function () {
                    $(document).on('change', ':file', function () {
                        if (this.files[0]) {
                            var file = this.files[0],
                                extension = file.name.substr((file.name.lastIndexOf('.') + 1)),
                                input = $(this),
                                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                            if (['pdf', 'png', 'jpg', 'rar'].indexOf(extension.toLowerCase()) < 0) {
                                input.trigger('fileTypeError');
                            }
                            else if (file.size > 2*5242880) {
                                input.trigger('fileSizeError');
                            }
                            else {
                                input.trigger('fileSelected', [numFiles, label]);
                            }
                        }
                    });


                    $(document).ready(function () {
                        $(':file').on('fileTypeError', function (event) {
                            $(this).val("");
                            $(this).parents('.input-group').find(':text').val('');
                            $(this).parents('.form-group').addClass('has-error').find('.help-block').text('File có định dạng pdf, png, jpg, rar');
                        });
                        $(':file').on('fileSizeError', function (event) {
                            $(this).val("");
                            $(this).parents('.input-group').find(':text').val('');
                            $(this).parents('.form-group').addClass('has-error').find('.help-block').text('Dung lượng file tối đa là 10MB');
                        });
                        $(':file').on('fileSelected', function (event, numFiles, label) {
                            var input = $(this).parents('.input-group').find(':text'),
                                log = numFiles > 1 ? numFiles + ' files selected' : label;

                            if (input.length) {
                                input.val(log);
                            }
                            $(this).parents('.form-group').removeClass('has-error').find('.help-block').text('');

                        });
                        $("#terminal_provinces").val("{{old('terminal_province_code') ?? $store->terminal_province_code}}").trigger('change', function () {
                            $("#terminal_districts").val("{{ old('terminal_district_code') ?? $store->terminal_district_code }}").trigger('change', function () {
                                $("#terminal_wards").val("{{ old('terminal_ward_code') ?? $store->terminal_ward_code }}").trigger('change')
                            })
                        });
                    });
                });
            </script>
            <script type="text/javascript">
                $("#terminal_provinces").change(function(event, callback){
                    $.ajax({
                        url: "{{ url('/api/get_districts_from_province_code') }}?province_code=" + $(this).val(),
                        method: 'GET',
                        success: function(data) {
                            $('#terminal_districts').html(data.html);
                            if (callback){
                                callback();
                            }
                            else {
                                $('#terminal_districts').trigger('change.select2');
                                setTimeout($("#terminal_districts").val($("#terminal_districts option:first").val()).change(),3000);
                            }

                        }
                    });
                });
                $("#terminal_districts").change(function(event, callback){
                    $.ajax({
                        url: "{{ url('/api/get_wards_from_district_code_and_province_code') }}?district_code=" + $(this).val()+'&province_code='+ $("#terminal_provinces").val(),
                        method: 'GET',
                        success: function(data) {
                            $('#terminal_wards').html(data.html);
                            if (callback){
                                callback();
                            }
                            else {
                                $('#terminal_wards').trigger('change.select2');
                            }
                        }
                    });
                });
                $("#departments").change(function(){
                    $.ajax({
                        url: "{{ url('/api/get_staff_from_department') }}?department_id=" + $(this).val(),
                        method: 'GET',
                        success: function(data) {
                            $('#staffs').html(data.html);
                            $('#staffs').trigger('change.select2');

                            setTimeout($("#staffs").val($("#staffs option:first").val()).change(),3000);

                        }
                    });
                });
            </script>
        @endsection


        {!! Form::close() !!}
    </div>
@endsection
