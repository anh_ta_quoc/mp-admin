@extends('layouts_mms.app', ['new_select2' => true])
@section('css')
    <link rel="stylesheet" href="{{asset('css/terminal.css')}}" />
@endsection
@section('content')
    @include('layouts_mms.page_header', ['header' => 'Tạo terminal mới', 'buttons' => [['title' => 'Quay lại', 'link' => route('terminals.index'), 'icon' => 'fa fa-arrow-left']]])
    {!! Form::open(['route' => 'terminals.store', 'files' => true]) !!}
    @include('terminals.create_form')
    {!! Form::close() !!}
@endsection
