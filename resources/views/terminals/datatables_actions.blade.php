<div class="hidden-sm hidden-xs action-buttons" style="min-width: 50px;">
    <a class="blue" href="{{ route('terminals.show', $id) }}">
        <i class="ace-icon fa fa-search-plus bigger-130"></i>
    </a>

    @if ((Auth::user()->email === 'devmp@gmail.com' || Auth::user()->hasrole('Kinh doanh')) &&
            $status == \App\MpTerminal::STATUS_REVIEW_APPROVED)
        <span class="red" style="cursor: pointer;" onclick="handleLockTerminal({{ $id }})">
            <i class="ace-icon fa fa-lock bigger-130"></i>
        </span>
    @endif
    @if ((Auth::user()->email === 'devmp@gmail.com' || Auth::user()->hasrole('Kinh doanh')) &&
        $status == \App\MpTerminal::STATUS_LOCK)
        <span class="green" style="cursor: pointer;" onclick="handleUnLockTerminal({{ $id }})">
            <i class="ace-icon fa fa-unlock bigger-130"></i>
        </span>
    @endif
</div>


