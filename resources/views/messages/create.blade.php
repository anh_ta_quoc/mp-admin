@extends('layouts_mms.app', ['new_select2' => true])

@section('content')
    @include('layouts_mms.page_header', ['header' => 'Tạo thông báo mới', 'buttons' => [['title' => 'Quay lại', 'link' => route('messages.index'), 'icon' => 'fa fa-arrow-left']]])
    <div class="row">
        @include('coreui-templates::common.errors')
        {!! Form::open(['route' => 'messages.store']) !!}

        @include('messages.fields')

        {!! Form::close() !!}
    </div>
@endsection
