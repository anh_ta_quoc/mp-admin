<div class="col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
    <div class="row">
        <div class="form-group col-xs-12">
            {!! Form::label('type', 'Loại thông báo') !!}
            <select class="form-control select2" id="type" name="notification_type">
                <option value="1">Thông báo từ quản trị viên</option>
            </select>
        </div>
        <div class="form-group col-xs-12">
            <input type="hidden" id="selected-receivers" value="{{ $message['receivers'] }}"/>
            {!! Form::label('receiver', 'Người nhận', ['class' => 'require']) !!}
            {{--            {!! Form::text('receiver', null, ['class' => 'form-control']) !!}--}}
            <select multiple="" class="chosen-select tag-input-style form-control" name="receivers[]" id="receiver" data-placeholder="Chọn người nhận">
                <option value="all">----- Tất cả -----</option>
                @foreach($merchants as $merchant)
                    <option value="{{ sprintf('%s-%s', $merchant['id'], $merchant['merchant_code']) }}">
                        {{ sprintf('%s - %s', $merchant['merchant_code'], $merchant['merchant_name']) }}
                    </option>
                @endforeach
            </select>
            <div class="error hidden" id="error-receivers">
            </div>
        </div>
        <div class="form-group col-xs-12">
            {!! Form::label('title', 'Tiêu đề', ['class' => 'require']) !!}
            {!! Form::text('title', null, ['class' => 'form-control msg-title', 'maxlength' => 50, 'placeholder' => 'Nhập tiêu đề tin nhắn']) !!}
            <div class="error hidden" id="error-title">
            </div>
        </div>
        <div class="form-group col-xs-12 col-lg-12">
            {!! Form::label('body', 'Nội dung', ['class' => 'require']) !!}
            {!! Form::textarea('body', null, ['class' => 'form-control msg-body', 'rows' => 3, 'maxlength' => 250, 'placeholder' => 'Nhập nội dung tin nhắn']) !!}
            <div class="error hidden" id="error-body">
            </div>
        </div>
        <div class="form-group col-xs-12">
            {!! Form::label('link', 'Link đính kèm', []) !!}
            {!! Form::text('link', null, ['class' => 'form-control msg-link', 'maxlength' => 255, 'placeholder' => 'Nhập link đính kèm']) !!}
            <div class="error hidden" id="error-link">
            </div>
        </div>
        <div class="form-group col-xs-12">
            <div class="row">
                <div class="col-xs-6">
                    <label class="middle">
                        <input class="ace" type="checkbox" id="is_schedule" name="is_schedule"
                            {{ $message['is_schedule'] === 1 ? 'checked' : '' }}>
                        <span class="lbl"> Hẹn giờ gửi (nếu không chọn thì sẽ là gửi ngay)</span>
                    </label>
                </div>
                <div class="col-xs-3 schedule-date-area" style="display: {{ $message['is_schedule'] === 1 ? 'block' : 'none' }};">
                    <div class="input-group">
                        <input class="form-control date-picker" id="schedule_date" name="schedule_date" type="text" data-date-format="dd/mm/yyyy"
                            value="{{ $message['schedule_at']->format('d/m/Y') }}"
                        />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
                <div class="col-xs-3 schedule-time-area" style="display: {{ $message['is_schedule'] === 1 ? 'block' : 'none' }};">
                    <div class="input-group bootstrap-timepicker">
                        <input name="schedule_time" id="schedule_time" type="text" class="form-control"
                               value="{{ $message['schedule_at']->format('H:i:s') }}"
                        />
                        <span class="input-group-addon">
                            <i class="fa fa-clock-o bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="error hidden" id="error-schedule">
            </div>
        </div>


        <!-- Submit Field -->
        <div class="form-group col-sm-12 center m-t-10">
            <a href="{{ route('messages.index') }}" class="btn btn-secondary">Hủy</a>
            {!! Form::submit('Lưu', ['class' => 'btn btn-primary btn-create-msg']) !!}
        </div>
    </div>
</div>

@section('css')
{{--    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />--}}
    <link rel="stylesheet" href="{{asset('css/bootstrap-timepicker.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endsection

@section('scripts')
    <script src="{{asset('js/chosen.jquery.min.js')}}"></script>
{{--    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>--}}
    <script src="{{asset('js/bootstrap-timepicker.min.js')}}"></script>
    <script type="text/javascript">
        var selected_receivers = $('#selected-receivers').val();
        $('#receiver').val(selected_receivers.split(','));
        $('.chosen-select').chosen({
            no_results_text: "Không tìm thấy merchant tương ứng!",
            search_contains: true,
        });
        $(window).on('resize.chosen', function() {
            //get its parent width
            var w = $('.chosen-select').parent().width();
            $('.chosen-select').siblings('.chosen-container').css({'width':w});
        }).triggerHandler('resize.chosen');
        $('#receiver').trigger('chosen:updated');
        $('.chosen-choices').css('max-height', '400px').css('overflow-y', 'scroll');

        var dateToday = new Date();
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true,
            startDate: dateToday,
        }).next().on('click', function(){
            $(this).prev().focus();
        });

        $('#schedule_time').timepicker({
            minuteStep: 1,
            showSeconds: true,
            showMeridian: false,
            disableFocus: true,
            icons: {
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down'
            }
        }).on('focus', function() {
            $('#schedule_time').timepicker('showWidget');
        }).next().on('click', function(){
            $(this).prev().focus();
        });

        $('#is_schedule').on('click', function () {
            if (!$(this).prop('checked')) {
                $('.schedule-date-area').css('display', 'none');
                $('.schedule-time-area').css('display', 'none');

                $('#error-schedule').html('').addClass('hidden');
            }
            else {
                $('.schedule-date-area').css('display', 'block');
                $('.schedule-time-area').css('display', 'block');
            }
        });

        $('.btn-create-msg').on('click', function () {
            var has_error_receiver = false;
            // check receiver
            if ($('#receiver').val().length === 0) {
                has_error_receiver = true;
                $('#error-receivers').html('Vui lòng chọn ít nhất 1 người nhận').removeClass('hidden');
            }
            else {
                if ($('#receiver').val().includes('all') && $('#receiver').val().length > 1) {
                    has_error_receiver = true;
                    $('#error-receivers').html('Chọn người nhận không hợp lệ, không được bao gồm tất cả và người nhận ' +
                        'khác').removeClass('hidden');
                }
                else {
                    has_error_receiver = false;
                    $('#error-receivers').html('').addClass('hidden');
                }
            }
            // check title
            var has_error_title = false;
            if ($('.msg-title').val().trim() === '') {
                has_error_title = true;
                $('#error-title').html('Vui lòng nhập tiêu đề').removeClass('hidden');
            }
            else {
                has_error_title = false;
                $('#error-title').html('').addClass('hidden');
            }
            // check body
            var has_error_body = false;
            if ($('.msg-body').val().trim() === '') {
                has_error_body = true;
                $('#error-body').html('Vui lòng nhập nội dung').removeClass('hidden');
            }
            else {
                has_error_body = false;
                $('#error-body').html('').addClass('hidden');
            }
            // check schedule
            var has_error_schedule = false;
            if ($('#is_schedule').prop('checked')) {
                var schedule_at = new Date($('#schedule_date').val().split("/").reverse().join("/") + ' ' + $('#schedule_time').val());
                var now = new Date();
                if (schedule_at < now) {
                    has_error_schedule = true;
                    $('#error-schedule').html('Không được chọn thời gian hẹn giờ gửi trong quá khứ').removeClass('hidden');
                }
                else {
                    has_error_schedule = false;
                    $('#error-schedule').html('').addClass('hidden');
                }
            }

            if (has_error_receiver || has_error_title || has_error_body || has_error_schedule) {
                return false;
            }
        })
    </script>
@endsection
