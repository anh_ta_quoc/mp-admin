{!! Form::open(['route' => ['messages.destroy', $id],
'id' => 'delete_form_'.$id,
'method' => 'delete',
'onsubmit' => 'return confirm("Bạn chắc chắn thực hiện thao tác?")']) !!}
<div class="hidden-sm hidden-xs action-buttons">
    <a class="blue" href="{{ route('messages.show', $id) }}">
        <i class="ace-icon fa fa-search-plus bigger-130"></i>
    </a>
    @if($schedule_at >= now())
        <a class="green" href="{{ route('messages.edit', $id) }}">
            <i class="ace-icon fa fa-pencil bigger-130"></i>
        </a>

        <a class="red" href="javascript:$('#{{'delete_form_'.$id}}').submit();">
            <i class="ace-icon fa fa-trash-o bigger-130" onclick=""></i>
        </a>
    @endif
</div>
{!! Form::close() !!}


