<div class="col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
    <div class="row">
        <div class="form-group col-xs-12">
            <div class="col-sm-4">
                {!! Form::label('type', 'Loại thông báo') !!}
            </div>
            <div class="col-sm-8">
                <label><b>Thông báo từ quản trị viên</b></label>
            </div>
        </div>
        <div class="form-group col-xs-12">
            <input type="hidden" id="selected-receivers" value="{{ $message['receivers'] }}"/>
            <div class="col-sm-4">
                {!! Form::label('receiver', 'Người nhận') !!}
            </div>
            <div class="col-sm-8" style="max-height: 400px; overflow-y: scroll;">
                @if($message['receivers'] === 'all')
                    <label><b>Tất cả</b></label>
                @else
                @php($listReceivers = explode(',', $message['receivers']))
                        @foreach ($listReceivers as $receiver)
                            @php($merchantByCode = \App\Libraries\BaseFunction::getMerchantByCode(explode('-', $receiver)[1]))
                            <label><b>{{ sprintf('%s - %s', $merchantByCode['merchant_code'], $merchantByCode['merchant_name']) ?? '' }}</b></label><br/>
                        @endforeach
                @endif
            </div>
        </div>
        <div class="form-group col-xs-12">
            <div class="col-sm-4">
                {!! Form::label('title', 'Tiêu đề') !!}
            </div>
            <div class="col-sm-8">
                <label><b>{{ $message['title'] }}</b></label>
            </div>
        </div>
        <div class="form-group col-xs-12 col-lg-12">
            <div class="col-sm-4">
                {!! Form::label('body', 'Nội dung') !!}
            </div>
            <div class="col-sm-8">
                <label><b>{{ $message['body'] }}</b></label>
            </div>
        </div>
        <div class="form-group col-xs-12">
            <div class="col-sm-4">
                {!! Form::label('is_schedule', 'Loại hẹn gửi') !!}
            </div>
            <div class="col-sm-8">
                <label><b>{{ $message['is_schedule'] === 1 ? 'Hẹn giờ gửi' : 'Gửi ngay' }}</b></label>
            </div>
        </div>
        @if($message['is_schedule'] === 1)
            <div class="form-group col-xs-12">
                <div class="col-sm-4">
                    {!! Form::label('sent_at', 'Giờ gửi') !!}
                </div>
                <div class="col-sm-8">
                    <label><b>{{ $message['schedule_at'] }}</b></label>
                </div>
            </div>
        @endif


        <!-- Submit Field -->
        <div class="form-group col-sm-6 center m-t-10">
            <a href="{{ route('messages.index') }}" class="btn btn-secondary">Về danh sách</a>
        </div>
    </div>
</div>
