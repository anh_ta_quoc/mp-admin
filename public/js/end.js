
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $(".select2").select2({
        'allowClear': true
    });
    $(".css-treeview li").not(':has(ul)').children("label").addClass('no-before');
});
$('.open-datetimepicker').click(function (event) {
    event.preventDefault();
    $('#datetimepicker').click();
});
$('input.checkid:checkbox').change(function () {
    if ($(this).is(":checked")) {
        $('div.rowid').addClass("menuitemshow");
    } else {
        $('div.rowid').removeClass("menuitemshow");
    }
});
$('.date').contextmenu(function() {
    return false;
});
$('.datet').contextmenu(function() {
    return false;
});
$('.date1').contextmenu(function() {
    return false;
});
$('.dateY').contextmenu(function() {
    return false;
});
