/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var flag = true;
var flagCTKM = true;

function getDsTranSuccessExcelUrl(id){
    return '/excelFile/' + id + '?file=BaoCaoGiaoDichThanhCong.xlsx'; 
}

function getListTransExcelUrl(id){
    return '/excelFile/' + id + '?file=DanhSachGiaoDich.xlsx';
}

function getListCTKMExcelUrl(id){
    return '/excelFile/' + id + '?file=BaoCaoDoanhSoCTKM.xlsx';
}

function getListTerminalExcelUrl(id){
    return '/excelFile/' + id + '?file=DanhSachTerminal.xlsx';
}

function getListTransRefundDetailExcelUrl(id){
    return '/excelFile/' + id + '?file=BaoCaoChiTietGDHoanTien.xlsx';
}

function getListTransRefundExcelUrl(id){
    return '/excelFile/' + id + '?file=DanhSachGiaoDichHoanTien.xlsx';
}

function getRefundDetailOutTermExcelUrl(id){
    return '/excelFile/' + id + '?file=DanhSachGiaoDichHoanTienKhacKy.xlsx';
}

function getDsTranSuccessByUpdateExcelUrl(id){
    return '/excelFile/' + id + '?file=BaoCaoGiaoDichThanhCongPhatSinh.xlsx';
}