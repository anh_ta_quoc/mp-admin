<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/27/2020
 * Time: 11:54 AM
 */


// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push("Home", route('home'), ['icon' => '<i class="default-icon fa fa-home home-icon"></i>']);
});

// trial user
Breadcrumbs::for('trials.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Tài khoản thử nghiệm', route('trials.index'));
});

// Quản lý merchant

Breadcrumbs::for('stores.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý merchant', route('stores.index'));
});

Breadcrumbs::for('stores.needApproveList', function ($trail) {
    $trail->parent('stores.index');
    $trail->push('Danh sách chờ duyệt', route('stores.needApproveList'));
});

Breadcrumbs::for('stores.registerList', function ($trail) {
    $trail->parent('stores.index');
    $trail->push('Danh sách gửi đăng ký', route('stores.registerList'));
});

Breadcrumbs::for('stores.approvedList', function ($trail) {
    $trail->parent('stores.index');
    $trail->push('Danh sách đã duyệt', route('stores.approvedList'));
});

Breadcrumbs::for('stores.deniedList', function ($trail) {
    $trail->parent('stores.index');
    $trail->push('Danh sách bị từ chối', route('stores.deniedList'));
});

Breadcrumbs::for('stores.activeList', function ($trail) {
    $trail->parent('stores.index');
    $trail->push('Danh sách đã hoạt động', route('stores.activeList'));
});

Breadcrumbs::for('stores.create', function ($trail) {
    $trail->parent('stores.index');
    $trail->push('Tạo mới merchant', route('stores.create'));
});

Breadcrumbs::for('stores.show', function ($trail, $id) {
    $trail->parent('stores.index');
    $trail->push('Xem thông tin merchant', route('stores.show', ['id' => $id]));
});

Breadcrumbs::for('stores.edit', function ($trail, $id) {
    $trail->parent('stores.index');
    $trail->push('Sửa thông tin merchant', route('stores.edit', ['id' => $id]));
});

// Quản lý terminal

Breadcrumbs::for('terminals.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý terminal', route('terminals.index'));
});

Breadcrumbs::for('terminals.needApproveList', function ($trail) {
    $trail->parent('terminals.index');
    $trail->push('Danh sách chờ duyệt', route('terminals.needApproveList'));
});

Breadcrumbs::for('terminals.approvedList', function ($trail) {
    $trail->parent('terminals.index');
    $trail->push('Danh sách đã duyệt', route('terminals.approvedList'));
});

Breadcrumbs::for('terminals.deniedList', function ($trail) {
    $trail->parent('terminals.index');
    $trail->push('Danh sách bị từ chối', route('terminals.deniedList'));
});

Breadcrumbs::for('terminals.create', function ($trail) {
    $trail->parent('terminals.index');
    $trail->push('Tạo mới terminal', route('terminals.create'));
});

Breadcrumbs::for('terminals.show', function ($trail, $id) {
    $trail->parent('terminals.index');
    $trail->push('Xem thông tin terminal', route('terminals.show', ['id' => $id]));
});

Breadcrumbs::for('terminals.edit', function ($trail, $id) {
    $trail->parent('terminals.index');
    $trail->push('Sửa thông tin terminal', route('terminals.edit', ['id' => $id]));
});

// Mở khóa user

Breadcrumbs::for('utils.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Mở khóa merchant user', route('utils.index'));
});

// Quản lý thông báo

Breadcrumbs::for('messages.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý thông báo', route('messages.index'));
});

Breadcrumbs::for('messages.create', function ($trail) {
    $trail->parent('messages.index');
    $trail->push('Tạo thông báo mới', route('messages.create'));
});

Breadcrumbs::for('messages.show', function ($trail, $id) {
    $trail->parent('messages.index');
    $trail->push('Xem chi tiết thông báo', route('messages.show', ['id' => $id]));
});

Breadcrumbs::for('messages.edit', function ($trail, $id) {
    $trail->parent('messages.index');
    $trail->push('Sửa thông báo', route('messages.edit', ['id' => $id]));
});

// Quản lý Banners

Breadcrumbs::for('banners.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý banner', route('banners.index'));
});

Breadcrumbs::for('banners.create', function ($trail) {
    $trail->parent('banners.index');
    $trail->push('Tạo banner mới', route('banners.create'));
});

Breadcrumbs::for('banners.show', function ($trail, $id) {
    $trail->parent('banners.index');
    $trail->push('Xem chi tiết banner', route('banners.show', ['id' => $id]));
});

Breadcrumbs::for('banners.edit', function ($trail, $id) {
    $trail->parent('banners.index');
    $trail->push('Sửa banner', route('banners.edit', ['id' => $id]));
});

// Quản lý SMS logging

Breadcrumbs::for('smsLogs.index', function ($trail) {
    $trail->parent('home');
    $trail->push('SMS Logging', route('smsLogs.index'));
});

Breadcrumbs::for('smsLogs.show', function ($trail, $id) {
    $trail->parent('smsLogs.index');
    $trail->push('Xem chi tiết SMS', route('smsLogs.show', ['id' => $id]));
});

// Quản lý permission

Breadcrumbs::for('permissions.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý quyền', route('permissions.index'));
});

Breadcrumbs::for('permissions.create', function ($trail) {
    $trail->parent('permissions.index');
    $trail->push('Tạo quyền mới', route('permissions.create'));
});

Breadcrumbs::for('permissions.show', function ($trail, $id) {
    $trail->parent('permissions.index');
    $trail->push('Xem chi tiết quyền', route('permissions.show', ['id' => $id]));
});

Breadcrumbs::for('permissions.edit', function ($trail, $id) {
    $trail->parent('permissions.index');
    $trail->push('Sửa quyền', route('permissions.edit', ['id' => $id]));
});

// Quản lý roles

Breadcrumbs::for('roles.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý vai trò', route('roles.index'));
});

Breadcrumbs::for('roles.create', function ($trail) {
    $trail->parent('roles.index');
    $trail->push('Tạo vai trò mới', route('roles.create'));
});

Breadcrumbs::for('roles.show', function ($trail, $id) {
    $trail->parent('roles.index');
    $trail->push('Xem chi tiết vai trò', route('roles.show', ['id' => $id]));
});

Breadcrumbs::for('roles.edit', function ($trail, $id) {
    $trail->parent('roles.index');
    $trail->push('Sửa vai trò', route('roles.edit', ['id' => $id]));
});

// Quản lý users

Breadcrumbs::for('users.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý tài khoản', route('users.index'));
});

Breadcrumbs::for('users.create', function ($trail) {
    $trail->parent('users.index');
    $trail->push('Tạo tài khoản mới', route('users.create'));
});

Breadcrumbs::for('users.show', function ($trail, $id) {
    $trail->parent('users.index');
    $trail->push('Xem chi tiết tài khoản', route('users.show', ['id' => $id]));
});

Breadcrumbs::for('users.edit', function ($trail, $id) {
    $trail->parent('users.index');
    $trail->push('Sửa tài khoản', route('users.edit', ['id' => $id]));
});

