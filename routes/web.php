<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});


Auth::routes();
Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('stores/{id}/sale_accept', 'StoreController@saleAccept')->name('stores.sale_accept');
    Route::get('stores/{id}/control_accept', 'StoreController@controlAccept')->name('stores.control_accept');
    Route::get('stores/{id}/notify_mms', 'StoreController@notifyMMS')->name('stores.notify_mms');
    Route::get('stores/{id}/retry_mms', 'StoreController@retryMMS')->name('stores.retry_mms');
    Route::get('stores/{id}/reject', 'StoreController@reject')->name('stores.reject');
    Route::get('stores/{id}/reopen_upload_document', 'StoreController@reopenUploadDocument')->name('stores.reopen');
    Route::post('stores/updateStore/{id}', 'StoreController@updateStore')->name('stores.updateStore');
    Route::post('stores/updateContract/{id}', 'StoreController@updateContract')->name('stores.updateContract');
    Route::post('stores/reUploadContract/{id}', 'StoreController@reUploadContract')->name('stores.reUploadContract');
    Route::post('stores/saleAction/{id}', 'StoreController@saleAction')->name('stores.saleAction');
    Route::post('stores/controlAction/{id}', 'StoreController@controlAction')->name('stores.controlAction');
    Route::post('stores/sendRequest/{id}', 'StoreController@sendRequest')->name('stores.sendRequest');
    Route::post('stores/lockMerchant', 'StoreController@lockMerchant')->name('stores.lockMerchant');
    Route::post('stores/unlockMerchant', 'StoreController@unlockMerchant')->name('stores.unlockMerchant');

    Route::get('stores/needApproveList', 'StoreController@needApproveList')->name('stores.needApproveList');
    Route::get('stores/activeList', 'StoreController@activeList')->name('stores.activeList');
    Route::get('stores/registerList', 'StoreController@registerList')->name('stores.registerList');
    Route::get('stores/approvedList', 'StoreController@approvedList')->name('stores.approvedList');
    Route::get('stores/deniedList', 'StoreController@deniedList')->name('stores.deniedList');
    Route::get('stores/lockedList', 'StoreController@lockedList')->name('stores.lockedList');
    Route::resource('stores', 'StoreController')->middleware('auth');

    Route::resource('trials', 'TrialController');


    Route::post('terminals/{id}/reject', 'TerminalController@reject')->name('terminals.reject');
    Route::get('terminals/{id}/approve', 'TerminalController@approve')->name('terminals.approve');
    Route::get('terminals/{id}/sendRequest', 'TerminalController@sendRequest')->name('terminals.sendRequest');
    Route::get('terminals/{id}/showFirstTerminal', 'TerminalController@showFirstTerminal')->name('terminals.showFirstTerminal');
    Route::get('terminals/{id}/editFirstTerminal', 'TerminalController@editFirstTerminal')->name('terminals.editFirstTerminal');
    Route::patch('terminals/updateFirstTerminal/{id}', 'TerminalController@updateFirstTerminal')->name('terminals.updateFirstTerminal');
    Route::get('terminals/needApproveList', 'TerminalController@needApproveList')->name('terminals.needApproveList');
    Route::get('terminals/approvedList', 'TerminalController@approvedList')->name('terminals.approvedList');
    Route::get('terminals/deniedList', 'TerminalController@deniedList')->name('terminals.deniedList');
    Route::get('terminals/lockedList', 'TerminalController@lockedList')->name('terminals.lockedList');
    Route::get('terminals/ajaxListMerchant', 'TerminalController@ajaxListMerchant')->name('terminals.ajaxListMerchant');
    Route::get('terminals/ajaxListMerchantWithAll', 'TerminalController@ajaxListMerchantWithAll')->name('terminals.ajaxListMerchantWithAll');
    Route::get('terminals/ajaxListTerminalByMerchantCode', 'TerminalController@ajaxListTerminalByMerchantCode')->name('terminals.ajaxListTerminalByMerchantCode');

    Route::get('terminals/ajaxFindBankAccount', 'TerminalController@ajaxFindBankAccount')->name('terminals.ajaxFindBankAccount');
    Route::post('stores/lockTerminal', 'TerminalController@lockTerminal')->name('terminals.lockTerminal');
    Route::post('stores/unlockTerminal', 'TerminalController@unlockTerminal')->name('terminals.unlockTerminal');

    Route::resource('terminals', 'TerminalController')->middleware('auth');

    Route::resource('terminalBatches', 'MpTerminalBatchController')->middleware('auth');
    Route::post('terminalBatches/{id}/reject', 'MpTerminalBatchController@reject')->name('terminalBatches.reject');
    Route::get('terminalBatches/{id}/approve', 'MpTerminalBatchController@approve')->name('terminalBatches.approve');
    Route::get('terminalBatches/{id}/reopenForEditTerminals', 'MpTerminalBatchController@reopenForEditTerminals')
        ->name('terminalBatches.reopenForEditTerminals');

    // enjoyments
    Route::get('enjoyments/changeEnjoyment', 'EnjoymentsController@changeEnjoyment')->name('enjoyments.changeEnjoyment');
    Route::get('enjoyments/index', 'EnjoymentsController@index')->name('enjoyments.index');
    Route::get('enjoyments/chooseMerchant', 'EnjoymentsController@chooseMerchant')->name('enjoyments.chooseMerchant');
    Route::post('enjoyments/chooseMerchant', 'EnjoymentsController@chooseMerchant')->name('enjoyments.chooseMerchant');
    Route::post('enjoyments/upsertEnjoyment', 'EnjoymentsController@upsertEnjoyment')->name('enjoyments.upsertEnjoyment');
    Route::get('enjoyments/ajaxFindBankAccount', 'EnjoymentsController@ajaxFindBankAccount')->name('enjoyments.ajaxFindBankAccount');
    Route::resource('enjoyments', 'EnjoymentsController')->middleware('auth');

    Route::resource('smsLogs', 'SmsLogController')->middleware('auth');

    Route::resource('users', 'UserController');

    Route::resource('roles', 'RoleController');

    Route::resource('permissions', 'PermissionController');

    Route::resource('mp_mid_tid_user', 'MpMidTidUserController');

    Route::get('mp_user/ajaxListPhone', 'MpUserController@ajaxListUserWithPhone')->name('mp_users.ajaxListUserWithPhone');



    Route::get('/api/get_districts_from_province_code',
        'API\ContentController@get_districts_from_province_code'
    );
    Route::get('/api/get_wards_from_district_code_and_province_code',
        'API\ContentController@get_wards_from_district_code_and_province_code'
    );
    Route::get('/api/get_staff_from_department',
        'API\ContentController@get_staff_from_department'
    );
    Route::get('/api/get_audit_logs',
        'API\ContentController@get_audit_logs'
    );
    Route::get('/api/search_merchant_users',
        'API\ContentController@search_merchant_users'
    );
    Route::get('/api/get_latest_terminal_of_merchant',
        'API\ContentController@get_latest_terminal_of_merchant'
    );
    Route::get('/api/get_combine_info_mer_ter',
        'API\ContentController@get_combine_info_mer_ter'
    );
    Route::get('/api/clear_filter',
        'API\ContentController@clearFilter'
    );


    Route::resource('messages', 'MessageController');
    Route::resource('utils', 'UtilController');
    Route::post('utils/resetUsers', 'UtilController@resetUsers')->name('utils.resetUsers');
    Route::resource('banners', 'BannerController');
});
