<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RevokeLockedTerminal
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $merchantCode;
    public $terminalId;

    /**
     * Create a new event instance.
     *
     * RevokeLockedMerchant constructor.
     * @param $merchantCode
     * @param $terminalId
     */
    public function __construct($merchantCode, $terminalId)
    {
        $this->merchantCode = $merchantCode;
        $this->terminalId = $terminalId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
