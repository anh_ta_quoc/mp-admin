<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendEmailCompleteStoreInfo
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $storeInfoId;
    public $eventCompleteName;

    /**
     * Create a new event instance.
     *
     * @param $storeInfoId
     * @param $eventCompleteName
     */
    public function __construct($storeInfoId, $eventCompleteName)
    {
        $this->storeInfoId = $storeInfoId;
        $this->eventCompleteName = $eventCompleteName;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
