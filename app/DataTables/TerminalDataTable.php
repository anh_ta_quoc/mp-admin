<?php

namespace App\DataTables;

use App\Models\Sync\QrTerminal;
use App\MpTerminal;
use Illuminate\Support\Str;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Yajra\DataTables\Services\DataTablesExportHandler;
use App\DataTables\CustomDataTableExportHanlder;
class TerminalDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    private $status= [];
    private $file_name = '';

    function __construct($status = [], $file_name='')
    {
        $this->status = $status;
        $this->file_name = $file_name;
    }

    public function dataTable($query)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $dataTable = new EloquentDataTable($query);
        $dataTable->filter(function ($query) {
//            $start = date('Y-m-d', strtotime('-3 days'));
//            $end = date('Y-m-d');
            $start = $end = null;
            if ($this->request()->has('daterange')) {
                if (!empty(request('daterange')['start'])) {
                    $start = request('daterange')['start'];
                }
                if (!empty(request('daterange')['end'])) {
                    $end = request('daterange')['end'];
                }
            }
            if ($start && $end) {
                $sql = 'created_at >= "' . $start . '" AND created_at <= "' . $end . ' 23:59:59"';
                $query->whereRaw($sql);
                session()->put('filter_daterange_start', $start);
                session()->put('filter_daterange_end', $end);
            }
            else {
                $start = date('Y-m-d', strtotime('-30 days'));
                $end = date('Y-m-d');
                $sql = 'created_at >= "' . $start . '" AND created_at <= "' . $end . ' 23:59:59"';
                $query->whereRaw($sql);
            }

            if ($this->request()->has('filter_query') && request('filter_query')) {
                session()->put('terminal_filter_query', trim(request('filter_query')));
                $keywordToLowerCase = '%' . Str::lower(trim(request('filter_query'))) . '%';
                $sql = sprintf('(LOWER(terminal_id) LIKE "%s" OR LOWER(terminal_name) LIKE "%s" OR
                    LOWER(merchant_code) LIKE "%s" OR LOWER(terminal_contact_name) LIKE "%s" OR
                    LOWER(terminal_contact_phone) LIKE "%s" OR LOWER(bank_number) LIKE "%s" OR
                    LOWER(bank_code) LIKE "%s")', $keywordToLowerCase, $keywordToLowerCase, $keywordToLowerCase,
                    $keywordToLowerCase, $keywordToLowerCase, $keywordToLowerCase, $keywordToLowerCase);
                $query->whereRaw($sql);
            }
            else {
                session()->remove('terminal_filter_query');
            }

            if ($this->request()->has('filter_status')) {
                $query->where('status', (int)request('filter_status'));
                session()->put('terminal_filter_status', request('filter_status'));
            } else {
                $query->whereIn('status', $this->status)->get();
                session()->remove('terminal_filter_status');
            }
            if ($this->request()->has('merchant_code') && trim(request('merchant_code')) != '') {
                $query->where('merchant_code', trim(request('merchant_code')));
            }
        }, true);
        return $dataTable
            ->addIndexColumn()
            ->addColumn('action', 'terminals.datatables_actions')
            ->editColumn('terminal_contact_phone', function (MpTerminal $temp) {
                $terminalContactPhone = $temp->terminal_contact_phone;
                if ($terminalContactPhone && strlen($terminalContactPhone) == 9) {
                    return sprintf('0%s', $temp->terminal_contact_phone);
                }
                else {
                    return $terminalContactPhone;
                }
            })
            ->editColumn('register_vnpayment', function (MpTerminal $temp) {
                if ($temp->register_vnpayment==1) {
                    return
                   '<input class="name" name="register_vnpayment" type="checkbox" value="1" checked disabled>';
                } else {
                    return
                        '<input class="name" name="register_vnpayment" type="checkbox" value="1" disabled >';

                }
            })
            ->editColumn('register_qr', function (MpTerminal $temp) {
                if ($temp->register_qr==1) {
                    return
                        '<input class="name" name="register_qr" type="checkbox" value="1" checked disabled>';
                } else {
                    return
                        '<input class="name" name="register_qr" type="checkbox" value="1"  disabled >';

                }
            })
            ->editColumn('created_at', function ($temp) {
                return date_format(new \DateTime($temp['created_at']), 'H:i:s d/m/Y') ?? '';
            })
            ->addColumn('str_register_qr', function (MpTerminal $temp) {
                    return $temp->register_qr == 1 ? 'Có' : 'Không';
            })
            ->addColumn('str_register_vnpayment', function (MpTerminal $temp) {
                return $temp->register_vnpayment == 1 ? 'Có' : 'Không';
            })
            ->rawColumns(['action', 'custom_status','register_qr','register_vnpayment'])
            ->order(function ($query){$query->orderBy('created_at', 'desc');});
    }

    /**
     * Get query source of dataTable.
     *
     * @param QrTerminal $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MpTerminal $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'Thao tác'])
            ->minifiedAjax('',
                'if ($("#date_range_search").val().trim()){data["daterange"] = {"start": date_range_search_startDate, "end": date_range_search_endDate}}
                if ($("#filter_status").val()){data["filter_status"] = $("#filter_status").val();}
                if ($("#filter_query").val()){data["filter_query"] = $("#filter_query").val();}
                if ($("#merchant_code").val()){data["merchant_code"] = $("#merchant_code").val();}'
            )
            ->parameters([
                'autoWidth' => false,
                'dom' => '<"row"<"col-xs-8 p-t-5"l><"col-xs-4 text-right hidden-print"B>>" +
                    "<"row m-t-10"<"col-sm-12"tr>>" +
                    "<"row"<"col-sm-6"i><"col-sm-6 hidden-print"p>>',
                'stateSave' => false,
                'language' => [
                    "emptyTable" => "Không có bản ghi nào",
                    "info" => "Hiển thị bản ghi _START_ - _END_ trên tổng _TOTAL_ bản ghi",
                    "infoEmpty" => "Hiển thị 0 bản ghi",
                    "infoFiltered" => "(lọc từ _MAX_ bản ghi)",
                    "infoPostFix" => "",
                    "thousands" => ",",
                    "lengthMenu" => "Hiển thị _MENU_ bản ghi",
                    "loadingRecords" => "Đang tải...",
                    "processing" => 'Đang xử lý...',
//                    "processing" =>  '<img src="https://admin-partner.vnpaytest.vn/vendor/backpack/crud/img/ajax-loader.gif" alt="Đang xử lý..."',
                    "search" => "Tìm kiếm: ",
                    "zeroRecords" => "Không tìm thấy bản ghi nào",
                    "paginate" => [
                        "first" => "«",
                        "last" => "»",
                        "next" => ">",
                        "previous" => "<"
                    ],
                    "aria" => [
                        "sortAscending" => ": activate to sort column ascending",
                        "sortDescending" => ": activate to sort column descending"],
                    "buttons" => [
                        "copy" => "Sao chép",
                        "excel" => "Excel",
                        "csv" => "CSV",
                        "pdf" => "PDF",
                        "print" => "In",
                        "colvis" => "Chọn cột hiển thị"
                    ]
                ],
                'order' => [[0, 'desc']],
                'buttons' => [
                    'export'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => 'STT','searchable' => false],
            ['data' => 'terminal_id', 'name' => 'terminal_id', 'title' => 'Mã điểm bán'],
            ['data' => 'terminal_name', 'name' => 'terminal_name', 'title' => 'Tên điểm bán'],
            ['data' => 'type_name', 'name' => 'type_name', 'title' => 'Sản phẩm kinh doanh', 'searchable' => false],
            ['data' => 'terminal_code', 'name' => 'terminal_code', 'title' => 'MST/CMND'],
            ['data' => 'terminal_contact_name', 'name' => 'terminal_contact_name', 'title' => 'Người liên hệ'],
            ['data' => 'terminal_contact_phone', 'name' => 'terminal_contact_phone', 'title' => 'SĐT liên hệ'],
            ['data' => 'bank_number', 'name' => 'bank_number', 'title' => 'Tài khoản thụ hưởng'],
            ['data' => 'bank_brand', 'name' => 'bank_brand', 'title' => 'Ngân hàng thụ hưởng','searchable' => false],
            ['data' => 'register_vnpayment', 'name'=>'register_vnpayment', 'title' => 'D Vụ PG','searchable' => false],
            ['data' => 'register_qr', 'name'=>'register_qr', 'title' => 'D Vụ QR','searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Ngày đăng ký'],
            ['data' => 'approve_reject_date', 'name' => 'approve_reject_date', 'title' => 'Ngày duyệt/từ chối','searchable' => false],
            ['data' => 'custom_status', 'name' => 'custom_status', 'title' => 'Trạng thái','searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return $this->file_name. time();
    }

    protected $exportColumns = [
        ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => 'STT'],
        ['data' => 'terminal_id', 'name' => 'terminal_id', 'title' => 'Mã điểm bán'],
        ['data' => 'terminal_name', 'name' => 'terminal_name', 'title' => 'Tên điểm bán'],
        ['data' => 'type_name', 'name' => 'type_name', 'title' => 'Sản phẩm kinh doanh'],
        ['data' => 'terminal_code', 'name' => 'terminal_code', 'title' => 'MST/CMND'],
        ['data' => 'terminal_contact_name', 'name' => 'terminal_contact_name', 'title' => 'Người liên hệ'],
        ['data' => 'terminal_contact_phone', 'name' => 'terminal_contact_phone', 'title' => 'SĐT liên hệ'],
        ['data' => 'bank_number', 'name' => 'bank_number', 'title' => 'Tài khoản thụ hưởng'],
        ['data' => 'bank_brand', 'name' => 'bank_brand', 'title' => 'Ngân hàng thụ hưởng'],
        ['data' => 'str_register_vnpayment', 'name'=>'register_vnpayment', 'title' => 'D Vụ PG'],
        ['data' => 'str_register_qr', 'name'=>'register_qr', 'title' => 'D Vụ QR'],
        ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Ngày đăng ký'],
        ['data' => 'approve_reject_date', 'name' => 'approve_reject_date', 'title' => 'Ngày duyệt/từ chối'],
        ['data' => 'custom_status', 'name' => 'custom_status', 'title' => 'Trạng thái'],
    ];

    protected $exportClass = TerminalsDataTableExportHandler::class;

}
