<?php

namespace App\DataTables;

use App\DataTables\CustomDataTableExportHanlder;
use App\Libraries\BaseFunction;
use App\Models\Sync\QrTerminal;
use App\MpTerminal;
use App\Store;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Yajra\DataTables\Services\DataTablesExportHandler;

class ListTerminalsInBatchDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    private $batch = null;

    function __construct($batch)
    {
        $this->batch = $batch;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->filter(function ($query) {
            $query->where('src', 'BATCH-' . $this->batch->id);
        }, true);
        return $dataTable
            ->addIndexColumn()
            ->editColumn('terminal_id', function ($tmp) {
                return sprintf('<a href="/terminals/%s">%s</a>', $tmp->id, $tmp->terminal_id);
            })
            ->editColumn('status', function ($tmp) {
                if ($tmp->mms_status >= 1) {
                    return '<div class="green"><span class="fa fa-check-circle"></span></div>';
                }
                else {
                    return '<div class="red"><span class="fa fa-times-circle"></span></div>';
                }
            })
            ->editColumn('mms_error_code', function ($tmp) {
                return $tmp->getMMSErrorDetail();
            })
            ->rawColumns(['terminal_id', 'status'])
            ->order(function ($query){$query->orderBy('created_at', 'desc');});
    }

    /**
     * Get query source of dataTable.
     *
     * @param QrTerminal $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $model = MpTerminal::query();
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
                'autoWidth' => false,
                "ordering" => false,
                'dom' => '<"row"<"col-xs-8 p-t-5"l><"col-xs-4 text-right hidden-print">>" +
                    "<"row m-t-10"<"col-sm-12"tr>>" +
                    "<"row"<"col-sm-6"i><"col-sm-6 hidden-print"p>>',
                'stateSave' => false,
                'language' => [
                    "emptyTable" => "Không có bản ghi nào",
                    "info" => "Hiển thị bản ghi _START_ - _END_ trên tổng _TOTAL_ bản ghi",
                    "infoEmpty" => "Hiển thị 0 bản ghi",
                    "infoFiltered" => "(lọc từ _MAX_ bản ghi)",
                    "infoPostFix" => "",
                    "thousands" => ",",
                    "lengthMenu" => "Hiển thị _MENU_ bản ghi",
                    "loadingRecords" => "Đang tải...",
                    "processing" => 'Đang xử lý...',
//                    "processing" =>  '<img src="https://admin-partner.vnpaytest.vn/vendor/backpack/crud/img/ajax-loader.gif" alt="Đang xử lý..."',
                    "search" => "Tìm kiếm: ",
                    "zeroRecords" => "Không tìm thấy bản ghi nào",
                    "paginate" => [
                        "first" => "«",
                        "last" => "»",
                        "next" => ">",
                        "previous" => "<"
                    ],
                    "aria" => [
                        "sortAscending" => ": activate to sort column ascending",
                        "sortDescending" => ": activate to sort column descending"],
                ],
                'order' => [[0, 'desc']],
                'buttons' => [
                    'export'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => 'STT','searchable' => false],
            ['data' => 'terminal_id', 'name' => 'terminal_id', 'title' => 'Terminal ID'],
            ['data' => 'terminal_name', 'name' => 'terminal_name', 'title' => 'Tên Terminal'],
            ['data' => 'status', 'name' => 'status', 'title' => 'Trạng thái kiểm tra'],
            ['data' => 'mms_error_code', 'name' => 'mms_error_code', 'title' => 'Lỗi'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return $this->file_name. time();
    }

}
