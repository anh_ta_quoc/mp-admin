<?php

namespace App\DataTables;

use App\Models\Sync\QrTerminal;
use App\MpTerminal;
use Illuminate\Support\Str;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Yajra\DataTables\Services\DataTablesExportHandler;
use App\DataTables\CustomDataTableExportHanlder;
class ListTerminalsByMerchantDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    function __construct()
    {

    }

    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);
        $dataTable->filter(function ($query) {
            $query->whereNotIn('status', [
                MpTerminal::STATUS_SALE_ADMIN_REQUESTED, MpTerminal::STATUS_SALE_APPROVED,
                MpTerminal::STATUS_SALE_DENIED, MpTerminal::STATUS_REVIEW_DENIED
            ]);
            if ($this->request()->has('merchant_code') && trim(request('merchant_code')) != '') {
                $query->where('merchant_code', trim(request('merchant_code')));
            }
        }, true);
        return $dataTable
            ->addIndexColumn()
            ->addColumn('action', 'enjoyments.datatables_actions')
            ->editColumn('created_at', function ($temp) {
                return date_format(new \DateTime($temp['created_at']), 'H:i:s d/m/Y') ?? '';
            })
            ->addColumn('str_register_qr', function (MpTerminal $temp) {
                    return $temp->register_qr == 1 ? 'Có' : 'Không';
            })
            ->addColumn('str_register_vnpayment', function (MpTerminal $temp) {
                return $temp->register_vnpayment == 1 ? 'Có' : 'Không';
            })
            ->rawColumns(['action', 'custom_status','register_qr','register_vnpayment'])
            ->order(function ($query){$query->orderBy('created_at', 'desc');});
    }

    /**
     * Get query source of dataTable.
     *
     * @param QrTerminal $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MpTerminal $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => '
                <input type="checkbox" id="check-all-terminals" onclick="check_all_terminals()">
                '])
            ->minifiedAjax('',
                'if ($("#merchant_code").val()){data["merchant_code"] = $("#merchant_code").val();}'
            )
            ->parameters([
                'autoWidth' => false,
                'dom' => '<"row"<"col-xs-8 p-t-5"l><"col-xs-4 text-right hidden-print"B>>" +
                    "<"row m-t-10"<"col-sm-12"tr>>" +
                    "<"row"<"col-sm-6"i><"col-sm-6 hidden-print"p>>',
                'stateSave' => false,
                'language' => [
                    "emptyTable" => "Không có bản ghi nào",
                    "info" => "Hiển thị bản ghi _START_ - _END_ trên tổng _TOTAL_ bản ghi",
                    "infoEmpty" => "Hiển thị 0 bản ghi",
                    "infoFiltered" => "(lọc từ _MAX_ bản ghi)",
                    "infoPostFix" => "",
                    "thousands" => ",",
                    "lengthMenu" => "Hiển thị _MENU_ bản ghi",
                    "loadingRecords" => "Đang tải...",
                    "processing" => 'Đang xử lý...',
//                    "processing" =>  '<img src="https://admin-partner.vnpaytest.vn/vendor/backpack/crud/img/ajax-loader.gif" alt="Đang xử lý..."',
                    "search" => "Tìm kiếm: ",
                    "zeroRecords" => "Không tìm thấy bản ghi nào",
                    "paginate" => [
                        "first" => "«",
                        "last" => "»",
                        "next" => ">",
                        "previous" => "<"
                    ],
                    "aria" => [
                        "sortAscending" => ": activate to sort column ascending",
                        "sortDescending" => ": activate to sort column descending"],
                    "buttons" => []
                ],
                "ordering" => false,
                'order' => [[0, 'desc']],
                'buttons' => [],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => 'STT','searchable' => false],
            ['data' => 'terminal_id', 'name' => 'terminal_id', 'title' => 'Mã điểm bán'],
            ['data' => 'terminal_name', 'name' => 'terminal_name', 'title' => 'Tên điểm bán'],
            ['data' => 'bank_account', 'name' => 'bank_account', 'title' => 'Chủ tài khoản thụ hưởng'],
            ['data' => 'bank_number', 'name' => 'bank_number', 'title' => 'Tài khoản thụ hưởng'],
            ['data' => 'bank_brand', 'name' => 'bank_brand', 'title' => 'Ngân hàng thụ hưởng','searchable' => false],
            ['data' => 'bank_branch', 'name' => 'bank_branch', 'title' => 'Chi nhánh','searchable' => false],
            ['data' => 'custom_status', 'name' => 'custom_status', 'title' => 'Trạng thái','searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return $this->file_name. time();
    }

    protected $exportColumns = [
        ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => 'STT'],
        ['data' => 'terminal_id', 'name' => 'terminal_id', 'title' => 'Mã điểm bán'],
        ['data' => 'terminal_name', 'name' => 'terminal_name', 'title' => 'Tên điểm bán'],
        ['data' => 'type_name', 'name' => 'type_name', 'title' => 'Sản phẩm kinh doanh'],
        ['data' => 'terminal_code', 'name' => 'terminal_code', 'title' => 'MST/CMND'],
        ['data' => 'terminal_contact_name', 'name' => 'terminal_contact_name', 'title' => 'Người liên hệ'],
        ['data' => 'terminal_contact_phone', 'name' => 'terminal_contact_phone', 'title' => 'SĐT liên hệ'],
        ['data' => 'bank_number', 'name' => 'bank_number', 'title' => 'Tài khoản thụ hưởng'],
        ['data' => 'bank_brand', 'name' => 'bank_brand', 'title' => 'Ngân hàng thụ hưởng'],
        ['data' => 'str_register_vnpayment', 'name'=>'register_vnpayment', 'title' => 'D Vụ PG'],
        ['data' => 'str_register_qr', 'name'=>'register_qr', 'title' => 'D Vụ QR'],
        ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Ngày đăng ký'],
        ['data' => 'approve_reject_date', 'name' => 'approve_reject_date', 'title' => 'Ngày duyệt/từ chối'],
        ['data' => 'custom_status', 'name' => 'custom_status', 'title' => 'Trạng thái'],
    ];

    protected $exportClass = TerminalsDataTableExportHandler::class;

}
