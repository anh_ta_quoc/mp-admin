<?php

namespace App\DataTables;

use App\SmsLog;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class SmsLogDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        if ($this->request()->has('daterange')) {
            $start = request('daterange')['start'];
            $end = request('daterange')['end'];
            if ($start && $end) {
                $dataTable->filter(function ($query) {
                    $sql = 'created_at >= "' . request('daterange')['start'] . '" AND created_at <= "' . request('daterange')['end'] . ' 23:59:59"';
                    $query->whereRaw($sql);
                }, true);
            }
        }
        return $dataTable->addColumn('action', 'sms_logs.datatables_actions')->order(function ($query){$query->orderBy('id', 'desc');});
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SmsLog $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(SmsLog $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('', 'if ($("#date_range_search").val().trim()){data["daterange"] = {"start": date_range_search_startDate, "end": date_range_search_endDate}}')
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'Thao tác'])
            ->parameters([
                'autoWidth' => false,
                'dom' => '<"row"<"col-xs-8 p-t-5"l><"col-xs-4 text-right hidden-print"B>>" +
                    "<"row m-t-10"<"col-sm-12"tr>>" +
                    "<"row"<"col-sm-6"i><"col-sm-6 hidden-print"p>>',
                'stateSave' => false,
                'language' => [
                    "emptyTable" => "Không có bản ghi nào",
                    "info" => "Hiển thị bản ghi _START_ - _END_ trên tổng _TOTAL_ bản ghi",
                    "infoEmpty" => "Hiển thị 0 bản ghi",
                    "infoFiltered" => "(lọc từ _MAX_ bản ghi)",
                    "infoPostFix" => "",
                    "thousands" => ",",
                    "lengthMenu" => "Hiển thị _MENU_ bản ghi",
                    "loadingRecords" => "Đang tải...",
                    "processing" => 'Đang xử lý...',
//                    "processing" =>  '<img src="https://admin-partner.vnpaytest.vn/vendor/backpack/crud/img/ajax-loader.gif" alt="Đang xử lý..."',
                    "search" => "Tìm kiếm: ",
                    "zeroRecords" => "Không tìm thấy bản ghi nào",
                    "paginate" => [
                        "first" => "«",
                        "last" => "»",
                        "next" => ">",
                        "previous" => "<"
                    ],
                    "aria" => [
                        "sortAscending" => ": activate to sort column ascending",
                        "sortDescending" => ": activate to sort column descending"],
                    "buttons" => [
                        "copy" => "Sao chép",
                        "excel" => "Excel",
                        "csv" => "CSV",
                        "pdf" => "PDF",
                        "print" => "In",
                        "colvis" => "Chọn cột hiển thị"
                    ]
                ],
                'order' => [[0, 'desc']],
                'buttons' => [
                    'export'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'phone', 'name' => 'phone', 'title' => 'Số điện thoại'],
            ['data' => 'message', 'name' => 'message', 'title' => 'Nội dung gửi'],
            ['data' => 'status', 'name' => 'status', 'title' => 'Trạng thái'],
            ['data' => 'error_code', 'name' => 'error_code', 'title' => 'Mã phản hồi'],
            ['data' => 'error_message', 'name' => 'error_message', 'title' => 'Nội dung phản hồi'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Thời gian gửi'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'sms_logsdatatable_' . time();
    }
}
