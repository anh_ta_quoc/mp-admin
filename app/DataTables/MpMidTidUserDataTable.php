<?php

namespace App\DataTables;

use App\Banner;
use App\MpMidTidUser;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class MpMidTidUserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('status_label', function (MpMidTidUser $record) {
                if ($record->status == MpMidTidUser::STATUS_ACTIVE) {
                    return '<span class="label label-sm label-success">Hoạt động</span>';

                } else {
                    return '<span class="label label-sm label-warning">Tạm dừng</span>';
                }
            })
            ->addColumn('type_label', function (MpMidTidUser $record) {
                 return explode("_", $record->type)[0];
            })
            ->addColumn('channel_label', function (MpMidTidUser $record) {
                return explode("_", $record->type)[1];
            })
            ->addColumn('phone', function (MpMidTidUser $record) {
                return $record->mp_user->phone;
            })
            ->addColumn('action', 'mp_mid_tid_user.datatables_actions')

            ->rawColumns(['action', 'status_label'])
            ->order(function ($query){$query->orderBy('id', 'desc');});
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Banner $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MpMidTidUser $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'Thao tác'])
            ->parameters([
                'autoWidth' => false,
                'dom' => '<"row"<"col-xs-8 p-t-5"l><"col-xs-4 text-right hidden-print"B>>" +
                    "<"row m-t-10"<"col-sm-12"tr>>" +
                    "<"row"<"col-sm-6"i><"col-sm-6 hidden-print"p>>',
                'stateSave' => false,
                'language' => [
                    "emptyTable" => "Không có bản ghi nào",
                    "info" => "Hiển thị bản ghi _START_ - _END_ trên tổng _TOTAL_ bản ghi",
                    "infoEmpty" => "Hiển thị 0 bản ghi",
                    "infoFiltered" => "(lọc từ _MAX_ bản ghi)",
                    "infoPostFix" => "",
                    "thousands" => ",",
                    "lengthMenu" => "Hiển thị _MENU_ bản ghi",
                    "loadingRecords" => "Đang tải...",
                    "processing" => 'Đang xử lý...',
//                    "processing" =>  '<img src="https://admin-partner.vnpaytest.vn/vendor/backpack/crud/img/ajax-loader.gif" alt="Đang xử lý..."',
                    "search" => "Tìm kiếm: ",
                    "zeroRecords" => "Không tìm thấy bản ghi nào",
                    "paginate" => [
                        "first" => "«",
                        "last" => "»",
                        "next" => ">",
                        "previous" => "<"
                    ],
                    "aria" => [
                        "sortAscending" => ": activate to sort column ascending",
                        "sortDescending" => ": activate to sort column descending"],
                    "buttons" => [
                        "copy" => "Sao chép",
                        "excel" => "Excel",
                        "csv" => "CSV",
                        "pdf" => "PDF",
                        "print" => "In",
                        "colvis" => "Chọn cột hiển thị"
                    ]
                ],
                'order' => [[0, 'desc']],
                'buttons' => [
                    'export'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'phone', 'name' => 'phone', 'title' => 'SDT'],
            ['data' => 'merchant_code', 'name' => 'merchant_code', 'title' => 'Merchant Code'],
            ['data' => 'type_label', 'name' => 'type_label', 'title' => 'Loại tài khoản'],
            ['data' => 'channel_label', 'name' => 'channel_label', 'title' => 'Kênh đăng nhập'],
            ['data' => 'status_label', 'name' => 'status_label', 'title' => 'Trạng thái'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Ngày tạo'],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'midtiduserdatatable_' . time();
    }
}
