<?php

namespace App\DataTables;

use App\DataTables\CustomDataTableExportHanlder;
use App\Libraries\BaseFunction;
use App\Models\Sync\QrTerminal;
use App\MpTerminal;
use App\Store;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Yajra\DataTables\Services\DataTablesExportHandler;

class ListTerminalsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    private $status= [];
    private $file_name = '';
    private $store;

    function __construct($store, $status = [], $file_name='')
    {
        $this->status = $status;
        $this->file_name = $file_name;
        $this->store = $store;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->filter(function ($query) {
            $query->where('merchant_code', $this->store->merchant_code);
            if ($this->request()->has('filter_status')) {
                $query->where('status', (int)request('filter_status'));
            }
        }, true);
        return $dataTable
            ->addIndexColumn()
            ->addColumn('action', function ($temp){
                if ($this->store->status == BaseFunction::STATUS_CONTROL_ACCEPTED){
                    $route = route('terminals.show', $temp->id);
                }
                else{
                    $route = route('terminals.showFirstTerminal', $this->store->id);
                }
                return view('stores.list_terminals_table_actions', compact('route'))->render();
            })
            ->editColumn('terminal_contact_phone', function ($temp) {
                return strlen($temp->terminal_contact_phone) == 10 ? $temp->terminal_contact_phone :
                    sprintf('0%s', $temp->terminal_contact_phone);
            })
            ->editColumn('bank_brand', function ($temp) {
                return $temp['bank_brand'] ?? $temp['bank_branch'];
            })
            ->editColumn('created_at', function ($temp) {
                return date_format(new \DateTime($temp['created_at']), 'H:i:s d/m/Y') ?? '';
            })
            ->editColumn('approve_reject_date', function ($temp) {
                return $temp['approve_reject_date'] ?? '';
            })
            ->editColumn('custom_status', function ($temp) {
                return $temp['status_label'] ?? $temp['custom_status'];
            })
            ->editColumn('register_vnpayment', function ($temp) {
                $registerVnpaymentChecked = false;
                if (in_array('terminal_register_vnpayment', array_keys(array($temp))) &&
                    $temp['terminal_register_vnpayment'] == 1) {
                    $registerVnpaymentChecked = true;
                }
                elseif (in_array('register_vnpayment', array_keys(array($temp))) &&
                    $temp['register_vnpayment'] == 1) {
                    $registerVnpaymentChecked = true;
                }
                if ($registerVnpaymentChecked) {
                    return
                        '<input class="name" name="register_vnpayment" type="checkbox" value="1" checked disabled>';
                } else {
                    return
                        '<input class="name" name="register_vnpayment" type="checkbox" value="1"  disabled >';

                }
            })
            ->rawColumns(['action', 'custom_status','register_qr','register_vnpayment'])
            ->order(function ($query){$query->orderBy('created_at', 'desc');});
    }

    /**
     * Get query source of dataTable.
     *
     * @param QrTerminal $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        if (!in_array($this->store->status, [BaseFunction::STATUS_CONTROL_ACCEPTED, BaseFunction::STATUS_LOCK])) {
            $model = Store::query();
        }
        else {
            $model = MpTerminal::query();
        }
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'Thao tác'])
            ->minifiedAjax('', 'if ($("#filter_status").val()){data["filter_status"] = $("#filter_status").val();}'
            )
            ->parameters([
                'autoWidth' => false,
                "ordering" => false,
                'dom' => '<"row"<"col-xs-8 p-t-5"l><"col-xs-4 text-right hidden-print">>" +
                    "<"row m-t-10"<"col-sm-12"tr>>" +
                    "<"row"<"col-sm-6"i><"col-sm-6 hidden-print"p>>',
                'stateSave' => false,
                'language' => [
                    "emptyTable" => "Không có bản ghi nào",
                    "info" => "Hiển thị bản ghi _START_ - _END_ trên tổng _TOTAL_ bản ghi",
                    "infoEmpty" => "Hiển thị 0 bản ghi",
                    "infoFiltered" => "(lọc từ _MAX_ bản ghi)",
                    "infoPostFix" => "",
                    "thousands" => ",",
                    "lengthMenu" => "Hiển thị _MENU_ bản ghi",
                    "loadingRecords" => "Đang tải...",
                    "processing" => 'Đang xử lý...',
//                    "processing" =>  '<img src="https://admin-partner.vnpaytest.vn/vendor/backpack/crud/img/ajax-loader.gif" alt="Đang xử lý..."',
                    "search" => "Tìm kiếm: ",
                    "zeroRecords" => "Không tìm thấy bản ghi nào",
                    "paginate" => [
                        "first" => "«",
                        "last" => "»",
                        "next" => ">",
                        "previous" => "<"
                    ],
                    "aria" => [
                        "sortAscending" => ": activate to sort column ascending",
                        "sortDescending" => ": activate to sort column descending"],
                ],
                'order' => [[0, 'desc']],
                'buttons' => [
                    'export'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => 'STT','searchable' => false],
            ['data' => 'terminal_id', 'name' => 'terminal_id', 'title' => 'Terminal ID'],
            ['data' => 'terminal_name', 'name' => 'terminal_name', 'title' => 'Tên Terminal'],
            ['data' => 'terminal_contact_name', 'name' => 'terminal_contact_name', 'title' => 'Tên liên hệ'],
            ['data' => 'terminal_contact_phone', 'name' => 'terminal_contact_phone', 'title' => 'SĐT liên hệ'],
            ['data' => 'bank_number', 'name' => 'bank_number', 'title' => 'Tài khoản thụ hưởng'],
            ['data' => 'bank_brand', 'name' => 'bank_brand', 'title' => 'Ngân hàng','searchable' => false],
            ['data' => 'bank_branch', 'name' => 'bank_branch', 'title' => 'Chi nhánh','searchable' => false],
            ['data' => 'bank_account', 'name' => 'bank_account', 'title' => 'Chủ tài khoản','searchable' => false],
            ['data' => 'register_vnpayment', 'name' => 'register_vnpayment', 'title' => 'CTT','searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Ngày đăng ký', 'searchable' => false],
            ['data' => 'approve_reject_date', 'name' => 'approve_reject_date', 'title' => 'Ngày duyệt/từ chối', 'searchable' => false],
            ['data' => 'custom_status', 'name' => 'custom_status', 'title' => 'Trạng thái','searchable' => false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return $this->file_name. time();
    }

}
