<?php

namespace App\DataTables;

use App\MpTerminalBatch;
use App\User;
use Illuminate\Support\Str;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class MpTerminalBatchDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->filter(function ($query) {
            $start = $end = null;
            if ($this->request()->has('daterange')) {
                if (!empty(request('daterange')['start'])) {
                    $start = request('daterange')['start'];
                }
                if (!empty(request('daterange')['end'])) {
                    $end = request('daterange')['end'];
                }
            }
            if ($start && $end) {
                $sql = 'created_at >= "' . $start . '" AND created_at <= "' . $end . ' 23:59:59"';
                $query->whereRaw($sql);
            }

            if ($this->request()->has('filter_query') && request('filter_query')) {
                $keywordToLowerCase = '%' . Str::lower(trim(request('filter_query'))) . '%';
                $sql = sprintf('(LOWER(merchant_code) LIKE "%s")', $keywordToLowerCase);
                $query->whereRaw($sql);
            }

            if ($this->request()->has('filter_status')) {
                $query->where('status', (int)request('filter_status'));
            }
        }, true);
        return $dataTable
            ->addIndexColumn()
            ->addColumn('action', 'mp_terminal_batches.datatables_actions')
            ->addColumn('error_count', function ($tmp) {
                return sprintf('%s / %s', $tmp->getErrorTerminals(), $tmp->total_rows);
            })
            ->editColumn('updated_by', function ($tmp) {
                if (!$tmp->updated_by) {
                    return '';
                }
                $user = User::where('id', $tmp->updated_by)->first();
                return $user->name ?? '';
            })
            ->rawColumns([
                'file_contract_download_link',
                'file_authorize_download_link',
                'file_terminals_download_link',
                'status_label',
                'action'
            ])
            ->order(function ($query){$query->orderBy('id', 'desc');});
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\MpTerminalBatch $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MpTerminalBatch $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'Thao tác'])
            ->minifiedAjax('',
                'if ($("#date_range_search").val().trim()){data["daterange"] = {"start": date_range_search_startDate, "end": date_range_search_endDate}}
                if ($("#filter_status").val()){data["filter_status"] = $("#filter_status").val();}
                if ($("#filter_query").val()){data["filter_query"] = $("#filter_query").val();}'
            )
            ->parameters([
                'autoWidth' => false,
                "ordering" => false,
                'dom' => '<"row"<"col-xs-8 p-t-5"l><"col-xs-4 text-right hidden-print"B>>" +
                    "<"row m-t-10"<"col-sm-12"tr>>" +
                    "<"row"<"col-sm-6"i><"col-sm-6 hidden-print"p>>',
                'stateSave' => false,
                'language' => [
                    "emptyTable" => "Không có bản ghi nào",
                    "info" => "Hiển thị bản ghi _START_ - _END_ trên tổng _TOTAL_ bản ghi",
                    "infoEmpty" => "Hiển thị 0 bản ghi",
                    "infoFiltered" => "(lọc từ _MAX_ bản ghi)",
                    "infoPostFix" => "",
                    "thousands" => ",",
                    "lengthMenu" => "Hiển thị _MENU_ bản ghi",
                    "loadingRecords" => "Đang tải...",
                    "processing" => 'Đang xử lý...',
//                    "processing" =>  '<img src="https://admin-partner.vnpaytest.vn/vendor/backpack/crud/img/ajax-loader.gif" alt="Đang xử lý..."',
                    "search" => "Tìm kiếm: ",
                    "zeroRecords" => "Không tìm thấy bản ghi nào",
                    "paginate" => [
                        "first" => "«",
                        "last" => "»",
                        "next" => ">",
                        "previous" => "<"
                    ],
                    "aria" => [
                        "sortAscending" => ": activate to sort column ascending",
                        "sortDescending" => ": activate to sort column descending"],
                    "buttons" => [
                        "copy" => "Sao chép",
                        "excel" => "Excel",
                        "csv" => "CSV",
                        "pdf" => "PDF",
                        "print" => "In",
                        "colvis" => "Chọn cột hiển thị"
                    ]
                ],
                'order' => [[0, 'desc']],
                'buttons' => [
                    'export'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => 'STT','searchable' => false],
            ['data' => 'merchant_name', 'name' => 'merchant_name', 'title' => 'Tên doanh nghiệp', 'searchable' => false],
            ['data' => 'merchant_code', 'name' => 'merchant_code', 'title' => 'MST/CMND'],
            ['data' => 'file_contract_download_link', 'name' => 'file_contract_download_link', 'title' => 'Biên bản hợp đồng','searchable' => false],
            ['data' => 'file_terminals_download_link', 'name' => 'file_terminals_download_link', 'title' => 'File Terminals', 'searchable' => false],

            ['data' => 'updated_by', 'name' => 'updated_by', 'title' => 'Người cập nhật', 'searchable' => false],
            ['data' => 'error_count', 'name' => 'error_count', 'title' => 'Số lượng lỗi', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Ngày đăng kí', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Ngày duyệt/từ chối', 'searchable' => false],
            ['data' => 'reject_reason', 'name' => 'reject_reason', 'title' => 'Lý do từ chối', 'searchable' => false],
            ['data' => 'status_label', 'name' => 'status_label', 'title' => 'Trạng thái', 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'mp_terminal_batchesdatatable_' . time();
    }
}
