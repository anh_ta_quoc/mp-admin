<?php

namespace App\DataTables;

use App\Message;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class MessageDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        return $dataTable
            ->addColumn('status_label', function (Message $message) {
                return $message->getStatusLabel($message->status);
            })
            ->addColumn('type_label', function (Message $message) {
                return $message->getTypeLabel($message->type);
            })
            ->addColumn('user_name', function (Message $message) {
                return $message->user->name ?? 'Hệ thống';
            })
            ->addColumn('action', 'messages.datatables_actions')
            ->order(function ($query){$query->orderBy('id', 'desc');});
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Message $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Message $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'Thao tác'])
            ->parameters([
                'autoWidth' => false,
                'dom' => '<"row"<"col-xs-8 p-t-5"l><"col-xs-4 text-right hidden-print"B>>" +
                    "<"row m-t-10"<"col-sm-12"tr>>" +
                    "<"row"<"col-sm-6"i><"col-sm-6 hidden-print"p>>',
                'stateSave' => false,
                'language' => [
                    "emptyTable" => "Không có bản ghi nào",
                    "info" => "Hiển thị bản ghi _START_ - _END_ trên tổng _TOTAL_ bản ghi",
                    "infoEmpty" => "Hiển thị 0 bản ghi",
                    "infoFiltered" => "(lọc từ _MAX_ bản ghi)",
                    "infoPostFix" => "",
                    "thousands" => ",",
                    "lengthMenu" => "Hiển thị _MENU_ bản ghi",
                    "loadingRecords" => "Đang tải...",
                    "processing" => 'Đang xử lý...',
//                    "processing" =>  '<img src="https://admin-partner.vnpaytest.vn/vendor/backpack/crud/img/ajax-loader.gif" alt="Đang xử lý..."',
                    "search" => "Tìm kiếm: ",
                    "zeroRecords" => "Không tìm thấy bản ghi nào",
                    "paginate" => [
                        "first" => "«",
                        "last" => "»",
                        "next" => ">",
                        "previous" => "<"
                    ],
                    "aria" => [
                        "sortAscending" => ": activate to sort column ascending",
                        "sortDescending" => ": activate to sort column descending"],
                    "buttons" => [
                        "copy" => "Sao chép",
                        "excel" => "Excel",
                        "csv" => "CSV",
                        "pdf" => "PDF",
                        "print" => "In",
                        "colvis" => "Chọn cột hiển thị"
                    ]
                ],
                'order' => [[0, 'desc']],
                'buttons' => [
                    'export'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'title', 'name' => 'title', 'title' => 'Tiêu đề'],
            ['data' => 'body', 'name' => 'body', 'title' => 'Nội dung'],
            ['data' => 'link', 'name' => 'link', 'title' => 'Link đi kèm'],
            ['data' => 'type_label', 'name' => 'type_label', 'title' => 'Loại thông báo'],
            ['data' => 'status_label', 'name' => 'status_label', 'title' => 'Trạng thái'],
            ['data' => 'schedule_at', 'name' => 'schedule_at', 'title' => 'Giờ hẹn gửi'],
            ['data' => 'user_name', 'name' => 'user_name', 'title' => 'Người gửi'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'messagesdatatable_' . time();
    }
}
