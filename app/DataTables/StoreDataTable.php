<?php

namespace App\DataTables;

use App\Libraries\BaseFunction;
use App\Store;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Excel;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTablesExportHandler;

class StoreDataTable extends DataTable
{
    private $statusQuery = null;
    private $customColumns = null;

    function __construct($statusQuery = null, $customColumns = null)
    {
        $this->statusQuery = $statusQuery;
        $this->customColumns = $customColumns;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        if ($this->request()->get('action') != 'excel') {
            $dataTable->filter(function ($query) {
                if ($this->request()->has('filter_src') && request('filter_src')) {
                    session()->put('store_info_filter_src', trim(request('filter_src')));
                    switch (request('filter_src')) {
                        case 'MR':
                            $rawQuery = ['(src = "MR" AND mms_status >= 1)'];
                            break;
                        case 'BE':
                            $rawQuery = ['(src is null)'];
                            break;
                        case 'SAPO':
                            $rawQuery = ['(src = "SAPO" AND mms_status >= 1)'];
                            break;
                        case 'MMS':
                            $rawQuery = ['(src = "SYNC_MMS")'];
                            break;
                        default:
                            $rawQuery = ['((src is null or src != "SAPO") or (src = "SAPO" and mms_status >= 1))'];
                    }
                } else {
                    $rawQuery = ['((src is null or src != "SAPO") or (src = "SAPO" and mms_status >= 1))'];
                    session()->remove('store_info_filter_src');
                }
                if ($this->request()->has('filter_query') && request('filter_query')) {
                    session()->put('store_info_filter_query', trim(request('filter_query')));
                    $keyword = '%' . Str::lower($this->request->get('filter_query')) . '%';
                    if (count($rawQuery) == 0) {
                        $rawQuery = ['(LOWER(merchant_code) LIKE "' . $keyword . '" OR LOWER(merchant_name) LIKE "' . $keyword . '" OR LOWER(merchant_contact_name) LIKE "' . $keyword . '")'];
                    }
                    else {
                        $rawQuery[] = '(LOWER(merchant_code) LIKE "' . $keyword . '" OR LOWER(merchant_name) LIKE "' . $keyword . '" OR LOWER(merchant_contact_name) LIKE "' . $keyword . '")';
                    }
                }
                else {
                    session()->remove('store_info_filter_query');
                }

                $start = null;
                $end = null;
                if ($this->request()->has('daterange')) {
                    if (!empty(request('daterange')['start'])) {
                        $start = request('daterange')['start'];
                    }
                    if (!empty(request('daterange')['end'])) {
                        $end = request('daterange')['end'];
                    }
                }
                if ($start && $end) {
                    $rawQuery[] = '(created_at >= "' . $start . ' 00:00:00" AND created_at <= "' . $end . ' 23:59:59")';
                    session()->put('filter_daterange_start', $start);
                    session()->put('filter_daterange_end', $end);
                }
                if ($this->request()->has('filter_status')) {
                    session()->put('store_info_filter_status', trim(request('filter_status')));
                    if (request('filter_status') !== "99") {
                        if (request('filter_status') === '0') {
                            $rawQuery[] = '(
                                status = 0 and (merchant_file_contract is not null or merchant_file_contract != "") and
                                (need_reupload_contract = 0 or need_reupload_contract is null)
                            )';
                        }
                        elseif (request('filter_status') === "-10") {
                            $rawQuery[] = '(status = -10 or (trial_user_id is not null and mms_status = -1) or
                            (src = "SAPO" and mms_status = -1)
                            )';
                        } else {
                            $rawQuery[] = sprintf('(status = %s)', request('filter_status'));
                        }
                    }
                    else {
                        $rawQuery[] = '(
                    (status = 0 and merchant_file_contract is null and (need_reupload_contract = 0 or need_reupload_contract is null)) or
                    (status = 5 and need_reupload_contract = 1)
                    )';
                    }
                } elseif ($this->statusQuery) {
                    $rawQuery[] = sprintf('(%s)', $this->statusQuery);
                    session()->remove('store_info_filter_status');
                }
                if ($this->request()->has('filter_business_type') && trim(request('filter_business_type')) != '') {
                    session()->put('store_info_filter_business_type', trim(request('filter_business_type')));
                    $query->where('merchant_type_business', trim(request('filter_business_type')));
                }
                else {
                    session()->remove('store_info_filter_business_type');
                }
                $query->whereRaw(implode(' and ', $rawQuery));
            }, true);
            return $dataTable
                ->addIndexColumn()
                ->addColumn('action', 'stores.datatables_actions')
                ->rawColumns(['action', 'status_label'])
                ->editColumn('merchant_contact_phone', function (Store $store) {
                    if (substr($store->merchant_contact_phone, 0, 1) == '0') {
                        return $store->merchant_contact_phone;
                    } else {
                        return sprintf('0%s', $store->merchant_contact_phone);
                    }
                })
                ->editColumn('created_at', function ($temp) {
                    return date_format(new \DateTime($temp['created_at']), 'H:i:s d/m/Y') ?? '';
                })
                ->order(function ($query) {
                    $query->orderBy('created_at', 'desc');
                });
        } else {
            return $dataTable;
        }
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Store $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Store $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $export_button = "";
        if (in_array(\Route::current()->getName(), [
            'stores.activeList',
            'stores.index'
        ])) {
            $export_button = "B";
        }

        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('',
                'if ($("#date_range_search").val().trim()){data["daterange"] = {"start": date_range_search_startDate, "end": date_range_search_endDate}}
                if ($("#filter_status").val()){data["filter_status"] = $("#filter_status").val();}
                if ($("#filter_src").val()){data["filter_src"] = $("#filter_src").val();}
                if ($("#filter_query").val()){data["filter_query"] = $("#filter_query").val();}
                if ($("#filter_business_type").val()){data["filter_business_type"] = $("#filter_business_type").val();}'
            )
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'Thao tác'])
            ->parameters([
                'serverSide' => true,
                'autoWidth' => false,
                'dom' => '<"row"<"col-xs-8 p-t-5"l><"col-xs-4 text-right hidden-print"' . $export_button . '>>" +
                    "<"row m-t-10"<"col-sm-12"tr>>" +
                    "<"row"<"col-sm-6"i><"col-sm-6 hidden-print"p>>',
                'stateSave' => false,
                'language' => [
                    "emptyTable" => "Không có bản ghi nào",
                    "info" => "Hiển thị bản ghi _START_ - _END_ trên tổng _TOTAL_ bản ghi",
                    "infoEmpty" => "Hiển thị 0 bản ghi",
                    "infoFiltered" => "(filtered from _MAX_ total entries)",
                    "infoPostFix" => "",
                    "thousands" => ",",
                    "lengthMenu" => "Hiển thị _MENU_ bản ghi",
                    "loadingRecords" => "Đang tải...",
                    "processing" => 'Đang xử lý...',
//                    "processing" =>  '<img src="https://admin-partner.vnpaytest.vn/vendor/backpack/crud/img/ajax-loader.gif" alt="Đang xử lý..."',
                    "search" => "Tìm kiếm: ",
                    "zeroRecords" => "Không tìm thấy bản ghi nào",
                    "paginate" => [
                        "first" => "«",
                        "last" => "»",
                        "next" => ">",
                        "previous" => "<"
                    ],
                    "aria" => [
                        "sortAscending" => ": activate to sort column ascending",
                        "sortDescending" => ": activate to sort column descending"],
                    "buttons" => [
                        "copy" => "Sao chép",
                        "excel" => "Excel",
                        "csv" => "CSV",
                        "pdf" => "PDF",
                        "print" => "In",
                        "colvis" => "Chọn cột hiển thị"
                    ]
                ],
                'order' => [[0, 'desc']],
                'buttons' => [
                    'export'
                ],
            ]);


    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        if ($this->customColumns) {
            return $this->customColumns;
        }
        return [
            ['data' => 'merchant_name', 'name' => 'merchant_name', 'title' => 'Tên doanh nghiệp'],
            ['data' => 'merchant_code', 'name' => 'merchant_code', 'title' => 'MST/CMTND doanh nghiệp'],
            ['data' => 'contract_number', 'name' => 'contract_number', 'title' => 'Số hợp đồng'],
            ['data' => 'merchant_contact_name', 'name' => 'merchant_contact_name', 'title' => 'Người liên hệ'],
            ['data' => 'merchant_contact_phone', 'name' => 'merchant_contact_phone', 'title' => 'SĐT liên hệ'],
            ['data' => 'type_business_name', 'name' => 'type_business_name', 'title' => 'Loại hình doanh nghiệp', 'searchable' => false],
            ['data' => 'type_name', 'name' => 'type_name', 'title' => 'Loại hình kinh doanh', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Ngày tạo'],
            ['data' => 'status_label', 'name' => 'status_label', 'title' => 'Trạng thái', 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Danh sách Merchant_' . Carbon::now()->format('dmY');
    }

    protected $exportClass = StoresDataTableExportHandler::class;

    protected function getDataForExport()
    {
            $query = Store::query();

            if ($this->request()->has('filter_src') && request('filter_src')) {
                switch (request('filter_src')) {
                    case 'MR':
                        $rawQuery = ['(src = "MR" AND mms_status >= 1)'];
                        break;
                    case 'BE':
                        $rawQuery = ['(src is null)'];
                        break;
                    case 'SAPO':
                        $rawQuery = ['(src = "SAPO" AND mms_status >= 1)'];
                        break;
                    case 'MMS':
                        $rawQuery = ['(src = "SYNC_MMS")'];
                        break;
                    default:
                        $rawQuery = ['((src is null or src != "SAPO") or (src = "SAPO" and mms_status >= 1))'];
                }
            } else {
                $rawQuery = ['((src is null or src != "SAPO") or (src = "SAPO" and mms_status >= 1))'];
            }
            if ($this->request->get('filter_query')) {
                $keyword = '%' . Str::lower($this->request->get('filter_query')) . '%';
                if (count($rawQuery) == 0) {
                    $rawQuery = ['(LOWER(merchant_code) LIKE "' . $keyword . '" OR LOWER(merchant_name) LIKE "' . $keyword . '" OR LOWER(merchant_contact_name) LIKE "' . $keyword . '")'];
                }
                else {
                    $rawQuery[] = '(LOWER(merchant_code) LIKE "' . $keyword . '" OR LOWER(merchant_name) LIKE "' . $keyword . '" OR LOWER(merchant_contact_name) LIKE "' . $keyword . '")';
                }
            }
            $start = null;
            $end = null;
            if ($this->request()->has('daterange')) {
                if (!empty(request('daterange')['start'])) {
                    $start = request('daterange')['start'];
                }
                if (!empty(request('daterange')['end'])) {
                    $end = request('daterange')['end'];
                }
            }
            if ($start && $end) {
                $rawQuery[] = '(created_at >= "' . $start . ' 00:00:00" AND created_at <= "' . $end . ' 23:59:59")';
            }
            if ($this->request()->has('filter_status')) {
                if (request('filter_status') !== "99") {
                    if (request('filter_status') === '0') {
                        $rawQuery[] = '(
                            status = 0 and (merchant_file_contract is not null or merchant_file_contract != "") and
                            (need_reupload_contract = 0 or need_reupload_contract is null)
                        )';
                    } else {
                        $rawQuery[] = sprintf('(status = %s)', request('filter_status'));
                    }
                } else {
                    $rawQuery[] = '(
                    (status = 0 and merchant_file_contract is null and (need_reupload_contract = 0 or need_reupload_contract is null)) or
                    (status = 5 and need_reupload_contract = 1)
                    )';
                }
            } elseif ($this->statusQuery) {
                $rawQuery[] = sprintf('(%s)', $this->statusQuery);
            }
            if ($this->request()->has('filter_business_type') && trim(request('filter_business_type')) != '') {
//            $query->where('merchant_type_business', trim(request('filter_business_type')));
                $query->whereRaw(sprintf("merchant_type_business = '%s'", trim(request('filter_business_type'))));
            }
            $query->whereRaw(implode(' and ', $rawQuery));
            $query->orderBy('created_at', 'desc');
            $results = collect(DB::connection('rest_api_db')->select(DB::raw("select  a.id as 'id', a.created_by as 'created_by' , a.trial_user_id as 'trial_id', " .
                "a.merchant_name as 'merchant_name', " .
                "a.merchant_code as 'merchant_code', " .
                "a.contract_code as 'contract_code', " .
                "a.contract_number as 'contract_number', " .
                "a.contract_date as 'contract_date', " .
                "a.merchant_contact_email as 'merchant_contact_email', " .
                "a.merchant_contact_name as 'merchant_contact_name', " .
                "a.merchant_contact_phone as 'merchant_contact_phone', " .
                "a.merchant_type_business as 'merchant_type_business', " .
                "a.merchant_type as 'merchant_type', " .
                "a.status as 'status', " .
                "a.operator_note as 'operator_note', " .
                "DATE_FORMAT(a.created_at, '%d/%m/%Y') as 'created_at', " .
                "a.created_at as 'real_created_at' ," .
                "a.src as 'src' " .
                "from (" . $query->toSql() . ") a order by real_created_at desc")));

            $results1 = collect(Db::connection('oracle')->select(DB::raw("select merchant_code, account_vnmart from mms.qr_account_vnmart where  (merchant_code = '" . implode("' or merchant_code  ='", array_filter(Arr::pluck($results, 'merchant_code'))) . "') order by merchant_code , created_date asc ")));

            $results2 = collect(Db::connection('oracle')->select(DB::raw("select merchant_code, TO_CHAR(min(pay_date),'DD/MM/YYYY') pay_date from mms.qr_transaction where (merchant_code = '" . implode("' or merchant_code  ='", array_filter(Arr::pluck($results, 'merchant_code'))) . "') group by merchant_code")));

            $results3 = collect(Db::select(DB::raw("SELECT a.store_id , b.user_id , c.name, c.email , DATE_FORMAT(a.max_created_at, '%d/%m/%Y') as 'c_date' from (SELECT store_id, max(created_at) as 'max_created_at' FROM `audit_logs` WHERE message='Kinh doanh duyệt' and  (store_id =".implode(" or store_id= ", array_filter(Arr::pluck($results, 'id'))).") group by store_id ) a join audit_logs b on a.store_id = b.store_id and a.max_created_at = b.created_at left join users c on b.user_id = c.id ")));

            $results4 = collect(Db::select(DB::raw("SELECT a.store_id , b.user_id , c.name, c.email , DATE_FORMAT(a.max_created_at, '%d/%m/%Y') as 'c_date' from (SELECT store_id, max(created_at) as 'max_created_at' FROM `audit_logs` WHERE message='Đối soát duyệt' and  (store_id =".implode(" or store_id= ", array_filter(Arr::pluck($results, 'id'))).") group by store_id ) a join audit_logs b on a.store_id = b.store_id and a.max_created_at = b.created_at left join users c on b.user_id = c.id ")));
            if (count(array_filter(Arr::pluck($results, 'created_by'))) > 0) {

                $results5 = collect(Db::select(DB::raw("SELECT  id, name, email from users where (id = ".implode(" or id =", array_filter(Arr::pluck($results, 'created_by'))).")")));
            } else {
                $results5 = null;
            }
            if (count(array_filter(Arr::pluck($results, 'trial_id'))) > 0) {

                $results6 = collect(Db::connection('rest_api_db')->select(DB::raw("SELECT id, name, email from trial_user where (id = ".implode(" or id =", array_filter(Arr::pluck($results, 'trial_id'))).")")));
            } else {
                $results6 = null;
            }
            $final_results = array();
            foreach ($results as $key => $value) {
                $src = '';
                $created_by = '';
                if ($value->src == 'SYNC_MMS') {
                    $created_by = ($results5 != null && $results5->where('id', $value->created_by)->first()) ? $results5->where('id', $value->created_by)->first()->email : 'Unknown';
                    $src = 'MMS';
                }
                elseif ($value->src == 'SAPO') {
                    $created_by = 'SAPO';
                    $src = 'SAPO';
                }
                elseif ($value->trial_id != null) {
                    $created_by = ($results6 != null && $results6->where('id', $value->trial_id)->first()) ? $results6->where('id', $value->trial_id)->first()->email : 'Unknown';
                    $created_by = $created_by;
                    $src = 'MR';
                } else {
                    $created_by = ($results5 != null && $results5->where('id', $value->created_by)->first()) ? $results5->where('id', $value->created_by)->first()->email : 'Unknown';
                    $created_by = $created_by;
                    $src = ($value->src) ? $value->src : 'Admin Site';

                }
                if (\Route::current()->getName() == 'stores.activeList'){
                    $item['STT'] = $key + 1;
                    $item['Số ví'] = ($results1->where('merchant_code', $value->merchant_code)->first()) ? $results1->where('merchant_code', $value->merchant_code)->first()->account_vnmart : '';
                    $item['Tên MC'] =$value->merchant_name;
                    $item['Mã MC'] = $value->merchant_code;
                    $item['Ngày duyệt'] = ($results4->where('store_id', $value->id)->first()) ? $results4->where('store_id', $value->id)->first()->c_date : '';
                    $item['Kỳ thanh toán'] = 'T+1';
                    $item['Phí nội đia'] = '0.88%';
                    $item['Phí quốc tế'] = '';
                    $item['Loại HĐ'] = 'Thông thường';
                    $item['Số HĐ/PL'] = $value->contract_number;
                    $item['Ngày ký HĐ'] = $value->contract_date;
                    $item['Ngày PS GD đầu tiên'] = ($results2->where('merchant_code', $value->merchant_code)->first()) ? $results2->where('merchant_code', $value->merchant_code)->first()->pay_date : '';
                    $item['Ngày bắt đầu tính phí'] = $value->contract_date;
                    $item['Thời gian ưu đãi'] = '';
                    $item['Email liên hệ'] = $value->merchant_contact_email;
                    $item['Ghi chú'] = '';
                    $item['Người khởi tạo'] = $created_by;
                    $item['Ngày khởi tạo'] = $value->created_at;
                    $item['KD Duyệt'] = ($results3->where('store_id', $value->id)->first()) ? $results3->where('store_id', $value->id)->first()->email : '';
                    $item['Ngày KD Duyệt'] = ($results3->where('store_id', $value->id)->first()) ? $results3->where('store_id', $value->id)->first()->c_date : '';
                    $item['ĐS Duyệt'] = ($results4->where('store_id', $value->id)->first()) ? $results4->where('store_id', $value->id)->first()->email : '';
                    $item['Nguồn Khởi tạo'] = $src;
                }
                else {
                    $item['STT'] = $key + 1;
                    $item['Tên doanh nghiệp'] =$value->merchant_name;
                    $item['MST/CMTND doanh nghiệp'] = $value->merchant_code;
                    $item['Số hợp đồng'] = $value->contract_number;
                    $item['Người liên hệ'] = $value->merchant_contact_name;
                    $item['SĐT liên hệ'] = $value->merchant_contact_phone;
                    $item['Loại hình doanh nghiệp'] = Store::getTypeBusinessName($value->merchant_type_business);
                    $item['Loại hình kinh doanh'] = Store::getTypeName($value->merchant_type);
                    $item['Ngày tạo'] = $value->created_at;
                    $item['Trạng thái'] = BaseFunction::getStoreStatus()[$value->status];;
                }
                array_push($final_results, $item);
            }
            return $final_results;

    }
}
