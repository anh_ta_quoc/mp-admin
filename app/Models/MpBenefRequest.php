<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class MpBenefRequest extends Model
{
    public $table = 'mp_benef_request';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const STATUS_INIT = 0;
    const STATUS_WAIT_SENT_RESPONSE = 1;
    const STATUS_SENT = 10;
    const STATUS_SEND_FAILED = 11;
    const STATUS_SEND_ERROR = 12;
    const STATUS_REJECTED = 20;
    const STATUS_APPROVED = 30;
    const STATUS_UPDATE_FAILED = 90;
    const STATUS_UPDATED = 100;
    const STATUS_PARTIAL_UPDATED = 110;

    const STATUS = [
        self::STATUS_INIT => 'Khởi tạo',
        self::STATUS_SENT => 'Chờ duyệt',
        self::STATUS_SEND_FAILED => 'Gửi yêu cầu lỗi',
        self::STATUS_REJECTED => 'Từ chối',
        self::STATUS_APPROVED => 'Đã nhận thông tin duyệt (Chờ cập nhật)',
        self::STATUS_UPDATE_FAILED => 'Cập nhật lỗi',
        self::STATUS_UPDATED => 'Thành công',
    ];

    public $fillable = [
        'id',
        'request_id',
        'merchant_code',
        'terminals',
        'terminal_names',
        'terminals_add_failed',
        'add_response_payload',
        'update_payloads',
        'benef_number',
        'benef_name',
        'benef_type',
        'benef_bank_name',
        'benef_bank_code',
        'benef_bank_branch',
        'doc_url',
        'create_user',
        'ref',
        'status',
        'mms_status',
        'mms_code',
        'process_lock',
        'process_msg',
        'retry_count',
        'created_by',
        'updated_by'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'request_id' => 'string',
        'merchant_code' => 'string',
        'terminals' => 'string',
        'add_response_payload' => 'string',
        'update_payloads' => 'string',
        'benef_number' => 'string',
        'benef_name' => 'string',
        'benef_type' => 'string',
        'benef_bank_name' => 'string',
        'benef_bank_code' => 'string',
        'benef_bank_branch' => 'string',
        'doc_url' => 'string',
        'create_user' => 'string',
        'status' => 'integer',
        'process_lock' => 'string',
        'process_msg' => 'string'
    ];

    protected $appends = [];

    const FIELD_MEANINGS = [
        'request_id' => 'Request ID',
        'merchant_code' => 'Merchant Code',
        'terminals' => 'Terminals',
        'benef_number' => 'Số TK thụ hưởng',
        'benef_name' => 'Tên TK thụ hưởng',
        'benef_type' => 'Loại thụ hưởng',
        'benef_bank_name' => 'Tên NH thụ hưởng',
        'benef_bank_code' => 'Mã NH thụ hưởng',
        'benef_bank_branch' => 'Chi nhánh NH thụ hưởng',
        'doc_url' => 'Công văn thay đổi',
        'create_user' => 'Tên người thụ hưởng'
    ];

    public static function getFieldMeanings($field)
    {
        $fieldMeanings = self::FIELD_MEANINGS;
        return in_array($field, array_keys($fieldMeanings)) ? $fieldMeanings["$field"] : 'Unknown';
    }
}
