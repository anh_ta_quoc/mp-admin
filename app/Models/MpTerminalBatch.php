<?php


namespace App;


use App\Libraries\BaseFunction;
use Illuminate\Database\Eloquent\Model;

class MpTerminalBatch extends Model
{
    public $table = 'mp_terminal_batch';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const STATUS_INIT = 0;

    const STATUS_PRECHECK_PROCESSING = 10;
    const STATUS_PRECHECK_ERROR = 11;

    const STATUS_WAIT_SALE_APPROVE = 20;
    const STATUS_SALE_DENIED = 21;

    const STATUS_WAIT_CONTROL_APPROVE = 30;
    const STATUS_CONTROL_DENIED = 31;

    const STATUS_CONTROL_APPROVED = 40;
    const STATUS_WAIT_MMS = 50;
    const STATUS_MMS_ERROR = 51;

    const STATUS_CONTROL_REOPEN = 60;

    const STATUS_DONE = 100;

    const STATUS = [
        self::STATUS_INIT => 'Khởi tạo',

        self::STATUS_PRECHECK_PROCESSING => 'Đang xử lý',
        self::STATUS_PRECHECK_ERROR => 'Lỗi kiểm tra file',

        self::STATUS_WAIT_SALE_APPROVE => 'Chờ KD duyệt',
        self::STATUS_SALE_DENIED => 'KD từ chối',

        self::STATUS_WAIT_CONTROL_APPROVE => 'Chờ ĐS duyệt',
        self::STATUS_CONTROL_DENIED => 'ĐS Từ chôi',

        self::STATUS_CONTROL_APPROVED => 'Chờ MMS phản hồi',
        self::STATUS_WAIT_MMS => 'Chờ MMS phản hồi',
        self::STATUS_MMS_ERROR => 'Lỗi MMS phản hồi',

        self::STATUS_CONTROL_REOPEN => 'ĐS mở lại HS',

        self::STATUS_DONE => 'Hoạt động',
    ];

    const STATUS_LABEL = [
        self::STATUS_INIT => 'label label-primary',

        self::STATUS_PRECHECK_PROCESSING => 'label label-warning',
        self::STATUS_PRECHECK_ERROR => 'label label-danger',

        self::STATUS_WAIT_SALE_APPROVE => 'label label-warning',
        self::STATUS_SALE_DENIED => 'label label-danger',

        self::STATUS_WAIT_CONTROL_APPROVE => 'label label-warning',
        self::STATUS_CONTROL_DENIED => 'label label-danger',

        self::STATUS_CONTROL_APPROVED => 'label label-warning',
        self::STATUS_WAIT_MMS => 'label label-info',
        self::STATUS_MMS_ERROR => 'label label-danger',

        self::STATUS_CONTROL_REOPEN => 'label label-danger',

        self::STATUS_DONE => 'label label-success',
    ];

    public $fillable = [
        'id',
        'merchant_code',
        'bank_code',
        'bank_branch',
        'bank_number',
        'bank_account',
        'file_contract',
        'file_authorize',
        'file_terminals',
        'file_result',
        'status',
        'total_rows',
        'precheck_success',
        'process_lock',
        'process_msg',
        'retry_count',
        'created_by',
        'updated_by',
        'reject_reason',
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'merchant_code' => 'string',
        'bank_code' => 'string',
        'bank_branch' => 'string',
        'bank_number' => 'string',
        'bank_account' => 'string',
        'file_contract' => 'string',
        'file_authorize' => 'string',
        'file_terminals' => 'string',
        'file_result' => 'string',
        'status' => 'integer',
        'total_rows' => 'integer',
        'precheck_success' => 'integer',
        'process_lock' => 'string',
        'process_msg' => 'string',
        'retry_count' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'reject_reason' => 'string',
    ];

    protected $appends = array(
        'merchant_name',
        'bank_name',
        'file_contract_download_link',
        'file_authorize_download_link',
        'file_terminals_download_link',
        'status_label',
    );

//    public function getFileResultAttribute()
//    {
//        $attribute = $this->attributes;
//        $fileResult = 'fsafadsfdsafsa';
//        return $fileResult;
//    }

    public function getMerchantNameAttribute()
    {
        $merchant = BaseFunction::getMerchantByCode($this->merchant_code);
        if ($merchant) return $merchant->merchant_name;
        return "";
    }

    public function getBankNameAttribute()
    {
        $bank = Bank::where('bank_code', $this->bank_code)->first();
        if ($bank) {
            return $bank->bank_name;
        } else {
            return '';
        }
    }

    public function getFileContractDownloadLinkAttribute()
    {
        $file_path = '/api_uploads/terminal_batch/' . $this->merchant_code . '/' . $this->file_contract;
        return "<a download href='" . $file_path . "'><span class='fa fa-download'></span></a>";
    }

    public function getFileAuthorizeDownloadLinkAttribute()
    {
        $file_path = '/api_uploads/terminal_batch/' . $this->merchant_code . '/' . $this->file_authorize;
        return "<a download href='" . $file_path . "'><span class='fa fa-download'></span></a>";
    }

    public function getFileTerminalsDownloadLinkAttribute()
    {
        $file_path = '/api_uploads/terminal_batch/' . $this->merchant_code . '/' . $this->file_terminals;
        return "<a download href='" . $file_path . "'><span class='fa fa-download'></span></a>";
    }

    public function getStatusLabelAttribute()
    {
        return "<label class='" . self::STATUS_LABEL[$this->status] . "'>" . self::STATUS[$this->status] . "</label>";
    }


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'merchant_code' => 'required',
        'bank_code' => 'required',
        'bank_branch' => 'required',
        'bank_number' => 'required',
        'bank_account' => 'required',
        'file_contract' => 'required',
        'file_authorize' => 'required',
        'file_terminals' => 'required'
    ];

    public function getErrorTerminals()
    {
        if ($this->status == self::STATUS_PRECHECK_ERROR){
            return $this->total_rows - $this->precheck_success;
        }
        elseif ($this->status == self::STATUS_MMS_ERROR){
            return MpTerminal::where('src', 'BATCH-' . $this->id)
                ->where('mms_status', MpTerminal::MMS_STATUS_FAILED)
                ->count();
        }
        else{
            return 0;
        }
    }
}
