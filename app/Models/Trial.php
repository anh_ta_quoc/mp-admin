<?php

namespace App;

use Eloquent as Model;

/**
 * Class Trial
 * @package App
 * @version December 20, 2019, 7:10 am UTC
 *
 * @property string name
 * @property string phone
 * @property string email
 */
class Trial extends Model
{
//    use SoftDeletes;

    public $table = 'trial_user';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


//    protected $dates = ['deleted_at'];

    public $connection = "rest_api_db";

    public $fillable = [
        'name',
        'phone',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'phone' => 'required',
        'created_at' => 'required',
        'updated_at' => 'required'
    ];

    
}
