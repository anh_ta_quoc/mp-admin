<?php

namespace App;

use App\Department;
use App\Libraries\BaseFunction;
use App\Staff;
use App\AuditLog;
use Eloquent as Model;
use App\BusinessProduct;
use App\User;
use App\TypeMerchant;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class MpMidTidUser
 * @package App
 * @version December 16, 2019, 11:28 am UTC
 *
 * @property integer user_id
 * @property string merchant_id
 * @property string merchant_code
 * @property string terminal_id
 * @property string terminal_code
 * @property string type
 * @property string status
 */
class MpMidTidUser extends Model
{
//    use SoftDeletes;

    public $table = 'mp_mid_tid_user';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;


    public $connection = "rest_api_db";

    public $fillable = [
        'user_id',
        'status',
        'merchant_id',
        'merchant_code',
        'type',
        'terminal_id',
        'terminal_code'
    ];

    public function mp_user()
    {
        return $this->belongsTo('App\MpUser','user_id','id');
    }
    public function store()
    {
        return $this->belongsTo('App\Store','merchant_code','merchant_code');
    }
    public function mp_permissions()
    {
        return $this->hasMany('App\MpMapPermission','mid_tid_user_id','id');
    }


}
