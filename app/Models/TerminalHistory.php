<?php

namespace App;

use App\Bank;
use App\BusinessProduct;
use App\District;
use App\Libraries\BaseFunction;
use App\Models\Sync\QrMerchant;
use App\Province;
use App\Staff;
use App\Ward;
use Eloquent as Model;
use App\User;

class TerminalHistory extends Model
{

    public $table = 'terminal_history';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const TYPE_OPERATOR_CREATE_TERMINAL = 1;
    const TYPE_SALE_ADMIN_REQUESTED = 10;
    const TYPE_SALE_APPROVED = 20;
    const TYPE_SALE_DENIED = 21;
    const TYPE_SALE_REOPEN = 22;
    const TYPE_REVIEW_APPROVED = 30;
    const TYPE_REVIEW_DENIED = 31;
    const TYPE_REVIEW_REOPEN = 32;
    const TYPE_UPDATE_INFO = 42;
    const TYPE_SYSTEM_SYNC = 52;
    const TYPE_MMS_SYNC = 62;
    const TYPE_LOCK = 71;
    const TYPE_UNLOCK = 72;


    const TYPE_BATCH_SALE_APPROVED = 200;
    const TYPE_BATCH_SALE_DENIED = 210;
    const TYPE_BATCH_REVIEW_APPROVED = 300;
    const TYPE_BATCH_REVIEW_DENIED = 310;


    const TYPES = [
        self::TYPE_OPERATOR_CREATE_TERMINAL => 'Tạo mới Terminal',
        self::TYPE_SALE_ADMIN_REQUESTED => 'Sale Admin gửi duyệt',
        self::TYPE_SALE_APPROVED => 'Kinh doanh duyệt',
        self::TYPE_SALE_DENIED => 'Kinh doanh từ chối',
        self::TYPE_SALE_REOPEN => 'Kinh doanh mở lại hồ sơ',
        self::TYPE_REVIEW_APPROVED => 'Đối soát duyệt',
        self::TYPE_REVIEW_DENIED => 'Đối soát từ chối',
        self::TYPE_REVIEW_REOPEN => 'Đối soát mở lại hồ sơ',
        self::TYPE_UPDATE_INFO => 'Sửa thông tin',
        self::TYPE_SYSTEM_SYNC => 'Hệ thống đồng bộ',
        self::TYPE_MMS_SYNC => 'Đồng bộ từ MMS',
        self::TYPE_LOCK => 'Khóa Terminal',
        self::TYPE_UNLOCK => 'Mở khóa Terminal',

        self::TYPE_BATCH_SALE_APPROVED => 'Kinh doanh duyệt lô',
        self::TYPE_BATCH_SALE_DENIED => 'Kinh doanh từ chối lô',
        self::TYPE_BATCH_REVIEW_APPROVED => 'Đối soát duyệt lô',
        self::TYPE_BATCH_REVIEW_DENIED => 'Đối soát từ chối lô',

    ];

    public $fillable = [
        'id',
        'mp_terminal_id',
        'terminal_id',
        'staff_id',
        'type',
        'reason',
        'old_data',
        'new_data'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'mp_terminal_id' => 'integer',
        'terminal_id' => 'string',
        'staff_id' => 'integer',
        'type' => 'integer',
        'reason' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    protected $appends = array(
        'staff_name',
        'type_name'
    );

    public function getStaffNameAttribute()
    {
        $user = User::where('id', $this->staff_id)->first();
        if ($user) {
            return $user->name;
        }
        return '';
    }

    public function getTypeNameAttribute()
    {
        return self::TYPES[$this->type] ?? 'Unknown';
    }
}
