<?php

namespace App;

use Eloquent as Model;
use App\User;

/**
 * Class Message
 * @package App
 * @version January 13, 2020, 8:37 am UTC
 *
 * @property string title
 * @property string body
 * @property string link
 * @property boolean read
 * @property integer type
 * @property boolean status
 * @property boolean track_sent
 * @property boolean track_read
 * @property integer user_id
 * @property integer plan_send_number
 * @property integer sent_number
 * @property integer plan_read_number
 * @property integer read_number
 * @property string|\Carbon\Carbon schedule_at
 * @property string|\Carbon\Carbon sent_at
 */
class Message extends Model
{

    public $table = 'message';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'title',
        'body',
        'link',
        'read',
        'type',
        'status',
        'schedule_at',
        'sent_at',
        'receivers',
        'user_id',
        'track_sent',
        'track_read',
        'is_schedule',
        'plan_send_number',
        'sent_number',
        'plan_read_number',
        'read_number',
        'trace_id',
        'tid_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'body' => 'string',
        'link' => 'string',
        'read' => 'boolean',
        'type' => 'integer',
        'status' => 'integer',
        'schedule_at' => 'datetime',
        'sent_at' => 'datetime',
        'receivers' => 'longtext'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'title' => 'required',
//        'link' => 'required',
//        'read' => 'required',
//        'tracking' => 'required',
//        'type' => 'required',
//        'status' => 'required'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getStatusLabel($status=0)
    {
        switch ($status) {
            case 0:
                return 'Khởi tạo';
            case 1:
                return 'Đã xử lý';
            default:
                return 'Không xác định';
        }
    }

    public function getTypeLabel($type=1)
    {
        return 'Thông báo hệ thống';
    }
}
