<?php

namespace App;

use App\Department;
use App\Libraries\BaseFunction;
use App\Staff;
use App\AuditLog;
use Eloquent as Model;
use App\BusinessProduct;
use App\User;
use App\TypeMerchant;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
/**
 * Class MpMapPermission
 * @package App
 * @version December 16, 2019, 11:28 am UTC
 *
 * @property integer mid_tid_user_id
 * @property string permission_id



 */
class MpMapPermission extends Model
{
//    use SoftDeletes;

    public $table = 'mp_map_permission';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $connection = "rest_api_db";

    public $fillable = [
        'mid_tid_user_id',
        'permission_id'
    ];



}
