<?php

namespace App;

use App\Department;
use App\Libraries\BaseFunction;
use App\Staff;
use App\AuditLog;
use Eloquent as Model;
use App\BusinessProduct;
use App\User;
use App\TypeMerchant;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
/**
 * Class MpMidTidUser
 * @package App
 * @version December 16, 2019, 11:28 am UTC
 *
 * @property string name
 * @property string phone
 * @property string email
 * @property string ums_id
 * @property string status

 */
class MpUser extends Model
{
//    use SoftDeletes;

    public $table = 'mp_user';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;


    public $connection = "rest_api_db";

    public $fillable = [
        'name',
        'status',
        'email',
        'phone',
        'ums_id'
    ];



}
