<?php

namespace App;

use App\Bank;
use App\BusinessProduct;
use App\District;
use App\Libraries\BaseFunction;
use App\Models\Sync\QrMerchant;
use App\Province;
use App\Ward;
use Eloquent as Model;
use App\User;

class MpTerminal extends Model
{

    public $table = 'mp_terminals';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    const STATUS_INIT = 0;
    const STATUS_REOPEN = 1;
    const STATUS_REVIEW_REOPEN = 2;
    const STATUS_SALE_ADMIN_REQUESTED = 10;
    const STATUS_SALE_APPROVED = 20;
    const STATUS_SALE_DENIED = 21;
    const STATUS_REVIEW_APPROVED = 30;
    const STATUS_REVIEW_DENIED = 31;
    const STATUS_LOCK = -10;

    const STATUS = [
        self::STATUS_REVIEW_APPROVED => 'Hoạt động',
        self::STATUS_INIT => 'Khởi tạo',
        self::STATUS_REOPEN => 'KD Mở lại hồ sơ',
        self::STATUS_REVIEW_REOPEN => 'ĐS Mở lại hồ sơ',
        self::STATUS_SALE_ADMIN_REQUESTED => 'Chờ KD duyệt',
        self::STATUS_SALE_APPROVED => 'Chờ ĐS duyệt',
        self::STATUS_SALE_DENIED => 'KD từ chối',
        self::STATUS_REVIEW_DENIED => 'ĐS từ chối',
        self::STATUS_LOCK => 'Đóng',
    ];

    const MMS_STATUS_FAILED = -1;
    const MMS_STATUS_INIT = 0;
    const MMS_STATUS_CREATED = 1;
    const MMS_STATUS = [
        self::MMS_STATUS_INIT => 'Khởi tạo',
        self::MMS_STATUS_FAILED => 'Đăng ký lỗi',
        self::MMS_STATUS_CREATED => 'Đăng ký thành công',
    ];

    const RECEIVE_METHOD = [
        "0" => 'Nhận tiền bằng tài khoản ngân hàng',
        "1" => 'Nhận tiền bằng tài khoản ví',
    ];


    public $fillable = [
        'id',
        'merchant_code',
        'terminal_id',
        'terminal_code',
        'terminal_name',
        'terminal_type',
        'product_description',
        'mcc',
        'terminal_address',
        'terminal_province_code',
        'terminal_district_code',
        'terminal_ward_code',
        'website',
        'facebook',
        'file_contract',
        'file_auth_letter',
        'file_other',
        'register_qr',
        'register_vnpayment',
        'bank_code',
        'bank_branch',
        'bank_number',
        'bank_account',
        'receive_method',
        'terminal_contact_name',
        'terminal_contact_phone',
        'terminal_contact_email',
        'terminal_app',
        'status',
        'mms_status',
        'created_by',
        'updated_by',
        'src',
        'mms_error_code',
        'lock_edit',
        'receive_phone',
        'receive_email',
        'src',
        'terminal_app_user'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'merchant_code' => 'string',
        'terminal_id' => 'string',
        'terminal_code' => 'string',
        'terminal_name' => 'string',
        'terminal_type' => 'string',
        'product_description' => 'string',
        'mcc' => 'string',

        'terminal_address' => 'string',
        'terminal_province_code' => 'string',
        'terminal_district_code' => 'string',
        'terminal_ward_code' => 'string',

        'website' => 'string',
        'facebook' => 'string',
        'file_contract' => 'string',
        'file_auth_letter' => 'string',
        'file_other' => 'string',

        'register_qr' => 'integer',
        'register_vnpayment' => 'integer',

        'bank_code' => 'string',
        'bank_branch' => 'string',
        'bank_number' => 'string',
        'bank_account' => 'string',
        'receive_method' => 'string',

        'terminal_contact_name' => 'string',
        'terminal_contact_phone' => 'string',
        'terminal_contact_email' => 'string',

        'terminal_app' => 'integer',
        'status' => 'integer',
        'mms_status' => 'integer',
        'mms_error_code' => 'string',
        'lock_edit' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'merchant_code' => 'required',
        'terminal_name' => 'required',
        'terminal_type' => 'required',

        'product_description' => 'required',
        'mcc' => 'required',
        'terminal_province_code' => 'required',
        'terminal_district_code' => 'required',
        'terminal_ward_code' => 'required',

        'file_contract' => 'required',

        'bank_code' => 'required',
        'bank_branch' => 'required',
        'bank_number' => 'required',
        'bank_account' => 'required',
        'receive_method' => 'required',

        'terminal_contact_name' => 'required',
        'terminal_contact_phone' => 'required',
        'terminal_contact_email' => 'required',
    ];

    protected $appends = array(
        'terminal_province_name',
        'terminal_district_name',
        'terminal_ward_name',
        'type_business_name',
        'type_name',
        'bank_name',
        'bank_brand',
        'receive_method_name',
        'mcc_name',
        'approve_reject_date',
        'custom_status',
        'str_bank_number',
        'bank_title',
        'terminal_type_name'
    );

    public static $fieldLabel = [
        'merchant_code' => 'Mã merchant',
        'terminal_id' => 'Terminal ID',
        'terminal_code' => 'Mã terminal',
        'terminal_name' => 'Tên terminal',
        'terminal_type' => 'Loại hình kinh doanh',
        'product_description' => 'Mô tả thêm',
        'mcc' => 'MCC',
        'terminal_address' => 'Địa chỉ điểm bán',
        'terminal_province_code' => 'Tỉnh thành',
        'terminal_district_code' => 'Quận huyện',
        'terminal_ward_code' => 'Phường xã',
        'website' => 'Website',
        'facebook' => 'Facebook',
        'file_contract' => 'Biên bản hợp đồng',
        'file_auth_letter' => 'Giấy ủy quyền',
        'file_other' => 'Giấy tờ khác',
        'register_qr' => 'Đăng ký QR',
        'register_vnpayment' => 'Đăng ký cổng thanh toán',
        'bank_code' => 'Ngân hàng',
        'bank_branch' => 'Chi nhánh ngân hàng',
        'bank_number' => 'Số tài khoản',
        'bank_account' => 'Chủ tài khoản',
        'receive_method' => 'Phương thức nhận tiền',
        'terminal_contact_name' => 'Tên liên hệ',
        'terminal_contact_phone' => 'Số điện thoại liên hệ',
        'terminal_contact_email' => 'Email liên hệ',
        'terminal_app' => 'Ter app',
        'status' => 'Trạng thái',
        'mms_status' => 'Trạng thái gửi MMS',
        'created_by' => 'Tạo bởi',
        'updated_by' => 'Thao tác bởi',
        'src' => 'Nguồn ter',
        'terminal_app_user' => 'Tên đăng nhập ter app'
    ];

    public function getTypeBusinessNameAttribute()
    {
        $merchant = BaseFunction::getMerchantByCode($this->merchant_code);
        if ($merchant){
            $merchantType = BaseFunction::getMerchantTypeBusiness();
            return $merchantType[$merchant->merchant_type_enterprise] ?? '';
        }
        return '';
    }

    public function getTypeNameAttribute(){
        $businessProducts = BusinessProduct::all()->pluck('name','type_code');
        return $businessProducts[$this->terminal_type] ?? '';
    }

    public function getTerminalStatus(){
        return self::STATUS[$this->status] ?? '';
    }

    public function getTerminalTypeNameAttribute() {
        $business_type = BusinessProduct::where('type_code', $this->terminal_type)->first();
        if ($business_type) {
            return $business_type->bp_title;
        }
        return '';
    }

    public function getTerminalStatusLabel(){
        $label = 'danger';
        if (in_array($this->status, [self::STATUS_INIT, self::STATUS_REOPEN, self::STATUS_REVIEW_REOPEN])){
            $label = 'primary';
        }
        else if (in_array($this->status, [self::STATUS_SALE_ADMIN_REQUESTED])){
            $label = 'warning';
        }
        else if (in_array($this->status, [self::STATUS_SALE_APPROVED])){
            $label = 'info';
        }
        else if (in_array($this->status, [self::STATUS_REVIEW_APPROVED])){
            $label = 'success';
        }
        else if (in_array($this->status, [self::STATUS_LOCK])) {
            $label = 'inverse';
        }
        return '<label class="label label-' . $label . '">' . self::STATUS[$this->status] ?? " " . '</label>';
    }

    public function getCustomStatusAttribute(){
        return $this->getTerminalStatusLabel();
    }

    public function getTerminalProvinceNameAttribute(){
        $province = Province::where('province_code', $this->terminal_province_code)->first();
        if ($province) {
            return $province->province_name;
        }
        return '';

    }
    public function getTerminalDistrictNameAttribute(){
        $district = District::where('district_code', $this->terminal_district_code)->first();
        if ($district) {
            return $district->district_name;
        }
        return '';
    }
    public function getTerminalWardNameAttribute(){
        $ward = Ward::where('wards_code', $this->terminal_ward_code)->first();
        if ($ward) {
            return $ward->wards_name;
        }
        return '';
    }
    public function getBankNameAttribute() {
        $bank = Bank::where('bank_code', $this->bank_code)->first();
        if ($bank) {
            return $bank->bank_name;
        }
        else {
            return '';
        }
    }
    public function getBankBrandAttribute() {
        $bank = Bank::where('bank_code', $this->bank_code)->first();
        if ($bank) {
            return $bank->brand_name;
        }
        else {
            return '';
        }
    }
    public function getBankTitleAttribute() {
        $bank = Bank::where('bank_code', $this->bank_code)->first();
        if ($bank) {
            return $bank->bank_title;
        }
        else {
            return '';
        }
    }
    public function getReceiveMethodNameAttribute() {
        return self::RECEIVE_METHOD[$this->receive_method] ?? '';
    }
    public function getMccNameAttribute() {
        $mcc = MccVnpay::where('type_code', $this->mcc)->first();
        if ($mcc) {
            return $mcc->type_code . ' - ' . $mcc->brand_name;
        }
        else {
            return '';
        }
    }

    public function getApproveRejectDateAttribute() {
        if (in_array($this->status, [
            self::STATUS_SALE_APPROVED, self::STATUS_SALE_DENIED,
            self::STATUS_REVIEW_APPROVED, self::STATUS_REVIEW_DENIED,
            self::STATUS_LOCK
        ])) {
            return date('H:i:s d/m/Y', strtotime($this->updated_at));
        }
        else {
            return '';
        }
    }

    public function getStrBankNumberAttribute() {
        return '\''. $this->bank_number;
    }

    public function getMMSErrorDetail() {
        $mappingError = [
            '01' => 'Thất bại',
            '02' => 'Tạo vnmart thất bại',
            '08' => 'Timeout',
            '11' => 'Không tồn tại mã merchant',
            '12' => 'Không tồn tại tên merchant',
            '14' => 'Không tồn tại tên viết tắt merchant',
            '15' => 'Trùng tên terminal',
            '16' => 'Trùng terminal id',
            '17' => 'Trùng số điện thoại terminal app',
            '96' => 'Bảo trì',
            '99' => 'Lỗi',
            '400' => 'Lỗi rule trường thông tin'
        ];

        if (in_array($this->mms_error_code, array_keys($mappingError))) {
            return $mappingError[$this->mms_error_code];
        }
        return null;
    }
}
