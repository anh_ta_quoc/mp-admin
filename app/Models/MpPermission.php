<?php

namespace App;

use App\Department;
use App\Libraries\BaseFunction;
use App\Staff;
use App\AuditLog;
use Eloquent as Model;
use App\BusinessProduct;
use App\User;
use App\TypeMerchant;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
/**
 * Class MpPermission
 * @package App
 * @version December 16, 2019, 11:28 am UTC
 *
 * @property string name
 * @property string type
 * @property string function_id
 * @property string href


 */
class MpPermission extends Model
{
//    use SoftDeletes;

    public $table = 'mp_permission';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $connection = "rest_api_db";

    public $fillable = [
        'name',
        'type',
        'function_id',
        'status'

    ];
    public function childs() {

        return $this->hasMany('App\MpPermission','parent_id','function_id') ;

    }



}
