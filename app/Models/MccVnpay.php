<?php

namespace App;

use App\Bank;
use App\BusinessProduct;
use App\District;
use App\Libraries\BaseFunction;
use App\Models\Sync\QrMerchant;
use App\Province;
use App\Staff;
use App\Ward;
use Eloquent as Model;
use App\User;

class MccVnpay extends Model
{

    public $table = 'mcc_vnpay';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $connection = "rest_api_db";

    public $fillable = [
        'id',
        'type_code',
        'brand_name',
        'full_name',
        'description',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type_code' => 'string',
        'brand_name' => 'string',
        'full_name' => 'string',
        'description' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
