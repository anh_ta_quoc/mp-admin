<?php

namespace App;

use Eloquent as Model;

class FirebaseUser extends Model
{

    public $table = 'firebase_user';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
