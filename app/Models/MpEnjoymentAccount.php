<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class MpEnjoymentAccount extends Model
{
    public $table = 'mp_enjoyment_account';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'id',
        'merchant_code',
        'fullname',
        'account_number',
        'bank_code',
        'bank_name',
        'branch'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'merchant_code' => 'string',
        'fullname' => 'string',
        'account_number' => 'string',
        'bank_code' => 'string',
        'bank_name' => 'string',
        'branch' => 'string',
    ];

    protected $appends = [];

    const FIELD_MEANINGS = [
        'merchant_code' => 'Mã merchant',
        'fullname' => 'Tên chủ tài khoản',
        'account_number' => 'Số tài khoản',
        'bank_code' => 'Mã ngân hàng',
        'bank_name' => 'Tên ngân hàng',
        'branch' => 'Chi nhánh',
    ];

    public static function getFieldMeanings($field)
    {
        $fieldMeanings = self::FIELD_MEANINGS;
        return in_array($field, array_keys($fieldMeanings)) ? $fieldMeanings["$field"] : 'Unknown';
    }
}
