<?php

namespace App;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SmsLog
 * @package App
 * @version December 17, 2019, 2:30 am UTC
 *
 * @property string phone
 * @property string message
 * @property string status
 * @property string error_code
 * @property string error_message
 */
class SmsLog extends Model
{
//    use SoftDeletes;

    public $table = 'sms_logs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


//    protected $dates = ['deleted_at'];

    public $connection = "rest_api_db";

    public $fillable = [
        'phone',
        'message',
        'status',
        'error_code',
        'error_message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'phone' => 'string',
        'message' => 'string',
        'status' => 'string',
        'error_code' => 'string',
        'error_message' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'phone' => 'required',
        'message' => 'required'
    ];

    
}
