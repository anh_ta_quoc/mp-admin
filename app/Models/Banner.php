<?php

namespace App;

use Eloquent as Model;
use App\User;

/**
 * Class Banner
 * @package App
 * @version Feb 12, 2020, 11:22 am UTC
 *
 * @property string content
 * @property string receivers
 * @property boolean status
 */
class Banner extends Model
{

    public $table = 'banner';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'content',
        'status',
        'receivers',
        'user_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'content' => 'longtext',
        'status' => 'integer',
        'receivers' => 'longtext',
        'user_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'title' => 'required',
//        'link' => 'required',
//        'read' => 'required',
//        'tracking' => 'required',
//        'type' => 'required',
//        'status' => 'required'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getStatusLabel($status=0)
    {
        switch ($status) {
            case 0:
                return 'Đang tắt';
            case 1:
                return 'Đang bật';
            default:
                return 'Không xác định';
        }
    }
}
