<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Store;
use App\User;
class AuditLog extends Model
{


    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'audit_logs';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['user_id', 'store_id', 'message'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


    public function getMerchantNameAttribute()
    {
        $store = Store::where('id', $this->store_id)->first();
        if ($store) {
            return $store->merchant_name;
        }
        else {
            return 'Unknown';
        }
    }

    public function getTrialUserInfoAttribute()
    {
        $store = Store::where('id', $this->store_id)->first();
        if ($store) {
            return $store->trial_user->name . ' - ' . $store->trial_user->phone;
        }
        else {
            return 'Unknown';
        }
    }

    public function getUserName() {
        $user = User::where('id', $this->user_id)->first();
        if ($user) {
            return $user->name;
        }
        else {
            return 'Merchant';
        }
    }
    public function getEmail() {
        $user = User::where('id', $this->user_id)->first();
        if ($user) {
            return $user->email;
        }
        else {
            return 'Unknown';
        }
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->timezone('Asia/Bangkok');;
    }
  }
