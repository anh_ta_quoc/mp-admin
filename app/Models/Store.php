<?php

namespace App;

use App\Department;
use App\Libraries\BaseFunction;
use App\Staff;
use App\AuditLog;
use Eloquent as Model;
use App\BusinessProduct;
use App\User;
use App\TypeMerchant;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
/**
 * Class Store
 * @package App
 * @version December 16, 2019, 11:28 am UTC
 *
 * @property integer trial_user_id
 * @property integer status
 * @property string bank_branch
 * @property string bank_code
 * @property string bank_number
 * @property string currency
 * @property string merchant_address
 * @property string merchant_brand
 * @property string merchant_code
 * @property string merchant_contact_email
 * @property string merchant_contact_name
 * @property string merchant_contact_phone
 * @property string merchant_district_code
 * @property string merchant_name
 * @property string merchant_province_code
 * @property string merchant_type
 * @property string merchant_type_business
 * @property string merchant_ward_code
 * @property string terminal_address
 * @property string terminal_contact_email
 * @property string terminal_contact_name
 * @property string terminal_contact_phone
 * @property string terminal_description
 * @property string terminal_district_code
 * @property string terminal_id
 * @property string terminal_name
 * @property string terminal_province_code
 * @property string terminal_type
 * @property string terminal_ward_code
 * @property string bank_account
 * @property integer mms_status
 * @property string merchant_file_business_cert
 * @property string merchant_file_contract
 * @property string merchant_date_of_issue
 * @property string merchant_place_of_issue
 * @property string department_id
 * @property string staff_id
 * @property string merchant_file_business_tax_cert
 * @property string merchant_file_domain_cert
 * @property string merchant_file_identify_card
 * @property integer wallet_status
 */
class Store extends Model
{
//    use SoftDeletes;

    public $table = 'store_info';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


//    protected $dates = ['deleted_at'];

    public $connection = "rest_api_db";

    public $fillable = [
        'trial_user_id',
        'status',
        'bank_branch',
        'bank_code',
        'bank_number',
        'currency',
        'merchant_address',
        'merchant_brand',
        'merchant_code',
        'merchant_contact_email',
        'merchant_contact_name',
        'merchant_contact_phone',
        'merchant_district_code',
        'merchant_name',
        'merchant_province_code',
        'merchant_type',
        'merchant_type_business',
        'merchant_ward_code',
        'terminal_address',
        'terminal_contact_email',
        'terminal_contact_name',
        'terminal_contact_phone',
        'terminal_description',
        'terminal_district_code',
        'terminal_id',
        'terminal_name',
        'terminal_province_code',
        'terminal_type',
        'terminal_ward_code',
        'terminal_mcc',
        'bank_account',
        'mms_status',
        'merchant_file_business_cert',
        'merchant_file_contract',
        'merchant_date_of_issue',
        'merchant_place_of_issue',
        'department_id',
        'staff_id',
        'merchant_file_business_tax_cert',
        'merchant_file_domain_cert',
        'merchant_file_identify_card',
        'wallet_status',
        'issue_invoice',
        'terminal_register_vnpayment',
        'terminal_website',
        'created_by',
        'updated_by',
        'merchant_website',
        'merchant_business_address',
        'contract_number',
        'contract_code',
        'contract_date',
        'src',
        'sync_first_terminal',
        'merchant_app_user',
        'terminal_app_user',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'trial_user_id' => 'integer',
        'status' => 'integer',
        'bank_branch' => 'string',
        'bank_code' => 'string',
        'bank_number' => 'string',
        'currency' => 'string',
        'merchant_address' => 'string',
        'merchant_brand' => 'string',
        'merchant_code' => 'string',
        'merchant_contact_email' => 'string',
        'merchant_contact_name' => 'string',
        'merchant_contact_phone' => 'string',
        'merchant_district_code' => 'string',
        'merchant_name' => 'string',
        'merchant_province_code' => 'string',
        'merchant_type' => 'string',
        'merchant_type_business' => 'string',
        'merchant_ward_code' => 'string',
        'terminal_address' => 'string',
        'terminal_contact_email' => 'string',
        'terminal_contact_name' => 'string',
        'terminal_contact_phone' => 'string',
        'terminal_description' => 'string',
        'terminal_district_code' => 'string',
        'terminal_mcc' => 'string',
        'terminal_id' => 'string',
        'terminal_name' => 'string',
        'terminal_province_code' => 'string',
        'terminal_type' => 'string',
        'terminal_ward_code' => 'string',
        'bank_account' => 'string',
        'mms_status' => 'integer',
        'merchant_file_business_cert' => 'string',
        'merchant_file_contract' => 'string',
        'merchant_date_of_issue' => 'string',
        'merchant_place_of_issue' => 'string',
        'department_id' => 'string',
        'staff_id' => 'string',
        'merchant_file_business_tax_cert' => 'string',
        'merchant_file_domain_cert' => 'string',
        'merchant_file_identify_card' => 'string',
        'wallet_status' => 'integer',
        'issue_invoice' => 'integer',
        'terminal_register_vnpayment' => 'integer',
        'terminal_website' => 'string',
        'merchant_app_user' => 'string',
        'terminal_app_user' => 'string',
    ];
    protected $appends = array(
        'staff_info',
        'department_name',
        'type_business_name',
        'type_name',
        'merchant_province_name',
        'merchant_district_name',
        'merchant_ward_name',
        'terminal_province_name',
        'terminal_district_name',
        'terminal_ward_name',
        'trial_name',
        'custom_status',
        'bank_name',
        'bank_title',
        'bank_brand',
        'status_label',
        'str_created_at',
        'terminal_type_name'
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'merchant_name' => 'required',
        'merchant_brand' => 'required',
        'merchant_code'=>'required',
        'merchant_type_business'=>'required',
        'merchant_type'=>'required',
        'merchant_province_code'=>'required',
        'merchant_district_code'=>'required',
        'merchant_ward_code'=>'required',
        'merchant_address'=>'required',
        'merchant_contact_name'=>'required',
        'merchant_contact_email'=>'required|email',
        'merchant_contact_phone'=>'required|digits:9',
        'terminal_id'=>'required',
        'terminal_name'=>'required',
        'terminal_type'=>'required',
        'terminal_province_code'=>'required',
        'terminal_district_code'=>'required',
        'terminal_ward_code'=>'required',
        'terminal_address'=>'required',
        'terminal_contact_name'=>'required',
        'terminal_contact_email'=>'required|email',
        'terminal_contact_phone'=>'required|digits:9',
        'currency'=>'required',
        'bank_number'=>'required',
        'bank_account'=>'required',
        'bank_code'=>'required',
        'bank_branch'=>'required',
        'department_id'=>'required',
        'staff_id'=>'required',
        'merchant_file_business_cert'=>'required',
        'merchant_file_contract'=>'required'
    ];
    public static $messages = [
        'merchant_name.required' => 'Tên doanh nghiệp không được bỏ trống',
        'merchant_brand.required' => 'Tên viết tắt doanh nghiệp không được bỏ trống',
        'merchant_code.required'=>'MST/CMTND doanh nghiệp không được bỏ trống',
        'merchant_type_business.required'=>'Loại hình doanh nghiệp không được bỏ trống',
        'merchant_type.required'=>'Loại hình kinh doanh không được bỏ trống',
        'merchant_province_code.required'=>'Tỉnh thành doanh nghiệp không được bỏ trống',
        'merchant_district_code.required'=>'Quận/huyện doanh nghiệp không đươc bỏ trống',
        'merchant_ward_code.required'=>'Phường/xã doanh nghiệp không được bỏ trống',
        'merchant_address.required'=>'Địa chỉ đăng ký kinh doanh không được bỏ trống',
        'merchant_contact_name.required'=>'Tên người đăng ký không được bỏ trống',
        'merchant_contact_email.required'=>'Email liên hệ doanh nghiệp không được bỏ trống',
        'merchant_contact_phone.required'=>'Số điện thoại liện hệ doanh nghiệp không được bỏ trống',
        'terminal_id.required'=>'Mã điểm bán không được bỏ trống',
        'terminal_name.required'=>'Tên điểm bán không được bỏ trống',
        'terminal_type.required'=>'Sản phẩm kinh doanh không được bỏ trống',
        'terminal_province_code.required'=>'Tỉnh thành điểm bán không được bỏ trống',
        'terminal_district_code.required'=>'Quận/huyện điểm bán không được bỏ trống',
        'terminal_ward_code.required'=>'Phường/xã điểm bán không được bỏ trống',
        'terminal_address.required'=>'Địa chỉ kinh doanh điểm bán không được bỏ trống',
        'terminal_contact_name.required'=>'Tên người đăng ký không được bỏ trống',
        'terminal_contact_email.required'=>'Email liên hệ điểm bán không được bỏ trống',
        'terminal_contact_phone.required'=>'Số điện thoại liên hệ điểm bán không được bỏ trống',
        'currency.required'=>'Tiền tệ không dược bỏ trống',
        'bank_number.required'=>'Số tài khoản ngân hàng không được bỏ trống',
        'bank_account.required'=>'Chủ tài khoản không được bỏ trống',
        'bank_code.required'=>'Tên ngân hàng không được bỏ trống',
        'bank_branch.required'=>'Chi nhánh ngân hàng không được bỏ trống',
        'department_id.required'=>'Phòng ban không được bỏ trống',
        'staff_id.required'=>'Nhân viên không được bỏ trống',
        'merchant_file_business_cert.required'=>'Giấy phép đăng ký kinh doanh không được bỏ trống',
        'merchant_file_contract.required'=>'Hợp đồng không được bỏ trống',

        'merchant_contact_email.email'=>'Email sai định dạng. Vui lòng thử lại',
        'terminal_contact_email.email'=>'Email sai định dạng. Vui lòng thử lại',
        'terminal_contact_phone.digits'=>'Số điện thoại sai định dạng. Vui lòng thử lại',
        'merchant_contact_phone.digits'=>'Số điện thoại sai định dạng. Vui lòng thử lại',

    ];

    public static $fieldLabel = [
        'merchant_address' => 'Địa chỉ đăng ký kinh doanh',
        'merchant_brand' => 'Tên viết tắt',
        'merchant_code' => 'Mã merchant',
        'merchant_contact_email' => 'Email liên hệ',
        'merchant_contact_name' => 'Tên liên hệ',
        'merchant_contact_phone' => 'Số điện thoại liên hệ',
        'merchant_district_code' => 'Quận huyện',
        'merchant_name' => 'Tên merchant',
        'merchant_province_code' => 'Tỉnh thành',
        'merchant_type' => 'Loại hình kinh doanh',
        'merchant_type_business' => 'Loại hình doanh nghiệp',
        'merchant_ward_code' => 'Phường xã',
        'merchant_file_business_cert' => 'Giấy phép kinh doanh',
        'merchant_file_contract' => 'Hợp đồng VNPAY',
        'merchant_file_business_tax_cert' => 'GPĐK tài khoản với CQT',
        'merchant_file_identify_card' => 'CMND/CCCD',
        'merchant_file_domain_cert' => 'Giấy phép đăng ký tên miền',
        'department_id' => 'Phòng ban',
        'staff_id' => 'Nhân viên',
        'issue_invoice' => 'Xuất hóa đơn',
        'merchant_website' => 'Website doanh nghiệp',
        'merchant_business_address' => 'Địa chỉ kinh doanh',
        'contract_number' => 'Số hợp đồng',
        'contract_code' => 'Tên viết tắt hợp đồng',
        'contract_date' => 'Ngày ký hợp đồng',
        'terminal_register_vnpayment' => 'Cổng TT'
    ];

    public function getStatusLabelAttribute(){
        if ($this->mms_status == BaseFunction::STATUS_MMS_CREAT_FAILED &&
            ($this->trial_user_id || $this->src == "SAPO")
        ) {
            return '<label class="' . BaseFunction::getStoreStatusClass()[BaseFunction::STATUS_DRAFT] . '">' .
                BaseFunction::getStoreStatus()[BaseFunction::STATUS_DRAFT] ?? "Unknown" . '</label>';
        }
        if ($this->status == 0 && !$this->merchant_file_contract) {
            return '<label class="' . BaseFunction::getStoreStatusClass()[99] . '">' .
                BaseFunction::getStoreStatus()[99] ?? "Unknown" . '</label>';
        }

        return '<label class="' . BaseFunction::getStoreStatusClass()[$this->status] . '">' .
            BaseFunction::getStoreStatus()[$this->status] ?? "Unknown" . '</label>';
    }

    public function getStaffInfoAttribute()
    {
        $staff = Staff::where('staff_id', $this->staff_id)->first();
        if ($staff) {
            return $staff->full_name . ' - ' . $staff->staff_code;
        }
        return '';
    }

    public function getDepartmentNameAttribute()
    {
        $department = Department::where('department_id', $this->department_id)->first();
        if ($department) {
            return $department->department_name;
        }
        return '';
    }
    public function getTypeBusinessNameAttribute()
    {
        $merchantType = BaseFunction::getMerchantTypeBusiness();
        return $merchantType[$this->merchant_type_business] ?? 'Unknown';
    }
    public function getTypeNameAttribute(){
        $merchantTypes = TypeMerchant::all()->pluck('brand_name','type_code');
        return $merchantTypes[$this->merchant_type] ?? 'Unknown';

    }
    public static function getTypeBusinessName($merchant_type_business)
    {
        $merchantType = BaseFunction::getMerchantTypeBusiness();
        return $merchantType[$merchant_type_business] ?? 'Unknown';
    }
    public static function getTypeName($merchant_type){
        $merchantTypes = TypeMerchant::all()->pluck('brand_name','type_code');
        return $merchantTypes[$merchant_type] ?? 'Unknown';

    }

    public function getMerchantProvinceNameAttribute(){
        $province = Province::where('province_code', $this->merchant_province_code)->first();
        if ($province) {
            return $province->province_name;
        }
        return '';

    }
    public function getMerchantDistrictNameAttribute(){
        $district = District::where('district_code', $this->merchant_district_code)->first();
        if ($district) {
            return $district->district_name;
        }
        return '';
    }
    public function getMerchantWardNameAttribute(){
        $ward = Ward::where('wards_code', $this->merchant_ward_code)->first();
        if ($ward) {
            return $ward->wards_name;
        }
        return '';
    }

    public function getTerminalProvinceNameAttribute(){
        $province = Province::where('province_code', $this->terminal_province_code)->first();
        if ($province) {
            return $province->province_name;
        }
        return '';

    }
    public function getTerminalDistrictNameAttribute(){
        $district = District::where('district_code', $this->terminal_district_code)->first();
        if ($district) {
            return $district->district_name;
        }
        return '';
    }
    public function getTerminalWardNameAttribute(){
        $ward = Ward::where('wards_code', $this->terminal_ward_code)->first();
        if ($ward) {
            return $ward->wards_name;
        }
        return '';
    }
    public function getTrialNameAttribute(){
        if (isset($this->trial_user_id)) {
            return Trial::where('id',$this->trial_user_id)->first()->name;
        } else {
            return '';
        }
    }
    public function getCustomStatusAttribute() {
        if ($this->merchant_file_contract) {
            return BaseFunction::getStoreStatus()[$this->status];
        }
        else {
            if ($this->status < 0) {
                return 'Chờ hoàn thiện hồ sơ';
            }
            else {
                return 'Chờ cập nhật Hợp đồng';
            }
        }
    }
    public function getBankNameAttribute() {
        $bank = Bank::where('bank_code', $this->bank_code)->first();
        if ($bank) {
            return $bank->bank_name;
        }
        else {
            return '';
        }
    }
    public function getBankTitleAttribute() {
        $bank = Bank::where('bank_code', $this->bank_code)->first();
        if ($bank) {
            return $bank->bank_title;
        }
        else {
            return '';
        }
    }
    public function getBankBrandAttribute() {
        $bank = Bank::where('bank_code', $this->bank_code)->first();
        if ($bank) {
            return $bank->brand_name;
        }
        else {
            return '';
        }
    }

    public function getAccountVnmartAttribute() {

        $result = collect(DB::connection('oracle')->select('select account_vnmart from mms.qr_account_vnmart
          where merchant_code = :merchant_code', ['merchant_code' => $this->merchant_code]))->first();
        if ($result) {
            return $result->account_vnmart;
        } else {
            return '';
        }
    }
    public function getApprovedCrossCheckAttribute() {
        $auditLog = AuditLog::where ('store_id', $this->id)
                              ->where('message', 'Đối soát duyệt')
                              ->orderBy('created_at','DESC')
                              ->first();
        if ($auditLog) {
            return $auditLog;
        } else {
            return Null;
        }
    }

    public function getApprovedSaleAttribute() {
        $auditLog = AuditLog::where ('store_id', $this->id)
            ->where('message', 'Kinh doanh duyệt')
            ->orderBy('created_at','DESC')
            ->first();
        if ($auditLog) {
            return $auditLog;
        } else {
            return Null;
        }
    }

    public function getFirstTransactionAttribute() {
        $result = collect(DB::connection('oracle')->select('select min(pay_date) as pay_date from mms.qr_transaction
          where merchant_code = :merchant_code ', ['merchant_code' => $this->merchant_code]))->first();
        if ($result) {
            return $result->pay_date;
        } else {
            return '';
        }
    }

    public function getStrCreatedAtAttribute() {
        return $this->created_at->format('d/m/Y');
    }

    public function getMccNameAttribute() {
        $mcc = MccVnpay::where('type_code', $this->terminal_mcc)->first();
        if ($mcc) {
            return $mcc->type_code . ' - ' . $mcc->brand_name;
        }
        else {
            return '';
        }
    }

    public function getTerminalTypeNameAttribute() {
        $business_type = BusinessProduct::where('type_code', $this->terminal_type)->first();
        if ($business_type) {
            return $business_type->bp_title;
        }
        return '';
    }
}
