<?php

namespace App;

use Eloquent as Model;

class Notification extends Model
{

    public $table = 'notification';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
