<?php

namespace App\Mail\Terminal;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaleApproveTerminal extends Mailable
{
    use Queueable, SerializesModels;

    public $address = '';
    public $name = '';
    public $subject = '';
    public $terminalId = '';
    public $terminalName = '';
    public $merchantName = '';
    public $merchantCode = '';
    public $reason = '';
    public $terminalDetailUrl = '';

    /**
     * SaleApproveTerminal constructor.
     *
     * @param $address
     * @param $name
     * @param $subject
     * @param $terminalId
     * @param $terminalName
     * @param $merchantName
     * @param $merchantCode
     * @param $terminalDetailUrl
     */
    public function __construct($address, $name, $subject, $terminalId, $terminalName,
                                $merchantName, $merchantCode, $terminalDetailUrl)
    {
        $this->address = $address;
        $this->name = $name;
        $this->subject = $subject;
        $this->terminalId = $terminalId;
        $this->terminalName = $terminalName;
        $this->merchantName = $merchantName;
        $this->merchantCode = $merchantCode;
        $this->terminalDetailUrl = $terminalDetailUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.terminal.sale_approve')
            ->from($this->address, $this->name)
            ->subject($this->subject);
    }
}
