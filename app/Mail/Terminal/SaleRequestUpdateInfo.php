<?php

namespace App\Mail\Terminal;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaleRequestUpdateInfo extends Mailable
{
    use Queueable, SerializesModels;

    public $address = '';
    public $name = '';
    public $subject = '';
    public $terminalId = '';
    public $terminalName = '';
    public $merchantName = '';
    public $merchantCode = '';
    public $reason = '';
    public $terminalDetailUrl = '';

    /**
     * SaleRequestUpdateInfo constructor.
     *
     * @param $address
     * @param $name
     * @param $subject
     * @param $terminalId
     * @param $terminalName
     * @param $merchantName
     * @param $merchantCode
     * @param $reason
     * @param $terminalDetailUrl
     */
    public function __construct($address, $name, $subject, $terminalId, $terminalName,
                                $merchantName, $merchantCode, $reason, $terminalDetailUrl)
    {
        $this->address = $address;
        $this->name = $name;
        $this->subject = $subject;
        $this->terminalId = $terminalId;
        $this->terminalName = $terminalName;
        $this->merchantName = $merchantName;
        $this->merchantCode = $merchantCode;
        $this->reason = $reason;
        $this->terminalDetailUrl = $terminalDetailUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.terminal.sale_request_update_info')
            ->from($this->address, $this->name)
            ->subject($this->subject);
    }
}
