<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaleApproveStoreInfo extends Mailable
{
    use Queueable, SerializesModels;

    public $address = '';
    public $name = '';
    public $subject = '';
    public $store_info_url = '';
    public $merchant_brand = '';

    /**
     * NewTicketComment constructor.
     * @param $address
     * @param $name
     * @param $subject
     * @param $store_info_url
     * @param $merchant_brand
     */
    public function __construct($address, $name, $subject, $store_info_url, $merchant_brand)
    {
        $this->address = $address;
        $this->name = $name;
        $this->subject = $subject;
        $this->store_info_url = $store_info_url;
        $this->merchant_brand = $merchant_brand;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.sale_approve_store_info')
            ->from($this->address, $this->name)
            ->subject($this->subject);
    }
}
