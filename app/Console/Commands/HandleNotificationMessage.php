<?php

namespace App\Console\Commands;

use App\FirebaseUser;
use App\Libraries\Firebase;
use App\Libraries\Push;
use App\Message;
use App\Models\Sync\QrMerchant;
use App\Models\Sync\QrMerchantUser;
use App\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use DateTime;

class HandleNotificationMessage extends Command
{
    private $firebase = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:handleNotificationMessage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = new Firebase();
    }

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);

        Log::channel('handle_notification_message')->info(sprintf("start: %s", now()));
        $handle_limit = 200;
        $total_handle = 0;
        $total_success = 0;
        $process_lock = uniqid();
        $update_query = sprintf("update message set process_lock = '%s' where status = 0 and schedule_at <= NOW()
        and process_lock = '' order by updated_at asc limit %s", $process_lock, $handle_limit);
//        $update_query = sprintf("update message as m1
//        INNER JOIN (select id from message where status = 0 and schedule_at <= NOW() and process_lock = '' order by updated_at asc limit %s) as m2
//        ON m1.id = m2.id
//        set m1.process_lock = '%s'", $handle_limit, $process_lock);
        DB::update(DB::raw($update_query));

        $messages = Message::where('process_lock', $process_lock)->get();
        $listSuccessIds = [];
        if (is_object($messages) && sizeof($messages) > 0) {
            foreach ($messages as $message) {
                $total_handle ++;
                $res_each_message = $this->handleEachMessage($message);
                if ($res_each_message) {
                    $total_success ++;
//                    $message->status = 1;
//                    $message->save();
                    $listSuccessIds[] = $message->id;
                }
            }
        }

        $update_query_remove_process_lock = sprintf("update message set process_lock = '' where process_lock = '%s'", $process_lock);
        DB::update(DB::raw($update_query_remove_process_lock));
        if (count($listSuccessIds) > 0) {
            $update_query_success_status = sprintf("update message set status = 1 where id in (%s)", implode(',', $listSuccessIds));
            DB::update(DB::raw($update_query_success_status));
        }

        if ($total_handle > 0) {
            Log::channel('handle_notification_message')->info(sprintf("Process: %s success/ %s total", $total_success, $total_handle));
        }

        Log::channel('handle_notification_message')->info(sprintf("end: %s", now()));
    }

    /**
     * Handle one message
     *
     * @param $message
     * @return bool
     * @throws \Exception
     */
    private function handleEachMessage($message)
    {
        if (!$message) {
            return false;
        }

        $res = true;
        $listReceivers = explode(',', $message->receivers);
        foreach ($listReceivers as $receiver) {
            if ($receiver === 'all') {
                $this->handleSendAll($message);
            }
            else {
                $receiverInfo = explode('-', $receiver);
                if (count($receiverInfo) > 0) {
                    // mms users
                    $listMerchantUsers = $this->getUsersByMerchantId(trim($receiverInfo[0]));
                    foreach($listMerchantUsers as $merchantUser) {
                        $this->handleEachUser($merchantUser, trim($receiverInfo[0]), trim($receiverInfo[1]), $message);
                    }
                    // ums users
                    $listUMSUserIds = $this->getUMSUserByMerchantId(trim($receiverInfo[0]));
                    if (count($listUMSUserIds) > 0) {
                        foreach($listUMSUserIds as $mpUser) {
                            $this->handleEachUMSUser($mpUser->user_id, trim($receiverInfo[1]), $message);
                        }
                    }
                }
                else {
                    $res = false;
                }
            }
        }

        return $res;
    }

    /**
     * Handle message send all merchants
     *
     * @param $message
     * @throws \Exception
     */
    private function handleSendAll($message) {
        $merchants = QrMerchant::all();
        if ($merchants) {
            foreach ($merchants as $merchant) {
                // mms users
                $qrMerchantUsers = QrMerchantUser::where('merchant_id', $merchant['id'])
                    ->where('channel_login', 1)
                    ->get();
                if (is_object($qrMerchantUsers) && sizeof($qrMerchantUsers) > 0) {
                    foreach($qrMerchantUsers as $merchantUser) {
                        $this->handleEachUser($merchantUser, $merchant['merchant_code'], $message);
                    }
                }
                // ums users
                $listUMSUserIds = $this->getUMSUserByMerchantId($merchant['id']);
                if (count($listUMSUserIds) > 0) {
                    foreach($listUMSUserIds as $mpUser) {
                        $this->handleEachUMSUser($mpUser->user_id, $merchant['id'], $message);
                    }
                }
            }
        }
        echo("handle all done\r\n");
    }

    private function getListActiveTids($userId, $merchantId) {
        $listActiveTids = [];
        // TEST
        $apiUrl = 'http://10.22.7.105:8080/MMSMCAPI/rest/users/getTerminalActive';
        // LIVE
        if (config('app.env') == 'production') {
            $apiUrl = 'http://10.20.6.59:9006/MMSMCAPI/rest/users/getTerminalActive';
        }
        try {
            $client = new \GuzzleHttp\Client();

            $jsonSend = json_encode([
                'id' => $userId,
                'merchantID' => $merchantId
            ], true);
            $response = $client->post($apiUrl, ['body' => $jsonSend]);
            $mms_resp = json_decode($response->getBody());
            if ($mms_resp) {
                foreach ($mms_resp as $terminalObj) {
                    $listActiveTids[] = $terminalObj->terminalID;
                }
            }
        } catch (\Exception $e) {

        }

        return $listActiveTids;
    }

    /**
     * Get list users by merchant id
     *
     * @param $merchantId
     * @return array
     */
    private function getUsersByMerchantId($merchantId)
    {
        return QrMerchantUser::where('merchant_id', $merchantId)
            ->where('channel_login', 1)
            ->get();
    }

    /**
     * Get list ums user id by merchant id
     * @param $merchantId
     * @return mixed
     */
    private function getUMSUserByMerchantId($merchantId)
    {
        return DB::connection('rest_api_db')->table('mp_mid_tid_user')
            ->selectRaw("DISTINCT(user_id)")
            ->whereRaw(sprintf("
            merchant_id = %s
            ", $merchantId))
            ->get();
    }

    /**
     * Handle message with each merchant user
     *
     * @param $merchantUser
     * @param $merchantId
     * @param $merchantCode
     * @param $message
     * @throws \Exception
     */
    private function handleEachUser($merchantUser, $merchantId, $merchantCode, $message)
    {
        $listActiveTids = $this->getListActiveTids($merchantUser['id'], $merchantId);
        if (!$message['tid_id'] || $merchantUser['parent_id'] == 0 || in_array($message['tid_id'], $listActiveTids)) {
            $listFirebaseTokens = $this->getFirebaseTokenByUser($merchantUser['id'], 'MMS', $message);
            $existedNotification = $this->createNotification($merchantCode, $message, $merchantUser['id'], 'MMS');
            $this->processNotification($existedNotification, $listFirebaseTokens);
        }
    }

    /**
     * Handle message with each UMS user
     *
     * @param $mpUserId
     * @param $merchantCode
     * @param $message
     * @return bool
     * @throws \Exception
     */
    private function handleEachUMSUser($mpUserId, $merchantCode, $message)
    {
        $mpMidTidUser = DB::connection('rest_api_db')->table('mp_mid_tid_user')
            ->selectRaw("user_id, merchant_code, type")
            ->whereRaw(sprintf("
            user_id = %s and merchant_code = '%s'
            ", $mpUserId, $merchantCode))
            ->first();
        if (!$mpMidTidUser) {
            return false;
        }
        $allowHandle = false;
        if (!$message['tid_id']) {
            $allowHandle = true;
        }
        if (in_array(strtoupper($mpMidTidUser->type), ['MERCHANT_WEB', 'MERCHANT_APP'])) {
            $allowHandle = true;
            if (strtoupper($mpMidTidUser->type) == 'MERCHANT_APP') {
                $isBlock = DB::connection('rest_api_db')->table('block_noti')
                    ->selectRaw("id")
                    ->whereRaw(sprintf("
                    user_id = %s and merchant_code = '%s' and type = 'mid' and is_block = 1
                    and (terminal_id is null or terminal_id = '%s')
                    ", $mpUserId, $merchantCode, $message['tid_id']))
                    ->first();
                if ($isBlock) {
                    $allowHandle = false;
                }
            }
        }
        else {
            $mpMidTidUserTerminals = DB::connection('rest_api_db')->table('mp_mid_tid_user')
                ->selectRaw("terminal_id")
                ->whereRaw(sprintf("
                user_id = %s and merchant_code = '%s'
                and type = '%s'
                ", $mpUserId, $merchantCode, strtolower($mpMidTidUser->type)))
                ->get();
            $listActiveTids = [];
            echo("\n");
            if ($mpMidTidUserTerminals) {
                foreach ($mpMidTidUserTerminals as $mpMidTidUserTerminal) {
                    $listActiveTids[] = $mpMidTidUserTerminal->terminal_id;
                }
            }
            if (in_array($message['tid_id'], $listActiveTids)) {
                $allowHandle = true;
                if (strtoupper($mpMidTidUser->type) == 'TERMINAL_APP') {
                    $isBlock = DB::connection('rest_api_db')->table('block_noti')
                        ->selectRaw("id")
                        ->whereRaw(sprintf("
                    user_id = %s and merchant_code = '%s' and type = 'tid' and is_block = 1
                    and terminal_id = '%s'
                    ", $mpUserId, $merchantCode, $message['tid_id']))
                        ->first();
                    if ($isBlock) {
                        $allowHandle = false;
                    }
                }
            }
        }
        if ($allowHandle) {
            $listFirebaseTokens = $this->getFirebaseTokenByUser($mpUserId, 'UMS', $message);
            $existedNotification = $this->createNotification($merchantCode, $message, $mpUserId, 'UMS');
            $this->processNotification($existedNotification, $listFirebaseTokens);
        }
    }

    /**
     * Handle notification in db
     *
     * @param $notification
     * @param $listFirebaseTokens
     * @throws \Exception
     */
    private function processNotification($notification, $listFirebaseTokens) {
        if ($notification->status != 1) {
            $resSend = $this->handleEachNotification($notification, $listFirebaseTokens);
            if ($resSend) {
                $notification->status = 1;
                $notification->sent_at = now('GMT+7');
            }
            else {
                $notification->retry_number = $notification->retry_number + 1;
            }
        }
        else {
            $notification->sent_at = now('GMT+7');
        }
        $notification->save();
    }

    /**
     * Create new notification
     *
     * @param $merchantCode
     * @param $message
     * @param $userId
     * @param $userSrc
     * @return Notification
     */
    private function createNotification($merchantCode, $message, $userId, $userSrc)
    {
        $newNotification = new Notification();
        $newNotification->message_id = $message['id'];
        if ($userSrc == 'UMS') {
            $newNotification->payload = $this->buildPayload($message, $userId);
        }
        else {
            $newNotification->payload = $this->buildPayload($message);
        }
        $newNotification->retry_number = 0;
        $newNotification->status = 0;
        $newNotification->user_id = $userId;
        $newNotification->merchant_code = $merchantCode;
        $newNotification->user_src = $userSrc;
        $newNotification->read = 0;
        $newNotification->message_type = $message['type'];
        $newNotification->save();

        return $newNotification;
    }

    /**
     * Build message payload from message
     *
     * @param $message
     * @param $userId
     * @return false|string
     */
    private function buildPayload($message, $userId = null)
    {
        /*
         * Sample payload
         * {"title": "fafdsa", "body": "fdsafdsafdsafa", "link": "https://google.com", "read": 0, "type": 1,
         * "tracking": 1, "status": 0, "id": 4}
         */
        $payloadArr = [
            'id' => $message['id'],
            'title' => $message['title'],
            'body' => $message['body'],
            'link' => $message['link'],
            'type' => $message['type']
        ];
        if ($userId) {
            $phone = DB::connection('rest_api_db')->table('mp_user')
                ->select('phone')
                ->where('id', $userId)
                ->first();
            if ($phone) {
                $payloadArr['accountId'] = $phone->phone;
            }
        }
        return json_encode($payloadArr);
    }
    /**
     * Handle one notification
     *
     * @param $notification
     * @param $listFirebaseTokens
     * @return bool
     * @throws \Exception
     */
    private function handleEachNotification($notification, $listFirebaseTokens)
    {
        $parsedPayload = $this->parsePayload($notification->payload);
        $push = new Push();
        $push->setTitle($parsedPayload->title);
        $push->setMessage($parsedPayload->body);
        $push->setPayload($notification->payload);

        $json = $push->getPush();

        // sent to list firebase users
        $response = false;
        $logInfo = "----------------------\n";
        $logInfo .= "Send notification:\n";
        $logInfo .= sprintf(" - notification id: %s, user_id: %s, user_src: %s\n", $notification['id'], $notification['user_id'], $notification['user_src']);
        if (count($listFirebaseTokens) > 0) {
            foreach ($listFirebaseTokens as $firebaseToken) {
                try {
                    $logInfo .= sprintf(" - send data: \ntitle: %s\nmessage: %s\nbody: %s\n",
                        $parsedPayload->title,
                        $parsedPayload->body,
                        $notification->payload
                    );
                    $sendFirebaseRes = $this->firebase->send($firebaseToken, $json, $parsedPayload->title, $parsedPayload->body);
                    $logInfo .= sprintf(" - firebase response: %s\n", $sendFirebaseRes);
                    $parsedResponse = json_decode($sendFirebaseRes);
                    if (!$response) {
                        $response = $parsedResponse->success == 1 ?? false;
                    }
                    if ($parsedResponse->success == 1) {
                        $logInfo .= " - result: success\n";
                    }
                    else {
                        $logInfo .= sprintf(" - result: failed\n");
                    }
                }
                catch (\Exception $e) {
                    $logInfo .= sprintf(" - firebase exception: %s\n", $e->getMessage());
                }
            }
        }
        else {
            $logInfo .= " - not found user to send notification\n";
        }
        Log::channel('handle_send_notification')->info(sprintf("%s", $logInfo));

        return $response;
    }

    /**
     * Parse json payload to object
     *
     * @param $payload
     * @return array|mixed
     */
    private function parsePayload($payload)
    {
        try {
            return json_decode($payload);
        }
        catch (\Exception $e) {
            return [];
        }
    }

    /**
     * Get list firebase users active by user id and user src
     *
     * @param $user_id
     * @param $user_src
     * @param $message
     * @return array
     * @throws \Exception
     */
    private function getFirebaseTokenByUser($user_id, $user_src, $message)
    {
        $compareDate = new DateTime;
        $compareDate->modify('-7 days');
        $formattedCompareDate = $compareDate->format('Y-m-d H:i:s');
        $listFirebaseTokens = [];
        $firebaseUsers = FirebaseUser::where('user_id', $user_id)
            ->where('user_src', $user_src)
            ->where('updated_at', '>=', $formattedCompareDate)
            ->get();
        if (is_object($firebaseUsers) && sizeof($firebaseUsers) > 0) {
            foreach ($firebaseUsers as $firebaseUser) {
                $listFirebaseTokens[] = $firebaseUser['firebase_token'];
            }
        }

        return $listFirebaseTokens;
    }
}
