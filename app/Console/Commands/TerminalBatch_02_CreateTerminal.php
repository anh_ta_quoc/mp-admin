<?php

namespace App\Console\Commands;

use App\MpTerminalBatch;
use App\MpTerminal;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\IOFactory as IOFactoryAlias;

class TerminalBatch_02_CreateTerminal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:TerminalBatch_02_CreateTerminal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    const MAX_RETRY = 3;
    const START_ROW = 3;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $log_msg = "---\n";
        $process_lock = $this->lock_process();
        if (!$process_lock) {
            $log_msg .= "Failed to lock process\n";
        } else {
            $batch = MpTerminalBatch::where('process_lock', $process_lock)->first();

            if ($batch) {
                $log_msg .= "Processing process_lock:" . $process_lock . "\n";
                $log_msg .= "Batch ID:" . $batch->id . "\n";
                $process_error = $this->process($batch);

                if ($process_error) {
                    $log_msg .= $process_error . "\n";
                }
                $log_msg .= "Create Terminal done\n";
            } else {
                $log_msg = "No record to process\n";
            }
        }
        print ($log_msg);
        Log::channel('terminal_batch_create_ter_logs')->info($log_msg);
    }

    private function lock_process()
    {
        $process_lock = uniqid();
        try {
            $query = sprintf("
                update mp_terminal_batch set process_lock = '%s'
                where status = %s and (process_lock = '' or process_lock is null) and retry_count < %s
                order by updated_at asc limit 1",
                $process_lock,
                MpTerminalBatch::STATUS_CONTROL_APPROVED,
                self::MAX_RETRY
            );
            DB::update(DB::raw($query));
            return $process_lock;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function unlock_process($batch, $process_msg, $status = null, $increase_retry_count = false)
    {
        $batch->process_lock = null;
        $batch->process_msg = $process_msg;

        if ($increase_retry_count) {
            $batch->retry_count += 1;
        }

        if ($status != null) {
            $batch->status = $status;
            $batch->retry_count = 0;
        }
        $batch->save();
    }

    private function create_terminal($batch, $row)
    {
        return MpTerminal::create([
            'merchant_code' => $batch->merchant_code,
            'terminal_id' => $row[1],
            'terminal_name' => $row[2],
            'terminal_type' => $row[9],
            'mcc' => $row[11],
            'terminal_province_code' => $row[5],
            'terminal_district_code' => $row[6],
            'terminal_ward_code' => $row[7],
            'terminal_address' => $row[4],
            'bank_code' => $batch->bank_code,
            'bank_branch' => $batch->bank_branch,
            'bank_number' => $batch->bank_number,
            'bank_account' => $batch->bank_account,
            'receive_method' => 0,
            'terminal_contact_name' => $row[18],
            'terminal_contact_phone' => $row[19],
            'terminal_contact_email' => $row[20],
            'src' => sprintf('%s-%s', 'BATCH', $batch->id),
            'file_contract' => $batch->file_contract,
            'file_auth_letter' => $batch->file_authorize,
            'receive_phone' => $row[16],
            'receive_email' => $row[17],
        ]);
    }

    private function process($batch)
    {
        try {
            $file_path = public_path('api_uploads/terminal_batch/' . $batch->merchant_code . '/' . $batch->file_terminals);
            if (file_exists($file_path)) {
                $spreadsheet = IOFactoryAlias::load($file_path);
                $worksheet = $spreadsheet->getActiveSheet();
                $rows = $worksheet->toArray();

                DB::beginTransaction();
                try {
                    foreach ($rows as $row_idx => $row) {
                        if ($row_idx >= self::START_ROW - 1) {
                            $this->create_terminal($batch, $row);
                        }
                    }
                    $this->unlock_process($batch, '', MpTerminalBatch::STATUS_WAIT_MMS);
                    DB::commit();
                    return null;
                } catch (\Exception $e) {
                    DB::rollBack();
                    throw new \Exception($e->getMessage());
                }
            } else {
                return 'File not found';
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $this->unlock_process($batch, $message, null, true);
            return $message;
        }
    }
}
