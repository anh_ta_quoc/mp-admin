<?php

namespace App\Console\Commands;

use App\AuditLog;
use App\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Psr7\str;

class PickMerchantFromStoreInfoBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:pickMerchantFromStoreInfoBackup {--merchantCodes=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->hasOption('merchantCodes') || !$this->option('merchantCodes')) {
            echo("Option merchantCodes is required\n");
            return;
        }

        $merchantCodes = explode(',', trim($this->option('merchantCodes')));
        foreach ($merchantCodes as $merchantCode) {
            $storeInfoBk = DB::connection('rest_api_db')->table('store_info_bk')
                ->where('merchant_code', $merchantCode)
                ->first();
            if ($storeInfoBk) {
                echo(sprintf("Handle merchant: %s\n", $merchantCode));
                try {
                    $storeInfoId = $storeInfoBk->id;
                    unset($storeInfoBk->id);
                    $createdNewStoreSuccess = $this->createNewStoreInfo($merchantCode, json_decode(json_encode($storeInfoBk), true));
                    if ($createdNewStoreSuccess) {
                        $auditLogs = $this->getAuditLogs($storeInfoId);
                        if ($auditLogs && sizeof($auditLogs) > 0) {
                            foreach ($auditLogs as $auditLog) {
                                unset($auditLog->id);
                                $newStore = Store::where('merchant_code', $merchantCode)->first();
                                $auditLog->store_id = $newStore->id;
                                try {
                                    AuditLog::insert(json_decode(json_encode($auditLog), true));
                                } catch (\Exception $e) {

                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    echo(sprintf("insert failed with message: %s", $e->getMessage()));
                }
                echo("done\n");
            } else {
                echo(sprintf("Store info with merchant code: %s does not existed\n", $merchantCode));
            }
        }
    }

    private function createNewStoreInfo($merchantCode, $insertData)
    {
        if (!$merchantCode) {
            return null;
        }
        $existedStoreInfo = Store::where('merchant_code', $merchantCode)->first();
        if ($existedStoreInfo) {
            return null;
        }
        try {
            return Store::insert($insertData);
        }
        catch (\Exception $e) {
            echo(sprintf("create merchant %s failed because %s", $merchantCode, str($e->getMessage())));
            return null;
        }
    }

    private function getAuditLogs($storeInfoId)
    {
        if (!$storeInfoId) {
            return false;
        }
        return DB::table('audit_logs_bk')->where('store_id', $storeInfoId)->get();
    }
}
