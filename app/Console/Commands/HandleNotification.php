<?php

namespace App\Console\Commands;

use App\FirebaseUser;
use App\Libraries\Firebase;
use App\Libraries\Push;
use App\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use DateTime;

class HandleNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        $handle_limit = 1000;
        $limit_retry = 5;
        $total_handle = 0;
        $total_success = 0;
        $process_lock = uniqid();
        $update_query = sprintf("update notification as n1
        INNER JOIN (select id from notification where status = 0 and process_lock = '' and retry_number <= 10 order by updated_at asc limit %s) as n2
        ON n1.id = n2.id
        set n1.process_lock = '%s'", $handle_limit, $process_lock);
        DB::update(DB::raw($update_query));

        $notifications = Notification::where('process_lock', $process_lock)->get();
        if (is_object($notifications) && sizeof($notifications)) {
            $firebase = new Firebase();
            foreach ($notifications as $notification) {
                $total_handle ++;
                $i = 1;
                while ($i <= $limit_retry) {
                    $resSend = $this->handleEachNotification($firebase, $notification);
                    if ($resSend) {
                        $total_success ++;
                        $notification->status = 1;
                        $notification->save();
                        break;
                    }
                    else {
                        $notification->retry_number += 1;
                        $notification->save();
                    }
                    $i ++;
                }
            }
        }

        $update_query_remove_process_lock = sprintf("update notification set process_lock = '', sent_at = '%s' where process_lock = '%s'", now('GMT+7'), $process_lock);
        DB::update(DB::raw($update_query_remove_process_lock));

        if ($total_handle > 0) {
            Log::channel('handle_send_notification')->info(sprintf("Process: %s success/ %s total", $total_success, $total_handle));
        }
    }

    /**
     * Handle one notification
     *
     * @param $firebase
     * @param $notification
     * @return bool
     * @throws \Exception
     */
    private function handleEachNotification($firebase, $notification)
    {
        $parsedPayload = $this->parsePayload($notification->payload);
        $push = new Push();
        $push->setTitle($parsedPayload->title);
        $push->setMessage($parsedPayload->body);
        $push->setPayload($notification->payload);

        $json = $push->getPush();

        // sent to list firebase users
        $response = false;
        $logInfo = "----------------------\n";
        $logInfo .= "Send notification:\n";
        $logInfo .= sprintf(" - notification id: %s, user_id: %s, user_src: %s\n", $notification['id'], $notification['user_id'], $notification['user_src']);
        $listFirebaseTokens = $this->getFirebaseTokenByUser($notification['user_id'], $notification['user_src']);
        if (count($listFirebaseTokens) > 0) {
            foreach ($listFirebaseTokens as $firebaseToken) {
                try {
                    $logInfo .= sprintf(" - send data: \ntitle: %s\nmessage: %s\nbody: %s\n",
                        $parsedPayload->title,
                        $parsedPayload->body,
                        $notification->payload
                    );
                    $sendFirebaseRes = $firebase->send($firebaseToken, $json );
                    $logInfo .= sprintf(" - firebase response: %s\n", $sendFirebaseRes);
                    $parsedResponse = json_decode($sendFirebaseRes);
                    if (!$response) {
                        $response = $parsedResponse->success == 1 ?? false;
                    }
                    if ($parsedResponse->success == 1) {
                        $logInfo .= " - result: success\n";
                    }
                    else {
                        $logInfo .= sprintf(" - result: failed\n");
                    }
                }
                catch (\Exception $e) {
                    $logInfo .= sprintf(" - firebase exception: %s\n", $e->getMessage());
                }
            }
        }
        else {
            $logInfo .= " - not found user to send notification\n";
        }
        Log::channel('handle_send_notification')->info(sprintf("%s", $logInfo));

        return $response;
    }

    /**
     * Get list firebase users active by user id and user src
     *
     * @param $user_id
     * @param $user_src
     * @return array
     * @throws \Exception
     */
    private function getFirebaseTokenByUser($user_id, $user_src)
    {
        $compareDate = new DateTime;
        $compareDate->modify('-30 minutes');
        $formattedCompareDate = $compareDate->format('Y-m-d H:i:s');
        $listFirebaseTokens = [];
        $firebaseUsers = FirebaseUser::where('user_id', $user_id)
            ->where('user_src', $user_src)
            ->where('updated_at', '>=', $formattedCompareDate)
            ->get();
        if (is_object($firebaseUsers) && sizeof($firebaseUsers) > 0) {
            foreach ($firebaseUsers as $firebaseUser) {
                $listFirebaseTokens[] = $firebaseUser['firebase_token'];
            }
        }

        return $listFirebaseTokens;
    }

    /**
     * Parse json payload to object
     *
     * @param $payload
     * @return array|mixed
     */
    private function parsePayload($payload)
    {
        try {
            return json_decode($payload);
        }
        catch (\Exception $e) {
            return [];
        }
    }
}
