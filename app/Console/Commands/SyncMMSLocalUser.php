<?php

namespace App\Console\Commands;

//use App\Models\Sync\QrLocalUser;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncMMSLocalUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:syncMMSLocalUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync MMS Local User to MP User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $qrLocalUsers = QrLocalUser::all();
        $qrLocalUsers = DB::table('qr_local_user')->select('*')
            ->get();
        $totalSuccess = 0;
        $totalRecords = sizeof($qrLocalUsers);
        if ($qrLocalUsers && $totalRecords > 0) {
            foreach ($qrLocalUsers as $qrLocalUser) {
                $email = trim($qrLocalUser->username) . '@vnpay.vn';
                $existed = User::whereRaw(sprintf("UPPER(email) = '%s'", strtoupper($email)))
                    ->count();
                if ($existed == 0) {
                    $insertData = $this->buildInsertData($qrLocalUser, $email);
                    if ($insertData) {
                        try {
                            User::create($insertData);
                            echo(sprintf("inserted %s\n", $email));
                            $totalSuccess ++;
                        }
                        catch (\Exception $e) {

                        }
                    }
                }
            }
        }

        echo(sprintf("Handle success: %s/%s", $totalSuccess, $totalRecords));
    }

    private function buildInsertData($qrLocalUser, $email)
    {
        if (!$qrLocalUser) {
            return null;
        }

        return [
            'name' => $qrLocalUser->fullname,
            'email' => $email,
            'password' => \Hash::make('Digi@123'),
        ];
    }
}
