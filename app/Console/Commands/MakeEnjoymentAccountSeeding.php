<?php

namespace App\Console\Commands;

use App\Bank;
use App\MpEnjoymentAccount;
use App\MpTerminal;
use App\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class MakeEnjoymentAccountSeeding extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:makeEnjoymentAccountSeeding';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);

        $merchants = Store::all();
        if (is_object($merchants) && sizeof($merchants) > 0) {
            foreach ($merchants as $merchant) {
                echo(sprintf("merchant_code: %s\n", $merchant->merchant_code));
                $this->handleEachMerchant($merchant);
            }
        }
        echo("done");
    }

    private function handleEachMerchant($merchant) {
        if (!$merchant) {
            return false;
        }
        $mpTerminals = MpTerminal::where('merchant_code', $merchant->merchant_code)
            ->where('bank_number', '!=', '')
            ->get();
        if (is_object($mpTerminals) && sizeof($mpTerminals) > 0) {
            foreach ($mpTerminals as $mpTerminal) {
                if ($this->countEnjoymentByMerchant($merchant->merchant_code, $mpTerminal->bank_number) === 0) {
                    $insertData = [
                        'merchant_code' => $merchant->merchant_code,
                        'fullname' => $mpTerminal->bank_account,
                        'account_number' => $mpTerminal->bank_number,
                        'bank_code' => $merchant->bank_code,
                        'bank_name' => $mpTerminal->bank_name,
                        'branch' => $merchant->bank_branch
                    ];
                    if ($mpTerminal->bank_number) {
                        $this->createNewEnjoymentAccount($insertData);
                    }
                }
            }
        }
        else {
            if ($this->countEnjoymentByMerchant($merchant->merchant_code, $merchant->bank_number) === 0) {
                $insertData = [
                    'merchant_code' => $merchant->merchant_code,
                    'fullname' => $merchant->bank_account,
                    'account_number' => $merchant->account_number,
                    'bank_code' => $merchant->bank_code,
                    'bank_name' => $merchant->bank_name,
                    'branch' => $merchant->bank_branch
                ];
                if ($merchant->account_number) {
                    $this->createNewEnjoymentAccount($insertData);
                }
            }
        }
    }

    private function countEnjoymentByMerchant($merchantCode, $accountNumber)
    {
        return MpEnjoymentAccount::where('merchant_code', $merchantCode)
            ->where('account_number', $accountNumber)
            ->count();
    }

    private function createNewEnjoymentAccount($insertData)
    {
        try {
            MpEnjoymentAccount::create($insertData);
        }
        catch (\Exception $e) {
            $errorMsg = sprintf(
                "Upsert enjoyment account for merchant: %s with account number: %s failed: %s\n",
                $insertData['merchant_code'],
                $insertData['account_number'],
                $e->getMessage()
            );
            echo($errorMsg);
            Log::log('error', $errorMsg);
        }
    }
}
