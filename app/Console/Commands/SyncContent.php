<?php

namespace App\Console\Commands;

use App\Bank;
use App\District;
use App\MccVnpay;
use App\Merchant;
use App\Models\Sync\QrMccVnpay;
use App\Province;
use App\Terminal;
use App\BusinessProduct;

use App\Ward;
use App\Models\Sync\QrBank;
use App\Models\Sync\QrDistrict;
use App\Models\Sync\QrMerchant;
use App\Models\Sync\QrProvince;
use App\Models\Sync\QrTerminal;
use App\Models\Sync\QrWard;
use App\Models\Sync\QrBusinessProduct;
use App\Models\Sync\QrTypeMerchant;
use App\TypeMerchant;

use Carbon\Carbon;
use Illuminate\Console\Command;

class SyncContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:content {content?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calling php command to sync static data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);

        $content = $this->argument('content') ? $this->argument('content') : "all";

        $this->info('Start Sync Database at :' . Carbon::now());
        if ($content == 'bank' || $content == 'all') {
            $this->info('--Start Sync Bank Table at :' . Carbon::now());
            $this->syncBank();
            $this->info('--End Sync Bank Table at :' . Carbon::now());


        }
        if ($content == 'type_merchant' || $content == 'all') {
            $this->info('--Start Sync Type Merchant Table ' . Carbon::now());
            $this->syncTypeMerchant();
            $this->info('--End Sync Type Merchant Table ' . Carbon::now());

        }

        if ($content == 'district' || $content == 'all') {
            $this->info('--Start Sync District Table ' . Carbon::now());
            $this->syncDistrict();
            $this->info('--End Sync District Table ' . Carbon::now());

        }
        if ($content == 'merchant' || $content == 'all') {
            $this->info('--Start Sync Merchant Table' . Carbon::now());
            $this->syncMerchant();
            $this->info('--End Sync Merchant Table' . Carbon::now());

        }
        if ($content == 'terminal' || $content == 'all') {
            $this->info('--Start Sync Terminal Table' . Carbon::now());
            $this->syncTerminal();
            $this->info('--End Sync Terminal Table' . Carbon::now());

        }
        if ($content == 'business_product' || $content == 'all') {
            $this->info('--Start Sync Business Product Table ' . Carbon::now());
            $this->syncBusinessProduct();
            $this->info('--End Sync Business Product Table at :' . Carbon::now());

        }

        if ($content == 'province' || $content == 'all') {
            $this->info('--Start Sync Province Table ' . Carbon::now());
            $this->syncProvince();
            $this->info('--End Sync Province Table ' . Carbon::now());
        }

        if ($content == 'ward' || $content == 'all') {
            $this->info('--Start Sync Ward Table ' . Carbon::now());
            $this->syncWard();
            $this->info('--End Sync Ward Table ' . Carbon::now());

        }
        if ($content == 'mcc_vnpay' || $content == 'all') {
            $this->info('--Start Sync Mcc Vnpay Table ' . Carbon::now());
            $this->syncMccVnpay();
            $this->info('--End Sync Mcc Vnpay Table ' . Carbon::now());

        }
        $this->info('End Sync Database at :' . Carbon::now());


    }

    private function syncBank()
    {
        $qrBanks = QrBank::all();
        Bank::truncate();
        foreach ($qrBanks as $qrBank) {
            $bank = Bank::findOrNew($qrBank->id);
            $bank->id = $qrBank->id;
            $bank->bank_code = $qrBank->bank_code;
            $bank->bank_name = $qrBank->bank_name;
            $bank->brand_name = $qrBank->brand_name;
            $bank->bank_vnmart_account = $qrBank->bank_vnmart_account;
            $bank->description = $qrBank->description;
            $bank->created_at = $qrBank->created_date;
            $bank->updated_at = $qrBank->modify_date;
            $bank->status = $qrBank->status;
            $bank->save();
        }
    }


    private function syncDistrict()
    {
        $qrDistricts = QrDistrict::all();
        District::truncate();
        foreach ($qrDistricts as $qrDistrict) {
            $district = District::findOrNew($qrDistrict->id);
            $district->id = $qrDistrict->id;
            $district->province_code = $qrDistrict->province_code;
            $district->district_code = $qrDistrict->district_code;
            $district->district_name = $qrDistrict->district_name;
            $district->created_at = $qrDistrict->created_date;
            $district->save();
        }
    }

    private function syncMerchant()
    {
        $qrMerchants = QrMerchant::all();
        Merchant::truncate();
        foreach ($qrMerchants as $qrMerchant) {
            $merchant = Merchant::findOrNew($qrMerchant->id);
            $merchant->id = $qrMerchant->id;
            $merchant->merchant_code = $qrMerchant->merchant_code;
            $merchant->merchant_brand = $qrMerchant->merchant_brand;
            $merchant->master_merchant_code = $qrMerchant->master_merchant_code;
            $merchant->merchant_name = $qrMerchant->merchant_name;
            $merchant->address = $qrMerchant->address;
            $merchant->business_address = $qrMerchant->business_address;
            $merchant->department_id = $qrMerchant->department_id;
            $merchant->description = $qrMerchant->description;
            $merchant->district_code = $qrMerchant->district_code;
            $merchant->merchant_type = $qrMerchant->merchant_type;
            $merchant->merchant_type_enterprise = $qrMerchant->merchant_type_enterprise;
            $merchant->pin_code = $qrMerchant->pin_code;
            $merchant->provider_code = $qrMerchant->provider_code;
            $merchant->province_code = $qrMerchant->province_code;
            $merchant->service_code = $qrMerchant->service_code;
            $merchant->staff_id = $qrMerchant->staff_id;
            $merchant->wards_code = $qrMerchant->wards_code;
            $merchant->website = $qrMerchant->website;
            $merchant->created_at = $qrMerchant->created_date;
            $merchant->updated_at = $qrMerchant->modify_date;
            $merchant->save();


        }
    }


    private function syncProvince()
    {
        $qrProvinces = QrProvince::all();
        Province::truncate();
        foreach ($qrProvinces as $qrProvince) {
            $province = Province::findOrNew($qrProvince->id);
            $province->id = $qrProvince->id;
            $province->province_code = $qrProvince->province_code;
            $province->province_name = $qrProvince->province_name;
            $province->brand_name = $qrProvince->brand_name;
            $province->created_at = $qrProvince->created_date;
            $province->save();
        }
    }

    private function syncMccVnpay()
    {
        $qrMccVnpays = QrMccVnpay::all();
        MccVnpay::truncate();
        foreach ($qrMccVnpays as $qrMccVnpay) {
            $mcc_vnpay = MccVnpay::findOrNew($qrMccVnpay->id);
            $mcc_vnpay->id = $qrMccVnpay->id;
            $mcc_vnpay->type_code = $qrMccVnpay->type_code;
            $mcc_vnpay->brand_name = $qrMccVnpay->brand_name;
            $mcc_vnpay->full_name = $qrMccVnpay->full_name;
            $mcc_vnpay->description = $qrMccVnpay->description;
            $mcc_vnpay->status = $qrMccVnpay->status;
            $mcc_vnpay->save();
        }
    }


    private function syncTerminal()
    {
        $qrTerminals = QrTerminal::all();
        Terminal::truncate();
        foreach ($qrTerminals as $qrTerminal) {
            $terminal = Terminal::findOrNew($qrTerminal->id);
            $terminal->id = $qrTerminal->id;
            $terminal->terminal_code = $qrTerminal->terminal_id;
            $terminal->tax_code = $qrTerminal->tax_code;
            $terminal->merchant_id = $qrTerminal->merchant_id;
            $terminal->terminal_name = $qrTerminal->terminal_name;
            $terminal->account_id = $qrTerminal->account_id;
            $terminal->business_address = $qrTerminal->business_address;
            $terminal->business_product = $qrTerminal->business_product;
            $terminal->district_code = $qrTerminal->district_code;
            $terminal->facebook = $qrTerminal->facebook;
            $terminal->file_name = $qrTerminal->file_name;
            $terminal->mcc = $qrTerminal->mcc;
            $terminal->product_description = $qrTerminal->product_description;
            $terminal->province_code = $qrTerminal->province_code;
            $terminal->register_ott = $qrTerminal->register_ott;
            $terminal->register_qr = $qrTerminal->register_qr;
            $terminal->register_sms = $qrTerminal->register_sms;
            $terminal->register_vnpayment = $qrTerminal->register_vnpayment;
            $terminal->service_code = $qrTerminal->service_code;
            $terminal->status = $qrTerminal->status;
            $terminal->terminal_address = $qrTerminal->terminal_address;
            $terminal->terminal_document = $qrTerminal->terminal_document;
            $terminal->the_first = $qrTerminal->the_first;
            $terminal->wards_code = $qrTerminal->wards_code;
            $terminal->website = $qrTerminal->website;
            $terminal->website_business = $qrTerminal->website_business;
            $terminal->account_vnmart_id = $qrTerminal->account_vnmart_id;
            $terminal->created_at = $qrTerminal->created_date;
            $terminal->updated_at = $qrTerminal->modify_date;
            $terminal->save();

        }
    }


    private function syncWard()
    {
        $qrWards = QrWard::all();
        Ward::truncate();
        foreach ($qrWards as $qrWard) {
            $ward = Ward::findOrNew($qrWard->id);
            $ward->id = $qrWard->id;
            $ward->province_code = $qrWard->province_code;
            $ward->district_code = $qrWard->district_code;
            $ward->wards_code = $qrWard->wards_code;
            $ward->wards_name = $qrWard->wards_name;
            $ward->created_at = $qrWard->created_date;
            $ward->save();
        }
    }

    private function syncBusinessProduct()
    {
        $qrBusinessProducts = QrBusinessProduct::all();
        BusinessProduct::truncate();
        foreach ($qrBusinessProducts as $qrBusinessProduct) {
            $businessProduct = BusinessProduct::findOrNew($qrBusinessProduct->id);
            $businessProduct->id = $qrBusinessProduct->id;
            $businessProduct->name = $qrBusinessProduct->name;
            $businessProduct->status = $qrBusinessProduct->status;
            $businessProduct->description = $qrBusinessProduct->description;
            $businessProduct->type_code = $qrBusinessProduct->type_code;
            $businessProduct->created_at = $qrBusinessProduct->created_date;
            $businessProduct->updated_at = $businessProduct->updated_date;
            $businessProduct->save();
        }
    }

    private function syncTypeMerchant()
    {
        $qrTypeMerchants = QrTypeMerchant::all();
        TypeMerchant::truncate();
        foreach ($qrTypeMerchants as $qrTypeMerchant) {
            $typeMerchant = TypeMerchant::findOrNew($qrTypeMerchant->id);
            $typeMerchant->id = $qrTypeMerchant->id;
            $typeMerchant->type_code = $qrTypeMerchant->type_code;
            $typeMerchant->brand_name = $qrTypeMerchant->brand_name;
            $typeMerchant->full_name = $qrTypeMerchant->full_name;
            $typeMerchant->description = $qrTypeMerchant->description;
            $typeMerchant->status = $qrTypeMerchant->status;
            $typeMerchant->created_at = $qrTypeMerchant->created_date;
            $typeMerchant->save();
        }
    }

}
