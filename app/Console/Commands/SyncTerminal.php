<?php

namespace App\Console\Commands;

use App\Libraries\BaseFunction;
use App\MpTerminal;
use App\Store;

use App\TerminalHistory;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SyncTerminal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:syncTerminal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calling php command to sync terminal data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);

        $activeMerchants = Store::where('status', BaseFunction::STATUS_CONTROL_ACCEPTED)
            ->whereNull('sync_first_terminal')
            ->take(100)
            ->get();
        if ($activeMerchants && sizeof($activeMerchants) > 0) {
            foreach ($activeMerchants as $merchant) {
                $this->createTerminalIfNotExisted($merchant);
                $merchant->update([
                    'sync_first_terminal' => 1
                ]);
            }
            echo(sprintf("process %s records", sizeof($activeMerchants)));
        }
        else {
            echo("no store info found");
        }
    }

    private function createTerminalIfNotExisted($merchant)
    {
        $existedTerminal = MpTerminal::where('merchant_code', $merchant['merchant_code'])
            ->where('terminal_id', $merchant['terminal_id'])
            ->first();
        if (!$existedTerminal) {
            $data = [
                'merchant_code' => $merchant['merchant_code'],
                'terminal_id' => $merchant['terminal_id'],
                'terminal_name' => $merchant['terminal_name'],
                'terminal_type' => $merchant['terminal_type'],
                'product_description' => $merchant['terminal_description'],
                'mcc' => $merchant['terminal_mcc'] ?? '630000',
                'terminal_address' => $merchant['terminal_address'],
                'terminal_province_code' => $merchant['terminal_province_code'],
                'terminal_district_code' => $merchant['terminal_district_code'],
                'terminal_ward_code' => $merchant['terminal_ward_code'],
                'website' => $merchant['terminal_website'],
                'facebook' => '',
                'file_contract' => '',
                'file_auth_letter' => '',
                'file_other' => '',
                'register_qr' => 1,
                'register_vnpayment' => $merchant['terminal_register_vnpayment'],
                'bank_code' => $merchant['bank_code'],
                'bank_branch' => $merchant['bank_branch'],
                'bank_number' => $merchant['bank_number'],
                'bank_account' => $merchant['bank_account'],
                'terminal_contact_name' => $merchant['terminal_contact_name'],
                'terminal_contact_phone' => $merchant['terminal_contact_phone'],
                'terminal_contact_email' => $merchant['terminal_contact_email'],
                'status' => MpTerminal::STATUS_REVIEW_APPROVED,
                'created_by' => $merchant['created_by'],
                'terminal_app_user' => $merchant['terminal_app_user'],
            ];

            try {
                $mpTerminal = MpTerminal::create($data);
                $mpTerminal->created_at = $merchant['created_at'];
                $mpTerminal->updated_at = $merchant['updated_at'];
                $mpTerminal->save();
                $this->insertTerminalHistory($merchant, $mpTerminal);
                $this->info(sprintf('Done sync merchant %s - at %s', $merchant['merchant_code'], Carbon::now()));
            }
            catch (\Exception $e) {

            }
        }
    }

    private function insertTerminalHistory($merchant, $mpTerminal)
    {
        $data = [
            'mp_terminal_id' => $mpTerminal['id'],
            'terminal_id' => $mpTerminal['terminal_id'],
            'staff_id' => 1,
            'type' => TerminalHistory::TYPE_SYSTEM_SYNC,
            'reason' => '',
        ];
        if ($merchant['trial_user_id']) {
            $data['reason'] = 'MC khởi tạo hồ sơ';
        }
        else {
            if ($merchant['src'] == 'SYNC_MMS') {
                $data['type'] = TerminalHistory::TYPE_MMS_SYNC;
                $data['reason'] = 'Đồng bộ dữ liệu MMS về';
            }
            else {
                $data['reason'] = 'Sale Admin khởi tạo ĐCNTT';
            }
        }

        try {
            TerminalHistory::create($data);
        }
        catch (\Exception $e) {

        }
    }
}
