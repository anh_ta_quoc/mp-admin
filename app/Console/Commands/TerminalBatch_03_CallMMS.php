<?php

namespace App\Console\Commands;

use App\Libraries\MmsService\MmsClient;
use App\MpTerminal;
use App\MpTerminalBatch;
use App\TerminalHistory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TerminalBatch_03_CallMMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:TerminalBatch_03_CallMMS';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    const MAX_RETRY = 3;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);

        $log_msg = "---\n";
        $process_lock = $this->lock_process();
        if (!$process_lock) {
            $log_msg .= "Failed to lock process\n";
        } else {
            $batch = MpTerminalBatch::where('process_lock', $process_lock)->first();

            if ($batch) {
                $log_msg .= "Processing process_lock:" . $process_lock . "\n";
                $log_msg .= "Batch ID:" . $batch->id . "\n";
                $process_error = $this->process($batch);

                if ($process_error) {
                    $log_msg .= $process_error . "\n";
                }
                $log_msg .= "Call MMS done\n";
            } else {
                $log_msg = "No record to process\n";
            }
        }
        print ($log_msg);
        Log::channel('terminal_batch_create_ter_logs')->info($log_msg);
    }

    private function lock_process()
    {
        $process_lock = uniqid();
        try {
            $query = sprintf("
                update mp_terminal_batch set process_lock = '%s'
                where status = %s and (process_lock = '' or process_lock is null) and retry_count < %s
                order by updated_at asc limit 1",
                $process_lock,
                MpTerminalBatch::STATUS_WAIT_MMS,
                self::MAX_RETRY
            );
            DB::update(DB::raw($query));
            return $process_lock;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function unlock_process($batch, $process_msg, $status = null, $increase_retry_count = false)
    {
        $batch->process_lock = null;
        $batch->process_msg = $process_msg;

        if ($increase_retry_count) {
            $batch->retry_count += 1;
        }

        if ($status != null) {
            $batch->status = $status;
            $batch->retry_count = 0;
        }
        $batch->save();
    }

    private function process($batch){
        try {
            $mpTerminals = MpTerminal::where('src', sprintf('%s-%s', 'BATCH', $batch->id))
                ->where('status', '<', MpTerminal::STATUS_REVIEW_APPROVED)
                ->get();
            if ($mpTerminals) {
                $client = new MmsClient('TER_CREATE');
                $errorDetail = '';
                foreach ($mpTerminals as $mpTerminal) {
                    echo("process terminal " . $mpTerminal->terminal_name . "\n");

                    // log create mp terminal
                    $this->createMpTerminalLog(
                        $batch->created_by, $mpTerminal,
                        TerminalHistory::TYPE_OPERATOR_CREATE_TERMINAL,
                        '', null, null, $batch->created_at, $batch->updated_at
                    );

                    $mms_response = $client->endpoint->endpointRequest($mpTerminal);
                    if (!($mms_response && $mms_response['success'] == true)) {
                        $errorDetail .= 'call mms terminal: ' . $mpTerminal->terminal_name . ' failed' . "\n";
                        $mpTerminal->mms_status = MpTerminal::MMS_STATUS_FAILED;
                        if ($mms_response) {
                            $mpTerminal->mms_error_code = $mms_response['code'];
                            $mpTerminal->lock_edit = 1;
                        }
                    }
                    else {
                        $mpTerminal->status = MpTerminal::STATUS_REVIEW_APPROVED;
                        $mpTerminal->mms_status = MpTerminal::MMS_STATUS_CREATED;

                        // create sale approve log for mp terminal
                        $batchHistorySaleApprove = TerminalHistory::where('mp_terminal_id', $batch->id)
                            ->where('type', TerminalHistory::TYPE_BATCH_SALE_APPROVED)
                            ->first();
                        if ($batchHistorySaleApprove) {
                            $this->createMpTerminalLog(
                                $batchHistorySaleApprove->staff_id,
                                $mpTerminal,
                                TerminalHistory::TYPE_SALE_APPROVED,
                                '', null, null,
                                $batchHistorySaleApprove->created_at,
                                $batchHistorySaleApprove->updated_at
                            );
                        }
                        // create review approve log for mp terminal
                        $batchHistoryReviewApprove = TerminalHistory::where('mp_terminal_id', $batch->id)
                            ->where('type', TerminalHistory::TYPE_BATCH_REVIEW_APPROVED)
                            ->first();
                        if ($batchHistoryReviewApprove) {
                            $this->createMpTerminalLog(
                                $batchHistoryReviewApprove->staff_id,
                                $mpTerminal,
                                TerminalHistory::TYPE_REVIEW_APPROVED,
                                '', null, null,
                                $batchHistoryReviewApprove->created_at,
                                $batchHistoryReviewApprove->updated_at
                            );
                        }
                    }

                    $mpTerminal->save();
                }
                if ($errorDetail !== '') {
                    $this->unlock_process($batch, $errorDetail, MpTerminalBatch::STATUS_MMS_ERROR, true);
                }
                else {
                    $this->unlock_process($batch, '', MpTerminalBatch::STATUS_DONE, false);
                    return $errorDetail;
                }
                return $errorDetail;
            }
            return null;
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $this->unlock_process($batch, $message, null, true);
            return $message;
        }
    }

    private function createMpTerminalLog($staffId, $mpTerminal, $type, $reason = '', $oldData = null, $newData = null, $created_at = null, $updated_at = null)
    {
        try {
            $existed = TerminalHistory::where('mp_terminal_id', $mpTerminal->id)
                ->where('type', $type)
                ->first();
            if (!$existed) {
                $insertData = [
                    'mp_terminal_id' => $mpTerminal->id,
                    'terminal_id' => $mpTerminal->terminal_id,
                    'staff_id' => $staffId,
                    'type' => $type,
                    'reason' => $reason,
                ];
                if ($oldData) {
                    $insertData['old_data'] = $oldData;
                }
                if ($newData) {
                    $insertData['new_data'] = $newData;
                }
                $terminalHistory = TerminalHistory::create($insertData);
                if ($created_at || $updated_at) {
                    if ($created_at) {
                        $terminalHistory->created_at = $created_at;
                    }
                    if ($updated_at) {
                        $terminalHistory->updated_at = $updated_at;
                    }
                    $terminalHistory->save();
                }
            }
        }
        catch (\Exception $e) {

        }
    }
}
