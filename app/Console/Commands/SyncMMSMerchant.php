<?php

namespace App\Console\Commands;

use App\AuditLog;
use App\Libraries\BaseFunction;
use App\Models\Sync\QrBusinessProduct;
use App\Models\Sync\QrEnjoymentAccount;
use App\Models\Sync\QrMerchant;
use App\Models\Sync\QrMerchantDocument;
use App\Models\Sync\QrMerchantInfo;
use App\Models\Sync\QrTerminal;
use App\Models\Sync\QrTerminalContact;
use App\Models\Sync\QrTypeMerchant;
use App\MpTerminal;
use App\Store;

use App\TerminalHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class SyncMMSMerchant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:syncMMSMerchant {--lastId=} {--merchantCode=}';

    private $documentUrl = '';

    private $storeInfo = null;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calling php command to sync MMS Merchant data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (config('app.env') == 'production') {
            $this->documentUrl = 'http://113.52.45.119:8080/QrcodeUploadAPI/resources/document/';
        }
        else {
            $this->documentUrl = 'http://10.22.7.105:8080/QrcodeUploadAPI/resources/document/';
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $lastId = (int)$this->option('lastId') ?? 1;
        $mmsMerchantQuery = QrMerchant::where('id', '>', $lastId)
            ->whereIn('status', [-1, 1, 5, 6]);
        if ($this->hasOption('merchantCode') && $this->option('merchantCode')) {
            $mmsMerchantQuery = $mmsMerchantQuery->where('merchant_code', trim($this->option('merchantCode')));
        }
        $mmsMerchants = $mmsMerchantQuery->get();
        if ($mmsMerchants) {
            foreach ($mmsMerchants as $mmsMerchant) {
                echo(sprintf("handle merchant: %s\n", $mmsMerchant->merchant_code));
                try {
                    $existedStoreInfo = Store::where('merchant_code', $mmsMerchant->merchant_code)
                        ->first();
                    if ($existedStoreInfo) {
                        echo(sprintf("merchant: %s existed\n", $mmsMerchant->merchant_code));
                        break;
                    }
                    $insertData = $this->prepareData($mmsMerchant);
                    $mpMerchant = Store::create($insertData);
                    $mpMerchant->created_at = date("Y-m-d H:i:s", strtotime($mmsMerchant->created_date));
                    if ($mmsMerchant->modify_date) {
                        $mpMerchant->updated_at = date("Y-m-d H:i:s", strtotime($mmsMerchant->modify_date));
                    }
                    $mpMerchant->save();
                    $this->createAuditLog($mpMerchant, 'Hệ thống đồng bộ', 'Đồng bộ dữ liệu MMS về');
                    if ($insertData['status'] == 2) {
                        if ($insertData['src'] == "SYNC_MMS") {
                            $this->createAuditLog($mpMerchant, 'Đối soát duyệt', '',
                                $this->getMMSUser($mmsMerchant->process_user),
                                $mmsMerchant->created_date, $mmsMerchant->modify_date
                            );
                        }
                        else {
                            if ($this->storeInfo) {
                                $oldAuditLog = $this->getAuditLog($this->storeInfo->id);
                                if ($oldAuditLog) {
                                    $this->createAuditLog($mpMerchant, 'Đối soát duyệt',
                                        $oldAuditLog->operator_note, $oldAuditLog->user_id, $oldAuditLog->created_at,
                                        $oldAuditLog->updated_at);
                                }
                            }
                        }
                    }
                    $this->writeLog($mmsMerchant->id, $mmsMerchant->merchant_code, $mmsMerchant->toJson());
                }
                catch (\Exception $e) {
                    echo("-------\n");
                    echo(sprintf("error merchant: %s\n", $mmsMerchant->merchant_code));
                    echo(sprintf("%s\n", $e->getMessage()));
                }
            }
        }
        echo("done sync");
    }

    private function prepareData($mmsMerchant)
    {
        $storeInfoData = [
            'merchant_code' => $mmsMerchant->merchant_code,
            'merchant_name' => $mmsMerchant->merchant_name,
            'merchant_brand' => $mmsMerchant->merchant_brand,
            'merchant_address' => $mmsMerchant->address,
            'merchant_business_address' => $mmsMerchant->business_address ?
                $mmsMerchant->business_address : $mmsMerchant->address,
            'merchant_province_code' => $mmsMerchant->province_code,
            'merchant_district_code' => $mmsMerchant->district_code,
            'merchant_ward_code' => $mmsMerchant->wards_code,
            'merchant_type' => $this->getMerchantType($mmsMerchant->merchant_type),
            'merchant_type_business' => $mmsMerchant->merchant_type_enterprise,
            'mms_status' => 2,
            'department_id' => $mmsMerchant->department_id,
            'staff_id' => $mmsMerchant->staff_id,
            'status' => $this->mappingStatus($mmsMerchant->status),
            'merchant_website' => $mmsMerchant->website,
            'currency' => 'VND',
        ];

        $storeInfoData['created_by'] = $this->getMMSUser($mmsMerchant->create_user);
        $storeInfoData['src'] = 'SYNC_MMS';
        // addition data
        $storeInfoData = array_merge($storeInfoData, $this->getFirstTerminal($mmsMerchant));
        $storeInfoData = array_merge($storeInfoData, $this->getDocumentInfo($mmsMerchant->merchant_code));
        $storeInfoData = array_merge($storeInfoData, $this->getMerchantContactInfo($mmsMerchant->merchant_code));

        if (in_array(strtolower($mmsMerchant->create_user), ['mcportal', 'api_khdn', 'acf4fb056b9aa04b972c2ef48dad0a23'])) {
            $this->storeInfo = $this->getStoreInfo($mmsMerchant->merchant_code);
            $storeInfo = $this->storeInfo;
            if ($storeInfo) {
                $storeInfoData['mms_status'] = max(1, $storeInfo->mms_status);
                $storeInfoData['status'] = $storeInfo->status;
                $storeInfoData['created_by'] = $storeInfo->created_by;
                $storeInfoData['trial_user_id'] = $storeInfo->trial_user_id;
                $storeInfoData['src'] = $storeInfo->src;
                $storeInfoData['merchant_date_of_issue'] = $storeInfo->merchant_date_of_issue;
                $storeInfoData['merchant_place_of_issue'] = $storeInfo->merchant_place_of_issue;
                $storeInfoData['contract_code'] = $storeInfo->contract_code;
                $storeInfoData['contract_date'] = $storeInfo->contract_date;
                $storeInfoData['contract_number'] = $storeInfo->contract_number;
                $storeInfoData['operator_note'] = $storeInfo->operator_note;
                $storeInfoData['process_lock'] = $storeInfo->process_lock;
                $storeInfoData['process_msg'] = $storeInfo->process_msg;
                $storeInfoData['request_id'] = $storeInfo->request_id;
                $storeInfoData['retry_count'] = $storeInfo->retry_count;
                $storeInfoData['partner_status'] = $storeInfo->partner_status;
                $storeInfoData['process_status'] = $storeInfo->process_status;
                // merchant files
                $storeInfoData = array_merge($storeInfoData, [
                    'merchant_file_business_cert' => $storeInfo->merchant_file_business_cert,
                    'merchant_file_contract' => $storeInfo->merchant_file_contract,
                    'merchant_file_business_tax_cert' => $storeInfo->merchant_file_business_tax_cert,
                    'merchant_file_domain_cert' => $storeInfo->merchant_file_domain_cert,
                    'merchant_file_identify_card' => $storeInfo->merchant_file_identify_card,
                ]);
            }
        }

        return $storeInfoData;
    }

    private function getMMSUser($mmsUser)
    {
        $local_user = DB::table('qr_local_user')
            ->where('check_username', $mmsUser)
            ->first();
        if ($local_user) {
            $user = User::whereRaw(sprintf("UPPER(email) = '%s'", strtoupper(trim($local_user->username) . '@vnpay.vn')))
                ->first();
            return $user->id ?? null;
        }
        return null;
    }

    private function getMerchantType($type_id)
    {
        $merchantType = QrTypeMerchant::where('id', $type_id)->first();
        return $merchantType->type_code ?? '';
    }

    private function getMerchantContactInfo($mmsMerchantCode)
    {
        $merchantContact = QrMerchantInfo::where('merchant_code', $mmsMerchantCode)->first();
        if ($merchantContact) {
            return [
                'merchant_contact_name' => $merchantContact->contact_name,
                'merchant_contact_email' => $merchantContact->contact_email,
                'merchant_contact_phone' => $merchantContact->contact_phone,
                'issue_invoice' => $merchantContact->issue_invoice,
            ];
        }
        else {
            return [];
        }
    }

    private function getDocumentInfo($mmsMerchantCode)
    {
        $merchantDocument = QrMerchantDocument::where('merchant_code', $mmsMerchantCode)->first();
        $res = [];
        if ($merchantDocument) {
            if (!in_array($merchantDocument->profile_status, [-1, 1])) {
                $res['status'] = -1;
                $res['mms_status'] = 1;
            }
            else {
                $res['mms_status'] = 2;
            }
            $res['merchant_file_business_cert'] = $merchantDocument->business_cert ?
                sprintf('%s%s', $this->documentUrl, $merchantDocument->business_cert) : null;
            $res['merchant_file_contract'] = $merchantDocument->contract ?
                sprintf('%s%s', $this->documentUrl, $merchantDocument->contract) : null;
            $res['merchant_file_business_tax_cert'] = $merchantDocument->business_tax_cert ?
                sprintf('%s%s', $this->documentUrl, $merchantDocument->business_tax_cert) : null;
            $res['merchant_file_domain_cert'] = $merchantDocument->domain_cert ?
                sprintf('%s%s', $this->documentUrl, $merchantDocument->domain_cert) : null;
            $res['merchant_file_identify_card'] = $merchantDocument->identify_card ?
                sprintf('%s%s', $this->documentUrl, $merchantDocument->identify_card) : null;
            return $res;
        }
        else {
            return [];
        }
    }

    private function getFirstTerminal($mmsMerchantMerchant)
    {
        $firstTerminal = QrTerminal::where('merchant_id', $mmsMerchantMerchant->id)->orderBy('id', 'asc')->first();
        if ($firstTerminal) {
            $terminal = [
                'terminal_address' => $firstTerminal->business_address,
                'terminal_province_code' => $firstTerminal->province_code ?? '',
                'terminal_district_code' => $firstTerminal->district_code ?? '',
                'terminal_ward_code' => $firstTerminal->wards_code ?? '',
                'terminal_id' => $firstTerminal->terminal_id,
                'terminal_name' => $firstTerminal->terminal_name,
                'terminal_type' => $this->getBusinessProduct($firstTerminal->business_product),
                'terminal_description' => $firstTerminal->product_description,
                'currency' => 'VND',
                'terminal_register_vnpayment' => $firstTerminal->register_vnpayment == 1 ? 1 : 0,
                'terminal_website' => $firstTerminal->website ?? $firstTerminal->website_business,
                'wallet_status' => $firstTerminal->account_vnmart_id == 1 ? 1: 0,
                'terminal_mcc' => $firstTerminal->mcc ?? '',
            ];
            $terminal = array_merge($terminal, $this->getTerminalContact($mmsMerchantMerchant->merchant_code,
                $firstTerminal->terminal_id));
            $terminal = array_merge($terminal, $this->getBankInfo($firstTerminal->account_id));

            return $terminal;
        }
        else {
            return [];
        }
    }

    private function getBusinessProduct($businessProductId)
    {
        $businessProduct = QrBusinessProduct::where('id', $businessProductId)->first();
        if ($businessProduct) {
            return $businessProduct->type_code;
        }
        else {
            $businessProduct = QrBusinessProduct::where('type_code', $businessProductId)->first();
            return $businessProduct ? $businessProduct->type_code : null;
        }
    }

    private function getTerminalContact($merchant_code, $terminal_id)
    {
        $terminalContact = QrTerminalContact::where('merchant_code', $merchant_code)
            ->where('terminal_id', $terminal_id)
            ->first();
        if ($terminalContact) {
            return [
                'terminal_contact_email' => $terminalContact->email ?? '',
                'terminal_contact_name' => $terminalContact->fullname ?? '',
                'terminal_contact_phone' => $terminalContact->phone ?? '',
            ];
        }
        else {
            return [];
        }
    }

    private function getBankInfo($accountId)
    {
        if (!$accountId) {
            return [];
        }
        $enjoyment = QrEnjoymentAccount::where('id', $accountId)->first();
        if (!$enjoyment) {
            return [];
        }
        else {
            return [
                'bank_code' => $enjoyment->bank_code,
                'bank_name' => $enjoyment->bank_name,
                'bank_branch' => $enjoyment->branch,
                'bank_account' => $enjoyment->fullname ?? '',
                'bank_number' => $enjoyment->account_number,
            ];
        }
    }

    private function mappingStatus($mmsStatus)
    {
        $mapping = [
            -1 => BaseFunction::STATUS_LOCK,
            1 => BaseFunction::STATUS_CONTROL_ACCEPTED,
            5 => BaseFunction::STATUS_SALE_REJECTED,
            6 => BaseFunction::STATUS_CONTROL_REJECTED
        ];
        if (in_array($mmsStatus, array_keys($mapping))) {
            return $mapping[$mmsStatus];
        }
        return $mmsStatus;
    }

    private function getStoreInfo($merchantCode)
    {
        $bkTable = "store_info_bk";
        $storeInfo = DB::connection('rest_api_db')->select(sprintf("
        SELECT * FROM %s
        WHERE merchant_code = '%s' ORDER BY created_at DESC LIMIT 1
        ", $bkTable, $merchantCode));
        if (is_array($storeInfo) && count($storeInfo) > 0) {
            return $storeInfo[0];
        }
        return null;
    }

    private function getAuditLog($storeInfoId, $message = "Đối soát duyệt")
    {
        $bkTable = "audit_logs_bk";
        $audiLogBk = DB::connection('mysql')->select(sprintf("
        SELECT * FROM %s
        WHERE store_id = %s AND message = '%s' LIMIT 1
        ", $bkTable, $storeInfoId, $message));
        if (is_array($audiLogBk) && count($audiLogBk) > 0) {
            return $audiLogBk[0];
        }
        return null;
    }

    private function writeLog($mmsId, $merchantCode, $data_sync)
    {
        Log::channel('sync_mms_merchant')->info(sprintf("Sync: mms_id: %s - merchant_code: %s -
        data sync: %s", $mmsId, $merchantCode, $data_sync));
    }

    public function createAuditLog($store, $message, $operatorNote = '', $userId = 1, $createdAt = null, $updatedAt = null)
    {
        $auditLog = new AuditLog();
        $auditLog->user_id = $userId;
        $auditLog->store_id = $store->id;
        $auditLog->old_data = "{}";
        $auditLog->new_data = "{}";
        $auditLog->message = $message;
        $auditLog->operator_note = $operatorNote;
        try {
            if ($createdAt) {
                $auditLog->created_at = $createdAt;
            }
            if ($updatedAt) {
                $auditLog->updated_at = $updatedAt;
            }
            $auditLog->save();
        }
        catch (\Exception $e) {

        }
    }
}
