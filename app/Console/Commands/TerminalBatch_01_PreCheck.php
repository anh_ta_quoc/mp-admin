<?php

namespace App\Console\Commands;

use App\Http\Requests\CreateTerminalRequest;
use App\MpTerminalBatch;
use App\Rules\BankNumberRule;
use App\Rules\MerchantContactNameRule;
use App\Rules\MobileRule;
use App\Rules\TerminalAddressRule;
use App\Rules\TerminalIdExistedRule;
use App\Rules\TerminalIdRule;
use App\Rules\TerminalNameExistedRule;
use App\Rules\TerminalNameRule;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\IOFactory as IOFactoryAlias;

class TerminalBatch_01_PreCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:TerminalBatch_01_PreCheck';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    const MAX_RETRY = 3;
    const START_ROW = 3;
    const ERROR_COLUMN = 'V';
    const TERMINAL_KEYS = [
        'merchant_code' => 'Merchant Code',
        'terminal_id' => 'Terminal ID',
        'terminal_name' => 'Tên Terminal',
        'terminal_type' => 'Mã SP kinh doanh',
        'mcc' => 'MCC',
        'terminal_province_code' => 'Tỉnh/TP',
        'terminal_district_code' => 'Quận/Huyện',
        'terminal_ward_code' => 'Phường/Xã',
        'terminal_address' => 'Địa chỉ kinh doanh',
        'bank_code' => 'Ngân hàng thụ hưởng',
        'bank_branch' => 'Chi nhánh ngân hàng',
        'bank_number' => 'Số tài khoản',
        'bank_account' => 'Tên chủ tài khoản',
        'receive_method' => 'Hình thức nhận tiền',
        'terminal_contact_name' => 'Họ tên người liên hệ',
        'terminal_contact_phone' => 'Số điện thoại',
        'terminal_contact_email' => 'Email'
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $log_msg = "---\n";
        $process_lock = $this->lock_process();
        if (!$process_lock) {
            $log_msg .= "Failed to lock process\n";
        } else {
            $batch = MpTerminalBatch::where('process_lock', $process_lock)->first();

            if ($batch) {
                $log_msg .= "Processing process_lock:" . $process_lock . "\n";
                $log_msg .= "Batch ID:" . $batch->id . "\n";
                $process_error = $this->process($batch);

                if ($process_error) {
                    $log_msg .= $process_error . "\n";
                }
                $log_msg .= "Pre-Check done\n";
            } else {
                $log_msg = "No record to process\n";
            }
        }
        print ($log_msg);
        Log::channel('terminal_batch_precheck_logs')->info($log_msg);
    }

    private function lock_process()
    {
        $process_lock = uniqid();
        try {
            $query = sprintf("
                update mp_terminal_batch set process_lock = '%s'
                where status = %s and (process_lock = '' or process_lock is null) and retry_count < %s
                order by updated_at asc limit 1",
                $process_lock,
                MpTerminalBatch::STATUS_INIT,
                self::MAX_RETRY
            );
            DB::update(DB::raw($query));
            return $process_lock;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function unlock_process($batch, $process_msg, $status = null, $increase_retry_count = false)
    {
        $batch->process_lock = null;
        $batch->process_msg = $process_msg;
        $file_result_name = str_replace('.xlsx', '_precheck_result.xlsx', $batch->file_terminals);
        $file_result_path = public_path('api_uploads/terminal_batch/' . $batch->merchant_code . '/' . $file_result_name);
        if (file_exists($file_result_path)){
            $batch->file_result = str_replace('.xlsx', '_precheck_result.xlsx', $batch->file_terminals);
        }

        if ($increase_retry_count) {
            $batch->retry_count += 1;
        }

        if ($status != null) {
            $batch->status = $status;
            $batch->retry_count = 0;
        }

        if ($batch->retry_count >= self::MAX_RETRY){
            $batch->status = MpTerminalBatch::STATUS_PRECHECK_ERROR;
            $batch->retry_count = 0;
        }
        $batch->save();
    }

    private function parse_row_message($errors)
    {
        if ($errors) {
            $errors_msg = '';
            foreach ($errors as $key => $error) {
                $errors_msg .= self::TERMINAL_KEYS[$key] . ': ' . implode(', ', $error) . "\n";
            }
            return $errors_msg;
        }
        return '';
    }

    private function check_row_data($batch, $row)
    {
        $terminal = [
            'merchant_code' => $batch->merchant_code,
            'terminal_id' => $row[1],
            'terminal_name' => $row[2],
            'terminal_type' => $row[9],
            'mcc' => $row[11],
            'terminal_province_code' => $row[5],
            'terminal_district_code' => $row[6],
            'terminal_ward_code' => $row[7],
            'terminal_address' => $row[4],
            'bank_code' => $batch->bank_code,
            'bank_branch' => $batch->bank_branch,
            'bank_number' => $batch->bank_number,
            'bank_account' => $batch->bank_account,
            'receive_method' => 0,
            'terminal_contact_name' => $row[18],
            'terminal_contact_phone' => $row[19],
            'terminal_contact_email' => $row[20]
        ];

        $validator = Validator::make($terminal, [
            'merchant_code' => 'required',
            'terminal_id' => [
                'required',
                'min:2',
                'max:8',
                new TerminalIdRule(),
                new TerminalIdExistedRule($batch->merchant_code)
            ],
            'terminal_name' => [
                'required',
                new TerminalNameRule(),
                new TerminalNameExistedRule(),
            ],
            'terminal_type' => 'required',
            'mcc' => 'required',
            'terminal_province_code' => 'required',
            'terminal_district_code' => 'required',
            'terminal_ward_code' => 'required',
            'terminal_address' => [
                'required',
//                new TerminalAddressRule(),
            ],

            'bank_code' => 'required',
            'bank_branch' => 'required',
            'bank_number' => [
                'required',
                new BankNumberRule(),
            ],
            'bank_account' => 'required',
            'receive_method' => 'required',

            'terminal_contact_name' => [
                'required',
                new MerchantContactNameRule(),
            ],
            'terminal_contact_phone' => [
                'required',
                new MobileRule(),
            ],
            'terminal_contact_email' => 'required|email|max:100',

        ]);
        return $this->parse_row_message($validator->errors()->messages());
    }

    private function process($batch)
    {
        try {
            $file_path = public_path('api_uploads/terminal_batch/' . $batch->merchant_code . '/' . $batch->file_terminals);
            if (file_exists($file_path)) {
                $file_error = false;
                $spreadsheet = IOFactoryAlias::load($file_path);
                $writer = IOFactoryAlias::createWriter($spreadsheet, "Xlsx");
                $worksheet = $spreadsheet->getActiveSheet();
                $rows = $worksheet->toArray();

                $total_rows = 0;
                $error_count = 0;
                $success_count = 0;

                foreach ($rows as $row_idx => $row) {
                    if ($row_idx >= self::START_ROW - 1) {
                        $row_error = $this->check_row_data($batch, $row);
                        $total_rows++;
                        if ($row_error) {
                            $file_error = true;
                            $error_count++;
                            $worksheet->setCellValue(self::ERROR_COLUMN . ($row_idx + 1), $row_error);
                        } else {
                            $success_count++;
                            $worksheet->setCellValue(self::ERROR_COLUMN . ($row_idx + 1), 'OK');
                        }
                    }
                }
                if (!$total_rows){
                    throw new \Exception("File format error");
                }
                $writer->save(str_replace('.xlsx', '_precheck_result.xlsx', $file_path));
                $batch->total_rows = $total_rows;
                $batch->precheck_success = $success_count;
                if ($file_error) {
                    $this->unlock_process($batch, $success_count . '/' . $total_rows . ' passed!', MpTerminalBatch::STATUS_PRECHECK_ERROR);
                    return $success_count . '/' . $total_rows . ' passed pre-check';
                } else {
                    $this->unlock_process($batch, '', MpTerminalBatch::STATUS_WAIT_SALE_APPROVE);
                    return null;
                }
            } else {
                return 'File not found';
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $this->unlock_process($batch, $message, null, true);
            return $message;
        }
    }

}
