<?php

namespace App\Console\Commands;

use App\Libraries\BaseFunction;
use App\Models\Sync\QrBusinessProduct;
use App\Models\Sync\QrEnjoymentAccount;
use App\Models\Sync\QrMerchant;
use App\Models\Sync\QrMerchantDocument;
use App\Models\Sync\QrMerchantInfo;
use App\Models\Sync\QrTerminal;
use App\Models\Sync\QrTerminalContact;
use App\Models\Sync\QrTypeMerchant;
use App\MpTerminal;
use App\Store;

use App\TerminalHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SyncMMSTerminal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:syncMMSTerminal {--lastId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calling php command to sync MMS Terminal data';

    private $documentUrl = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (config('app.env') == 'production') {
            $this->documentUrl = 'http://113.52.45.119:8080/QrcodeUploadAPI/resources/document/';
        }
        else {
            $this->documentUrl = 'http://10.22.7.105:8080/QrcodeUploadAPI/resources/document/';
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $lastId = (int)$this->option('lastId') ?? 1;
        $mmsTerminalQuery = QrTerminal::where('id', '>', $lastId)
            ->whereIn('status', [-1, 1, 5, 6]);
        if ($this->hasOption('terminalName') && $this->option('terminalName')) {
            $mmsTerminalQuery = $mmsTerminalQuery->where('terminal_name', trim($this->option('terminalName')));
        }
        $mmsTerminals = $mmsTerminalQuery->get();
        if ($mmsTerminals) {
            foreach ($mmsTerminals as $mmsTerminal) {
                $mmsMerchant = QrMerchant::where('id', $mmsTerminal->merchant_id)->first();
                echo(sprintf("handle terminal: %s - %s\n", $mmsMerchant->merchant_code, $mmsTerminal->terminal_id));
                try {
                    $existedMpTerminal = MpTerminal::where('terminal_name', $mmsTerminal->terminal_name)
                        ->first();
                    if ($existedMpTerminal) {
                        echo(sprintf("terminal: %s existed\n", $mmsMerchant->terminal_name));
                        break;
                    }
                    $insertData = $this->getTerminalData($mmsTerminal, $mmsMerchant);
                    if (count($insertData) > 0) {
                        try {
                            $mpTerminal = MpTerminal::create($insertData);
                            $mpTerminal->created_at = date("Y-m-d H:i:s", strtotime($mmsTerminal->created_date));
                            if ($mmsTerminal->modify_date) {
                                $mpTerminal->updated_at = date("Y-m-d H:i:s", strtotime($mmsTerminal->modify_date));
                            }
                            $mpTerminal->save();
                            $this->insertTerminalHistory($mpTerminal);
                            $this->writeLog(
                                $mmsTerminal->terminal_id,
                                $mmsTerminal->terminal_name,
                                $mmsMerchant->merchant_code, $mmsTerminal->toJson()
                            );
                        }
                        catch (\Exception $e) {
                            echo(sprintf("insert data failed: %s", $e->getMessage()));
                        }
                    }
                }
                catch (\Exception $e) {
                    echo(sprintf("get data failed: %s", $e->getMessage()));
                }
            }
        }
        echo("done sync");
    }

    /**
     * Get Terminal Data
     *
     * @param $mmsTerminal
     * @param $mmsMerchant
     * @return array
     */
    private function getTerminalData($mmsTerminal, $mmsMerchant)
    {
        if ($mmsTerminal) {
            $terminal = [
                'merchant_code' => $mmsMerchant->merchant_code,
                'terminal_address' => $mmsTerminal->business_address,
                'terminal_province_code' => $mmsTerminal->province_code ?? '',
                'terminal_district_code' => $mmsTerminal->district_code ?? '',
                'terminal_ward_code' => $mmsTerminal->wards_code ?? '',
                'terminal_id' => $mmsTerminal->terminal_id,
                'terminal_name' => $mmsTerminal->terminal_name,
                'terminal_type' => $this->getBusinessProduct($mmsTerminal->business_product),
                'product_description' => $mmsTerminal->product_description,
                'currency' => 'VND',
                'register_qr' => $mmsTerminal->register_qr ?? 1,
                'register_vnpayment' => $mmsTerminal->register_vnpayment == 1 ? 1 : 0,
                'website' => $mmsTerminal->website ?? $mmsTerminal->website_business,
                'receive_method' => $mmsTerminal->receive_money_method == 2 ? 1 : 0,
                'wallet_status' => $mmsTerminal->account_vnmart_id == 1 ? 1: 0,
                'mcc' => $mmsTerminal->mcc ?? '',
                'src' => 'SYNC_MMS',
                'status' => $this->mappingStatus($mmsTerminal->status),
                'facebook' => $mmsTerminal->facebook,
            ];

            if (in_array(strtolower($mmsTerminal->create_user), ['mcportal', 'api_khdn'])) {
                $storeInfo = Store::where('merchant_code', $mmsMerchant->merchant_code)
                    ->where('mms_status', 2)
                    ->orderBy('created_at', 'DESC')
                    ->first();
                if (!$storeInfo) {
                    return [];
                }

                $mpTerminal = $this->getMpTerminal($mmsTerminal->terminal_name);
                if ($mpTerminal) {
                    $terminal['created_by'] = $mpTerminal->created_by;
                    if ($mpTerminal->status == MpTerminal::STATUS_SALE_DENIED) {
                        $terminal['status'] = MpTerminal::STATUS_SALE_DENIED;
                    }
                    elseif ($mpTerminal->status == MpTerminal::STATUS_REVIEW_DENIED) {
                        $terminal['status'] = MpTerminal::STATUS_REVIEW_DENIED;
                    }
                    // mp info
                    $terminal['file_contract'] = $mpTerminal->file_contract;
                    $terminal['file_auth_letter'] = $mpTerminal->file_auth_letter;
                    $terminal['file_other'] = $mpTerminal->file_other;
                    $terminal['src'] = $mpTerminal->src;
                }
                else {
                    $terminal['created_by'] = $this->getCreatedByFromMerchant($mmsTerminal->merchant_code);
                }
            }
            else {
                $terminal['created_by'] = $this->getCreatedBy($mmsTerminal->create_user, $mmsTerminal->merchant_code);
                if ($mmsTerminal->terminal_document) {
                    $terminal['file_contract'] = $mmsTerminal->terminal_document ?
                        sprintf('%s%s', $this->documentUrl, $mmsTerminal->terminal_document) : null;
                }
            }

            $terminal = array_merge($terminal, $this->getTerminalContact($mmsMerchant->merchant_code,
                $mmsTerminal->terminal_id));
            $terminal = array_merge($terminal, $this->getBankInfo($mmsTerminal->account_id));

            return $terminal;
        }
        else {
            return [];
        }
    }

    private function getMpTerminal($terminalName)
    {
        $bkTable = "mp_terminals_bk";
        $mpTerminal = DB::connection('mysql')->select(sprintf("
        SELECT * FROM %s
        WHERE terminal_name = '%s' LIMIT 1
        ", $bkTable, $terminalName));
        if (is_array($mpTerminal) && count($mpTerminal) > 0) {
            return $mpTerminal[0];
        }
        return null;
    }

    private function getBusinessProduct($businessProductId)
    {
        $businessProduct = QrBusinessProduct::where('id', $businessProductId)->first();
        if ($businessProduct) {
            return $businessProduct->type_code ?? '';
        }
        else {
            $businessProduct = QrBusinessProduct::where('type_code', $businessProductId)->first();
            return $businessProduct && $businessProduct->type_code ? $businessProduct->type_code : '';
        }
    }

    private function getTerminalContact($merchant_code, $terminal_id)
    {
        $terminalContact = QrTerminalContact::where('merchant_code', $merchant_code)
            ->where('terminal_id', $terminal_id)
            ->first();
        if ($terminalContact) {
            return [
                'terminal_contact_email' => $terminalContact->email ?? '',
                'terminal_contact_name' => $terminalContact->fullname ?? '',
                'terminal_contact_phone' => $terminalContact->phone ?? '',
            ];
        }
        else {
            return [];
        }
    }

    private function getBankInfo($accountId)
    {
        if (!$accountId) {
            return [];
        }
        $enjoyment = QrEnjoymentAccount::where('id', $accountId)
            ->first();
        if (!$enjoyment) {
            return [];
        }
        else {
            return [
                'bank_code' => $enjoyment->bank_code,
                'bank_name' => $enjoyment->bank_name,
                'bank_branch' => $enjoyment->branch,
                'bank_account' => $enjoyment->fullname ?? '',
                'bank_number' => $enjoyment->account_number,
            ];
        }
    }

    private function mappingStatus($mmsStatus)
    {
        $mapping = [
            -1 => MpTerminal::STATUS_LOCK,
            1 => MpTerminal::STATUS_REVIEW_APPROVED,
            2 => MpTerminal::STATUS_INIT,
            3 => MpTerminal::STATUS_SALE_ADMIN_REQUESTED,
            4 => MpTerminal::STATUS_SALE_APPROVED,
            5 => MpTerminal::STATUS_SALE_DENIED,
            6 => MpTerminal::STATUS_REVIEW_DENIED
        ];
        if (in_array($mmsStatus, array_keys($mapping))) {
            return $mapping[$mmsStatus];
        }
        return 100;
    }

    private function getCreatedBy($createUser, $merchantCode)
    {
        $user = User::where('email', 'LIKE', $createUser . '%')
            ->first();
        return $user->id ?? $this->getCreatedByFromMerchant($merchantCode);
    }

    private function getCreatedByFromMerchant($merchantCode) {
        $storeInfo = Store::where('merchant_code', $merchantCode)
            ->first();

        return (is_object($storeInfo) && sizeof($storeInfo) > 0) ? $storeInfo->created_by : null;
    }

    private function writeLog($terminalId, $terminalName, $merchantCode, $dataSync)
    {
        Log::channel('sync_mms_terminal')->info(sprintf("Sync: terminal_id: %s - terminal_name: %s - merchant_code: %s -
        data sync: %s", $terminalId, $terminalName, $merchantCode, $dataSync));
    }

    private function insertTerminalHistory($mpTerminal)
    {
        $data = [
            'mp_terminal_id' => $mpTerminal['id'],
            'terminal_id' => $mpTerminal['terminal_id'],
            'staff_id' => 1,
            'type' => TerminalHistory::TYPE_MMS_SYNC,
            'reason' => 'Đồng bộ dữ liệu MMS về',
        ];

        try {
            TerminalHistory::create($data);
        }
        catch (\Exception $e) {

        }
    }
}
