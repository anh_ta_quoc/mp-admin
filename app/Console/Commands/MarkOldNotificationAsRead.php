<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Psr7\str;

class MarkOldNotificationAsRead extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:markOldNotificationAsRead';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark all old notifications before today as read';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);

        $today = new \DateTime();
        $today->setTime(0,0);

        $queryMarkOldNotificationAsRead = sprintf("
        UPDATE notification SET `read` = 1
        WHERE `read` = 0 AND created_at < '%s' AND process_lock = ''
        ", date_format($today, 'Y-m-d H:i:s'));

        try {
            DB::update(DB::raw($queryMarkOldNotificationAsRead));
            echo("done\n");
        }
        catch (\Exception $e) {
            echo(sprintf("update failed because %s", str($e->getMessage())));
        }
    }
}
