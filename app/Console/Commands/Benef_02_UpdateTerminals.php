<?php

namespace App\Console\Commands;

use App\Http\Requests\CreateTerminalRequest;
use App\Models\Sync\QrAccountVnmart;
use App\MpBenefRequest;
use App\MpTerminal;
use App\MpTerminalBatch;
use App\Rules\BankNumberRule;
use App\Rules\MerchantContactNameRule;
use App\Rules\MobileRule;
use App\Rules\TerminalAddressRule;
use App\Rules\TerminalIdExistedRule;
use App\Rules\TerminalIdRule;
use App\Rules\TerminalNameExistedRule;
use App\Rules\TerminalNameRule;
use App\TerminalHistory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\IOFactory as IOFactoryAlias;

class Benef_02_UpdateTerminals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:Benef_02_UpdateTerminals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    const MAX_RETRY = 3;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $log_msg = "---\n";
        $process_lock = $this->lock_process();
        if (!$process_lock) {
            $log_msg .= "Failed to lock process\n";
        } else {
            $req = MpBenefRequest::where('process_lock', $process_lock)->first();

            if ($req) {
                $log_msg .= "Processing process_lock:" . $process_lock . "\n";
                $log_msg .= "Request ID:" . $req->id . "\n";
                $process_error = $this->process($req);

                if ($process_error) {
                    $log_msg .= $process_error . "\n";
                }
                $log_msg .= "Update terminals done\n";
            } else {
                $log_msg = "No record to process\n";
            }
        }
        print ($log_msg);
        Log::channel('terminal_batch_precheck_logs')->info($log_msg);
    }

    private function lock_process()
    {
        $process_lock = uniqid();
        try {
            $query = sprintf("
                update mp_benef_request set process_lock = '%s'
                where (status = %s) and (process_lock = '' or process_lock is null) and retry_count < %s
                order by updated_at asc limit 1",
                $process_lock,
                MpBenefRequest::STATUS_APPROVED,
                self::MAX_RETRY
            );
            DB::update(DB::raw($query));
            return $process_lock;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function unlock_process($req, $process_msg, $status = null, $increase_retry_count = false)
    {
        $req->process_lock = null;
        $req->process_msg = $process_msg;

        if ($increase_retry_count) {
            $req->retry_count += 1;
        }

        if ($status != null) {
            $req->status = $status;
            $req->retry_count = 0;
        }

        if ($req->retry_count >= self::MAX_RETRY){
            $req->status = MpBenefRequest::STATUS_UPDATE_FAILED;
            $req->retry_count = 0;
        }
        $req->save();
    }

    private function process($req)
    {
        try {
            $acc_nos = [];
            $approve_data = json_decode($req->update_payloads);
            if ($approve_data){
                foreach ($approve_data->createBenef as $ter){
                    if ($ter->status == 0){
                        $acc_nos[] = $ter->acc_no;
                    }
                }
                if (count($acc_nos)){
                    $arr_terminals = QrAccountVnmart::where('merchant_code', $req->merchant_code)
                        ->whereIn('account_vnmart', $acc_nos)
                        ->pluck('terminal_id')->toArray();
                    if ($arr_terminals) {
                        MpTerminal::where('merchant_code', $req->merchant_code)->whereIn('terminal_id', $arr_terminals)
                            ->update([
                                'bank_code' => $req->benef_bank_code,
                                'bank_branch' => $req->benef_bank_branch,
                                'bank_number' => $req->benef_number,
                                'bank_account' => $req->benef_name
                            ]);
                        foreach ($arr_terminals as $ter){
                            $terminal = MpTerminal::where('merchant_code', $req->merchant_code)->where('terminal_id', $ter)->first();
                            $insertData = [
                                'mp_terminal_id' => $terminal->id,
                                'terminal_id' => $terminal->terminal_id,
                                'staff_id' => 1,
                                'type' => TerminalHistory::TYPE_UPDATE_INFO,
                                'reason' => 'Thay đổi thông tin thụ hưởng theo request: ' . $req->id,
                                'old_data' => "",
                                'new_data' => "",
                            ];
                            TerminalHistory::create($insertData);
                        }

                        $this->unlock_process($req, '', MpBenefRequest::STATUS_UPDATED, false);
                        return '';
                    }
                    else {
                        $this->unlock_process($req, 'No terminal founded', null, true);
                        return 'No terminal founded';
                    }
                }
                else {
                    $this->unlock_process($req, 'No acc_no founded', null, true);
                    return 'No acc_no founded';
                }
            }
            else {
                $this->unlock_process($req, 'No approve data founded', null, true);
                return 'No approve data founded';
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $this->unlock_process($req, $message, null, true);
            return $message;
        }
    }

}
