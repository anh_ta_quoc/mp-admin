<?php

namespace App\Console\Commands;

use App\Http\Requests\CreateTerminalRequest;
use App\Libraries\MmsService\MmsClient;
use App\Models\Sync\QrAccountVnmart;
use App\MpBenefRequest;
use App\MpTerminalBatch;
use App\Rules\BankNumberRule;
use App\Rules\MerchantContactNameRule;
use App\Rules\MobileRule;
use App\Rules\TerminalAddressRule;
use App\Rules\TerminalIdExistedRule;
use App\Rules\TerminalIdRule;
use App\Rules\TerminalNameExistedRule;
use App\Rules\TerminalNameRule;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\IOFactory as IOFactoryAlias;

class Benef_01_SendRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:Benef_01_SendRequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    const MAX_RETRY = 3;

    private $request_data = '';
    private $response = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $log_msg = "---\n";
        $process_lock = $this->lock_process();
        if (!$process_lock) {
            $log_msg .= "Failed to lock process\n";
        } else {
            $req = MpBenefRequest::where('process_lock', $process_lock)->first();

            if ($req) {
                $log_msg .= "Processing process_lock:" . $process_lock . "\n";
                $log_msg .= "Request ID:" . $req->request_id . "\n";
                $process_error = $this->process($req);

                if ($this->request_data != '') {
                    $log_msg .= "Request data:\n" . $this->request_data . "\n";
                }
                if ($this->response != '') {
                    $log_msg .= "Response data:\n" . $this->response . "\n";
                }

                if ($process_error) {
                    $log_msg .= $process_error . "\n";
                } else {
                    $log_msg .= "Send request done\n";
                }
            } else {
                $log_msg = "No record to process\n";
            }
        }
        print ($log_msg);
        Log::channel('terminal_batch_precheck_logs')->info($log_msg);
    }

    private function lock_process()
    {
        $process_lock = uniqid();
        try {
            $query = sprintf("
                update mp_benef_request set process_lock = '%s'
                where status = %s and (process_lock = '' or process_lock is null) and retry_count < %s
                order by updated_at asc limit 1",
                $process_lock,
                MpBenefRequest::STATUS_INIT,
                self::MAX_RETRY
            );
            DB::update(DB::raw($query));
            return $process_lock;
        } catch (\Exception $e) {
            print_r($e->getMessage());
            return null;
        }
    }

    private function unlock_process($req, $process_msg, $status = null, $increase_retry_count = false)
    {
        $req->process_lock = null;
        $req->process_msg = $process_msg;

        if ($increase_retry_count) {
            $req->retry_count += 1;
        }

        if ($status != null) {
            $req->status = $status;
            $req->retry_count = 0;
        }

        if ($req->retry_count >= self::MAX_RETRY) {
            $req->status = MpBenefRequest::STATUS_SEND_FAILED;
            $req->retry_count = 0;
        }
        $req->save();
    }

    private function process($req)
    {
        try {
            $params = [];
            $arr_terminals = explode(',', $req->terminals);
            if ($arr_terminals) {
                foreach ($arr_terminals as $ter) {
                    $vnmart = QrAccountVnmart::where('merchant_code', $req->merchant_code)->where('terminal_id', $ter)->first();
                    if ($vnmart) {
                        $params[] = [
                            'request_id' => $vnmart->account_vnmart . '-' . $req->id,
                            'acc_no' => $vnmart->account_vnmart,
                            'benef_bank_branch' => $req->benef_bank_branch,
                            "benef_bank_code" => $req->benef_bank_code,
                            "benef_bank_name" => $req->benef_bank_name,
                            "benef_name" => $req->benef_name,
                            "benef_number" => $req->benef_number,
                            "doc_url" => env("APP_URL") . $req->doc_url,
                            "app_id" => "MCP",
                            "benef_type" => 0,
                            "create_user" => ""
                        ];
                    }
                }
            }
            if ($params) {
                $body = [
                    'id' => $req->id,
                    'createBenef' => $params
                ];
                $this->request_data = json_encode($body, JSON_PRETTY_PRINT);
                $mms_response = $this->notifyMMS($body, 'ADD_BENEF');
                $this->response = json_encode($mms_response, JSON_PRETTY_PRINT);
                if (!($mms_response && $mms_response['success'] == true)) {
                    $req->mms_status = -1;
                    $req->mms_code = $mms_response['code'];
                    $req->save();
                    $this->unlock_process($req, 'Call MMS failed', null, true);
                    return "Call MMS failed";
                } else {
                    $this->unlock_process($req, '', MpBenefRequest::STATUS_WAIT_SENT_RESPONSE, false);
                    return null;
                }
            }
            $this->unlock_process($req, 'Call MMS failed', null, true);
            return 'No acc_no founded';
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $this->unlock_process($req, $message, null, true);
            return $message;
        }
    }

    public function notifyMMS($params, $key)
    {
        $client = new MmsClient($key);
        return $client->endpoint->endpointRequest($params);
    }
}
