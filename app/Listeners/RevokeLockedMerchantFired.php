<?php

namespace App\Listeners;

use App\Events\RevokeLockedMerchant;
use App\Events\SendEmailCompleteStoreInfo;

use Illuminate\Support\Facades\Mail;

class RevokeLockedMerchantFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RevokeLockedMerchantFired  $event
     * @return void
     */
    public function handle(RevokeLockedMerchant $event)
    {
        if (config('app.env') == 'production') {
            $apiUrl = 'https://api-partner.vnpay.vn/api/merchant/revoke';
        }
        else {
            $apiUrl = 'https://api-partner.vnpaytest.vn/api/merchant/revoke';
        }

        $jsonSend = json_encode([
            'merchant_code' => $event->merchantCode
        ], true);
        try {
            $client = new \GuzzleHttp\Client([
                'headers' => ['Content-Type' => 'application/json',
                    'accept-language' => 'en-US,en;q=0.8']
            ]);
            $client->post($apiUrl, ['body' => $jsonSend]);
        }
        catch (\Exception $e) {

        }
    }
}
