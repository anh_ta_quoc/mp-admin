<?php

namespace App\Listeners;

use App\Events\SendEmailHandleMpTerminal;
use App\Libraries\BaseFunction;
use App\Mail\ReopenContractStoreInfo;
use App\Mail\SaleAdminUploadDocument;
use App\Mail\SaleApproveStoreInfo;
use App\Mail\Terminal\ControlRejectTerminal;
use App\Mail\Terminal\ControlRequestUpdateInfo;
use App\Mail\Terminal\RequestSaleApprove;
use App\Mail\Terminal\SaleApproveTerminal;
use App\Mail\Terminal\SaleRejectTerminal;
use App\Mail\Terminal\SaleRequestUpdateInfo;
use App\MpTerminal;
use App\Store;
use Illuminate\Log\Logger;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailHandleTerminalFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param SendEmailHandleMpTerminal $event
     */
    public function handle(SendEmailHandleMpTerminal $event)
    {
        $mailInfo = config('mail');
        $terminal = MpTerminal::find($event->mpTerminalId);
        $merchant = BaseFunction::getMerchantByCode($terminal->merchant_code);
        $to_email = ['tuan.dv@teko.vn']; // default email
        switch ($event->eventName) {
            case 'request-sale-approve':
                $mailContent = new RequestSaleApprove(
                    $mailInfo['username'],
                    $mailInfo['from']['name'],
                    sprintf('%s - %s yêu cầu bổ sung đăng ký mới', $merchant['merchant_code'], $merchant['merchant_name']),
                    $terminal->terminal_id,
                    $terminal->terminal_name,
                    $merchant->merchant_name,
                    $merchant->merchant_code,
                    sprintf('%s/terminals/%s', config('app.url'), $event->mpTerminalId)
                );
                $to_email = explode(',', $mailInfo['business_email']);
                break;
            case 'sale-request-update-info':
                $mailContent = new SaleRequestUpdateInfo(
                    $mailInfo['username'],
                    $mailInfo['from']['name'],
                    sprintf('KD mở lại hồ sơ %s - %s', $terminal->terminal_id, $terminal->terminal_name),
                    $terminal->terminal_id,
                    $terminal->terminal_name,
                    $merchant->merchant_name,
                    $merchant->merchant_code,
                    $event->reason,
                    sprintf('%s/terminals/%s', config('app.url'), $event->mpTerminalId)
                );
                $to_email = explode(',', $mailInfo['sale_admin_email']);
                break;
            case 'sale-approve-terminal':
                $mailContent = new SaleApproveTerminal(
                    $mailInfo['username'],
                    $mailInfo['from']['name'],
                    sprintf('%s - %s yêu cầu bổ sung đăng ký mới', $merchant['merchant_code'], $merchant['merchant_name']),
                    $terminal->terminal_id,
                    $terminal->terminal_name,
                    $merchant->merchant_name,
                    $merchant->merchant_code,
                    sprintf('%s/terminals/%s', config('app.url'), $event->mpTerminalId)
                );
                $to_email = explode(',', $mailInfo['control_email']);
                break;
            case 'sale-reject-terminal':
                $mailContent = new SaleRejectTerminal(
                    $mailInfo['username'],
                    $mailInfo['from']['name'],
                    sprintf('KD từ chối duyệt hồ sơ %s - %s', $terminal->terminal_id, $terminal->terminal_name),
                    $terminal->terminal_id,
                    $terminal->terminal_name,
                    $merchant->merchant_name,
                    $merchant->merchant_code,
                    $event->reason,
                    sprintf('%s/terminals/%s', config('app.url'), $event->mpTerminalId)
                );
                $to_email = explode(',', $mailInfo['sale_admin_email']);
                break;
            case 'control-request-update-info':
                $mailContent = new ControlRequestUpdateInfo(
                    $mailInfo['username'],
                    $mailInfo['from']['name'],
                    sprintf('ĐS mở lại hồ sơ %s - %s', $terminal->terminal_id, $terminal->terminal_name),
                    $terminal->terminal_id,
                    $terminal->terminal_name,
                    $merchant->merchant_name,
                    $merchant->merchant_code,
                    $event->reason,
                    sprintf('%s/terminals/%s', config('app.url'), $event->mpTerminalId)
                );
                $to_email = explode(',', $mailInfo['sale_admin_email']);
                break;
            case 'control-reject-terminal':
                $mailContent = new ControlRejectTerminal(
                    $mailInfo['username'],
                    $mailInfo['from']['name'],
                    sprintf('ĐS từ chối hồ sơ %s - %s', $terminal->terminal_id, $terminal->terminal_name),
                    $terminal->terminal_id,
                    $terminal->terminal_name,
                    $merchant->merchant_name,
                    $merchant->merchant_code,
                    $event->reason,
                    sprintf('%s/terminals/%s', config('app.url'), $event->mpTerminalId)
                );
                $to_email = explode(',', $mailInfo['sale_admin_email']);
                break;
            default:
                break;
        }

        Mail::to($to_email)->cc(['tuan.dv@teko.vn','anh.tq@teko.vn'])->send($mailContent);
    }
}
