<?php

namespace App\Listeners;

use App\Events\SendEmailCompleteStoreInfo;
use App\Mail\ReopenContractStoreInfo;
use App\Mail\SaleAdminUploadDocument;
use App\Mail\SaleApproveStoreInfo;
use App\Store;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailCompleteStoreInfoFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmailCompleteStoreInfo  $event
     * @return void
     */
    public function handle(SendEmailCompleteStoreInfo $event)
    {
        $mailInfo = config('mail');
        $storeInfo = Store::find($event->storeInfoId);
        $to_email = ['tuan.dv@teko.vn']; // default email
        switch ($event->eventCompleteName) {
            case 'reopen-contract':
                $mailContent = new ReopenContractStoreInfo(
                    $mailInfo['username'],
                    $mailInfo['from']['name'],
                    'Hồ sơ Merchant ' . $storeInfo->merchant_brand . ' đã được mở lại để upload hợp đồng.',
                    sprintf('%s/stores/%s', config('app.url'), $event->storeInfoId),
                    $storeInfo->merchant_brand
                );
                $to_email = explode(',', $mailInfo['sale_admin_email']);
//                    'thanh.tt@teko.vn';
                break;
            case 'sale-approve':
                $mailContent = new SaleApproveStoreInfo(
                    $mailInfo['username'],
                    $mailInfo['from']['name'],
                    'Hồ sơ Merchant ' . $storeInfo->merchant_brand . ' đang chờ duyệt!',
                    sprintf('%s/stores/%s', config('app.url'), $event->storeInfoId),
                    $storeInfo->merchant_brand
                );
                $to_email = explode(',', $mailInfo['control_email']);
                    #'qcteam300@gmail.com';
                break;
            case 'sale-admin-upload-document':
                $mailContent = new SaleAdminUploadDocument(
                    $mailInfo['username'],
                    $mailInfo['from']['name'],
                    'Hồ sơ Merchant ' . $storeInfo->merchant_brand . ' đang chờ duyệt!',
                    sprintf('%s/stores/%s', config('app.url'), $event->storeInfoId),
                    $storeInfo->merchant_brand
                );
                $to_email = explode(',', $mailInfo['business_email']);
//                    'qcteam100@gmail.com';
                break;
            default:
                break;
        }

        Mail::to($to_email)->cc(['tuan.dv@teko.vn','anh.tq@teko.vn'])->send($mailContent);
    }
}
