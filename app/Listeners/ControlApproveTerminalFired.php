<?php

namespace App\Listeners;

use App\Events\ControlApproveTerminal;

use App\MpTerminal;
use App\MpTerminalBatch;
use Illuminate\Support\Facades\Mail;

class ControlApproveTerminalFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ControlApproveTerminal  $event
     * @return void
     */
    public function handle(ControlApproveTerminal $event)
    {
        try {
            $totalErrorTerminals = MpTerminal::where('src', $event->mpTerminalSrc)
                ->where('mms_status', MpTerminal::MMS_STATUS_FAILED)
                ->count();
            if ($totalErrorTerminals == 0) {
                $batchInfo = explode('-', $event->mpTerminalSrc);
                MpTerminalBatch::where('id', $batchInfo[1])
                    ->update(['status' => MpTerminalBatch::STATUS_DONE]);
            }
        }
        catch (\Exception $e) {

        }
    }
}
