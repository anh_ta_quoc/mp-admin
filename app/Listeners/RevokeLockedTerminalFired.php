<?php

namespace App\Listeners;

use App\Events\RevokeLockedTerminal;
use App\Events\SendEmailCompleteStoreInfo;

use Illuminate\Support\Facades\Mail;

class RevokeLockedTerminalFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param RevokeLockedTerminal $event
     * @return void
     */
    public function handle(RevokeLockedTerminal $event)
    {
        if (config('app.env') == 'production') {
            $apiUrl = 'https://api-partner.vnpay.vn/api/terminal/revoke';
        }
        else {
            $apiUrl = 'https://api-partner.vnpaytest.vn/api/terminal/revoke';
        }

        $jsonSend = json_encode([
            'merchant_code' => $event->merchantCode,
            'terminal_id' => $event->terminalId,
        ], true);
        try {
            $client = new \GuzzleHttp\Client([
                'headers' => ['Content-Type' => 'application/json',
                    'accept-language' => 'en-US,en;q=0.8']
            ]);
            $client->post($apiUrl, ['body' => $jsonSend]);
        }
        catch (\Exception $e) {

        }
    }
}
