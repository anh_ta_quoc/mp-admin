<?php

namespace App\Libraries;

    class MpApiService {
        private $apiEndpoint = null;
        private $apiPath = null;
        private $apiMethod = null;
        private $accessToken = null;
        private $body = null;
        private $client = null;


        public function __construct($apiPath, $apiMethod, $body = null, $accessToken=null)
        {
            if (config('app.env') == 'production') {
                $this->apiEndpoint = 'https://api-partner.vnpay.vn/api/';
            }
            else {
                $this->apiEndpoint = 'https://api-partner.vnpaytest.vn/api/';
            }

            $this->apiPath = $apiPath;
            $this->apiMethod = $apiMethod;
            $this->accessToken = $accessToken;
            $this->body = $body;
            $this->client = $this->buildClient($accessToken);
        }

        private function buildClient($accessToken)
        {
            if ($accessToken) {
                return new \GuzzleHttp\Client([
                    'headers' => ['Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $accessToken,
                        'accept-language' => 'en-US,en;q=0.8']
                ]);
            }
            else {
                return new \GuzzleHttp\Client([
                    'headers' => ['Content-Type' => 'application/json',
                        'accept-language' => 'en-US,en;q=0.8']
                ]);
            }
        }

        public function executeApi()
        {
            $response = null;
            switch (strtoupper($this->apiMethod)) {
                case 'GET':
                    $response = $this->client->get($this->apiEndpoint . $this->apiPath);
                    break;
                case 'POST':
                    $response = $this->client->post($this->apiEndpoint . $this->apiPath, $this->body);
                    break;
                default:
                    break;
            }

            return $response;
        }
    }
?>
