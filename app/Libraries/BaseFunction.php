<?php
/**
 * Created by PhpStorm.
 * User: anhtq
 * Date: 12/24/2019
 * Time: 11:32 AM
 */

namespace App\Libraries;


use App\Models\Sync\QrMerchant;
use App\Models\Sync\QrTerminal;
use App\MpTerminal;
use App\User;

class BaseFunction
{
    public const STATUS_DRAFT = -10;
    public const STATUS_SALE_REOPEN_TO_EDIT = -7;
    public const STATUS_CONTROL_REOPEN_TO_EDIT = -5;
    public const STATUS_INIT = -1;
    public const STATUS_REPOPEN = -2;
    public const STATUS_WAIT_FOR_ACTION = 0;
    // approve status
    public const STATUS_SALE_ACCEPTED = 1;
    public const STATUS_CONTROL_ACCEPTED = 2;
    // reject status
    public const STATUS_SALE_REJECTED = 3;
    public const STATUS_CONTROL_REJECTED = 4;
    // reopen contract
    public const STATUS_REOPEN_CONTRACT = 5;
    // need update contract
    public const STATUS_NEED_UPDATE_CONTRACT = 99;
    // lock
    public const STATUS_LOCK = -20;

    public static function getStoreStatus()
    {
        return [
            self::STATUS_INIT => 'Chờ hoàn thiện HS',
            self::STATUS_REPOPEN=>'Mở lại HS',
            self::STATUS_WAIT_FOR_ACTION => 'Chờ KD duyệt',
            self::STATUS_SALE_ACCEPTED => 'Chờ ĐS duyệt',
            self::STATUS_CONTROL_ACCEPTED => 'Hoạt động',
            self::STATUS_SALE_REJECTED => 'KD từ chối',
            self::STATUS_CONTROL_REJECTED => 'ĐS từ chối',
            self::STATUS_REOPEN_CONTRACT => 'Chờ cập nhật HĐ',
            self::STATUS_NEED_UPDATE_CONTRACT => 'Chờ cập nhật HĐ',
            self::STATUS_SALE_REOPEN_TO_EDIT => 'KD mở lại hồ sơ',
            self::STATUS_CONTROL_REOPEN_TO_EDIT => 'ĐS mở lại hồ sơ',
            self::STATUS_DRAFT => 'Khởi tạo',
            self::STATUS_LOCK => 'Đóng'
        ];
    }

    public static function getStoreStatusClass() {
        return [
            self::STATUS_INIT => 'label label-primary',
            self::STATUS_REPOPEN=>'label label-primary',
            self::STATUS_WAIT_FOR_ACTION => 'label label-warning',
            self::STATUS_SALE_ACCEPTED => 'label label-info',
            self::STATUS_CONTROL_ACCEPTED => 'label label-success',
            self::STATUS_SALE_REJECTED => 'label label-danger',
            self::STATUS_CONTROL_REJECTED => 'label label-danger',
            self::STATUS_REOPEN_CONTRACT => 'label label-primary',
            self::STATUS_NEED_UPDATE_CONTRACT => 'label label-primary',
            self::STATUS_SALE_REOPEN_TO_EDIT => 'label label-primary',
            self::STATUS_CONTROL_REOPEN_TO_EDIT => 'label label-primary',
            self::STATUS_DRAFT => 'label label-primary',
            self::STATUS_LOCK => 'label label-inverse label-lock',
        ];
    }

    public const transitStatus = [
        self::STATUS_DRAFT . '->' . self::STATUS_WAIT_FOR_ACTION => 'Sale Admin gửi duyệt',
        self::STATUS_CONTROL_REOPEN_TO_EDIT . '->' . self::STATUS_WAIT_FOR_ACTION => 'Sale Admin gửi duyệt',
        self::STATUS_SALE_REOPEN_TO_EDIT . '->' . self::STATUS_WAIT_FOR_ACTION => 'Sale Admin gửi duyệt',
        self::STATUS_INIT . '->' . self::STATUS_WAIT_FOR_ACTION => 'Chờ duyệt',
        self::STATUS_WAIT_FOR_ACTION . '->' . self::STATUS_SALE_ACCEPTED => 'Kinh doanh duyệt',
        self::STATUS_WAIT_FOR_ACTION . '->' . self::STATUS_SALE_REJECTED => 'Kinh doanh từ chối',
        self::STATUS_SALE_ACCEPTED . '->' . self::STATUS_CONTROL_ACCEPTED => 'Đối soát duyệt',
        self::STATUS_SALE_ACCEPTED . '->' . self::STATUS_CONTROL_REJECTED => 'Đối soát từ chối',
        self::STATUS_SALE_REJECTED . '->' . self::STATUS_REPOPEN => 'Mở cập nhật hồ sơ',
        self::STATUS_CONTROL_REJECTED . '->' . self::STATUS_REPOPEN => 'Mở cập nhật hồ sơ',
        self::STATUS_WAIT_FOR_ACTION . '->' . self::STATUS_REOPEN_CONTRACT => 'Mở cập nhật hợp đồng',
        self::STATUS_CONTROL_ACCEPTED . '->' . self::STATUS_SALE_ACCEPTED => 'Gửi MMS bị lỗi',
        self::STATUS_SALE_ACCEPTED . '->' . self::STATUS_CONTROL_REOPEN_TO_EDIT => 'Đối soát trả lại hồ sơ',
    ];

    public const ignoreOperatorLog = [
        self::STATUS_SALE_ACCEPTED,
        self::STATUS_CONTROL_ACCEPTED
    ];

    public static function transitStatusMessage($fromStatus, $toStatus) {
        return array_key_exists($fromStatus . '->' . $toStatus, self::transitStatus) ?
            self::transitStatus[$fromStatus . '->' . $toStatus] : 'Không xác định';
    }

    public const STATUS_MMS_WAIT_FOR_CREATE = 0;
    public const STATUS_MMS_CREATED = 1;
    public const STATUS_MMS_CREAT_FAILED = -1;
    public const STATUS_MMS_APPROVED = 2;
    public const STATUS_MMS_APPROVED_FAILED = 3;
    public static function getStoreMMSStatus()
    {
        return [
            self::STATUS_MMS_WAIT_FOR_CREATE => 'Chờ tạo trên MMS',
            self::STATUS_MMS_CREATED => 'Đã tạo trên MMS',
            self::STATUS_MMS_CREAT_FAILED => 'Tạo trên MMS lỗi',
            self::STATUS_MMS_APPROVED => 'Đã duyệt trên MMS',
            self::STATUS_MMS_APPROVED_FAILED => 'Duyệt lỗi trên MMS',
        ];
    }

    // merchant type business
    public const MERCHANT_TYPE_BUSINESS_COMPANY = '00';
    public const MERCHANT_TYPE_BUSINESS_HOUSEHOLD = '01';
    public const MERCHANT_TYPE_BUSINESS_PERSONAL = '02';
    public static function getMerchantTypeBusiness()
    {
        return [
            self::MERCHANT_TYPE_BUSINESS_COMPANY => 'Công ty',
            self::MERCHANT_TYPE_BUSINESS_HOUSEHOLD => 'Hộ kinh doanh',
            self::MERCHANT_TYPE_BUSINESS_PERSONAL => 'Cá nhân',
        ];
    }

    public static function log($msg)
    {
        if (is_array($msg)) {
            $message = json_encode($msg, true);
        } else {
            $message = $msg;
        }
        @file_put_contents(storage_path('logs/debug.log'), $message . "\n", FILE_APPEND);
    }

    /**
     * Post json data to API
     *
     * @param $apiUrl
     * @param $content
     * @param $mms_token
     * @return bool
     */
    public static function sendToApi($apiUrl, $content, $mms_token)
    {
        $jsonSend = json_encode($content, true);
        $merchantCode = $content['merchant']['merchantCode'];
        try {
            $client = new \GuzzleHttp\Client([
                'headers' => ['Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $mms_token,
                    'accept-language' => 'en-US,en;q=0.8']
            ]);
            self::log("MMS Request for UPDATE: ". $jsonSend);

            $response = $client->post($apiUrl, ['body' => $jsonSend]);
            $mms_resp = json_decode($response->getBody());

            $response_code = $mms_resp->code;
            self::log("MMS Response for UPDATE  : ".(string) $response->getBody()." with merchantCode=".$merchantCode);

        } catch (\Exception $e) {
            $response_code = $e->getCode();
            self::log("MMS Response for UPDATE : ".$e->getMessage()." with merchantCode=".$merchantCode);

        }

        if ($response_code === '00') {
            return true;
        }
        else {
            return false;
        }
    }

    public static function sendUpdateToMMS($store)
    {
        if (in_array($store->mms_status, [self::STATUS_MMS_CREATED, self::STATUS_MMS_APPROVED_FAILED]) &&
            $store->status == self::STATUS_CONTROL_ACCEPTED &&
            $store->merchant_file_contract ) {
            $content = [
               'document' => [
                   'businessCert' => $store->merchant_file_business_cert,
                   'businessTaxCert'=>$store->merchant_file_business_tax_cert,
                   'domainCert'=>$store->merchant_file_domain_cert,
                   'identifyCard'=>$store->merchant_file_identify_card,
                   'contract' => $store->merchant_file_contract,
                   'profileStatus' => 1
               ],
               'merchant' => [
                   'merchantCode' => $store->merchant_code
               ],
               'user' => [
                   'email' => $store->merchant_contact_email,
                   'userName' => 'mcportal'
               ]
            ];

            $apiUrl = config('app.MMS_API')."/merchant/update-old-ver";
            $mms_token = config('app.MMS_BASIC_TOKEN');
            return self::sendToApi($apiUrl, $content, $mms_token);
        }
        return false;

    }
    public static function convertPhone($phone){

        if (substr($phone, 0, 1) != '0') {
            $phone = '0'.$phone;
        }
        return $phone;
    }

    public static function sendCreateToMMS($id)
    {
        $content = \App\Store::find($id);
        /*
         * Prepare Data to call
         */
        $data['document']['profileStatus'] = 1;
        $data['document']['businessCert'] = $content->merchant_file_business_cert;
        $data['document']['businessTaxCert'] = $content->merchant_file_business_tax_cert;
        $data['document']['domainCert'] = $content->merchant_file_domain_cert;
        $data['document']['identityCard'] = $content->merchant_file_identify_card;
        $data['document']['contract'] = $content->merchant_file_contract;

        $data['merchant']['staff'] = $content->staff_id ?? 21;
        $data['merchant']['department'] = $content->department_id ?? 21;

        $data['merchant']['district'] = $content->merchant_district_code;
        $data['merchant']['id'] = 0;
        $data['merchant']['masterMerchant'] = 'A000000775';
        $data['merchant']['mcTypeEnterprise'] = $content->merchant_type_business;
        $data['merchant']['merchantAddress'] = $content->merchant_address;
        $data['merchant']['merchantBrand'] = $content->merchant_brand;
        $data['merchant']['merchantBusinessAddress'] = $content->terminal_address;
        $data['merchant']['merchantCode'] = $content->merchant_code;
        $data['merchant']['merchantName'] = $content->merchant_name;
        $data['merchant']['merchantType'] = $content->merchant_type;
        $data['merchant']['province'] = $content->merchant_province_code;
        $data['merchant']['rmAuth'] = 1;
        $data['merchant']['wards'] = $content->merchant_ward_code;

        $data['merchantContact']['email'] = $content->merchant_contact_email;
        $data['merchantContact']['phone'] = self::convertPhone($content->merchant_contact_phone);
        $data['merchantContact']['name'] = $content->merchant_contact_name;

        $data['midAccInfo']['appUser'] = $content->merchant_app_user ?
            self::convertPhone($content->merchant_app_user) : self::convertPhone($content->merchant_contact_phone);
        $data['midAccInfo']['registerOTT'] = 2;
        $data['midAccInfo']['registerSMS'] = 2;
        $data['midAccInfo']['toCreateUser'] = 1;
        $data['midAccInfo']['toMerchant'] = 1;
        $data['midAccInfo']['receiveEmail'] = $content->merchant_contact_email;
        $data['midAccInfo']['isssueInvoice'] = $content->issue_invoice ? 1 : 2;

        $data['terminal']['accountHolder'] = $content->bank_account;
        $data['terminal']['accountNumber'] = $content->bank_number;
        $data['terminal']['bankBranch'] = $content->bank_branch;
        $data['terminal']['bankCode'] = $content->bank_code;
        $data['terminal']['businessProduct'] = $content->terminal_type;
        $data['terminal']['currency'] = $content->currency;
        $data['terminal']['district'] = $content->terminal_district_code;
        $data['terminal']['mccVNPAY'] = $content->terminal_mcc ?? "630000";
        $data['terminal']['province'] = $content->terminal_province_code;
        $data['terminal']['registerQrcode'] = 1;
        $data['terminal']['registerVnpayment'] = 2;
        $data['terminal']['terminalID'] = $content->terminal_id;
        $data['terminal']['terminalName'] = $content->terminal_name;
        $data['terminal']['wards'] = $content->terminal_ward_code;
        $data['terminal']['terminalBusinessAddress'] = $content->terminal_address;
        $data['terminal']['registerVnpayment'] = $content->terminal_register_vnpayment ? 1 : 2;
        $data['terminal']['websiteBusiness'] = $content->terminal_website ?? '';
        $data['terminal']['walletType'] = "1";

        $data['terminalContact']['email'] = $content->terminal_contact_email;
        $data['terminalContact']['name'] = $content->terminal_contact_name;
        $data['terminalContact']['phone'] = self::convertPhone($content->terminal_contact_phone);

        $data['tidAccInfo']['appUser'] = $content->terminal_app_user ?
            self::convertPhone($content->terminal_app_user) : self::convertPhone($content->terminal_contact_phone);
        $data['tidAccInfo']['createTerminalApp'] = $content->terminal_app_user ? 1 : 2;
        $data['tidAccInfo']['registerOTT'] = 2;
        $data['tidAccInfo']['registerSMS'] = 2;
        $data['tidAccInfo']['toCreateUser'] = 2;
        $data['tidAccInfo']['toTerminal'] = 2;

        $data['user']['email'] = $content->merchant_contact_email;
        $data['user']['userName'] = 'mcportal';

        $response_code = "999";
        try {
            $client = new \GuzzleHttp\Client([
                'headers' => ['Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . config('app.MMS_BASIC_TOKEN'),
                    'accept-language' => 'en-US,en;q=0.8']
            ]);
            self::log("----------------------");
            self::log("MMS Request for ADD: ". json_encode($data));
            self::log(sprintf("Request at -- %s --", now()));

            $apiUrl = config('app.MMS_API')."/merchant/create-merchant";

            $response = $client->post($apiUrl, ['body' => json_encode($data)]);
            $mms_resp = json_decode($response->getBody());

            $response_code = $mms_resp->code;
            self::log("MMS Response for ADD  : ".(string) $response->getBody()." with merchantCode=".$content->merchant_code);

        } catch (\Exception $e) {
            $response_code = $e->getCode();
            self::log("MMS Response for ADD : ".$e->getMessage()." with merchantCode=".$content->merchant_code);

        }
        self::log("Response_code:" .$response_code);
        return $response_code;

    }

    public static function getMerchantByCode($merchantCode)
    {
        $merchant = QrMerchant::where('merchant_code', $merchantCode)->first();
        return $merchant ?? null;
    }

    public static function checkMerchantCodeExistCombine($code, $id = null) {
        $existed = QrMerchant::where('merchant_code', '=', trim($code))
            ->whereIn('master_merchant_code', ['VNPAY', '908405', 'A000000775']);
        if (!$existed->first()) {
            $existed = MpTerminal::where('terminal_code', '=', trim($code));
            if ($id) {
                $existed = $existed->where('id', '!=', $id);
            }
            if (!$existed->first()) {
                $existed = \App\Store::where('merchant_code', '=', trim($code));
            }
        }

        return $existed->first();
    }

    public static function checkTerminalNameExistCombine($value, $id = null, $merchantCode = null) {
        $existed = MpTerminal::whereRaw(
            sprintf("UPPER(REPLACE(terminal_name, ' ', '')) = UPPER(REPLACE('%s', ' ', ''))", $value)
        );
        if ($id) {
            $existed = $existed->where('id', '!=', $id);
        }
        if (!$existed->first()) {
            $existed = \App\Store::where('terminal_name', '=', trim($value));
            if ($merchantCode) {
                $existed = $existed->where('merchant_code', '!=' , $merchantCode);
            }
        }

        return $existed->first();
    }

    public static function checkTerminalIdExistCombine($value, $merchant_code, $id = null) {
        $merchant = QrMerchant::where('merchant_code', $merchant_code)->first();
        if ($merchant){
            $existed = QrTerminal::where('terminal_id', '=', trim($value))->where('merchant_id', $merchant->id);
            if (!$existed->first()) {
                $existed = MpTerminal::where('terminal_id', '=', trim($value))->where('merchant_code', $merchant_code);
                if ($id) {
                    $existed = $existed->where('id', '!=', $id);
                }
                if (!$existed->first()) {
                    $existed = \App\Store::where('terminal_id', '=', trim($value))->where('merchant_code', $merchant_code);
                }
            }

            return $existed->first();
        }
        return null;
    }

    public static function getUserNameById($userId)
    {
        $defaultUserName = 'mcportal';
        if (!$userId) {
            return $defaultUserName;
        }
        $user = User::where('id', $userId)->first();
        if (!$user) {
            return $defaultUserName;
        }
        else {
            $emailInfo = explode('@', $user['email']);
            return $emailInfo[0];
        }
    }

    public const PAGE_HEADER = [
        'stores' => [
            'stores.index' => 'Danh sách Merchant',
            'stores.activeList' => 'Danh sách đã hoạt động',
            'stores.registerList' => 'Danh sách gửi đăng ký',
            'stores.needApproveList' => 'Danh sách chờ duyệt',
            'stores.deniedList' => 'Danh sách bị từ chối',
            'stores.lockedList' => 'Danh sách đóng',
        ]
    ];

    public const MMS_CODE_MAPPING = [
        '02' => 'Tạo VNMART thất bại',
        '11' => 'Trùng mã merchant',
        '12' => 'Trùng tên merchant',
        '13' => 'Trùng địa chỉ merchant',
        '14' => 'Trùng tên viết tắt merchant',
        '15' => 'Trùng tên terminal',
        '16' => 'Trùng số điện thoại merchant',
        '17' => 'Trùng số điện thoại terminal',
        '96' => 'Hệ thống đang bảo trì',
    ];
}
