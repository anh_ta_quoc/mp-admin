<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 10:38 AM
 */

namespace App\Libraries\MmsService;

use App\Libraries\MmsService\Services\Merchant\ChangeStatus;
use App\Libraries\MmsService\Services\Merchant\MerchantCreate;
use App\Libraries\MmsService\Services\Merchant\UpdateMid;
use App\Libraries\MmsService\Services\Merchant\UpdateOldVer;
use App\Libraries\MmsService\Services\Merchant\UpdateTid;
use App\Libraries\MmsService\Services\Terminal\AddBenef;
use App\Libraries\MmsService\Services\Terminal\TerminalCreate;
use App\Libraries\MmsService\Services\Terminal\ChangeStatus as TerminalChangeStatus;

class MmsClient
{
    private $config = null;
    public $endpoint = null;

    public function __construct($key)
    {
        $this->config = new Config();
        $this->endpoint = $this->getEndPoint($key);
    }

    private function getEndPoint($key)
    {
        if ($this->config != null) {
            $endpoint = $this->config->getEndpoint($key);
            return $endpoint != null ? new $endpoint : null;
        }
        return null;
    }

}

class Config
{
    public $baseUrl;
    private $endpoints;
    public $mms_token;

    public function __construct()
    {
        $this->baseUrl = config('app.MMS_API');
        $this->mms_token = config('app.MMS_BASIC_TOKEN');
        $this->endpoints = [
            'MC_CREATE' => MerchantCreate::class,
            'MC_UPDATE_OLD_VER' => UpdateOldVer::class,
            'MC_CHANGE_STATUS' => ChangeStatus::class,
            'MC_UPDATE_MID' => UpdateMid::class,

            'TER_CREATE' => TerminalCreate::class,
            'MC_UPDATE_TID' => UpdateTid::class,
            'TER_CHANGE_STATUS' => TerminalChangeStatus::class,
            'ADD_BENEF' => AddBenef::class,
        ];
    }

    public function getEndpoint($name)
    {
        if (isset($this->endpoints[$name]))
            return $this->endpoints[$name];
        return null;
    }
}
