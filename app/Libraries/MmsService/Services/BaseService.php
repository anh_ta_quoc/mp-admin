<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 2:53 PM
 */

namespace App\Libraries\MmsService\Services;

use App\Libraries\MmsService\Config;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;

class BaseService
{
    public $endpoint = '';
    public $method = 'POST';
    public $headers;
    public $params = [];

    private $client;
    private $config;

    public function __construct()
    {
        $this->config = new Config();
        $this->client = new HttpClient([
            'base_uri' => $this->config->baseUrl,
        ]);
    }

    private function buildHeaders()
    {
        $this->headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . $this->config->mms_token,
            'accept-language' => 'en-US,en;q=0.8'
        ];
    }

    public function endpointRequest($data)
    {
        $response = null;
        if ($this->endpoint) {
            try {
                $this->buildHeaders();
                if (method_exists($this, 'buildParams')){
                    $this->buildParams($data);
                }
                $log = sprintf("\n--- Request API:\nBase URL:%s\nAPI Path: %s\nHeaders: %s\nBody: %s",
                    $this->config->baseUrl,
                    $this->endpoint,
                    json_encode($this->headers),
                    json_encode($this->params, true)
                );
                $response = $this->client->request($this->method, $this->endpoint, [
                    'headers' => $this->headers,
                    'body' => json_encode($this->params, true)
                ]);

                $mms_resp = json_decode($response->getBody());
                $response_code = $mms_resp->code;

                $log .= sprintf("\n--- Response:\nStatus:%s\nContent: %s",
                    $response_code,
                    $mms_resp ? $response->getBody() : $response->getBody()->getContents()
                );
                Log::channel('mms_api')->info($log);
                if ($response_code == '00') {
                    return [
                        'success' => true,
                        'code' => $response_code,
                        'message' => 'success',
                        'response' => $mms_resp ? $mms_resp : $response->getBody()->getContents()
                    ];
                }
                else {
                    return [
                        'success' => false,
                        'code' => $response_code,
                        'message' => 'Lỗi gửi MMS, vui lòng liên hệ kỹ thuật',
                        'response' => $mms_resp ? $mms_resp : $response->getBody()->getContents()
                    ];
                }
            } catch (ClientException $e) {
                $response = $e->getResponse();
                Log::channel('mms_api')->error(sprintf("\n--- Response:\nStatus:%s\nMessage: %s\nBody: %s",
                    $e->getCode(),
                    $e->getMessage(),
                    $response->getBody()->getContents()
                ));
                return [
                    'success' => false,
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'response' => $response->getBody()->getContents()
                ];
            } catch (\Exception $e) {
                Log::channel('mms_api')->error(sprintf("\n--- Response:\nStatus:%s\nMessage: %s",
                    $e->getCode(),
                    $e->getMessage()
                ));
                return [
                    'success' => false,
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ];
            }
        }
        Log::channel('mms_api')->error(sprintf("\n--- Endpoint not found"));
        return [
            'success' => false,
            'code' => 404,
            'message' => 'Endpoint not found'
        ];
    }
}
