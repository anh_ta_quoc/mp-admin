<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 1:30 PM
 */

namespace App\Libraries\MmsService\Services\Merchant;

use App\Libraries\MmsService\Services\BaseService;

class ChangeStatus extends BaseService
{
    public $endpoint = '/merchant/change-status';
    public $method = 'POST';

    protected function buildParams($store)
    {
        $this->params = [
            "merchantCode" => $store->merchant_code,
            "terminalId" => $store->terminal_id,
            "changeType" => "MID"
        ];
    }
}
