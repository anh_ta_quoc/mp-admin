<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 1:30 PM
 */

namespace App\Libraries\MmsService\Services\Merchant;

use App\Libraries\MmsService\Services\BaseService;

class MerchantCreate extends BaseService
{
    public $endpoint = '/merchant/create-merchant';
    public $method = 'POST';
}