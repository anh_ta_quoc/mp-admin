<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 1:30 PM
 */

namespace App\Libraries\MmsService\Services\Merchant;

use App\Libraries\BaseFunction;
use App\Libraries\MmsService\Services\BaseService;
use App\MpTerminal;

class UpdateMid extends BaseService
{
    public $endpoint = '/merchant/update-mid-tid';
    public $method = 'POST';

    protected function buildParams($store)
    {
        $this->params = [
            "document" => [
                'businessCert' => $store->merchant_file_business_cert,
                'businessTaxCert' => $store->merchant_file_business_tax_cert,
                'domainCert' => $store->merchant_file_domain_cert,
                'identifyCard' => $store->merchant_file_identify_card,
                'contract' => $store->merchant_file_contract,
            ],
            "merchant" => [
                "department" => $store->department_id,
                "province" => $store->merchant_province_code,
                "district" => $store->merchant_district_code,
                "wards" => $store->merchant_ward_code,

                "mcTypeEnterprise" => $store->merchant_type_business,
                "merchantAddress" => $store->merchant_address,
                "merchantBrand" => $store->merchant_brand,
                "merchantBusinessAddress" => $store->merchant_business_address,
                "merchantCode" => $store->merchant_code,
                "merchantName" => $store->merchant_name,
                "merchantType" => $store->merchant_type,
                "merchantWebsite" => $store->merchant_website,
                "staff" => $store->staff_id,
            ],
            "merchantContact" => [
                "email" => $store->merchant_contact_email,
                "name" => $store->merchant_contact_name,
            ],
            "midAccInfo" => [
                "isssueInvoice" => $store->issue_invoice == 1 ? 1 : 2,
            ],
            'terminal' => [
                'accountHolder' => $store['bank_account'],
                'accountNumber' => $store['bank_number'],
                'bankBranch' => $store['bank_branch'],
                'bankCode' => $store['bank_code'],
                'businessProduct' => $store['terminal_type'],
                'productDesc' => $store['terminal_description'],
                'terminalBusinessAddress' => $store['terminal_address'] ?? '',
                'province' => $store['terminal_province_code'],
                'district' => $store['terminal_district_code'],
                'wards' => $store['terminal_ward_code'],
                'websiteBusiness' => $store['terminal_website'] ?? '',
                'website' => $store['terminal_website'] ?? '',
                'facebook' => '',
                'mccVNPAY' => $store['terminal_mcc'],
                'terminalName' => $store['terminal_name'],
                'walletType' => "1",
                'registerQrcode' => 1,
                'registerVnpayment' => $store['terminal_register_vnpayment'] ? 1 : 2,
                'updateAccTid' => 1,
                'terminalID' => $store['terminal_id'],
            ],
            'terminalContact' => [
                'email' => $store['terminal_contact_email'],
                'name' => $store['terminal_contact_name'],
                'phone' => $store['terminal_contact_phone'],
            ],
            "user" => [
                "email" => $store->merchant_contact_email
            ]
        ];
    }
}
