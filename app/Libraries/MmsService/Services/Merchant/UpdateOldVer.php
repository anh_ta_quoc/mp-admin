<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 1:30 PM
 */

namespace App\Libraries\MmsService\Services\Merchant;


use App\Libraries\MmsService\Services\BaseService;

class UpdateOldVer extends BaseService
{
    public $endpoint = '/merchant/update-old-ver';
    public $method = 'POST';
}