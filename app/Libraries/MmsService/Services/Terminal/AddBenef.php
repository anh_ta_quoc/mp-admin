<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 1:30 PM
 */

namespace App\Libraries\MmsService\Services\Terminal;

use App\Libraries\MmsService\Services\BaseService;

class AddBenef extends BaseService
{
    public $endpoint = '/merchant/add-benef';
    public $method = 'POST';

    protected function buildParams($params)
    {
        $this->params = $params;
    }
}
