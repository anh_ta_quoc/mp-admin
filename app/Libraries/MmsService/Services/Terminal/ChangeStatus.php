<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 1:30 PM
 */

namespace App\Libraries\MmsService\Services\Terminal;

use App\Libraries\MmsService\Services\BaseService;

class ChangeStatus extends BaseService
{
    public $endpoint = '/merchant/change-status';
    public $method = 'POST';

    protected function buildParams($mpTerminal)
    {
        $this->params = [
            "merchantCode" => $mpTerminal->merchant_code,
            "terminalId" => $mpTerminal->terminal_id,
            "changeType" => "TID"
        ];
    }
}
