<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 1:30 PM
 */

namespace App\Libraries\MmsService\Services\Terminal;

use App\Libraries\BaseFunction;
use App\MpTerminal;
use App\Libraries\MmsService\Services\BaseService;


class TerminalCreate extends BaseService
{
    public $endpoint = '/terminal/create-terminal';
    public $method = 'POST';

    protected function buildParams(MpTerminal $terminal)
    {
        $merchant = BaseFunction::getMerchantByCode($terminal->merchant_code);
        $tidAppUser = '';
        if ($terminal->terminal_app_user) {
            $tidAppUser = $terminal->terminal_app_user;
            if (strlen($tidAppUser) === 9 && substr($tidAppUser, 0, 1) !== '0') {
                $tidAppUser = '0' . $tidAppUser;
            }
        }
        $this->params = [
            'merchantCode' => $terminal->merchant_code,
            'merchantBrand' => $merchant ? $merchant->merchant_brand : '',
            'merchantName' => $merchant ? $merchant->merchant_name : '',
            'terminal' => [
                'accountHolder' => $terminal->bank_account,
                'accountNumber' => $terminal->bank_number,
                'bankBranch' => $terminal->bank_branch,
                'bankCode' => $terminal->bank_code,
                'businessProduct' => $terminal->terminal_type,
                'productDesc' => $terminal->product_description,
                'currency' => 'VND',
                'terminalBusinessAddress' => $terminal->terminal_address ?? '',
                'province' => $terminal->terminal_province_code,
                'district' => $terminal->terminal_district_code,
                'wards' => $terminal->terminal_ward_code,
                'websiteBusiness' => $terminal->website ?? '',
                'facebook' => $terminal->facebook ?? '',
                'mccVNPAY' => $terminal->mcc,
                'registerQrcode' => $terminal->register_qr ? 1 : 2,
                'registerVnpayment' => $terminal->register_vnpayment ? 1 : 2,
                'terminalID' => $terminal->terminal_id,
                'terminalName' => $terminal->terminal_name,
                'tserviceCode' => '',
                'walletType' => $terminal->receive_method == 1 ? 2 : 1,
                'walletName' => '',
                'walletNumber' => '',
                'masterPan' => '',
                'unionpayPan' => '',
                'visaPan' => '',
            ],
            'terminalContact' => [
                'email' => $terminal->terminal_contact_email,
                'email1' => '',
                'email2' => '',
                'name' => $terminal->terminal_contact_name,
                'phone' => $terminal->terminal_contact_phone,
                'phone1' => '',
                'phone2' => ''
            ],
            'tidAccInfo' => [
                'appUser' => $tidAppUser,
                'createTerminalApp' => $terminal->terminal_app_user ? 1 : 2,
                'receiveEmail' => '',
                'receivePhone' => '',
                'registerOTT' => 2,
                'registerSMS' => 2,
                'toCreateUser' => 2,
                'toTerminal' => 2
            ],
            'user' => [
                'email' => $terminal->terminal_contact_email,
                'userName' => 'mcportal'
            ]
        ];
    }
}
