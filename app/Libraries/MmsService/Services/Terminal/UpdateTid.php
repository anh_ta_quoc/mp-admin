<?php
/**
 * Created by PhpStorm.
 * User: CristM
 * Date: 3/9/2020
 * Time: 1:30 PM
 */

namespace App\Libraries\MmsService\Services\Merchant;

use App\Libraries\MmsService\Services\BaseService;
use App\MpTerminal;

class UpdateTid extends BaseService
{
    public $endpoint = '/merchant/update-mid-tid';
    public $method = 'POST';

    protected function buildParams($terminal){
        $this->params = [
            'merchant' => [
                'merchantCode' => $terminal['merchant_code']
            ],
            'terminal' => [
                'accountHolder' => $terminal['bank_account'],
                'accountNumber' => $terminal['bank_number'],
                'bankBranch' => $terminal['bank_branch'],
                'bankCode' => $terminal['bank_code'],
                'businessProduct' => $terminal['terminal_type'],
                'productDesc' => $terminal['product_description'],
                'terminalBusinessAddress' => $terminal['terminal_address'] ?? '',
                'province' => $terminal['terminal_province_code'],
                'district' => $terminal['terminal_district_code'],
                'wards' => $terminal['terminal_ward_code'],
                'websiteBusiness' => $terminal['website'] ?? '',
                'facebook' => $terminal['facebook'] ?? '',
                'mccVNPAY' => $terminal['mcc'],
                'terminalName' => $terminal['terminal_name'],
                'walletType' => $terminal['receive_method'] == 1 ? 2 : 1,
                'registerQrcode' => $terminal['register_qr'] ? 1 : 2,
                'registerVnpayment' => $terminal['register_vnpayment'] ? 1 : 2,
                'updateAccTid' => 1,
                'terminalID' => $terminal['terminal_id'],
            ],
            'terminalContact' => [
                'email' => $terminal['terminal_contact_email'],
                'name' => $terminal['terminal_contact_name'],
                'phone' => $terminal['terminal_contact_phone'],
            ],
        ];
    }
}
