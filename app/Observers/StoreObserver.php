<?php

namespace App\Observers;

use App\Libraries\BaseFunction;
use App\Store;
use App\AuditLog;
use Auth;
class StoreObserver
{
    /**
     * Handle the store "created" event.
     *
     * @param  \App\Store  $store
     * @return void
     */
    public function created(Store $store)
    {
        //
    }

    /**
     * Handle the store "updated" event.
     *
     * @param  \App\Models\Store  $store
     * @return void
     */
    public function updated(Store $store)
    {
        // add log by manual function instead
    }

    /**
     * Handle the store "deleted" event.
     *
     * @param  \App\Models\Store  $store
     * @return void
     */
    public function deleted(Store $store)
    {
        //
    }

    /**
     * Handle the store "restored" event.
     *
     * @param  \App\Models\Store  $store
     * @return void
     */
    public function restored(Store $store)
    {
        //
    }

    /**
     * Handle the store "force deleted" event.
     *
     * @param  \App\Models\Store  $store
     * @return void
     */
    public function forceDeleted(Store $store)
    {
        //
    }
}
