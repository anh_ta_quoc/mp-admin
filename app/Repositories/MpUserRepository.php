<?php

namespace App\Repositories;

use App\MpMidTidUser;
use App\MpUser;
use App\Repositories\BaseRepository;

/**
 * Class MpUserRepository
 * @package App\Repositories
 * @version January 13, 2020, 8:37 am UTC
*/

class MpUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MpUser::class;
    }
}
