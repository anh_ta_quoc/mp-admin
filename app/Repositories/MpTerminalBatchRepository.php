<?php

namespace App\Repositories;

use App\MpTerminalBatch;
use App\Repositories\BaseRepository;

/**
 * Class MpTerminalBatchRepository
 * @package App\Repositories
 * @version June 4, 2020, 1:55 pm +07
*/

class MpTerminalBatchRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MpTerminalBatch::class;
    }
}
