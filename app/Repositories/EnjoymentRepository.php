<?php

namespace App\Repositories;

use App\MpEnjoymentAccount;
use App\Repositories\BaseRepository;

/**
 * Class BannerRepository
 * @package App\Repositories
 * @version January 13, 2020, 8:37 am UTC
*/

class EnjoymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'merchant_code',
        'fullname',
        'account_number',
        'bank_code',
        'bank_name',
        'branch'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MpEnjoymentAccount::class;
    }
}
