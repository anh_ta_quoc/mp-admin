<?php

namespace App\Repositories;

use App\SmsLog;
use App\Repositories\BaseRepository;

/**
 * Class SmsLogRepository
 * @package App\Repositories
 * @version December 17, 2019, 2:30 am UTC
*/

class SmsLogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'phone',
        'message',
        'status',
        'error_code',
        'error_message'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SmsLog::class;
    }
}
