<?php

namespace App\Repositories;

use App\MpMidTidUser;
use App\Repositories\BaseRepository;

/**
 * Class MpMidTidUserRepository
 * @package App\Repositories
 * @version January 13, 2020, 8:37 am UTC
*/

class MpMidTidUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MpMidTidUser::class;
    }
}
