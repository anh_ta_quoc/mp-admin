<?php

namespace App\Repositories;

use App\MpBenefRequest;
use App\MpEnjoymentAccount;
use App\Repositories\BaseRepository;

/**
 * Class BannerRepository
 * @package App\Repositories
 * @version January 13, 2020, 8:37 am UTC
*/

class MpBenefRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'request_id',
        'merchant_code',
        'terminals',
        'benef_number',
        'benef_name',
        'benef_type',
        'benef_bank_name',
        'benef_bank_code',
        'benef_bank_branch',
        'doc_url',
        'create_user',
        'ref',
        'status',
        'mms_status',
        'mms_code',
        'process_lock',
        'process_msg',
        'retry_count',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MpBenefRequest::class;
    }
}
