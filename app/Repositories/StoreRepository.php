<?php

namespace App\Repositories;

use App\Store;
use App\Repositories\BaseRepository;

/**
 * Class StoreRepository
 * @package App\Repositories
 * @version December 16, 2019, 11:28 am UTC
*/

class StoreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'trial_user_id',
        'status',
        'bank_branch',
        'bank_code',
        'bank_number',
        'currency',
        'merchant_address',
        'merchant_brand',
        'merchant_code',
        'merchant_contact_email',
        'merchant_contact_name',
        'merchant_contact_phone',
        'merchant_district_code',
        'merchant_name',
        'merchant_province_code',
        'merchant_type',
        'merchant_type_business',
        'merchant_ward_code',
        'terminal_address',
        'terminal_contact_email',
        'terminal_contact_name',
        'terminal_contact_phone',
        'terminal_description',
        'terminal_district_code',
        'terminal_id',
        'terminal_name',
        'terminal_province_code',
        'terminal_type',
        'terminal_ward_code',
        'bank_account',
        'mms_status',
        'merchant_file_business_cert',
        'merchant_file_contract',
        'merchant_date_of_issue',
        'merchant_place_of_issue',
        'department_id',
        'staff_id',
        'merchant_file_business_tax_cert',
        'merchant_file_domain_cert',
        'merchant_file_identify_card',
        'wallet_status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Store::class;
    }
}
