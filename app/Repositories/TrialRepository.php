<?php

namespace App\Repositories;

use App\Trial;
use App\Repositories\BaseRepository;

/**
 * Class TrialRepository
 * @package App\Repositories
 * @version December 20, 2019, 7:10 am UTC
*/

class TrialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'phone',
        'email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Trial::class;
    }
}
