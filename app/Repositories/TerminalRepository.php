<?php

namespace App\Repositories;

use App\Models\Sync\QrTerminal;
use App\Repositories\BaseRepository;
use App\MpTerminal;

/**
 * Class TerminalRepository
 * @package App\Repositories
 * @version January 13, 2020, 8:37 am UTC
*/

class TerminalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'terminal_id',
        'terminal_name',
        'merchant_id',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MpTerminal::class;
    }
}
