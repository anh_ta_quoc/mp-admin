<?php

namespace App\Http\Requests;

use App\Rules\BankNumberRule;
use App\Rules\MerchantAddressRule;
use App\Rules\MerchantAppUserRule;
use App\Rules\MerchantBrandExistedRule;
use App\Rules\MerchantBrandRule;
use App\Rules\MerchantContactNameRule;
use App\Rules\MerchantContactPhoneRule;
use App\Rules\MerchantMstExistedRule;
use App\Rules\MerchantMstRule;
use App\Rules\MerchantNameRule;
use App\Rules\MerchantSameContactPhoneRule;
use App\Rules\MobileRule;
use App\Rules\TerminalContactPhoneRule;
use App\Rules\TerminalIdRule;
use App\Rules\TerminalNameExistedRule;
use App\Rules\TerminalNameRule;
use App\Rules\TerminalSameContactPhoneRule;
use App\Rules\UploadFileRule;
use Illuminate\Foundation\Http\FormRequest;
use App\Store;

class CreateStoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        # merchant
        'merchant_name' => [
            'required',
            'max:100',
            new MerchantNameRule(),
        ],
        'merchant_brand' => [
            'required',
            'max:25',
            new MerchantBrandRule(),
            new MerchantBrandExistedRule(),
        ],
        'merchant_code' => [
            'required',
            'max:20',
            new MerchantMstRule(),
            new MerchantMstExistedRule(),
        ],
        'merchant_address' => [
            'required',
            'max:150',
            new MerchantAddressRule(),
        ],
//            'merchant_contact_name' => 'required|max:50',
        'merchant_contact_name' => [
            'required',
            'max:50',
            new MerchantContactNameRule(),
        ],
        'merchant_contact_phone' => [
            'required',
            new MobileRule(),
//            new MerchantSameContactPhoneRule(),
//            new MerchantContactPhoneRule(),
        ],
        'merchant_app_user' => [
            'required',
            new MobileRule(),
            new MerchantAppUserRule(),
            new MerchantSameContactPhoneRule(),
        ],
        'merchant_contact_email' => 'required|email|max:100',
        'merchant_type_business' => 'required',
        'merchant_type' => 'required',
        'merchant_province_code' => 'required',
        'merchant_district_code' => 'required',
        'merchant_ward_code' => 'required',
        # terminal
        'terminal_id' => [
            'required',
            'min:2',
            'max:8',
            new TerminalIdRule(),
        ],
        'terminal_name' => [
            'required',
            'max:20',
            new TerminalNameRule(),
            new TerminalNameExistedRule(),
        ],
        'terminal_type' => 'required',
        'terminal_mcc' => 'required',
        'terminal_province_code' => 'required',
        'terminal_district_code' => 'required',
        'terminal_ward_code' => 'required',
        'terminal_address' => 'required|max:150',
        'terminal_contact_name' => 'required|max:50',
        'terminal_contact_phone' => [
            'required',
            new MobileRule(),
        ],
        'terminal_app_user' => [
            'required',
            new MobileRule(),
            new TerminalContactPhoneRule(true),
            new MerchantAppUserRule(),
        ],
        'terminal_contact_email' => 'required|email|max:100',
        'bank_code' => 'required',
        'bank_branch' => 'required|max:100',
        'bank_number' => [
            'required',
            new BankNumberRule(),
        ],
        'bank_account' => 'required|max:150',
        'staff_id' => 'required',
        'department_id' => 'required',

        'merchant_file_business_cert' => [
            'required',
            new UploadFileRule(),
        ],
        'merchant_file_contract' => [
            'required',
            new UploadFileRule(),
        ],
        'merchant_file_domain_cert' => [
            new UploadFileRule(),
        ],
        'merchant_file_identify_card' => [
            new UploadFileRule(),
        ],
        'merchant_file_business_tax_cert' => [
            new UploadFileRule(),
        ],
        'contract_number' => [
            'required',
            'max:255',
        ],
        'contract_code' => [
            'required',
            'max:255',
        ],
        'contract_date' => [
            'required',
            'max:255',
        ],
    ];
    }
    public function messages()
    {
        return Store::$messages;
    }
}
