<?php

namespace App\Http\Requests;

use App\Rules\BankNumberRule;
use App\Rules\MerchantContactNameRule;
use App\Rules\MobileRule;
use App\Rules\TerminalAddressRule;
use App\Rules\TerminalCodeRule;
use App\Rules\TerminalContactPhoneRule;
use App\Rules\TerminalIdExistedRule;
use App\Rules\TerminalIdRule;
use App\Rules\TerminalNameExistedRule;
use App\Rules\TerminalNameRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateFirstTerminalRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'terminal_type' => 'required',
            'terminal_id' => [
                'required',
                'min:2',
                'max:8',
                new TerminalIdRule(),
            ],
            'terminal_name' => [
                new TerminalNameRule()
            ],
            'terminal_mcc' => 'required',
            'terminal_province_code' => 'required',
            'terminal_district_code' => 'required',
            'terminal_ward_code' => 'required',
//            'terminal_address' => [
//                new TerminalAddressRule(),
//            ],

            'bank_code' => 'required',
            'bank_branch' => 'required',
            'bank_number' => [
                'required',
                new BankNumberRule(),
            ],
            'bank_account' => 'required',

            'terminal_contact_name' => [
                'required',
                new MerchantContactNameRule(),
            ],
            'terminal_contact_phone' => [
                'required',
                new MobileRule(),
            ],
            'terminal_contact_email' => 'required|email|max:100',

        ];
    }
}
