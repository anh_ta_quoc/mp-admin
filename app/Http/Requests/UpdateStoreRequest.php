<?php

namespace App\Http\Requests;

use App\Rules\MerchantAddressRule;
use App\Rules\MerchantAppUserRule;
use App\Rules\MerchantBrandExistedRule;
use App\Rules\MerchantBrandRule;
use App\Rules\MerchantContactNameRule;
use App\Rules\MerchantContactPhoneRule;
use App\Rules\MerchantMstExistedRule;
use App\Rules\MerchantMstRule;
use App\Rules\MerchantNameRule;
use App\Rules\MobileRule;
use App\Rules\UploadFileRule;
use App\Rules\WebsiteRule;
use Illuminate\Foundation\Http\FormRequest;
use App\Store;

class UpdateStoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'merchant_name' => [
                'required',
                'max:100',
                new MerchantNameRule($this->input('id')),
            ],
            'merchant_brand' => [
                'required',
                'max:25',
                new MerchantBrandRule(),
                new MerchantBrandExistedRule($this->input('merchant_code')),
            ],
            'merchant_code' => [
                'required',
                'max:20',
                new MerchantMstRule(),
                new MerchantMstExistedRule($this->input('id')),
            ],
            'merchant_address' => [
                'required',
                'max:150',
                new MerchantAddressRule($this->input('id')),
            ],
            'merchant_business_address' => [
                'max:150',
            ],
            'merchant_contact_name' => [
                'required',
                'max:50',
                new MerchantContactNameRule(),
            ],
            'merchant_contact_email' => 'required|email|max:100',
            'merchant_contact_phone' => [
                'required',
                new MobileRule(),
//                new MerchantContactPhoneRule($this->input('id')),
            ],
            'merchant_type_business' => 'required',
            'merchant_type' => 'required',
            'merchant_province_code' => 'required',
            'merchant_district_code' => 'required',
            'merchant_ward_code' => 'required',
            'staff_id' => 'required',
            'department_id' => 'required',

            'merchant_file_business_cert' => [
                new UploadFileRule(),
            ],
            'merchant_file_contract' => [
                new UploadFileRule(),
            ],
            'merchant_file_domain_cert' => [
                new UploadFileRule(),
            ],
            'merchant_file_identify_card' => [
                new UploadFileRule(),
            ],
            'merchant_file_business_tax_cert' => [
                new UploadFileRule(),
            ],
            'contract_number' => [
                'required',
                'max:255',
            ],
            'contract_code' => [
                'required',
                'max:255',
            ],
            'contract_date' => [
                'required',
                'max:255',
            ],
            'merchant_website' => [
                new WebsiteRule()
            ]
        ];

        $store = Store::where('id', $this->input('id'))->first();
        if ($store && $store->src == "SAPO") {
            $rules['merchant_contact_phone'] = ['required'];
        }

        return $rules;
    }

    public function messages()
    {
        return Store::$messages;
    }
}
