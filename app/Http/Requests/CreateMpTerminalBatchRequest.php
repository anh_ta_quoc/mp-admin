<?php

namespace App\Http\Requests;

use App\Rules\BankNumberRule;
use Illuminate\Foundation\Http\FormRequest;
use App\MpTerminalBatch;

class CreateMpTerminalBatchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'merchant_code' => 'required',
            'bank_code' => 'required',
            'bank_branch' => 'required',
            'bank_number' => [
                'required',
                new BankNumberRule(),
            ],
            'bank_account' => [
                'required',
                'regex:/^[a-zA-Z\s]+$/'
            ],
            'file_contract' => 'required',
            'file_authorize' => 'required',
            'file_terminals' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'bank_account.regex' => 'Tên chủ tài khoản không đúng định dạng'
        ];
    }
}
