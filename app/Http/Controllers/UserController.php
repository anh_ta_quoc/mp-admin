<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Permission;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Role;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;
class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('users.index');
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        $permissions = Permission::all();
        return view('users.create',compact('roles', 'permissions'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required',

            'email' => 'required|email|unique:users,email',

            'password' => 'required|same:confirm-password',

//            'roles' => 'required'

        ]);


        $input = $request->all();

        $input['password'] = Hash::make($input['password']);


        $user = User::create($input);

        $user->assignRole($request->input('roles'));
        $user->syncPermissions($request->input('permission'));


        Flash::success('User saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        $roles = Role::pluck('name','name')->all();
        $permissions = Permission::all();

        $userRole = $user->roles->pluck('name','name')->all();
        $userPermission = $user->permissions->pluck('id','name')->all();

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.edit',compact('user','roles','userRole', 'permissions', 'userPermission'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [

            'name' => 'required',

            'email' => 'required|email|unique:users,email,'.$id,

            'new-password' => 'same:confirm-new-password',

        ], [
            'name.required' => 'Vui lòng nhập tên'
        ]);

        $input = $request->all();

        $user = User::find($id);

        if (!empty($input['new-password'])) {
            $new_password = trim($input['new-password']);
            $input['password'] = Hash::make($new_password);
        }

        $user->update($input);

        if ($request->has('roles')) {
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $user->assignRole($request->input('roles'));
        }
        elseif ($input['change-role'] == 'yes' && !$request->has('roles')) {
            DB::table('model_has_roles')->where('model_id',$id)->delete();
        }

        if ($request->has('permission')) {
            $user->syncPermissions($request->input('permission'));
        }

        Flash::success('Cập nhật thông tin thành công.');

        return redirect('users/' . $id . '/edit');
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }
}
