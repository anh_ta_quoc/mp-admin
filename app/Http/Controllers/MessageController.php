<?php

namespace App\Http\Controllers;

use App\DataTables\MessageDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMessageRequest;
use App\Http\Requests\UpdateMessageRequest;
use App\Models\Sync\QrMerchant;
use App\Repositories\MessageRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Response;
use function GuzzleHttp\Promise\all;

class MessageController extends AppBaseController
{
    /** @var  MessageRepository */
    private $messageRepository;

    public function __construct(MessageRepository $messageRepo)
    {
        $this->messageRepository = $messageRepo;
    }

    /**
     * Display a listing of the Message.
     *
     * @param MessageDataTable $messageDataTable
     * @return Response
     */
    public function index(MessageDataTable $messageDataTable)
    {
        return $messageDataTable->render('messages.index');
    }

    /**
     * Show the form for creating a new Message.
     *
     * @return Response
     */
    public function create()
    {
//        $merchants = QrMerchant::all();

        return view('messages.create');
    }

    /**
     * Store a newly created Message in storage.
     *
     * @param CreateMessageRequest $request
     *
     * @return Response
     */
    public function store(CreateMessageRequest $request)
    {
        $userInput = $request->all();
        $input['status'] = 0;
        $input['title'] = $userInput['title'];
        $input['body'] = $userInput['body'];
        $input['link'] = $userInput['link'] ?? '';
        $merchant = QrMerchant::where('merchant_code', $userInput['receivers'])->first();
        if (!$merchant) {
            Flash::success('Không tìm thấy merchant tương ứng.');
            return redirect()->back();
        }
        $input['receivers'] = sprintf('%s - %s', $merchant->id, $userInput['receivers']);
        $input['user_id'] = Auth::user()->id;
        if (isset($userInput['is_schedule'])) {
            $input['is_schedule'] = 1;
            $scheduleAt = date_create_from_format('d/m/Y H:i:s', sprintf('%s %s', $userInput['schedule_date'], $userInput['schedule_time']));
            if ($scheduleAt < now()) {
                return Redirect::back()->withErrors(['Giờ hẹn gửi không được là thời gian trong quá khứ']);
            }
            $input['schedule_at'] = $scheduleAt;
        }
        else {
            $input['schedule_at'] = now();
        }
        $input['track_read'] = 1;
        $input['track_sent'] = 1;

        $message = $this->messageRepository->create($input);

        Flash::success('Tạo thông báo thành công.');

        return redirect(route('messages.index'));
    }

    /**
     * Display the specified Message.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            Flash::error('Message not found');

            return redirect(route('messages.index'));
        }

        return view('messages.show')->with('message', $message);
    }

    /**
     * Show the form for editing the specified Message.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            Flash::error('Message not found');

            return redirect(route('messages.index'));
        }
        elseif ($message->schedule_at < now()) {
            Flash::error('Không thể sửa thông báo này do đã quá giờ gửi');

            return redirect(route('messages.index'));
        }

        $merchants = QrMerchant::all();
        return view('messages.edit')->with('message', $message)->with('merchants', $merchants);
    }

    /**
     * Update the specified Message in storage.
     *
     * @param  int              $id
     * @param UpdateMessageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMessageRequest $request)
    {
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            Flash::error('Message not found');

            return redirect(route('messages.index'));
        }
        else {
            if ($message->process_lock) {
                return Redirect::back()->withErrors(['Thông báo đang được xử lý, không thể sửa được nữa!']);
            }

            if ($message->status !== 0) {
                return Redirect::back()->withErrors(['Thông báo đã được xử lý, không thể sửa được nữa!']);
            }
        }

        $updateData = [
            'receivers' => implode(',', $request['receivers']),
            'title' => $request['title'],
            'body' => $request['body'],
            'link' => $request['link'],
        ];
        if (isset($request['is_schedule'])) {
            $updateData['is_schedule'] = 1;
            $scheduleAt = date_create_from_format('d/m/Y H:i:s', sprintf('%s %s', $request['schedule_date'], $request['schedule_time']));
            if ($scheduleAt < now()) {
                return Redirect::back()->withErrors(['Giờ hẹn gửi không được là thời gian trong quá khứ']);
            }
            $updateData['schedule_at'] = $scheduleAt;
        }
        else {
            $updateData['is_schedule'] = 0;
            $updateData['schedule_at'] = now();
        }
        $message = $this->messageRepository->update($updateData, $id);

        Flash::success('Cập nhật thông báo thành công.');

        return redirect(route('messages.index'));
    }

    /**
     * Remove the specified Message from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            Flash::error('Message not found');

            return redirect(route('messages.index'));
        } elseif ($message->schedule_at < now()) {
            Flash::error('Không thể xóa thông báo này do đã quá giờ gửi');

            return redirect(route('messages.index'));
        }

        $this->messageRepository->delete($id);

        Flash::success('Xóa thông báo thành công.');

        return redirect(route('messages.index'));
    }
}
