<?php

namespace App\Http\Controllers;

use App\AuditLog;
use App\Bank;
use App\DataTables\ListStoreTerminalDatatable;
use App\DataTables\ListTerminalsDataTable;
use App\DataTables\StoreDataTable;
use App\Department;
use App\District;
use App\Events\RevokeLockedMerchant;
use App\Events\SendEmailCompleteStoreInfo;
use App\Libraries\BaseFunction;
use App\Http\Requests\CreateStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use App\BusinessProduct;
use App\Libraries\MmsService\MmsClient;
use App\MccVnpay;
use App\MpTerminal;
use App\Province;
use App\Repositories\StoreRepository;
use App\Staff;
use App\Store;
use App\TerminalHistory;
use App\Trial;
use App\TypeMerchant;
use App\Ward;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;


class StoreController extends AppBaseController
{
    /** @var  StoreRepository */
    private $storeRepository;

    public function __construct(StoreRepository $storeRepo)
    {
        $this->storeRepository = $storeRepo;
    }

    /**
     * Display a listing of the Store.
     *
     * @param StoreDataTable $storeDataTable
     * @return Response
     */
    public function index(StoreDataTable $storeDataTable)
    {
        $trial_users = Trial::all();
        $filterStatus = [
            BaseFunction::STATUS_SALE_REOPEN_TO_EDIT,
            BaseFunction::STATUS_CONTROL_REOPEN_TO_EDIT,
            BaseFunction::STATUS_DRAFT,
            BaseFunction::STATUS_CONTROL_ACCEPTED,
            BaseFunction::STATUS_INIT,
            BaseFunction::STATUS_REPOPEN,
            BaseFunction::STATUS_NEED_UPDATE_CONTRACT,
            BaseFunction::STATUS_WAIT_FOR_ACTION,
            BaseFunction::STATUS_SALE_ACCEPTED,
            BaseFunction::STATUS_SALE_REJECTED,
            BaseFunction::STATUS_CONTROL_REJECTED,
            BaseFunction::STATUS_LOCK
        ];
        $merchantTypeBusiness= BaseFunction::getMerchantTypeBusiness();
        $queryStatus = '1 = 1';
        $storeDataTable = new StoreDataTable($queryStatus);
        return $storeDataTable->render('stores.index', compact('trial_users', 'filterStatus', 'merchantTypeBusiness'));
    }

    /**
     * Display a listing of the Store.
     *
     * @return mixed
     */
    public function needApproveList()
    {
        $trial_users = Trial::all();
        $filterStatus = [
          BaseFunction::STATUS_WAIT_FOR_ACTION,
          BaseFunction::STATUS_SALE_ACCEPTED,
        ];
        $merchantTypeBusiness= BaseFunction::getMerchantTypeBusiness();
        $queryStatus = 'status = 0 and merchant_file_contract is not null and
        (need_reupload_contract = 0 or need_reupload_contract is null) or status = ' . BaseFunction::STATUS_SALE_ACCEPTED;
        $storeDataTable = new StoreDataTable($queryStatus);
        return $storeDataTable->render('stores.index', compact('trial_users', 'filterStatus','merchantTypeBusiness'));
    }

    /**
     * Display list active merchant
     *
     * @return mixed
     */
    public function activeList()
    {
        $trial_users = Trial::all();
        $filterStatus = [];
        $queryStatus = sprintf('status = %s', BaseFunction::STATUS_CONTROL_ACCEPTED);
        $storeDataTable = new StoreDataTable($queryStatus);
        $merchantTypeBusiness= BaseFunction::getMerchantTypeBusiness();

        return $storeDataTable->render('stores.index', compact('trial_users', 'filterStatus','merchantTypeBusiness'));
    }

    /**
     * Display list new registers
     *
     * @return mixed
     */
    public function registerList()
    {
        $trial_users = Trial::all();
        $filterStatus = [
            BaseFunction::STATUS_DRAFT,
            BaseFunction::STATUS_INIT,
            BaseFunction::STATUS_REPOPEN,
            BaseFunction::STATUS_NEED_UPDATE_CONTRACT,
            BaseFunction::STATUS_SALE_REOPEN_TO_EDIT,
            BaseFunction::STATUS_CONTROL_REOPEN_TO_EDIT,
        ];
        $exceptStatuses = [
            BaseFunction::STATUS_WAIT_FOR_ACTION,
            BaseFunction::STATUS_SALE_ACCEPTED,
            BaseFunction::STATUS_CONTROL_ACCEPTED,
            BaseFunction::STATUS_SALE_REJECTED,
            BaseFunction::STATUS_CONTROL_REJECTED,
            BaseFunction::STATUS_LOCK,
        ];
        $queryStatus = sprintf('status not in (%s)', implode(',', $exceptStatuses));
        $storeDataTable = new StoreDataTable($queryStatus);
        $merchantTypeBusiness= BaseFunction::getMerchantTypeBusiness();

        return $storeDataTable->render('stores.index', compact('trial_users', 'filterStatus','merchantTypeBusiness'));
    }

    /**
     * Display a listing of the Store.
     *
     * @return mixed
     */
    public function approvedList()
    {
        $trial_users = Trial::all();
        $filterStatus = [
          BaseFunction::STATUS_SALE_ACCEPTED,
          BaseFunction::STATUS_CONTROL_ACCEPTED,
        ];
        $queryStatus = sprintf('status in (%s)', implode(',', $filterStatus));
        $storeDataTable = new StoreDataTable($queryStatus);
        $merchantTypeBusiness= BaseFunction::getMerchantTypeBusiness();

        return $storeDataTable->render('stores.index', compact('trial_users', 'filterStatus','merchantTypeBusiness'));
    }

    /**
     * Display a listing of the Store.
     *
     * @return mixed
     */
    public function lockedList()
    {
        $trial_users = Trial::all();
        $filterStatus = [
          BaseFunction::STATUS_LOCK,
        ];
        $queryStatus = sprintf('status = %s', BaseFunction::STATUS_LOCK);
        $storeDataTable = new StoreDataTable($queryStatus);
        $merchantTypeBusiness= BaseFunction::getMerchantTypeBusiness();

        return $storeDataTable->render('stores.index', compact('trial_users', 'filterStatus','merchantTypeBusiness'));
    }

    /**
     * Display a listing of the Store.
     *
     * @return mixed
     */
    public function deniedList()
    {
        $trial_users = Trial::all();
        $filterStatus = [
          BaseFunction::STATUS_SALE_REJECTED,
          BaseFunction::STATUS_CONTROL_REJECTED,
        ];
        $queryStatus = sprintf('status in (%s)', implode(',', $filterStatus));
        $customColumns = [
            ['data' => 'merchant_name', 'name' => 'merchant_name', 'title' => 'Tên doanh nghiệp'],
            ['data' => 'merchant_code', 'name' => 'merchant_code', 'title' => 'MST/CMTND doanh nghiệp'],
            ['data' => 'merchant_contact_name', 'name' => 'merchant_contact_name', 'title' => 'Người liên hệ'],
            ['data' => 'merchant_contact_phone', 'name' => 'merchant_contact_phone', 'title' => 'SĐT liên hệ'],
            ['data' => 'type_business_name', 'name' => 'type_business_name', 'title' => 'Loại hình doanh nghiệp', 'searchable' => false],
            ['data' => 'type_name', 'name' => 'type_name', 'title' => 'Loại hình kinh doanh', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Ngày tạo'],
            ['data' => 'status_label', 'name' => 'status_label', 'title' => 'Trạng thái', 'searchable' => false],

        ];
        $storeDataTable = new StoreDataTable($queryStatus, $customColumns);
        $merchantTypeBusiness= BaseFunction::getMerchantTypeBusiness();
        return $storeDataTable->render('stores.index', compact('trial_users', 'filterStatus','merchantTypeBusiness'));
    }

    /**
     * Show the form for creating a new Store.
     *
     * @return Response
     */
    public function create()
    {
        if (!(\Auth::user()->hasrole('Sale admin') || \Auth::user()->email === 'devmp@gmail.com')) {
            return redirect('/home');
        }

        $merchantTypeBusiness= array_merge(['' => '-- Chọn --'], BaseFunction::getMerchantTypeBusiness());
        $banks = Bank::all();
        $businessProducts = BusinessProduct::where('type_code', '!=', '')->get();
        $provinces = Province::all();
        $districts = District::where('province_code',$provinces->first()->province_code)->get();
        $wards = Ward::where('province_code',$provinces->first()->province_code)
                     ->where('district_code',$districts->first()->district_code)
                     ->get();
        $departments = Department::all();
        $merchantTypes = TypeMerchant::all();
        $staffs = Staff::where('department_id', $departments->first()->department_id)->get();
        $mcc = MccVnpay::all();

        // select 2 options
        $merchantTypeOptions = ['' => '-- Chọn --'];
        foreach ($merchantTypes as $merchantType) {
            $merchantTypeOptions[$merchantType->type_code] = $merchantType->bp_title;
        }
        $provinceOptions = ['' => '-- Chọn --'];
        foreach ($provinces as $province) {
            $provinceOptions[$province->province_code] = $province->province_name;
        }
        $districtOptions = ['' => '-- Chọn --'];
        foreach ($districts as $district) {
            $districtOptions[$district->district_code] = $district->district_name;
        }
        $wardsOptions = ['' => '-- Chọn --'];
        foreach ($wards as $ward) {
            $wardsOptions[$ward->wards_code] = $ward->wards_name;
        }
        $businessProductsOptions = ['' => '-- Chọn --'];
        foreach ($businessProducts as $businessProduct) {
            $businessProductsOptions[$businessProduct->type_code] = $businessProduct->bp_title;
        }
        $banksOptions = ['' => '-- Chọn --'];
        foreach ($banks as $bank) {
            $banksOptions[$bank->bank_code] = $bank->bank_title;
        }
        $departmentsOptions = ['' => '-- Chọn --'];
        foreach ($departments as $department) {
            $departmentsOptions[$department->department_id] = $department->department_name;
        }
        $staffsOptions = ['' => '-- Chọn --'];
        foreach ($staffs as $staff) {
            $staffsOptions[$staff->staff_id] = sprintf('%s (%s)', $staff->full_name, $staff->email);
        }

        return view('stores.create',compact('merchantTypeBusiness','banks','businessProducts',
            'provinces', 'districts','wards','departments','staffs', 'mcc','merchantTypes',
            'provinceOptions', 'districtOptions', 'wardsOptions', 'merchantTypeOptions', 'businessProductsOptions',
            'banksOptions', 'departmentsOptions', 'staffsOptions'
        ));
    }

    /**
     * Store a newly created Store in storage.
     *
     * @param CreateStoreRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreRequest $request)
    {
        $input = $request->all();
        $input['status'] = BaseFunction::STATUS_DRAFT;
        $input['mms_status'] = 0;
        $input['wallet_status'] = 0;
        $input['need_reupload_contract'] = 0;

        if ($request->has('terminal_register_vnpayment') && $request['terminal_register_vnpayment'] == '1'
            && empty($request['terminal_website'])
        ) {
            return redirect()->back()->withErrors(['terminal_website' => 'Thông tin này là bắt buộc'])->withInput();
        }

        if ($request->has('terminal_website') && !empty($request['terminal_website']) &&
            !filter_var($request['terminal_website'], FILTER_VALIDATE_URL)){
            return redirect()->back()->withErrors(['terminal_website' => 'Website không đúng định dạng'])->withInput();
        }

        if ($request->hasFile('merchant_file_business_cert')) {
            $image = $request->file('merchant_file_business_cert');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_business_cert'] = $new_name;
        }
        if ($request->hasFile('merchant_file_contract')) {
            $image = $request->file('merchant_file_contract');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_contract'] = $new_name;
        }
        if ($request->hasFile('merchant_file_domain_cert')) {
            $image = $request->file('merchant_file_domain_cert');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_domain_cert'] = $new_name;
        }
        if ($request->hasFile('merchant_file_identify_card')) {
            $image = $request->file('merchant_file_identify_card');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_identify_card'] = $new_name;
        }
        if ($request->hasFile('merchant_file_business_tax_cert')) {
            $image = $request->file('merchant_file_business_tax_cert');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_business_tax_cert'] = $new_name;
        }

        $input['created_by'] = \Auth::user()->id;
        $input['merchant_business_address'] = $input['merchant_address'];
        $store = $this->storeRepository->create($input);
        $this->createAuditLog($store, 'Tạo mới Merchant');
        if ($request->has('send_request')) {
            $store->status = BaseFunction::STATUS_WAIT_FOR_ACTION;
            $store->save();
            $this->createAuditLog($store, 'Sale Admin gửi duyệt');
        }

        Flash::success('Thêm mới thông tin đăng ký thành công');

        return redirect(route('stores.index'));
    }

    /**
     * Display the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $store = $this->storeRepository->find($id);

        if (empty($store)) {
            Flash::error('Store not found');

            return redirect(route('stores.index'));
        }

        $departments = Department::orderBy('department_name')->get();
        $filterable_status = array_keys(MpTerminal::STATUS);
        $hasMpTerminal = MpTerminal::where('merchant_code', $store->merchant_code)->count();
        $approveLog = AuditLog::where('store_id', $id)
            ->where('message', 'LIKE', '%duyệt%')
            ->orderBy('created_at', 'DESC')
            ->first();

        $listTerminalsDataTable = new listTerminalsDataTable($store, $filterable_status,'Danh sách_Terminal');

        return $listTerminalsDataTable->render('stores.show', compact('store', 'departments',
            'filterable_status', 'hasMpTerminal', 'approveLog'));
    }

    /**
     * Show the form for editing the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $store = $this->storeRepository->find($id);

        if (empty($store)) {
            Flash::error('Store not found');

            return redirect(route('stores.index'));
        }
        else {
            if (in_array($store->status, [
                BaseFunction::STATUS_SALE_REJECTED,
                BaseFunction::STATUS_CONTROL_REJECTED,
                BaseFunction::STATUS_LOCK
            ])) {
                Flash::error('Không thể sửa hồ sơ ở trạng thái này');

                return redirect(route('stores.index'));
            }
        }
        $merchantTypeBusiness= array_merge(['' => '-- Chọn --'], BaseFunction::getMerchantTypeBusiness());
        $banks = Bank::all();
        $businessProducts = BusinessProduct::all();
        $merchantTypes = TypeMerchant::all();
        $provinces = Province::all();
        $districts = District::where('province_code',$provinces->first()->province_code)->get();
        $wards = Ward::where('province_code',$provinces->first()->province_code)
            ->where('district_code',$districts->first()->district_code)
            ->get();
        $departments = Department::all();
        $staffs = Staff::where('department_id', $departments->first()->department_id)->get();
        $merchantTypeOptions = ['' => '-- Chọn --'];
        foreach ($merchantTypes as $merchantType) {
            $merchantTypeOptions[$merchantType->type_code] = $merchantType->bp_title;
        }
        $provinceOptions = ['' => '-- Chọn --'];
        foreach ($provinces as $province) {
            $provinceOptions[$province->province_code] = $province->province_name;
        }
        $districtOptions = ['' => '-- Chọn --'];
        foreach ($districts as $district) {
            $districtOptions[$district->district_code] = $district->district_name;
        }
        $wardsOptions = ['' => '-- Chọn --'];
        foreach ($wards as $ward) {
            $wardsOptions[$ward->wards_code] = $ward->wards_name;
        }
        $departmentsOptions = ['' => '-- Chọn --'];
        foreach ($departments as $department) {
            $departmentsOptions[$department->department_id] = $department->department_name;
        }
        $filterable_status = array_keys(MpTerminal::STATUS);

        $listTerminalsDataTable = new listTerminalsDataTable($store, $filterable_status,'Danh sách_Terminal');

        return $listTerminalsDataTable->render('stores.edit', compact('store', 'merchantTypeBusiness',
            'banks','businessProducts','provinces', 'districts','wards','departments','staffs','merchantTypes',
            'merchantTypeOptions', 'provinceOptions', 'districtOptions', 'wardsOptions',
            'departmentsOptions'
        ));
    }

    private function removeUnnecessaryAfterControlApprove($request, $exceptKeys) {
        if (count($exceptKeys) > 0) {
            foreach ($exceptKeys as $exceptKey) {
                if ($request->has(sprintf('%s', $exceptKey))) {
                    $request->request->remove(sprintf('%s', $exceptKey));
                }
            }
        }
    }

    public function update($id, UpdateStoreRequest $request)
    {

    }

    /**
     * Update the specified Store in storage.
     *
     * @param $id
     * @param UpdateStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function updateStore($id, UpdateStoreRequest $request)
    {
        $store = $this->storeRepository->find($id);

        if (empty($store)) {
            Flash::error('Store not found');

            return redirect(route('stores.index'));
        }
        else {
            if (in_array($store->status, [
                BaseFunction::STATUS_SALE_REJECTED,
                BaseFunction::STATUS_CONTROL_REJECTED,
                BaseFunction::STATUS_LOCK
            ])) {
                Flash::error('Không thể sửa hồ sơ ở trạng thái này');

                return redirect(route('stores.index'));
            }
        }

        if ($store->status == BaseFunction::STATUS_CONTROL_ACCEPTED) {
            $this->removeUnnecessaryAfterControlApprove($request, ['merchant_contact_phone', 'merchant_code']);
        }

        $input = $request->all();

        if (!$request->has('issue_invoice')){
            $input['issue_invoice'] = 0;
        }
        if (!$request->has('terminal_register_vnpayment')){
            $input['terminal_register_vnpayment'] = 0;
        }
        else{
            if (empty($request['terminal_website'])){
                return redirect()->back()->withErrors(['terminal_website' => 'Thông tin này là bắt buộc'])->withInput();
            }
        }

        if ($request->has('terminal_website') && !empty($request['terminal_website']) &&
            !filter_var($request['terminal_website'], FILTER_VALIDATE_URL)){
            return redirect()->back()->withErrors(['terminal_website' => 'Website không đúng định dạng'])->withInput();
        }
        if ($request->hasFile('merchant_file_business_cert')) {
            $image = $request->file('merchant_file_business_cert');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_business_cert'] = $new_name;
        }
        else {
            if (empty($store->merchant_file_business_cert)) {
                return redirect()->back()->withErrors(['merchant_file_business_cert' => 'Giấy tờ này là bắt buộc'])->withInput();
            }
        }
        if ($request->hasFile('merchant_file_contract')) {
            $image = $request->file('merchant_file_contract');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_contract'] = $new_name;
        }
        else {
            if (empty($store->merchant_file_contract)) {
                return redirect()->back()->withErrors(['merchant_file_contract' => 'Giấy tờ này là bắt buộc'])->withInput();
            }
        }
        if ($request->hasFile('merchant_file_domain_cert')) {
            $image = $request->file('merchant_file_domain_cert');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_domain_cert'] = $new_name;
        }
        if ($request->hasFile('merchant_file_identify_card')) {
            $image = $request->file('merchant_file_identify_card');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_identify_card'] = $new_name;
        }
        if ($request->hasFile('merchant_file_business_tax_cert')) {
            $image = $request->file('merchant_file_business_tax_cert');
            $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('api_uploads'), $new_name);
            $input['merchant_file_business_tax_cert'] = $new_name;
        }
        if ($request->has('send_request')) {
            $input['status'] = BaseFunction::STATUS_WAIT_FOR_ACTION;
        }
        if ($store->mms_status >= BaseFunction::STATUS_MMS_CREATED) {
            $old_store = $store->getAttributes();
            $updated_store = $this->storeRepository->update($input, $id);
            $mms_response = $this->callMMS($updated_store, 'MC_UPDATE_MID');
            if ($mms_response && $mms_response['success'] == true) {
                $log = $this->prepareEditLog($updated_store);
                if ($log) {
                    Flash::success('Sửa thông tin merchant thành công.');
                    $this->createAuditLog($store, 'Sửa thông tin merchant', $log);
                }

                if ($request->has('send_request')) {
                    $this->createAuditLog($store, 'Sale Admin gửi duyệt');
                }
            } else {
                $updated_store->update($old_store);
                Flash::error('Có lỗi hệ thống xảy ra, vui lòng liên hệ kỹ thuật!');
                return redirect(route('stores.edit', ['id' => $id]))->withInput();
            }
        }
        else {
            $store = $this->storeRepository->update($input, $id);
            $log = $this->prepareEditLog($store);
            if ($log) {
                Flash::success('Sửa thông tin merchant thành công.');
                $this->createAuditLog($store, 'Sửa thông tin merchant', $log);
            }

            if ($request->has('send_request')) {
                $this->createAuditLog($store, 'Sale Admin gửi duyệt');
            }
        }
        return redirect(route('stores.show', ['id' => $id]));
    }

    private function prepareEditLog($store)
    {
        $log = '';
        $changed = $store->getChanges();
        if (count($changed) == 0) {
            return $log;
        }
        else {
            foreach(array_keys($changed) as $change_field) {
                if (!in_array($change_field, ['status', 'updated_at'])) {
                    $log .= Store::$fieldLabel[$change_field] . ', ';
                }
            }
            if (!empty($log)) {
                $log = rtrim($log, ", ");
            }
        }
        return $log;
    }

    public function updateContract($id, Request $request)
    {
        $store = $this->storeRepository->find($id);
        if(!$request->hasFile("merchant_file_contract")) {
            return response()->json(["File upload not found"], 400);
        }
        $file = $request->file("merchant_file_contract");
        if(!$file->isValid()) {
            return response()->json(["invalid_file_upload"], 400);
        }
        $path = public_path() . "/api_uploads/";
        $file->move($path, $store->merchant_code . '_' . $file->getClientOriginalName());

        // update to db
        $store->merchant_file_contract = $store->merchant_code . '_' . $file->getClientOriginalName();
        if ($request->post('contract_number')) {
            $store->contract_number = $request->post('contract_number');
        }
        if ($request->post('contract_code')) {
            $store->contract_code = $request->post('contract_code');
        }
        if ($request->post('contract_date')) {
            $store->contract_date = $request->post('contract_date');
        }

        if ($request->post('department_id')) {
            $store->department_id = $request->post('department_id');
        }
        if ($request->post('staff_id')) {
            $store->staff_id = $request->post('staff_id');
        }
        $store->status = BaseFunction::STATUS_WAIT_FOR_ACTION;
        $store->need_reupload_contract = 0;
        $store->operator_note = '';
        $store->save();

        // send email to KD
//        event(new SendEmailCompleteStoreInfo($id, 'sale-admin-upload-document'));

        Flash::success('Upload hợp đồng thành công!');

        return redirect(route('stores.show', ['id' => $id]));
    }

    /**
     * Remove the specified Store from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $store = $this->storeRepository->find($id);

        if (empty($store)) {
            Flash::error('Store not found');

            return redirect(route('stores.index'));
        }

        $this->storeRepository->delete($id);

        Flash::success('Store deleted successfully.');

        return redirect(route('stores.index'));
    }


    public function saleAccept($id)
    {
        /*
         * Check permission
         */

        $content = $this->storeRepository->find($id);

        if ($content && $content->status == BaseFunction::STATUS_WAIT_FOR_ACTION && (!$content->trial_user_id || ($content->trial_user_id && $content->mms_status == 1))) {
            $content->update([
                'status' => BaseFunction::STATUS_SALE_ACCEPTED
            ]);

            // send email to DS
//            event(new SendEmailCompleteStoreInfo($id, 'sale-approve'));

            $this->createAuditLog($content, 'Kinh doanh duyệt');

            return [
                'status' => 'OK',
                'msg' => 'Duyệt thành công'
            ];
        }
        else {
            return [
                'status' => 'NOT_OK',
                'msg' => 'Hồ sơ này không đủ điều kiện để duyệt'
            ];
        }
    }


    public function controlAccept($id)
    {
        /*
          * Check permission
          */

        $content = $this->storeRepository->find($id);
        if ($content && $content->status == BaseFunction::STATUS_SALE_ACCEPTED && (!$content->trial_user_id || ($content->trial_user_id && $content->mms_status == 1))) {
            $oldStatus = $content->status;
            $content->update([
                'status' => BaseFunction::STATUS_CONTROL_ACCEPTED
            ]);
            $this->createAuditLog($content, 'Đối soát duyệt');

            return self::notifyMMS($id, $oldStatus);
        }
        else {
            return [
                'status' => 'NOT_OK',
                'msg' => 'Hồ sơ này không đủ điều kiện để duyệt'
            ];
        }
    }


    public function notifyMMS($id, $oldStatus)
    {
        /*
           * Check permission
           */

        $content = $this->storeRepository->find($id);
        $res = [
            'status' => 'OK',
            'msg' => 'Duyệt thành công'
        ];
        if ($content && $content->status == BaseFunction::STATUS_CONTROL_ACCEPTED) {
            // send to mms code here

            if ($content->trial_user_id | $content->src == "SAPO") {
                // case update merchant file to mms
                $response = BaseFunction::sendUpdateToMMS($content);
                if ($response) {
                    $content->update([
                        'mms_status' => BaseFunction::STATUS_MMS_APPROVED
                    ]);
                } else {
                    $content->update([
                        'mms_status' => BaseFunction::STATUS_MMS_APPROVED_FAILED,
                        'status' => $oldStatus
                    ]);
                }
            }
            else {
                // case create new MMS merchant
                $code = BaseFunction::sendCreateToMMS($id);
                if (in_array($code, ["00"])) {
                    $content->update([
                        'mms_status' => BaseFunction::STATUS_MMS_APPROVED,
                        'wallet_status' => $code == "00" ? 1: -1
                    ]);
                } else {
                    $content->update([
                        'mms_status' => BaseFunction::STATUS_MMS_CREAT_FAILED,
                        'status' => $oldStatus
                    ]);
                    $res['status'] = 'NOT_OK';
                    if (array_key_exists($code, BaseFunction::MMS_CODE_MAPPING)) {
                        $res['msg'] = BaseFunction::MMS_CODE_MAPPING[$code];
                    }
                    else {
                        $res['msg'] = 'Hệ thống có lỗi xảy ra, vui lòng thử lại sau ít phút!';
                    }

                    $this->createAuditLog($content, 'Gửi MMS bị lỗi', $res['msg']);
                }
            }

        }
        return $res;
    }
    public function retryMMS($id)
    {
        $content = Store::find($id);
        if ($content && $content->mms_status == BaseFunction::STATUS_MMS_CREAT_FAILED) {
            // send to mms code here
            $code = BaseFunction::sendCreateToMMS($id);
            // if success, then update mms_status

            if ($code == "00") {
                DB::beginTransaction();
                try {
                    $content->update([
                        'mms_status' => BaseFunction::STATUS_MMS_CREATED
                    ]);

                    DB::commit();
                } catch (\Exception $exception) {
                    DB::rollBack();
                }
                Flash::success('Thao tác gọi sang MMS thành công')->flash();

            } else {
                DB::beginTransaction();
                try {
                    $content->update([
                        'mms_status' => BaseFunction::STATUS_MMS_CREAT_FAILED
                    ]);

                    DB::commit();
                } catch (\Exception $exception) {
                    DB::rollBack();
                }
                Flash::error('Thao tác gọi sang MMS thất bại.Mã Lỗi :'.$code.'. Vui lòng thử lại sau');

            }
        }
        return redirect(route('stores.index'));


    }

    public function reject($id, $sale_note = '')
    {
        $content = $this->storeRepository->find($id);
        if (!$content) {
            return [
                'status' => 'NOT_OK',
                'msg' => 'Không tìm thấy thông tin đăng ký'
            ];
        }

        switch ($content->status) {
            case 0:
                if (in_array($content->src, ['MR', 'SAPO'])) {
                    $mms_response = $this->callMMS($content, 'MC_CHANGE_STATUS');
                    if ($mms_response && $mms_response['success'] == true) {
                        $mark_status = BaseFunction::STATUS_SALE_REJECTED;
                        $this->createAuditLog($content, 'Kinh doanh từ chối', $sale_note);
                    }
                    else {
                        return [
                            'status' => 'NOT_OK',
                            'msg' => 'Lỗi khóa bên MMS'
                        ];
                    }
                }
                else {
                    $mark_status = BaseFunction::STATUS_SALE_REJECTED;
                    $this->createAuditLog($content, 'Kinh doanh từ chối', $sale_note);
                }
                break;
            case 1:
                $mark_status = BaseFunction::STATUS_CONTROL_REOPEN_TO_EDIT;
                $this->createAuditLog($content, 'Đối soát trả lại hồ sơ', $sale_note);

                break;
            default:
                return [
                    'status' => 'NOT_OK',
                    'msg' => 'Trạng thái thông tin không phù hợp'
                ];
        }

        $content->status = $mark_status;
        $content->operator_note = $sale_note;
        $content->save();

        return [
            'status' => 'OK',
            'msg' => 'Thao tác thành công'
        ];
    }

    public function reopenUploadDocument($id, $sale_note = '') {
        $content = $this->storeRepository->find($id);
        if ($content && in_array($content->status, [BaseFunction::STATUS_SALE_REJECTED, BaseFunction::STATUS_CONTROL_REJECTED])) {
            $content->status = BaseFunction::STATUS_REPOPEN;
            $content->operator_note = $sale_note;
            $content->save();
        }
        else {
            return [
                'status' => 'NOT_OK',
                'msg' => 'Hồ sơ này không đủ điều kiện mở cập nhật'
            ];
        }
        return [
            'status' => 'OK',
            'msg' => 'Mở cập nhật hồ sơ thành công!'
        ];
    }

    public function reUploadContract($id, Request $request) {
        $store = $this->storeRepository->find($id);

        if (empty($store)) {
            Flash::error('Không tìm thấy thông tin đăng ký');

            return redirect(route('stores.index'));
        }

        $store->status = BaseFunction::STATUS_REOPEN_CONTRACT;
        $store->operator_note = trim($request->post('note'));
        $store->need_reupload_contract = 1;
        $store->save();

//        event(new SendEmailCompleteStoreInfo($id, 'reopen-contract'));

        Flash::success('Mở cập nhật hợp đồng thành công.');

        return redirect(route('stores.show', ['id' => $id]));
    }

    public function saleAction($id, Request $request) {
        $sale_note = $request->post('sale-note');
        if ($request->has('reopen')) {
            $content = $this->storeRepository->find($id);
            if ($content && $content->status == BaseFunction::STATUS_WAIT_FOR_ACTION) {
                $content->update([
                    'status' => BaseFunction::STATUS_SALE_REOPEN_TO_EDIT
                ]);
                $this->createAuditLog($content, 'Kinh doanh mở cập nhật hồ sơ', $sale_note);
            }
            return redirect(route('stores.show', ['id' => $id]));
        }
        else {
            $saleActionType = $request->post('sale-action-type');
            switch ($saleActionType) {
                case 'sale-reopen-document':
                    $res = self::reopenUploadDocument($id, $sale_note);
                    break;
                case 'sale-reject-contract':
                    $res = self::reject($id, $sale_note);
                    break;
                case 'sale-approve':
                    $res = self::saleAccept($id);
                    break;
                default:
                    $res = ['status' => 'NOT_OK', 'msg' => 'Hệ thống xảy ra lỗi, vui lòng thử lại sau ít phút'];
                    break;
            }
        }
        if ($res['status'] != 'OK') {
            Flash::error($res['msg']);
        }
        else {
            Flash::success($res['msg']);
        }

        return redirect(route('stores.show', ['id' => $id]));
    }

    public function controlAction($id, Request $request) {
        $control_note = $request->post('control-note');
        $controlActionType = $request->post('control-action-type');
        switch ($controlActionType) {
            case 'control-reject-contract':
                $res = self::reject($id, $control_note);
                break;
            case 'control-approve':
                $res = self::controlAccept($id);
                return $res;
            default:
                $res = ['status' => 'NOT_OK', 'msg' => 'Hệ thống xảy ra lỗi, vui lòng thử lại sau ít phút'];
                break;
        }
        if ($res['status'] != 'OK') {
            Flash::error($res['msg']);
        }
        else {
            Flash::success($res['msg']);
        }

        return redirect(route('stores.show', ['id' => $id]));
    }
    public function callMMS($store, $key)
    {
        $client = new MmsClient($key);
        return $client->endpoint->endpointRequest($store);
    }

    public function createAuditLog($store, $message, $operatorNote = '', $oldData = null)
    {
        $auditLog = new AuditLog();
        $auditLog->user_id = \Auth::user()->id;
        $auditLog->store_id = $store->id;
        $auditLog->old_data = $oldData ? $oldData->toJson() : $store->toJson();
        $auditLog->new_data = $store->toJson();
        $auditLog->message = $message;
        $auditLog->operator_note = $operatorNote;
        try {
            $auditLog->save();
        }
        catch (\Exception $e) {

        }
    }

    public function sendRequest($id, Request $request)
    {
        $store = $this->storeRepository->find($id);
        if ($store && in_array($store->status, [BaseFunction::STATUS_SALE_REOPEN_TO_EDIT,
                BaseFunction::STATUS_CONTROL_REOPEN_TO_EDIT, BaseFunction::STATUS_DRAFT])) {
            $store->status = BaseFunction::STATUS_WAIT_FOR_ACTION;
            $store->save();
            $this->createAuditLog($store, 'Sale Admin gửi duyệt');
        }

        return [
            'status' => 'OK',
            'msg' => ''
        ];
    }

    public function lockMerchant(Request $request)
    {
        if (!(\Auth::user()->email === 'devmp@gmail.com' || \Auth::user()->hasrole('Kinh doanh'))) {
            Flash::error('Bạn không có quyền thực hiện thao tác này');
            return redirect()->back();
        }
        if (!$request->has('storeId') || empty($request->get('storeId'))) {
            Flash::error('Param không hợp lệ');
            return redirect()->back();
        }
        $store = $this->storeRepository->find($request->get('storeId'));
        if (!$store) {
            Flash::error('Không tìm thấy merchant');
        }
        elseif ($store->mms_status < BaseFunction::STATUS_MMS_CREATED
            || $store->mms_status == BaseFunction::STATUS_LOCK) {
            Flash::error('Không thể khóa merchant ở trạng thái này');
        }
        else {
            $terminals = MpTerminal::where('merchant_code', $store->merchant_code)
                ->whereIn('status', [MpTerminal::STATUS_REVIEW_APPROVED, MpTerminal::STATUS_LOCK])
                ->get();
            ini_set('max_execution_time', 0);
            try {
                $mms_response = $this->callMMS($store, 'MC_CHANGE_STATUS');
                if ($mms_response && $mms_response['success'] == true) {
                    // update status
                    $oldStore = clone($store);
                    $store->status = BaseFunction::STATUS_LOCK;
                    $store->save();

                    // write audit log
                    $this->createAuditLog($store, 'Khóa merchant', trim($request->get('reason')), $oldStore);

                    // lock all terminal
                    $this->lockRelatedTerminals($store->merchant_code);

                    // revoke merchant
                    event(new RevokeLockedMerchant($store->merchant_code));

                    Flash::success(sprintf('Khóa merchant %s - %s thành công',
                        $store->merchant_code, $store->merchant_name
                    ));
                }
                else {
                    Flash::error('Lỗi gọi MMS');
                    return redirect()->back();
                }

                return redirect(route('stores.index'));
            }
            catch (\Exception $e) {
                Flash::error(sprintf('Có lỗi hệ thống xảy ra %s', $e->getMessage()));
            }
        }
        return redirect()->back();
    }

    /**
     * Lock all terminals which related to merchant
     *
     * @param $merchantCode
     */
    private function lockRelatedTerminals($merchantCode)
    {
        $terminals = MpTerminal::where('merchant_code', $merchantCode)
            ->whereIn('status', [MpTerminal::STATUS_REVIEW_APPROVED, MpTerminal::STATUS_LOCK])
            ->get();
        if ($terminals && sizeof($terminals) > 0) {
            foreach ($terminals as $terminal) {
                // update terminal to lock status
                $oldTerminal = clone($terminal);
                $terminal->status = MpTerminal::STATUS_LOCK;
                $terminal->save();
                // create lock history
                $insertData = [
                    'mp_terminal_id' => $terminal->id,
                    'terminal_id' => $terminal->terminal_id,
                    'staff_id' => auth()->user()->id,
                    'type' => TerminalHistory::TYPE_LOCK,
                    'reason' => 'Khóa merchant',
                    'old_data' => $oldTerminal->toJson(),
                    'new_data' => $terminal->toJson(),
                ];
                TerminalHistory::create($insertData);
            }
        }
    }

    public function unlockMerchant(Request $request)
    {
        if (!(\Auth::user()->email === 'devmp@gmail.com' || \Auth::user()->hasrole('Kinh doanh'))) {
            Flash::error('Bạn không có quyền thực hiện thao tác này');
            return redirect()->back();
        }
        if (!$request->has('storeId') || empty($request->get('storeId'))) {
            Flash::error('Param không hợp lệ');
            return redirect()->back();
        }
        $store = $this->storeRepository->find($request->get('storeId'));
        if (!$store) {
            Flash::error('Không tìm thấy merchant');
        }
        elseif ($store->status != BaseFunction::STATUS_LOCK) {
            Flash::error('Không thể mở khóa merchant ở trạng thái này');
        }
        else {
            ini_set('max_execution_time', 0);
            try {
                $mms_response = $this->callMMS($store, 'MC_CHANGE_STATUS');
                if ($mms_response && $mms_response['success'] == true) {
                    // update status
                    $oldStore = clone($store);
                    $previousInfo = $this->getPreviousInfo($request->get('storeId'), 'Khóa merchant');
                    $store->status = $previousInfo->status ?? BaseFunction::STATUS_CONTROL_ACCEPTED;
                    $store->save();

                    // write audit log
                    $this->createAuditLog($store, 'Mở khóa merchant', trim($request->get('reason')), $oldStore);

                    // unlock related terminals
                    $this->unLockRelatedTerminals($store->merchant_code);

                    // revoke merchant
                    event(new RevokeLockedMerchant($store->merchant_code));

                    Flash::success(sprintf(
                        'Mở khóa merchant %s - %s thành công',
                        $store->merchant_code, $store->merchant_name
                    ));
                }
                else {
                    Flash::error('Lỗi gọi MMS');
                    return redirect()->back();
                }

                return redirect(route('stores.index'));
            }
            catch (\Exception $e) {
                Flash::error(sprintf('Có lỗi hệ thống xảy ra: %s', $e->getMessage()));
            }
        }
        return redirect()->back();
    }

    /**
     * Unlock all terminals which related to merchant
     *
     * @param $merchantCode
     */
    private function unLockRelatedTerminals($merchantCode)
    {
        $terminals = MpTerminal::where('merchant_code', $merchantCode)
            ->whereIn('status', [MpTerminal::STATUS_LOCK])
            ->get();
        if ($terminals && sizeof($terminals) > 0) {
            foreach ($terminals as $terminal) {
                // update terminal to lock status
                $oldTerminal = clone($terminal);
                $terminal->status = MpTerminal::STATUS_REVIEW_APPROVED;
                $terminal->save();
                // create unlock history
                $insertData = [
                    'mp_terminal_id' => $terminal->id,
                    'terminal_id' => $terminal->terminal_id,
                    'staff_id' => auth()->user()->id,
                    'type' => TerminalHistory::TYPE_UNLOCK,
                    'reason' => 'Mở khóa merchant',
                    'old_data' => $oldTerminal->toJson(),
                    'new_data' => $terminal->toJson(),
                ];
                TerminalHistory::create($insertData);
            }
        }
    }

    private function getPreviousInfo($storeId, $message = '')
    {
        $audit_log = AuditLog::where('store_id', $storeId);
        if ($message) {
            $audit_log = $audit_log->where('message', $message);
        }

        $audit_log_obj = $audit_log
            ->orderBy('id', 'desc')
            ->first();
        if ($audit_log_obj) {
            try {
                return json_decode($audit_log_obj['old_data']);
            }
            catch (\Exception $e) {
                return [];
            }
        }
        else {
            return [];
        }
    }
}
