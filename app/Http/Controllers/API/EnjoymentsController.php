<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStoreAPIRequest;
use App\Http\Requests\API\UpdateStoreAPIRequest;
use App\MpBenefRequest;
use App\Store;
use App\Repositories\StoreRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class StoreController
 * @package App\Http\Controllers\API
 */
class EnjoymentsController extends AppBaseController
{
    public function updateBenefCr(Request $request)
    {
        $param = $request->all();
        if (isset($param['id']) && isset($param['createBenef']) && isset($param['action'])) {
            $req = MpBenefRequest::where('id', $param['id'])->first();
            if ($req) {
                if($param['action'] == 'add'){
                    if ($req->status == MpBenefRequest::STATUS_WAIT_SENT_RESPONSE){
                        $failed_ter = [];
                        $total = 0;
                        foreach ($param['createBenef'] as $ter){
                            if ($ter['status'] != 0){
                                $failed_ter[] = $ter['acc_no'];
                            }
                            $total ++;
                        }
                        $req->terminals_add_failed = implode(',', $failed_ter);
                        $req->add_response_payload = json_encode($param);
                        if ($total > count($failed_ter)){
                            $req->status = MpBenefRequest::STATUS_SENT;
                        }
                        else{
                            $req->status = MpBenefRequest::STATUS_SEND_ERROR;
                        }
                        $req->save();
                        return response()->json([
                            'code' => "00",
                            'message' => 'Success'
                        ]);
                    }
                    else {
                        return response()->json([
                            'code' => "01",
                            'message' => 'Request status invalid'
                        ]);
                    }
                }
                elseif ($param['action'] == 'update'){
                    if ($req->status == MpBenefRequest::STATUS_SENT){
                        $req->status = MpBenefRequest::STATUS_APPROVED;
                        $req->update_payloads = json_encode($param);
                        $req->save();
                        return response()->json([
                            'code' => "00",
                            'message' => 'Success'
                        ]);
                    }
                    else {
                        return response()->json([
                            'code' => "01",
                            'message' => 'Request status invalid'
                        ]);
                    }
                }

            } else {
                return response()->json([
                    'code' => "01",
                    'message' => 'Request not found'
                ]);
            }
        } else {
            return response()->json([
                'code' => "01",
                'message' => 'Invalid parameters'
            ]);
        }
    }
}
