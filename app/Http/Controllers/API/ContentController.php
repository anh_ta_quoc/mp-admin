<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-02-25
 * Time: 14:52
 */

namespace App\Http\Controllers\API;

use App\AuditLog;
use App\District;
use App\Libraries\BaseFunction;
use App\MpTerminal;
use App\Staff;
use App\Store;
use App\Ward;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller
{
    public function get_districts_from_province_code(Request $request)
    {
        if (!$request->province_code) {
            $html = '<option value=""></option>';
        } else {
            $html = '';
            $districts = District::where('province_code', $request->province_code)->get();
            foreach ($districts as $district) {
                $html .= '<option value="'.$district->district_code.'">'.$district->district_name.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function get_wards_from_district_code_and_province_code(Request $request)
    {

        if (!$request->district_code) {
            $html = '<option value=""></option>';
        } else {
            $html = '';
            $wards = Ward::where('province_code',$request->province_code)
                         ->where('district_code',$request->district_code)
                         ->get();
            foreach ($wards as $ward) {
                $html .= '<option value="'.$ward->wards_code.'">'.$ward->wards_name.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function get_staff_from_department(Request $request)
    {
        if (!$request->department_id) {
            $html = '<option value=""></option>';
        } else {
            $html = '';
            $staffs = Staff::where('department_id', $request->department_id)->orderBy('full_name')->get();
            if (count($staffs) >0) {
                foreach ($staffs as $staff) {
                    $html .= '<option value="' . $staff->staff_id . '">' .
                        sprintf('%s (%s)', $staff->full_name, $staff->email) . '</option>';
                }
            } else {
                $html = '<option value=""></option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function get_audit_logs(Request $request) {
        if (!$request->store_id) {
            $html = 'Chưa có lịch sử phê duyệt nào';
        } else {
            $store = Store::where('id', $request->store_id)->first();
            $audit_logs = AuditLog::where('store_id', $request->store_id)
                ->select('created_at', 'user_id', 'message', 'operator_note')
                ->orderBy('created_at', 'DESC')
                ->orderBy('id', 'DESC')
                ->get();
            $html = '
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tài khoản</th>
                        <th>Thời gian cập nhật</th>
                        <th>Nghiệp vụ</th>
                        <th>Ghi chú</th>
                    </tr>
                </thead>
                <tbody>
            ';
            if (count($audit_logs) > 0) {
                foreach ($audit_logs as $index => $audit_log) {
                    $html .= sprintf('<tr>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                            </tr>', $index + 1, $audit_log->getUserName(),
                            date('d-m-Y H:i:s', strtotime($audit_log->created_at)),
                        $audit_log->message, $audit_log->operator_note);
                }
            }
            if ($store->src == "SAPO") {
                $html .= sprintf('<tr>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                            </tr>',
                    count($audit_logs) + 1,
                    'SAPO API',
                    date('d-m-Y H:i:s', strtotime($store->created_at)),
                    'SAPO khởi tạo merchant',
                    ''
                );
            }
            elseif (!$store->created_by && $store->src != 'SYNC_MMS') {
                $html .= sprintf('<tr>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                            </tr>',
                    count($audit_logs) + 1,
                    $store->trial_user_id ? 'Merchant' : 'Sale Admin',
                    date('d-m-Y H:i:s', strtotime($store->created_at)),
                    $store->trial_user_id ? 'MC đăng ký dịch vụ' : 'Nhân viên khởi tạo hồ sơ',
                    ''
                );
            }

            $html .= '
                    </tbody>
                    </table>
                ';
        }

        return response()->json(['html' => $html]);
    }

    public function search_merchant_users(Request $request)
    {
        $key_search = $request->get('search');
        $list_users_db = DB::connection('oracle')->table('MMS.QR_MERCHANT')
            ->join('MMS.QR_MERCHANT_USERS', 'MMS.QR_MERCHANT.ID', '=', 'MMS.QR_MERCHANT_USERS.MERCHANT_ID')
            ->where('MMS.QR_MERCHANT_USERS.STATUS', 0)
            ->where(function ($q) use ($key_search) {
                $q->where('merchant_name', $key_search)
                    ->orWhere('merchant_code', $key_search);
            })
            ->select('MMS.QR_MERCHANT_USERS.ID', 'MMS.QR_MERCHANT_USERS.USERNAME', 'MMS.QR_MERCHANT_USERS.STATUS')
            ->get();

        return json_decode($list_users_db);
    }

    /**
     * Get latest terminal of merchant
     *
     * @param Request $request
     * @return string|null
     */
    public function get_latest_terminal_of_merchant(Request $request)
    {
        $merchantCode = $request->merchant_code ?? null;
        if (!$merchantCode) {
            return null;
        }
        $merchant = DB::connection('oracle')->table('MMS.QR_MERCHANT')
            ->where('MMS.QR_MERCHANT.MERCHANT_CODE', '=', $merchantCode)
            ->first();
        if (!$merchant) {
            return "";
        }

        $countTerminalsSync = DB::connection('oracle')->table('MMS.QR_TERMINAL')
            ->where('MMS.QR_TERMINAL.MERCHANT_ID', '=', $merchant->id)
            ->orderBy('MMS.QR_TERMINAL.CREATED_DATE', 'DESC')
            ->count();

        $countTerminals = DB::connection('mysql')->table('mp_terminals')
            ->where('merchant_code', '=', $merchant->merchant_code)
            ->count();

        return str_pad($countTerminalsSync + $countTerminals + 1, 4, "0", STR_PAD_LEFT);
    }

    public function get_combine_info_mer_ter(Request $request)
    {
        if (!$request->has('mp_terminal_id')) {
            return json_encode([
                'status' => 'NOT_OK',
                'data' => null
            ]);
        }
        $mpTerminal = MpTerminal::where('id', $request->get('mp_terminal_id'))->first();
        if (!$mpTerminal) {
            return json_encode([
                'status' => 'NOT_OK',
                'data' => null
            ]);
        }
        $storeInfo = Store::where('merchant_code', $mpTerminal->merchant_code)
            ->where('status', BaseFunction::STATUS_LOCK)
            ->first();
        if (!$storeInfo) {
            return json_encode([
                'status' => 'NOT_OK',
                'data' => null
            ]);
        }
        return json_encode([
            'status' => 'OK',
            'data' => [
                'merchant_code' => $storeInfo->merchant_code,
                'merchant_name' => $storeInfo->merchant_name,
                'terminal_id' => $mpTerminal->terminal_id,
                'terminal_name' => $mpTerminal->terminal_name,
            ]
        ]);
    }

    public function clearFilter(Request $request)
    {
        $filters = [
          'store_info' => [
              'store_info_filter_src',
              'store_info_filter_query',
              'filter_daterange_start',
              'filter_daterange_end',
              'store_info_filter_status',
              'store_info_filter_business_type',
          ],
          'terminal' => [
              'filter_daterange_start',
              'filter_daterange_end',
              'terminal_filter_status',
              'terminal_filter_query',
          ],
        ];
        if (in_array($request->get('module'), array_keys($filters))) {
           foreach ($filters[$request->get('module')] as $clearKey) {
               session()->remove(sprintf('%s', $clearKey));
           }
        }
        return json_encode([
            'status' => 'OK'
        ]);
    }
}
