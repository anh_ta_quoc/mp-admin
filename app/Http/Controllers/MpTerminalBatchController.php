<?php

namespace App\Http\Controllers;

use App\Bank;
use App\DataTables\ListTerminalsInBatchDataTable;
use App\DataTables\MpTerminalBatchDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMpTerminalBatchRequest;
use App\Http\Requests\UpdateMpTerminalBatchRequest;
use App\Libraries\BaseFunction;
use App\MpTerminal;
use App\MpTerminalBatch;
use App\Repositories\MpTerminalBatchRepository;
use App\Store;
use App\TerminalHistory;
use Flash;
use App\Http\Controllers\AppBaseController;
use Ramsey\Uuid\Uuid;
use Response;
use Symfony\Component\Console\Terminal;
use Symfony\Component\HttpFoundation\Request;

class MpTerminalBatchController extends AppBaseController
{
    /** @var  MpTerminalBatchRepository */
    private $mpTerminalBatchRepository;

    public function __construct(MpTerminalBatchRepository $mpTerminalBatchRepo)
    {
        $this->mpTerminalBatchRepository = $mpTerminalBatchRepo;
    }

    /**
     * Display a listing of the MpTerminalBatch.
     *
     * @param MpTerminalBatchDataTable $mpTerminalBatchDataTable
     * @return Response
     */
    public function index(MpTerminalBatchDataTable $mpTerminalBatchDataTable)
    {
        $filterable_status = array_keys(MpTerminalBatch::STATUS);

        return $mpTerminalBatchDataTable->render('mp_terminal_batches.index', compact('filterable_status'));
    }

    /**
     * Show the form for creating a new MpTerminalBatch.
     *
     * @return Response
     */
    public function create()
    {
        $banks = Bank::all();
        return view('mp_terminal_batches.create', compact(
            'banks'
        ));
    }

    /**
     * Store a newly created MpTerminalBatch in storage.
     *
     * @param CreateMpTerminalBatchRequest $request
     *
     * @return Response
     */
    public function store(CreateMpTerminalBatchRequest $request)
    {
        $input = $request->all();

        $folder_path = public_path('api_uploads/terminal_batch/' . $input['merchant_code']);

        if ($request->hasFile('file_contract')) {
            $file_contract = $request->file('file_contract');
            $new_name = Uuid::uuid4() . '.' . $file_contract->getClientOriginalExtension();
            $file_contract->move($folder_path, $new_name);
            $input['file_contract'] = $new_name;
        }

        if ($request->hasFile('file_authorize')) {
            $file_authorize = $request->file('file_authorize');
            $new_name = Uuid::uuid4() . '.' . $file_authorize->getClientOriginalExtension();
            $file_authorize->move($folder_path, $new_name);
            $input['file_authorize'] = $new_name;
        }

        if ($request->hasFile('file_terminals')) {
            $file_terminals = $request->file('file_terminals');
            $new_name = Uuid::uuid4() . '.' . $file_terminals->getClientOriginalExtension();
            $file_terminals->move($folder_path, $new_name);
            $input['file_terminals'] = $new_name;
        }

        $input['created_by'] = \Auth::user()->id;
        $input['updated_by'] = \Auth::user()->id;
        $input['status'] = MpTerminalBatch::STATUS_INIT;

        $mpTerminalBatch = $this->mpTerminalBatchRepository->create($input);

        Flash::success('Mp Terminal Batch saved successfully.');

        return redirect(route('terminalBatches.index'));
    }

    /**
     * Display the specified MpTerminalBatch.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $batch = $this->mpTerminalBatchRepository->find($id);

        if (empty($batch)) {
            Flash::error('Mp Terminal Batch not found');

            return redirect(route('terminalBatches.index'));
        }

        $listTerminalsInBatchDataTable = new ListTerminalsInBatchDataTable($batch);

        return $listTerminalsInBatchDataTable->render('mp_terminal_batches.show', compact('batch'));
    }

    /**
     * Show the form for editing the specified MpTerminalBatch.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mpTerminalBatch = $this->mpTerminalBatchRepository->find($id);

        if (empty($mpTerminalBatch)) {
            Flash::error('Mp Terminal Batch not found');

            return redirect(route('terminalBatches.index'));
        }

        return view('mp_terminal_batches.edit')->with('mpTerminalBatch', $mpTerminalBatch);
    }

    /**
     * Update the specified MpTerminalBatch in storage.
     *
     * @param  int              $id
     * @param UpdateMpTerminalBatchRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMpTerminalBatchRequest $request)
    {
        $mpTerminalBatch = $this->mpTerminalBatchRepository->find($id);

        if (empty($mpTerminalBatch)) {
            Flash::error('Mp Terminal Batch not found');

            return redirect(route('terminalBatches.index'));
        }

        $mpTerminalBatch = $this->mpTerminalBatchRepository->update($request->all(), $id);

        Flash::success('Mp Terminal Batch updated successfully.');

        return redirect(route('terminalBatches.index'));
    }

    /**
     * Remove the specified MpTerminalBatch from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mpTerminalBatch = $this->mpTerminalBatchRepository->find($id);

        if (empty($mpTerminalBatch)) {
            Flash::error('Mp Terminal Batch not found');

            return redirect(route('terminalBatches.index'));
        }

        $this->mpTerminalBatchRepository->delete($id);

        Flash::success('Mp Terminal Batch deleted successfully.');

        return redirect(route('terminalBatches.index'));
    }

    public function reject($id, Request $request){
        $batch = $this->mpTerminalBatchRepository->find($id);

        if (empty($batch)) {
            Flash::error('Không tìm thấy lô terminal này');

            return redirect(route('terminalBatches.index'));
        }

        $user = auth()->user();
        $rejectReason = $request->get('reject_reason') ?? null;
        if ($rejectReason === '0') {
            $rejectReason = $request->get('reason');
        }
        if ($batch->status == $this->mpTerminalBatchRepository->model()::STATUS_WAIT_SALE_APPROVE) {
            if ($user->hasrole('Kinh doanh')) {
                if ($request->has('reject_reason')) {
                    $this->mpTerminalBatchRepository->update([
                        'status' => $this->mpTerminalBatchRepository->model()::STATUS_SALE_DENIED,
                        'reject_reason' => $rejectReason
                    ], $id);
                    $this->saveLogHandleToDb($batch['id'], TerminalHistory::TYPE_BATCH_SALE_DENIED, $request->get('reason'));
                    Flash::info('Kinh doanh từ chối');
                }
            } else {
                Flash::error('You have no permission to reject.');
            }
        } else if ($batch->status == $this->mpTerminalBatchRepository->model()::STATUS_WAIT_CONTROL_APPROVE) {
            if ($user->hasrole('Đối soát')) {
                if ($request->has('reject_reason')) {
                $this->mpTerminalBatchRepository->update([
                    'status' => $this->mpTerminalBatchRepository->model()::STATUS_CONTROL_DENIED,
                    'reject_reason' => $rejectReason
                ], $id);
                    $this->saveLogHandleToDb($batch['id'], TerminalHistory::TYPE_BATCH_REVIEW_DENIED, $request->get('reason'));
                Flash::info('Đối soát từ chối');
            }
            } else {
                Flash::error('You have no permission to reject.');
            }
        } else {
            Flash::error('Terminal batch status not available');
        }
        return redirect(route('terminalBatches.show', ['id' => $id]));
    }

    public function approve($id, Request $request){
        $batch = $this->mpTerminalBatchRepository->find($id);
        if (empty($batch)) {
            Flash::error('Không tìm thấy lô terminal này');

            return redirect(route('terminalBatches.index'));
        }
        $user = auth()->user();
        if ($batch->status == $this->mpTerminalBatchRepository->model()::STATUS_WAIT_SALE_APPROVE) {
            if ($user->hasrole('Kinh doanh')) {
                $this->mpTerminalBatchRepository->update(['status' => $this->mpTerminalBatchRepository->model()::STATUS_WAIT_CONTROL_APPROVE], $id);
                $this->saveLogHandleToDb($batch['id'], TerminalHistory::TYPE_BATCH_SALE_APPROVED, $request->get('reason'));
                Flash::success('Duyệt lô terminal thành công!');
            } else {
                Flash::error('You have no permission to reject.');
            }
        } else if ($batch->status == $this->mpTerminalBatchRepository->model()::STATUS_WAIT_CONTROL_APPROVE) {
            if ($user->hasrole('Đối soát')) {
                $merchant = Store::where('merchant_code', $batch->merchant_code)->first();
                if (!$merchant) {
                    Flash::error('Không tồn tại merchant tương ứng với merchant code.' . $batch->merchant_code);
                }
                elseif ($merchant->status === BaseFunction::STATUS_LOCK) {
                    Flash::error(sprintf('Hiện tại merchant %s đang ở trạng thái đóng. Vui lòng kiểm tra lại!', $batch->merchant_code));
                }
                else {
                    $this->mpTerminalBatchRepository->update(['status' => $this->mpTerminalBatchRepository->model()::STATUS_CONTROL_APPROVED], $id);
                    $this->saveLogHandleToDb($batch['id'], TerminalHistory::TYPE_BATCH_REVIEW_APPROVED, $request->get('reason'));
                    Flash::success('Duyệt lô terminal thành công!');
                }
            } else {
                Flash::error('You have no permission to reject.');
            }
        } else {
            Flash::error('Terminal status not available');
        }
        return redirect(route('terminalBatches.show', ['id' => $id]));
    }

    public function reopenForEditTerminals($id, Request $request) {
        $mpTerminalBatch = $this->mpTerminalBatchRepository->find($id);
        if (!$mpTerminalBatch) {
            Flash::error('Không tìm thấy lô này.');
        }
        elseif ($mpTerminalBatch->status != MpTerminalBatch::STATUS_MMS_ERROR) {
            Flash::error('Không thể thao tác ở trạng thái này.');
        }

        try {
            MpTerminal::where('src', 'BATCH-' . $id)
                ->where('lock_edit', 1)
                ->where('mms_status', MpTerminal::MMS_STATUS_FAILED)
                ->update(['lock_edit' => 0]);
            $this->mpTerminalBatchRepository->update([
                'status' => MpTerminalBatch::STATUS_CONTROL_REOPEN
            ], $id);
            Flash::success('Mở sửa terminals thành công!');
        }
        catch (\Exception $e) {
            Flash::error(sprintf('Xảy ra lỗi thao tác: %s', $e->getMessage()));
        }

        return redirect(route('terminalBatches.show', ['id' => $id]));
    }

    private function saveLogHandleToDb($id, $type, $reason = '', $oldData = null, $newData = null)
    {
        try {
            $insertData = [
                'mp_terminal_id' => $id,
                'terminal_id' => "",
                'staff_id' => auth()->user()->id,
                'type' => $type,
                'reason' => $reason,
            ];
            if ($oldData) {
                $insertData['old_data'] = $oldData;
            }
            if ($newData) {
                $insertData['new_data'] = $newData;
            }
            TerminalHistory::create($insertData);
        }
        catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
