<?php

namespace App\Http\Controllers;

use App\Bank;
use App\DataTables\TerminalDataTable;
use App\District;
use App\Events\ControlApproveTerminal;
use App\Events\RevokeLockedTerminal;
use App\Events\SendEmailHandleMpTerminal;
use App\Http\Requests\CreateTerminalRequest;
use App\BusinessProduct;
use App\Http\Requests\UpdateFirstTerminalRequest;
use App\Http\Requests\UpdateTerminalRequest;
use App\Libraries\BaseFunction;
use App\Libraries\MmsService\MmsClient;
use App\MccVnpay;
use App\Models\Sync\QrMerchant;
use App\Store;
use App\Terminal;
use App\TerminalHistory;
use App\Province;
use App\Repositories\TerminalRepository;
use App\MpTerminal;
use App\Ward;
use Flash;
use Illuminate\Http\Request;
use Response;


class TerminalController extends AppBaseController
{
    /** @var  TerminalRepository */
    private $terminalRepository;

    public function __construct(TerminalRepository $terminalRepo)
    {
        $this->terminalRepository = $terminalRepo;
    }

    /**
     * Display a listing of the Store.
     *
     * @param TerminalDataTable $terminalDataTable
     * @return mixed
     */
    public function index()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $terminals = MpTerminal::take(10000)->get();

        $filterable_status = array_keys(MpTerminal::STATUS);
        $terminalDataTable = new TerminalDataTable($filterable_status,'Danh sách_Terminal');

        return $terminalDataTable->render('terminals.index', compact('terminals', 'filterable_status'));
    }

    /**
     * Show the form for creating a new Store.
     *
     * @return Response
     */
    public function create()
    {
        $banks = Bank::all();
        $businessProducts = BusinessProduct::where('type_code', '!=', '')->whereNotNull('type_code')->get();
        $provinces = Province::all();
        $districts = District::where('province_code', $provinces->first()->province_code)->get();
        $wards = Ward::where('province_code', $provinces->first()->province_code)
            ->where('district_code', $districts->first()->district_code)
            ->get();
        $mcc = MccVnpay::all();

        return view('terminals.create', compact(
            'businessProducts', 'banks',
            'provinces', 'districts', 'wards', 'mcc'
        ));
    }

    public function ajaxListMerchant(Request $request){
        $term = $request->q;

        if (empty(trim($term))) {
            return \response()->json([]);
        }

        $searchStatus = [BaseFunction::STATUS_CONTROL_ACCEPTED];
        if ($request->has('action') && $request->action == 'search') {
            $searchStatus[] = BaseFunction::STATUS_LOCK;
        }

        $merchants = Store::whereNotNull('merchant_code')
            ->whereIn('status', $searchStatus)
            ->whereRaw(sprintf("(UPPER(merchant_code) LIKE '%s' OR UPPER(merchant_name) LIKE '%s')",
                "%".mb_strtoupper($term, 'UTF-8')."%", "%".mb_strtoupper($term, 'UTF-8')."%"))
            ->get();

        $result = [];

        foreach ($merchants as $merchant){
            $result[] = ['id' => $merchant['merchant_code'], 'text' => sprintf(
                '%s - %s',
                $merchant['merchant_code'], $merchant['merchant_name']
            )];
        }

        return response()->json($result);
    }

    public function ajaxListTerminalByMerchantCode(Request $request){
        $term = $request->q;

        if (empty(trim($term))) {
            return \response()->json([]);
        }


        $terminals = MpTerminal::where('status', MpTerminal::STATUS_REVIEW_APPROVED)
            ->whereRaw(sprintf("(UPPER(merchant_code) LIKE '%s')",
                "%".mb_strtoupper($term, 'UTF-8')."%"))
            ->get();
        $result = [];

        foreach ($terminals as $terminal){
            $result[] = ['id' => $terminal['terminal_id'], 'text' => sprintf(
                '%s ',
                $terminal['terminal_id']
            )];
        }

        return response()->json($result);
    }

    public function ajaxListMerchantWithAll(Request $request){
        $term = $request->q;

        if (empty(trim($term))) {
            return \response()->json([]);
        }

        $merchants = Store::whereNotNull('merchant_code')
            ->where('status', BaseFunction::STATUS_CONTROL_ACCEPTED)
            ->whereRaw(sprintf("(UPPER(merchant_code) LIKE '%s' OR UPPER(merchant_name) LIKE '%s')",
                "%".mb_strtoupper($term, 'UTF-8')."%", "%".mb_strtoupper($term, 'UTF-8')."%"))
            ->get();

        $result = [['id' => 'all', 'text' => 'Tất cả Merchant']];
        foreach ($merchants as $merchant){
            $result[] = ['id' => $merchant['merchant_code'], 'text' => sprintf(
                '%s - %s',
                $merchant['merchant_code'], $merchant['merchant_name']
            )];
        }

        return response()->json($result);
    }

    public function ajaxFindBankAccount(Request $request){
        $result = [];
        if ($request->has('merchant_code') && $request->has('q')){
            $merchant_code = $request->merchant_code;
            $term = $request->q;
            if (empty(trim($term)) || empty(trim($merchant_code)) || $merchant_code == 'null') {
                return \response()->json([]);
            }

            $terminals = MpTerminal::where('merchant_code', $merchant_code)
                ->where('bank_number', $term)->get();

            foreach ($terminals as $terminal){
                $result[] = [
                    'bank_code' => $terminal['bank_code'],
                    'bank_branch' => $terminal['bank_branch'],
                    'bank_number' => $terminal['bank_number'],
                    'bank_account' => $terminal['bank_account']
                ];
            }
        }
        return response()->json($result);
    }

    /**
     * Store a newly created Terminal in storage.
     *
     * @param CreateTerminalRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateTerminalRequest $request)
    {
        $input = $request->all();
        if ($request->has('terminal_type') && $request['terminal_type'] == '0000'
            && empty($request['product_description'])
        ) {
            return redirect()->back()->withErrors(['product_description' => 'Thông tin này là bắt buộc'])->withInput();
        }
        if ($request->hasFile('file_contract')) {
            $input['file_contract'] = $request->file('file_contract')->store('public/terminal_files/' . $input['terminal_id']);
        }
        else {
            return redirect()->back()->withErrors(['file_contract' => 'Thông tin này là bắt buộc'])->withInput();
        }
        if ($request->hasFile('file_auth_letter')) {
            $input['file_auth_letter'] = $request->file('file_auth_letter')->store('public/terminal_files/' . $input['terminal_id']);
        }
        if ($request->hasFile('file_other')) {
            $input['file_other'] = $request->file('file_other')->store('public/terminal_files/' . $input['terminal_id']);
        }
        if ($request->has('register_qr')) {
            $input['register_qr'] = 1;
        }
        if ($request->has('register_vnpayment')) {
            if (!$request->has('website') || empty($request['website'])) {
                return redirect()->back()->withErrors(['website' => 'Thông tin này là bắt buộc'])->withInput();
            }
            elseif (!filter_var($request['website'], FILTER_VALIDATE_URL)) {
                return redirect()->back()->withErrors(['website' => 'Website không đúng định dạng'])->withInput();
            }
            $input['register_vnpayment'] = 1;
        }
        if ($request->has('create_terminal_app')) {
            $input['create_terminal_app'] = 1;
        }
        $send_request = false;
        if ($request->has('send_request')) {
            $send_request = true;
            $input['status'] = $this->terminalRepository->model()::STATUS_SALE_ADMIN_REQUESTED;
        }
        $input['created_by'] = \Auth::user()->id;
        $terminal = $this->terminalRepository->create($input);
        $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_OPERATOR_CREATE_TERMINAL);
        if ($terminal) {
            if ($send_request) {
                $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_SALE_ADMIN_REQUESTED);

                // send email
//                event(new SendEmailHandleMpTerminal($terminal['id'], 'request-sale-approve'));
            }
        }
        Flash::success('Tạo terminal thành công.');

        return redirect(route('terminals.index'));
    }

    /**
     * Display the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $terminal = $this->terminalRepository->find($id);

        if (empty($terminal)) {
            Flash::error('Không tìm thấy terminal tương ứng');

            return redirect(route('terminals.index'));
        }
        $histories = TerminalHistory::where('terminal_id', $terminal->terminal_id)
            ->where('mp_terminal_id', $id)
            ->orderBy('created_at', 'desc')->get();

        return view('terminals.show', compact('terminal', 'histories'));
    }

    /**
     * Display the specified Store.
     *
     * @param  int $storeId
     *
     * @return Response
     */
    public function showFirstTerminal($storeId)
    {
        $store = Store::where('id', $storeId)->first();

        if (!$store) {
            Flash::error('Không tìm thấy terminal tương ứng');

            return redirect(route('terminals.index'));
        }

        return view('terminals.showFirstTerminal', compact('store'));
    }

    /**
     * Show the form for editing the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $terminal = $this->terminalRepository->find($id);

        if (empty($terminal)) {
            Flash::error('Không tìm thấy terminal tương ứng');

            return redirect(route('terminals.index'));
        }
        if (!in_array($terminal->status, [
            $this->terminalRepository->model()::STATUS_INIT,
            $this->terminalRepository->model()::STATUS_REOPEN,
            $this->terminalRepository->model()::STATUS_REVIEW_REOPEN,
            $this->terminalRepository->model()::STATUS_REVIEW_APPROVED
        ])) {
            Flash::error('Không thế sửa terminal này');
            return redirect(route('terminals.index'));
        }

        $merchantCode = $terminal->merchant_code;
        $merchant = BaseFunction::getMerchantByCode($merchantCode);

        $banks = Bank::all();
        $businessProducts = BusinessProduct::where('type_code', '!=', '')->whereNotNull('type_code')->get();
        $provinces = Province::all();
        $districts = District::where('province_code', $provinces->first()->province_code)->get();
        $wards = Ward::where('province_code', $provinces->first()->province_code)
            ->where('district_code', $districts->first()->district_code)
            ->get();
        $mcc = MccVnpay::all();
        return view('terminals.edit', compact(
            'merchantCode', 'merchant',
            'businessProducts', 'banks',
            'provinces', 'districts', 'wards', 'terminal', 'mcc'
        ));
    }

    /**
     * Show the form for editing the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function editFirstTerminal($storeId)
    {
        $store = Store::where('id', $storeId)->first();

        if (empty($store)) {
            Flash::error('Không tìm thấy terminal tương ứng');

            return redirect(route('terminals.index'));
        }
//        if (in_array($store->status, [
//            BaseFunction::STATUS_SALE_ACCEPTED,
//            BaseFunction::STATUS_CONTROL_ACCEPTED,
//            BaseFunction::STATUS_SALE_REJECTED,
//            BaseFunction::STATUS_CONTROL_REJECTED
//        ])) {
//            Flash::error('Không thế sửa terminal này');
//            return redirect(route('terminals.index'));
//        }
        $banks = Bank::all();
        $businessProducts = BusinessProduct::where('type_code', '!=', '')->whereNotNull('type_code')->get();
        $provinces = Province::all();
        $districts = District::where('province_code', $provinces->first()->province_code)->get();
        $wards = Ward::where('province_code', $provinces->first()->province_code)
            ->where('district_code', $districts->first()->district_code)
            ->get();
        $mcc = MccVnpay::all();
        return view('terminals.editFirstTerminal', compact(
            'businessProducts', 'banks',
            'provinces', 'districts', 'wards', 'store', 'mcc'
        ));
    }

    public function updateFirstTerminal($storeId, UpdateFirstTerminalRequest $request) {
        $store = Store::where('id', $storeId)->first();
        if (!$store | $store->status == BaseFunction::STATUS_CONTROL_ACCEPTED) {
            Flash::error('Không thể sửa terminal này');

            return redirect(route('terminals.index'));
        }
        $input = $request->all();
        if ($request->has('terminal_type') && $request['terminal_type'] == '0000'
            && empty($request['terminal_description'])
        ) {
            return redirect()->back()->withErrors(['terminal_description' => 'Thông tin này là bắt buộc'])->withInput();
        }


        if(!$request->has('terminal_register_vnpayment')){
            $input['terminal_register_vnpayment'] = 0;
        }
        else {
            if (!$request->has('terminal_website') || empty($request['terminal_website'])) {
                return redirect()->back()->withErrors(['terminal_website' => 'Thông tin này là bắt buộc'])->withInput();
            }
            elseif (!filter_var($request['terminal_website'], FILTER_VALIDATE_URL)) {
                return redirect()->back()->withErrors(['terminal_website' => 'Website không đúng định dạng'])->withInput();
            }
            $input['terminal_register_vnpayment'] = 1;
        }

        if ($store->mms_status >= BaseFunction::STATUS_MMS_CREATED) {
            $old_store = $store->getAttributes();
            $updated_store = $store->update($input);
            $client = new MmsClient('MC_UPDATE_MID');
            $mms_response = $client->endpoint->endpointRequest($store);
            if ($mms_response && $mms_response['success'] == true) {
                Flash::success('Cập nhật thông tin terminal thành công.');
            } else {
                $updated_store->update($old_store);
                Flash::error('Có lỗi hệ thống xảy ra, vui lòng liên hệ kỹ thuật!');
            }
        }
        else {
            $store->update($input);
            Flash::success('Cập nhật thông tin terminal thành công.');
        }
        return redirect(route('terminals.showFirstTerminal', ['id' => $storeId]));
    }

    private function checkUpdateBeforeControlApprove($requestData, $id) {
        $errors = [];
//        if ($requestData->has('terminal_code') && $requestData['terminal_code'] &&
//            BaseFunction::checkMerchantCodeExistCombine($requestData['terminal_code'], $id)
//        ) {
//            $errors['terminal_code'] = 'MST/GTTT đã tồn tại trong hệ thống';
//        }
        if (!$requestData->has('terminal_name')) {
            $errors['terminal_name'] = 'Thông tin này là bắt buộc.';
        }
        elseif (BaseFunction::checkTerminalNameExistCombine($requestData['terminal_name'], $id)) {
            $errors['terminal_name'] = 'Tên điểm bán đã có trong hệ thống.';
        }
        return $errors;
    }

    private function removeUnnecessaryAfterControlApprove($request, $exceptKeys) {
        if (count($exceptKeys) > 0) {
            foreach ($exceptKeys as $exceptKey) {
                if ($request->has(sprintf('%s', $exceptKey))) {
                    $request->request->remove(sprintf('%s', $exceptKey));
                }
            }
        }
    }

    public function update($id, UpdateTerminalRequest $request)
    {
        $terminal = $this->terminalRepository->find($id);
        if ($terminal->status == $this->terminalRepository->model()::STATUS_REVIEW_APPROVED) {
            $this->removeUnnecessaryAfterControlApprove($request, ['terminal_code', 'terminal_id']);
        }
        else {
            $errors = $this->checkUpdateBeforeControlApprove($request, $id);
            if (count($errors) > 0) {
                return redirect()->back()->withErrors($errors)->withInput();
            }
        }
        $input = $request->all();
        if ($request->has('terminal_type') && $request['terminal_type'] == '0000'
            && empty($request['product_description'])
        ) {
            return redirect()->back()->withErrors(['product_description' => 'Thông tin này là bắt buộc'])->withInput();
        }
        if (empty($terminal)) {
            Flash::error('Không thể tìm thấy terminal này');

            return redirect(route('terminals.index'));
        }
        if (!in_array($terminal->status, [
            $this->terminalRepository->model()::STATUS_INIT,
            $this->terminalRepository->model()::STATUS_REOPEN,
            $this->terminalRepository->model()::STATUS_REVIEW_REOPEN,
            $this->terminalRepository->model()::STATUS_REVIEW_APPROVED
        ])) {
            Flash::error('Không thể sửa thông tin merchant này');
            return redirect(route('terminals.index'));
        }

        if ($request->hasFile('file_contract')) {
            $input['file_contract'] = $request->file('file_contract')->store('public/terminal_files/' . $request['terminal_id']);
        } else {
            unset($input['file_contract']);
        }
        if ($request->hasFile('file_auth_letter')) {
            $input['file_auth_letter'] = $request->file('file_auth_letter')->store('public/terminal_files/' . $request['terminal_id']);
        } else {
            unset($input['file_auth_letter']);
        }
        if ($request->hasFile('file_other')) {
            $input['file_other'] = $request->file('file_other')->store('public/terminal_files/' . $request['terminal_id']);
        } else {
            unset($input['file_other']);
        }
        $input['register_qr'] = 1;
        if(!$request->has('register_vnpayment')){
            $input['register_vnpayment'] = 0;
        }
        else {
            if (!$request->has('website') || empty($request['website'])) {
                return redirect()->back()->withErrors(['website' => 'Thông tin này là bắt buộc'])->withInput();
            }
            elseif (!filter_var($request['website'], FILTER_VALIDATE_URL)) {
                return redirect()->back()->withErrors(['website' => 'Website không đúng định dạng'])->withInput();
            }
            $input['register_vnpayment'] = 1;
        }

        $send_request = false;
        $updatedTerminal = null;
        if ($terminal->status == $this->terminalRepository->model()::STATUS_REVIEW_APPROVED) {
            $input['merchant_code'] = $terminal->merchant_code;
            $input['terminal_id'] = $terminal->terminal_id;
//            $input['terminal_name'] = $terminal->terminal_name;
            $mms_response = $this->notifyMMS($input, 'MC_UPDATE_TID');
            if ($mms_response && $mms_response['success'] == true) {
                $updatedTerminal = $this->terminalRepository->update($input, $id);
            } else {
                Flash::error('Có lỗi hệ thống xảy ra, vui lòng liên hệ kỹ thuật!');
                return redirect(route('terminals.edit', ['id' => $id]))->withInput();
            }
        } else {
            if ($request->has('send_request')) {
                $send_request = true;
                $input['status'] = $this->terminalRepository->model()::STATUS_SALE_ADMIN_REQUESTED;
            }

            $updatedTerminal = $this->terminalRepository->update($input, $id);
        }

        if ($terminal->status >= $this->terminalRepository->model()::STATUS_REOPEN) {
            $log = $this->prepareEditLog($updatedTerminal);
            $this->saveLogHandleToDb($id, $terminal->terminal_id, TerminalHistory::TYPE_UPDATE_INFO, $log);
        }
        if ($send_request) {
            $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_SALE_ADMIN_REQUESTED);

            // send email
//            event(new SendEmailHandleMpTerminal($terminal['id'], 'request-sale-approve'));
        }

        Flash::success('Cập nhật thông tin terminal thành công.');

        return redirect(route('terminals.show', ['id' => $id]));
    }


    public function sendRequest($id, Request $request)
    {
        $terminal = $this->terminalRepository->find($id);

        if (empty($terminal)) {
            Flash::error('Không tìm thấy terminal này');

            return redirect(route('terminals.index'));
        }
        $user = auth()->user();
        if ($terminal->status == $this->terminalRepository->model()::STATUS_INIT
        || $terminal->status == $this->terminalRepository->model()::STATUS_REOPEN
        || $terminal->status == $this->terminalRepository->model()::STATUS_REVIEW_REOPEN) {
            if ($user->hasrole('Sale admin')) {
                Flash::success('Gửi thông tin duyệt thành công!');
                $this->terminalRepository->update(['status' => $this->terminalRepository->model()::STATUS_SALE_ADMIN_REQUESTED], $id);
                $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_SALE_ADMIN_REQUESTED, $request->get('reason'));

                // send email
//                event(new SendEmailHandleMpTerminal($terminal['id'], 'request-sale-approve'));
            } else {
                Flash::error('You have no permission to send request.');
            }
        } else {
            Flash::error('Terminal status not available');
        }
        return redirect(route('terminals.show', ['id' => $id]));
    }

    public function reject($id, Request $request)
    {
        $terminal = $this->terminalRepository->find($id);

        if (empty($terminal)) {
            Flash::error('Không tìm thấy terminal này');

            return redirect(route('terminals.index'));
        }
        $user = auth()->user();
        if ($terminal->status == $this->terminalRepository->model()::STATUS_SALE_ADMIN_REQUESTED) {
            if ($user->hasrole('Kinh doanh')) {
                if ($request->has('reopen')) {
                    $this->terminalRepository->update(['status' => $this->terminalRepository->model()::STATUS_REOPEN], $id);
                    $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_SALE_REOPEN, $request->get('reason'));
//                    event(new SendEmailHandleMpTerminal($id, 'sale-request-update-info', $request->get('reason')));
                    Flash::info('Mở lại hồ sơ terminal!');
                } elseif ($request->has('reject')) {
                    $this->terminalRepository->update(['status' => $this->terminalRepository->model()::STATUS_SALE_DENIED], $id);
                    $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_SALE_DENIED, $request->get('reason'));
//                    event(new SendEmailHandleMpTerminal($id, 'sale-reject-terminal', $request->get('reason')));
                    Flash::info('Kinh doanh từ chối');
                }
            } else {
                Flash::error('You have no permission to reject.');
            }
        } else if ($terminal->status == $this->terminalRepository->model()::STATUS_SALE_APPROVED) {
            if ($user->hasrole('Đối soát')) {
                if ($request->has('reopen')) {
                    $this->terminalRepository->update(['status' => $this->terminalRepository->model()::STATUS_REVIEW_REOPEN], $id);
                    $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_REVIEW_REOPEN, $request->get('reason'));
//                    event(new SendEmailHandleMpTerminal($id, 'control-request-update-info', $request->get('reason')));
                    Flash::info('Mở lại thông tin terminal!');
                } elseif ($request->has('reject')) {
                    $this->terminalRepository->update(['status' => $this->terminalRepository->model()::STATUS_REVIEW_DENIED], $id);
                    $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_REVIEW_DENIED, $request->get('reason'));
//                    event(new SendEmailHandleMpTerminal($id, 'control-reject-terminal', $request->get('reason')));
                    Flash::info('Đối soát từ chối');
                }
            } else {
                Flash::error('You have no permission to reject.');
            }
        } else {
            Flash::error('Terminal status not available');
        }
        return redirect(route('terminals.show', ['id' => $id]));
    }

    public function approve($id, Request $request)
    {
        $terminal = $this->terminalRepository->find($id);
        if (empty($terminal)) {
            Flash::error('Không tìm thấy terminal này');

            return redirect(route('terminals.index'));
        }
        $user = auth()->user();
        if ($terminal->status == $this->terminalRepository->model()::STATUS_SALE_ADMIN_REQUESTED) {
            if ($user->hasrole('Kinh doanh')) {
                $this->terminalRepository->update(['status' => $this->terminalRepository->model()::STATUS_SALE_APPROVED], $id);
                $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_SALE_APPROVED);
//                event(new SendEmailHandleMpTerminal($id, 'sale-approve-terminal'));
                Flash::success('Duyệt terminal thành công!');
            } else {
                Flash::error('You have no permission to reject.');
            }
        } else if ($terminal->status == $this->terminalRepository->model()::STATUS_SALE_APPROVED) {
            if ($user->hasrole('Đối soát')) {
                $mms_response = $this->notifyMMS($terminal, 'TER_CREATE');
                if ($mms_response && $mms_response['success'] == true) {
                    $this->terminalRepository->update([
                        'status' => $this->terminalRepository->model()::STATUS_REVIEW_APPROVED,
                        'mms_status' => MpTerminal::MMS_STATUS_CREATED,
                        'mms_error_code' => null
                    ], $id);
                    $this->saveLogHandleToDb($terminal['id'], $terminal['terminal_id'], TerminalHistory::TYPE_REVIEW_APPROVED);

                    if (strpos($terminal['src'], 'BATCH') !== false) {
                        event(new ControlApproveTerminal($terminal['src']));
                    }

                    Flash::success('Duyệt terminal thành công!');
                }
                else {
                    $this->terminalRepository->update([
                        'mms_status' => MpTerminal::MMS_STATUS_FAILED,
                        'mms_error_code' => $mms_response['code']
                    ], $id);
                    Flash::error('Call MMS failed!');
                }
            } else {
                Flash::error('You have no permission to reject.');
            }
        } else {
            Flash::error('Terminal status not available');
        }
        return redirect(route('terminals.show', ['id' => $id]));
    }

    public function notifyMMS($terminal, $key)
    {
        $client = new MmsClient($key);
        return $client->endpoint->endpointRequest($terminal);
    }

    public function needApproveList() {

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $terminals = MpTerminal::take(10000)->get();
        $filterable_status = [
            $this->terminalRepository->model()::STATUS_SALE_ADMIN_REQUESTED,
            $this->terminalRepository->model()::STATUS_SALE_APPROVED
        ];
        $terminalDataTable = new TerminalDataTable($filterable_status,'Danh_sách_Terminal_chờ_duyệt');
        return $terminalDataTable->render('terminals.index', compact('terminals', 'filterable_status'));
    }

    public function approvedList() {
        $filterable_status = [];
        $terminalDataTable = new TerminalDataTable([$this->terminalRepository->model()::STATUS_REVIEW_APPROVED],
            'Danh sách_Terminal_đã_duyệt');

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $terminals = MpTerminal::take(10000)->get();
        return $terminalDataTable->render('terminals.index', compact('terminals', 'filterable_status'));
    }

    public function deniedList() {

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $terminals = MpTerminal::take(10000)->get();
        $filterable_status = [
            $this->terminalRepository->model()::STATUS_SALE_DENIED,
            $this->terminalRepository->model()::STATUS_REVIEW_DENIED
        ];
        $terminalDataTable = new TerminalDataTable($filterable_status,'Danh sách_Terminal_bị_từ_chối');
        return $terminalDataTable->render('terminals.index', compact('terminals', 'filterable_status'));
    }

    public function lockedList() {

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $terminals = MpTerminal::take(10000)->get();
        $filterable_status = [
            $this->terminalRepository->model()::STATUS_LOCK,
        ];
        $terminalDataTable = new TerminalDataTable($filterable_status,'Danh sách_Terminal_bị_từ_chối');
        return $terminalDataTable->render('terminals.index', compact('terminals', 'filterable_status'));
    }

    public function lockTerminal(Request $request)
    {
        if (!(\Auth::user()->email === 'devmp@gmail.com' || \Auth::user()->hasrole('Kinh doanh'))) {
            Flash::error('Bạn không có quyền thực hiện thao tác này');
            return redirect()->back();
        }
        $resHandleLock = $this->handleLockTerminal($request->get('mpTerminalId'), $request->get('reason'));
        if ($resHandleLock['status'] != 'OK') {
            Flash::error($resHandleLock['msg']);
        }
        else {
            Flash::success($resHandleLock['msg']);
        }
        return redirect()->back();
    }

    public function unLockTerminal(Request $request)
    {
        if (!(\Auth::user()->email === 'devmp@gmail.com' || \Auth::user()->hasrole('Kinh doanh'))) {
            Flash::error('Bạn không có quyền thực hiện thao tác này');
            return redirect()->back();
        }
        $resHandleLock = $this->handleUnLockTerminal($request->get('mpTerminalId'), $request->get('reason'));
        if ($resHandleLock['status'] != 'OK') {
            Flash::error($resHandleLock['msg']);
        }
        else {
            Flash::success($resHandleLock['msg']);
        }
        return redirect()->back();
    }

    public function handleLockTerminal($mpTerminalId, $reason = '') {
        if (!$mpTerminalId) {
            return [
                'status' => 'NOT_OK',
                'msg' => 'Terminal Id là bắt buộc'
            ];
        }
        $terminal = MpTerminal::where('id', $mpTerminalId)->first();
        if (!$terminal) {
            return [
                'status' => 'NOT_OK',
                'msg' => 'Không tìm thấy terminal tương ứng'
            ];
        }
        try {
            $client = new MmsClient('TER_CHANGE_STATUS');
            $mms_response = $client->endpoint->endpointRequest($terminal);
            if ($mms_response && $mms_response['success'] == true) {
                // update terminal to lock status
                $oldTerminal = clone ($terminal);
                $this->terminalRepository->update([
                    'status' => MpTerminal::STATUS_LOCK
                ], $mpTerminalId);
                // create lock history
                $this->saveLogHandleToDb(
                    $mpTerminalId,
                    $terminal->terminal_id,
                    TerminalHistory::TYPE_LOCK,
                    $reason,
                    $oldTerminal->toJson(),
                    $terminal->toJson()
                );

                event(new RevokeLockedTerminal($terminal->merchant_code, $terminal->terminal_id));

                return [
                    'status' => 'OK',
                    'msg' => sprintf('Khóa terminal %s - %s thành công', $terminal->terminal_id, $terminal->terminal_name)
                ];
            }
            else {
                return [
                    'status' => 'NOT_OK',
                    'msg' => 'Có lỗi hệ thống xảy ra, vui lòng liên hệ kỹ thuật!'
                ];
            }
        }
        catch (\Exception $e) {
            return [
                'status' => 'NOT_OK',
                'msg' => $e->getMessage()
            ];
        }
    }

    public function handleUnLockTerminal($mpTerminalId, $reason = '') {
        if (!$mpTerminalId) {
            return [
                'status' => 'NOT_OK',
                'msg' => 'Terminal Id là bắt buộc'
            ];
        }
        $terminal = MpTerminal::where('id', $mpTerminalId)->first();
        if (!$terminal) {
            return [
                'status' => 'NOT_OK',
                'msg' => 'Không tìm thấy terminal tương ứng'
            ];
        }
        try {
            $client = new MmsClient('TER_CHANGE_STATUS');
            $mms_response = $client->endpoint->endpointRequest($terminal);
            if ($mms_response && $mms_response['success'] == true) {
                // update terminal to lock status
                $oldTerminal = clone ($terminal);
                $this->terminalRepository->update([
                    'status' => MpTerminal::STATUS_REVIEW_APPROVED
                ], $mpTerminalId);
                // create lock history
                $this->saveLogHandleToDb(
                    $mpTerminalId,
                    $terminal->terminal_id,
                    TerminalHistory::TYPE_UNLOCK,
                    $reason,
                    $oldTerminal->toJson(),
                    $terminal->toJson()
                );
                return [
                    'status' => 'OK',
                    'msg' => sprintf('Mở khóa terminal %s - %s thành công', $terminal->terminal_id, $terminal->terminal_name)
                ];
            }
            else {
                return [
                    'status' => 'NOT_OK',
                    'msg' => 'Có lỗi hệ thống xảy ra, vui lòng liên hệ kỹ thuật!'
                ];
            }
        }
        catch (\Exception $e) {
            return [
                'status' => 'NOT_OK',
                'msg' => $e->getMessage()
            ];
        }
    }

    private function saveLogHandleToDb($id, $terminal_id, $type, $reason = '', $oldData = null, $newData = null)
    {
        try {
            $insertData = [
                'mp_terminal_id' => $id,
                'terminal_id' => $terminal_id,
                'staff_id' => auth()->user()->id,
                'type' => $type,
                'reason' => $reason,
            ];
            if ($oldData) {
                $insertData['old_data'] = $oldData;
            }
            if ($newData) {
                $insertData['new_data'] = $newData;
            }
            TerminalHistory::create($insertData);
        }
        catch (\Exception $e) {
            dd($e->getMessage());
        }
    }



    private function prepareEditLog($mpTerminal)
    {
        $log = '';
        $changed = $mpTerminal->getChanges();
        if (count($changed) == 0) {
            return $log;
        }
        else {
            foreach(array_keys($changed) as $change_field) {
                if (!in_array($change_field, ['status', 'updated_at'])) {
                    $log .= MpTerminal::$fieldLabel[$change_field] . ', ';
                }
            }
            if (!empty($log)) {
                $log = rtrim($log, ", ");
            }
        }
        return $log;
    }
}
