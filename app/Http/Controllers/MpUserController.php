<?php

namespace App\Http\Controllers;

use App\MpUser;
use App\Repositories\MpUserRepository;
use Flash;
use Response;
use Illuminate\Http\Request;

class MpUserController extends AppBaseController
{
    private $mpUserRepository;

    public function __construct(MpUserRepository $mpUserRepository)
    {
        $this->mpUserRepository = $mpUserRepository;
    }

    public function ajaxListUserWithPhone(Request $request)
    {
        $term = $request->q;

        if (empty(trim($term))) {
            return \response()->json([]);
        }


        $mp_users = MpUser::where('status', MpUser::STATUS_ACTIVE)
            ->where('phone', 'like', '%' . $term . '%')
            ->get();

        $result = [['id' => 'all', 'text' => 'Tất cả Số điện thoại']];
        foreach ($mp_users as $mp_user) {
            $result[] = ['id' => $mp_user['id'], 'text' => sprintf(
                '%s',
                $mp_user['phone']
            )];
        }
        return response()->json($result);

    }

}
