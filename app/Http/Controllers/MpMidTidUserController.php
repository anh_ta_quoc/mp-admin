<?php

namespace App\Http\Controllers;

use App\DataTables\BannerDataTable;
use App\DataTables\MpMidTidUserDataTable;
use App\MpMidTidUser;
use Illuminate\Http\Request;
use App\MpPermission;
use App\Repositories\MpMapPermissionRepository;
use App\Repositories\MpMidTidUserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Response;

class MpMidTidUserController extends AppBaseController
{

    private $mpMidTidUserRepository;
    private $mpMapPermissionRepository;

    public function __construct(
        MpMidTidUserRepository $mpMidTidUserRepository,
        MpMapPermissionRepository $mpMapPermissionRepository
    )
    {
        $this->mpMapPermissionRepository = $mpMapPermissionRepository;
        $this->mpMidTidUserRepository = $mpMidTidUserRepository;
    }

    /**
     * Display a listing of the MpMidTidUser.
     *
     * @param BannerDataTable $bannerDataTable
     * @return Response
     */
    public function index(MpMidTidUserDataTable $mpMidTidUserDataTable)
    {
        return $mpMidTidUserDataTable->render('mp_mid_tid_user.index');
    }

    /**
     * Show the form for creating a new Banner.
     *
     * @return Response
     */
    public function create()
    {
        $web_permissions = MpPermission::where('type', 'web')->where('parent_id', 0)->get();
        $app_permissions = MpPermission::where('type', 'app')->where('parent_id', 0)->get();
        return view('mp_mid_tid_user.create', compact('web_permissions', 'app_permissions'));
    }

    /**
     * Store a newly created Banner in storage.
     *
     * @param CreateBannerRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $type = $data['type'];
        /*
         * TODO: Validate data
         */
        $count_users = MpMidTidUser::where('user_id', $data['user_id'])
            ->where('type', $data['type'])
            ->where('merchant_code', $data['merchant_code'])
            ->count();
        if ($count_users > 0) {
            Flash::error('Tạo thất bại do tài khoản đã tồn tại.');
            return redirect()->back();
        }

        /*
         * TODO: Validate list permission
        */
        $per_type = strpos($type, 'web') !== false ? 'web' : 'app';
        $permissions = MpPermission::where('type', $per_type)
            ->whereIn('id', $data['permissions'])
            ->get();


        /*
         * TODO:Create mid tid user
         */


        if ($type != 'terminal_web') {
            $mid_tid_user = new MpMidTidUser();
            $mid_tid_user->user_id = $data['user_id'];
            $mid_tid_user->merchant_code = $data['merchant_code'];
            $mid_tid_user->type = $data['type'];
            $mid_tid_user->terminal_id = $data['terminal_id'];
            $mid_tid_user->status = MpMidTidUser::STATUS_ACTIVE;
            $mid_tid_user->save();
            if ($type == 'terminal_app') {
                foreach ($permissions as $permission) {
                    $map_permission = new MpPermission();
                    $map_permission->mid_tid_user_id = $mid_tid_user->id;
                    $map_permission->permission = $permission->id;
                    $map_permission->save();
                }
            }

        } else {
            foreach ($data['terminal_ids'] as $terminal_id) {

                $mid_tid_user = new MpMidTidUser();
                $mid_tid_user->user_id = $data['user_id'];
                $mid_tid_user->merchant_code = $data['merchant_code'];
                $mid_tid_user->type = $data['type'];
                $mid_tid_user->terminal_id = $terminal_id;
                $mid_tid_user->status = MpMidTidUser::STATUS_ACTIVE;
                $mid_tid_user->save();
                foreach ($permissions as $permission) {
                    $map_permission = new MpPermission();
                    $map_permission->mid_tid_user_id = $mid_tid_user->id;
                    $map_permission->permission = $permission->id;
                    $map_permission->save();
                }
            }
        }
        Flash::success('Tạo tài khoản thành công.');
        return redirect(route('mp_mid_tid_user.index'));
    }

    /**
     * Display the specified Banner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified Banner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $web_permissions = MpPermission::where('type', 'web')->where('parent_id', 0)->get();
        $app_permissions = MpPermission::where('type', 'app')->where('parent_id', 0)->get();
        $mp_mid_tid_user = MpMidTidUser::findOrFail($id);
        return view('mp_mid_tid_user.edit', compact('mp_mid_tid_user','web_permissions','app_permissions'));

    }

    /**
     * Update the specified Banner in storage.
     *
     * @param  int $id
     * @param UpdateBannerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBannerRequest $request)
    {

    }

    /**
     * Remove the specified Banner from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

    }
}
