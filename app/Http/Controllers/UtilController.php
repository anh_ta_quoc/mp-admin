<?php

namespace App\Http\Controllers;

use App\Models\Sync\QrMerchantUser;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Response;

class UtilController extends AppBaseController
{
    public function index(Request $requests)
    {
        return view('utils.index');
    }

    public function resetUsers(Request $request)
    {
        $list_user_ids = $request->get('merchant_user');
        if (is_array($list_user_ids) && count($list_user_ids) > 0) {
            foreach ($list_user_ids as $user_id) {
                $this->unlockUser((int)$user_id);
            }
        }

        Flash::success('Mở khóa cho các users đã chọn thành công!');

        return redirect(route('utils.index'));
    }

    private function unlockUser($userId)
    {
        $client = new \GuzzleHttp\Client([
            'headers' => ['Content-Type' => 'text/plain',
                'accept-language' => 'en-US,en;q=0.8']
        ]);

        $apiUrl = 'http://10.20.6.59:9006/MMSMCAPI/rest/users/blockUserMer';
        $client->post($apiUrl, ['body' => json_encode([
            'userId' => $userId,
            'status' => 1
        ])]);

        $this->logUnlockUser($userId);
    }

    private function logUnlockUser($userId)
    {
        $userMerchant = QrMerchantUser::where('id', $userId)->first();
        Log::channel('unlock_merchant_users')->info(sprintf('Mở khóa hỗ trợ vận hành bởi - %s - cho user merchant - %s', Auth::user()->name, $userMerchant['username']));
    }
}
