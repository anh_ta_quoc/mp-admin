<?php

namespace App\Http\Controllers;

use App\Bank;
use App\DataTables\BenefRequestDataTable;
use App\DataTables\ListTerminalsByMerchantDataTable;
use App\MpBenefRequest;
use App\MpEnjoymentAccount;
use App\MpTerminal;
use App\Repositories\EnjoymentRepository;
use App\Repositories\MpBenefRequestRepository;
use Flash;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Self_;
use Ramsey\Uuid\Uuid;
use Response;
use Illuminate\Http\Request;

class EnjoymentsController extends AppBaseController
{
    /** @var  EnjoymentRepository */
    private $benefRequestRepository;

    private const MAX_TER = 10;

    public function __construct(MpBenefRequestRepository $mpBenefRequestRepo)
    {
        $this->benefRequestRepository = $mpBenefRequestRepo;
    }

    public function index(BenefRequestDataTable $benefRequestDataTable)
    {
        return $benefRequestDataTable->render('enjoyments.index');
    }

    /**
     * Change enjoyment
     *
     * @return mixed
     */
    public function changeEnjoyment()
    {
        $terminalDataTable = new ListTerminalsByMerchantDataTable([],'Danh sách_Terminal');

        return $terminalDataTable->render('enjoyments.choose_merchant');
    }

    public function chooseMerchant(Request $request)
    {
        $merchantCode = $request->get('merchant_code');
        $mpTerminalIds = $request->get('mp-terminal-ids');
        $terminals = MpTerminal::where('merchant_code', $merchantCode)
            ->whereIn('terminal_id', $mpTerminalIds)
            ->get();
        $mpTerminalNames = [];
        if ($terminals) {
            foreach ($terminals as $terminal) {
                $mpTerminalNames[] = $terminal->terminal_name;
            }
        }
        $banks = Bank::all();

        return view('enjoyments.bank_info',
            compact('merchantCode', 'mpTerminalIds', 'mpTerminalNames', 'banks'));
    }

    public function upsertEnjoyment(Request $request)
    {
        $errors = [];
        $this->validateEnjoymentRequest($request, 'merchant_code', $errors);
        $this->validateEnjoymentRequest($request, 'account_number', $errors);
        // validate file
        if (!$request->hasFile('file_documentary')) {
            $errors['file_documentary'] = 'Công văn thay đổi thụ hưởng là bắt buộc';
        }

        $bank = Bank::where('bank_code', trim($request->get('bank_code')))->first();
        if (!$bank){
            $errors['bank_code'] = 'Không tìm thấy tên ngân hàng';
        }
        if (count($errors) > 0) {
            return redirect()->back()->withInput($request->all())->withErrors($errors);
        }
        // handle file
        $image = $request->file('file_documentary');
        $file_name = Uuid::uuid4();
        $new_name = $file_name . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('api_uploads'), $new_name);
//        $request_id = $this->randomString(trim($request->get('merchant_code')));

        $requestData = [];

        $terminals = explode(',', trim($request->get('mp-terminal-ids')));
        if (count($terminals) > self::MAX_TER){
            foreach (array_chunk($terminals, self::MAX_TER) as $arr_ids){
                $requestData[] = [
                    'request_id' => "",
                    'merchant_code' => trim($request->get('merchant_code')),
                    'terminals' => implode(',', $arr_ids),
                    'terminal_names' => trim($request->get('mp-terminal-names')),
                    'benef_number' => trim($request->get('account_number')),
                    'benef_name' => trim($request->get('bank_account')),
                    'benef_bank_code' => trim($request->get('bank_code')),
                    'benef_bank_branch' => trim($request->get('branch')),
                    'benef_bank_name' => $bank->bank_name,
                    'doc_url' => '/api_uploads/' . $new_name,
                    'create_user' => '',
                    'status' => MpBenefRequest::STATUS_INIT,
                    'ref' => $file_name,
                ];
            }

        }
        else{
            $requestData[] = [
                'request_id' => "",
                'merchant_code' => trim($request->get('merchant_code')),
                'terminals' => trim($request->get('mp-terminal-ids')),
                'terminal_names' => trim($request->get('mp-terminal-names')),
                'benef_number' => trim($request->get('account_number')),
                'benef_name' => trim($request->get('bank_account')),
                'benef_bank_code' => trim($request->get('bank_code')),
                'benef_bank_branch' => trim($request->get('branch')),
                'benef_bank_name' => $bank->bank_name,
                'doc_url' => '/api_uploads/' . $new_name,
                'create_user' => '',
                'status' => MpBenefRequest::STATUS_INIT,
                'ref' => "",
            ];
        }
        if (count($requestData)){
            try {
                DB::beginTransaction();
                foreach ($requestData as $reqData){
                    $this->benefRequestRepository->create($reqData);
                }
                DB::commit();
                Flash::success('Yêu cầu thay đổi thụ hưởng thành công');
                $banks = Bank::all();
                return view('enjoyments.show_result', compact('requestData', 'banks', 'new_name'));
            }
            catch (\Exception $e) {
                DB::rollBack();
                Flash::error('Yêu cầu thay đổi thụ hưởng thất bại. Vui lòng thử lại');
                return redirect()->back()->withInput($request->all())->withErrors($errors);
            }
        }
        else {
            Flash::error('Yêu cầu thay đổi thụ hưởng thất bại. Vui lòng thử lại');
            return redirect()->back()->withInput($request->all())->withErrors($errors);
        }
    }

    private function validateEnjoymentRequest(Request $request, $keyCheck, &$errors) {
        if (!$request->has("$keyCheck") || !$request->get("$keyCheck")) {
            $errors["$keyCheck"] = sprintf("%s là bắt buộc", MpEnjoymentAccount::getFieldMeanings($keyCheck));
        }
    }

    public function ajaxFindBankAccount(Request $request){
        if ($request->has('merchant_code') && $request->has('q')){
            $merchant_code = $request->merchant_code;
            $term = $request->q;
            if (empty(trim($term)) || empty(trim($merchant_code)) || $merchant_code == 'null') {
                return \response()->json([]);
            }

            $enjoyments = MpEnjoymentAccount::where('merchant_code', $merchant_code)
                ->where('account_number', $term)->get();
            return response()->json($enjoyments);
        }
        else {
            return response()->json([]);
        }
    }

    public function exportResult(Request $request)
    {

    }
    function randomString($merchant_code, $length=18) {
        $random = "";
        srand((double) microtime() * 1000000);
        for ($i = 0; $i < $length; $i++) {
            $random .= substr($merchant_code, (rand() % (strlen($merchant_code))), 1);
        }

        return $random;
    }
}
