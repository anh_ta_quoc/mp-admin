<?php

namespace App\Rules;

use App\Libraries\BaseFunction;
use App\Models\Sync\QrTerminal;
use App\MpTerminal;
use Illuminate\Contracts\Validation\Rule;

class TerminalNameExistedRule implements Rule
{
    private $id;
    private $merchantCode;

    /**
     * Create a new rule instance.
     *
     * @param null $id
     * @param null $merchantCode
     */
    public function __construct($id = null, $merchantCode = null)
    {
        $this->id = $id;
        $this->merchantCode = $merchantCode;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !BaseFunction::checkTerminalNameExistCombine($value, $this->id, $this->merchantCode);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tên điểm bán đã có trong hệ thống.';
    }
}
