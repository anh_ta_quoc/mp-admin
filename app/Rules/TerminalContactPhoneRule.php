<?php

namespace App\Rules;

use App\Libraries\MpApiService;
use App\Models\Sync\QrMerchant;
use App\Models\Sync\QrMerchantUser;
use App\Models\Sync\QrTerminal;
use App\MpTerminal;
use App\Store;
use Illuminate\Contracts\Validation\Rule;

class TerminalContactPhoneRule implements Rule
{
    private $checkUMS = false;
    private $showMessageUMS = false;

    /**
     * Create a new rule instance.
     *
     * TerminalContactPhoneRule constructor.
     * @param bool $checkUMS
     */
    public function __construct($checkUMS=false)
    {
        $this->checkUMS = $checkUMS;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $existed = MpTerminal::where('terminal_app_user', $value)->first();
        if ($existed) {
            return false;
        }

        if (Store::where('terminal_app_user', $value)
            ->orWhere('merchant_app_user', $value)
            ->first()) {
            return false;
        }

        if ($this->checkUMS) {
            if (strlen($value) === 9 && substr($value, 0, 1) !== '0') {
                $value = '0' . $value;
            }
            $mpApi = new MpApiService('temp_user/count_by_phone', 'POST', ['body' => json_encode([
                'phone' => $value
            ])]);
            try {
                $checkPhoneExisted = json_decode($mpApi->executeApi()->getBody());
                if ($checkPhoneExisted->count > 0) {
                    $this->showMessageUMS = true;
                    return false;
                }
                else {
                    return true;
                }
            }
            catch (\Exception $e) {
                return true;
            }
        }
        else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if ($this->showMessageUMS) {
            return 'Số điện thoại đã có trên hệ thống quản lý user tập trung.';
        }
        return 'Số điện thoại này đã có trong hệ thống.';
    }
}
