<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TerminalAddressRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $valid = true;
        if (strlen($value) > 0 && !preg_match('/^[\x00-\x7F]*$/', $value)) {
            $valid = false;
        }

        return $valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Địa chỉ chỉ bao gồm ký tự chữ, số, ký tự đặc biệt, khoảng trắng.';
    }
}
