<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UploadFileRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value) {
            if ($value->getSize() >= 2*5242880) {
                return false;
            }

            if (!in_array(strtolower($value->extension()), ['pdf', 'png', 'jpg', 'jpeg', 'rar'])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Chọn file có định dạng pdf,png,jpg,rar. Dung lượng file tối đa là 10MB.';
    }
}
