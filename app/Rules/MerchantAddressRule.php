<?php

namespace App\Rules;

use App\Libraries\BaseFunction;
use App\Models\Sync\QrMerchant;
use App\Store;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class MerchantAddressRule implements Rule
{
    private $id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id=null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // check from merchant table
        $store = Store::where('merchant_address', $value);
        if ($this->id) {
            $currentStore = Store::where('id', $this->id)->first();
            if ($currentStore->merchant_address == $value) {
                return true;
            }
            $store = $store->where('id', '!=', $this->id);
        }
        if ($store->first()) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Địa chỉ đăng ký kinh doanh đã có trong hệ thống.';
    }
}
