<?php

namespace App\Rules;

use App\Libraries\BaseFunction;
use App\Models\Sync\QrMerchant;
use App\Store;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class MerchantBrandRule implements Rule
{
    private $id;

    /**
     * Create a new rule instance.
     *
     * MerchantBrandRule constructor.
     * @param null $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match("/^[a-zA-Z0-9 ]*$/", $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tên viết tắt chỉ được phép có dấu cách, ký tự không dấu và số.';
    }
}
