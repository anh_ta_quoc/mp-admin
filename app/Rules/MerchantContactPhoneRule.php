<?php

namespace App\Rules;

use App\Libraries\BaseFunction;
use App\Models\Sync\QrMerchant;
use App\Models\Sync\QrMerchantUser;
use App\Models\Sync\QrTerminal;
use App\Store;
use Illuminate\Contracts\Validation\Rule;

class MerchantContactPhoneRule implements Rule
{
    private $id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id=null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$value) {
            return true;
        }
        else {
            $comparePhone = $value;
            if (strlen($comparePhone) == 9) {
                $comparePhone = '0' . $value;
            }
            $existed_by_merchant_phone = QrMerchant::where('app_user', '=', $comparePhone)
                ->first();

            if (!$existed_by_merchant_phone) {
                $existed_by_merchant_phone = QrMerchantUser::where('username', '=', $comparePhone)
                    ->first();

                if (!$existed_by_merchant_phone) {
                    $existed_by_merchant_phone = QrTerminal::where('terminal_app_user', '=', $comparePhone)->first();
                }
            }
            $store = Store::where('id', $this->id)->first();
            if ($store && $store->merchant_contact_phone == $value) {
                return true;
            }

            return !$existed_by_merchant_phone;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số điện thoại liên hệ đã có trong hệ thống, vui lòng nhập số điện thoại khác!';
    }
}
