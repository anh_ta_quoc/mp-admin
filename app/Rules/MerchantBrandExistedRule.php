<?php

namespace App\Rules;

use App\Libraries\BaseFunction;
use App\Models\Sync\QrMerchant;
use App\Store;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class MerchantBrandExistedRule implements Rule
{
    private $merchantCode;

    /**
     * Create a new rule instance.
     *
     * MerchantBrandRule constructor.
     * @param null $merchantCode
     */
    public function __construct($merchantCode = null)
    {
        $this->merchantCode = $merchantCode;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $existed_by_brand = Store::whereRaw(sprintf("UPPER(REPLACE(merchant_brand, ' ', '')) =
           UPPER(REPLACE('%s', ' ', ''))", $value));
        if ($this->merchantCode) {
            $existed_by_brand = $existed_by_brand->where('merchant_code', '!=', $this->merchantCode);
        }

        return !$existed_by_brand->first();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tên viết tắt doanh nghiệp đã có trong hệ thống.';
    }
}
