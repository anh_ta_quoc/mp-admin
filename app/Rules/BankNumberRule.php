<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class BankNumberRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $valid = true;
        if (strlen($value) > 30) {
            $valid = false;
        }
        if (!preg_match('/^[a-zA-Z0-9]+$/', $value)) {
            $valid = false;
        }

        return $valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số tài khoản không được quá 30 ký tự, chỉ bao gồm ký tự chữ tiếng việt không dấu và ký tự số.';
    }
}
