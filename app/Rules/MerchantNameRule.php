<?php

namespace App\Rules;

use App\Libraries\BaseFunction;
use App\Models\Sync\QrMerchant;
use App\Store;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class MerchantNameRule implements Rule
{
    private $id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id=null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // check from merchant table
        $store = Store::whereRaw("UPPER(merchant_name) = ?", [strtoupper($value)]);
        if ($this->id) {
            $store = $store->where('id', '!=', $this->id);
        }
        if ($store->first()) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tên doanh nghiệp đã có trong hệ thống.';
    }
}
