<?php

namespace App\Rules;

use App\Libraries\BaseFunction;
use App\Models\Sync\QrTerminal;
use App\MpTerminal;
use Illuminate\Contracts\Validation\Rule;

class TerminalIdExistedRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $id;
    private $merchant_code;

    public function __construct($merchant_code, $id = null)
    {
        $this->id = $id;
        $this->merchant_code = $merchant_code;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->id){
            $terminal = MpTerminal::where('id', $this->id)->first();
            if ($terminal){
                if ($terminal->status == MpTerminal::STATUS_REVIEW_APPROVED){
                    return true;
                }
                else{
                    return !BaseFunction::checkTerminalIdExistCombine($value, $this->merchant_code, $this->id);
                }
            }
            return false;
        }
        else {
            return !BaseFunction::checkTerminalIdExistCombine($value, $this->merchant_code);
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Terminal id đã có trong hệ thống.';
    }
}
