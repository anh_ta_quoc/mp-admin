<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TerminalSameContactPhoneRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value != request()->merchant_contact_phone;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số điện thoại liên hệ điểm bán không được trùng với số điện thoại liên hệ doanh nghiệp.';
    }
}
