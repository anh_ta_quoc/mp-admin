<?php

namespace App\Rules;

use App\Libraries\BaseFunction;
use App\Models\Sync\QrMerchant;
use App\Models\Sync\QrMerchantUser;
use App\Models\Sync\QrTerminal;
use App\MpTerminal;
use App\Store;
use Illuminate\Contracts\Validation\Rule;

class MerchantAppUserRule implements Rule
{
    private $id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id=null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$value) {
            return true;
        }
        else {
            if (!$this->id) {
                // case create new store info
                $storeExisted = Store::where('merchant_app_user', $value)
                    ->orWhere('terminal_app_user', $value)
                    ->first();
                if ($storeExisted) {
                    return false;
                }
                else {
                    $mpTerminalExisted = MpTerminal::where('terminal_app_user', $value)
                        ->first();
                    if ($mpTerminalExisted) {
                        return false;
                    }
                }
            }

            $comparePhone = $value;
            if (strlen($comparePhone) == 9) {
                $comparePhone = '0' . $value;
            }
            $existed_by_merchant_phone = QrMerchant::where('app_user', '=', $comparePhone)
                ->first();

            if (!$existed_by_merchant_phone) {
                $existed_by_merchant_phone = QrMerchantUser::where('username', '=', $comparePhone)
                    ->first();

                if (!$existed_by_merchant_phone) {
                    $existed_by_merchant_phone = QrTerminal::where('terminal_app_user', '=', $comparePhone)->first();
                }
            }

            if ($this->id) {
                $store = Store::where('id', $this->id)->first();
                if ($store && $store->merchant_app_user == $value) {
                    return true;
                }
            }

            return !$existed_by_merchant_phone;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số điện thoại đã có trong hệ thống.';
    }
}
